#!/bin/bash
# Note: you need to have nosetests 1.2.1 installed
REDIS_FILE=/data/redis/redis.sock
help(){

   echo "load_redis_from_scratch.sh prod /var/www/pylons/prod/qfab-production-local.ini"
}

if [ "$#" -eq 0 ]
    then
        help
        exit
fi

function run_or_skip (){

    echo -n " (r)un or (s)kip $1  "
    read run_or_skip
    if [ "$run_or_skip" == "r" ]; then
        return 0
    fi
    return 1
}

INSTANCE="$1"
INI="$2"
setup_gct_files(){
    run_or_skip $FUNCNAME 
    if [ $? == 0 ]; then
        /data/repo/git-working/stemformatics_tools/redis/s4m/gct_expression/initialise.sh $REDIS_FILE /var/www/pylons-data/$INSTANCE/GCTFiles/ 0
    fi
}

setup_yugene_files(){
    run_or_skip $FUNCNAME 
    if [ $? == 0 ]; then

        /data/repo/git-working/stemformatics_tools/redis/s4m/x_platform/initialise.sh $REDIS_FILE /var/www/pylons-data/$INSTANCE/CUMULATIVEFiles/ 0
    fi
}

setup_standard_deviation(){
    run_or_skip $FUNCNAME 
    if [ $? == 0 ]; then
        /data/repo/git-working/stemformatics_tools/redis/s4m/standard_deviation/initialise.sh $REDIS_FILE /var/www/pylons-data/$INSTANCE/StandardDeviationFiles/
    fi
}

setup_change_in_dataset_and_user(){
    run_or_skip $FUNCNAME 
    if [ $? == 0 ]; then
        echo "/data/repo/git-working/stemformatics_tools/redis/s4m/general/initialise.sh $INSTANCE $INI"
        /data/repo/git-working/stemformatics_tools/redis/s4m/general/initialise.sh "$INSTANCE" "$INI"
    fi

}



setup_mappings(){
    run_or_skip $FUNCNAME 
    if [ $? == 0 ]; then
        /data/repo/git-working/stemformatics_tools/redis/s4m/mappings/initialise.sh "$INSTANCE" "$INI"

    fi
}

setup_gct_files
setup_yugene_files
setup_standard_deviation
setup_change_in_dataset_and_user
setup_mappings


