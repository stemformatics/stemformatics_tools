#!/bin/sh
#cat import_feature_mappings.tsv | psql -h localhost -U portaladmin portal_beta -c "COPY stemformatics.feature_mappings FROM STDIN"
GCT_DIR="/var/www/pylons-data/SHARED/CUMULATIVEFiles"
OUTPUT_FILE="/tmp/mappings_for_replicates_to_chip_ids.txt"
if [ ! -f $OUTPUT_FILE ];
then
	psql -h localhost -U portaladmin portal_beta -c "copy (select ds_id,chip_id,md_value from biosamples_metadata where md_name = 'Replicate Group ID') to '$OUTPUT_FILE';"
fi

OLDIFS=$IFS
#for f in $GCT_DIR/*2000*.cumulative.txt
for f in $GCT_DIR/*.cumulative.txt
do
	#echo "Processing $f"
	ds_id=`echo "$f" | grep -o "[0-9]\+" `
    label_line=`cat $f | sed -n 1p|sed 's/\t/|/g'`
    header_file="$f".header
    head -n 1 "$f" > $header_file
    IFS="|"
    for label in $label_line; 
    do
        if [ ! "$label" = "Probe"  ] 
        then
            new_chip_id=`grep $ds_id $OUTPUT_FILE | grep "	$label$" | awk -F " " '{print $2}'`
            if [ ! -z "$new_chip_id" ];
            then
                search=$(echo "$label"|sed 's!\([]\*\.\$\/&[]\)!\\\1!g')
                echo $ds_id:::$search:::$new_chip_id 
                sed -i "1 s/\t$search\t/\t$new_chip_id\t/g" "$header_file"
                sed -i "1 s/\t$search$/\t$new_chip_id/g" "$header_file"
            fi
        fi
    done
    replace=`cat $header_file`
    sed  1s/^.*$/$replace/ -i "$f"
done
IFS=$OLDIFS

