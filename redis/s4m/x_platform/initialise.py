# initialise.py localhost /var/www/pylons-data/SHARED/CUMULATIVEFiles 0
# initialise.py localhost /var/www/pylons-data/SHARED/CUMULATIVEFiles 5012

import redis
import cPickle
import os
import re
import sys
from matricks import *

input_redis_server = sys.argv[1] # should be localhost
input_base_dir = sys.argv[2] # should be the CUMULATIVE files
input_ds_id = int(sys.argv[3])
 
base_dir = input_base_dir+'/'

r_server = redis.Redis(unix_socket_path=input_redis_server)
    
dataset_files = os.listdir(base_dir)

delimiter = '|'
datasets= ''
for data_file in dataset_files:
    

    if 'cumulative' in data_file:            
        m = re.search('[0-9]+',data_file)
        
        ds_id = int(m.group(0))
        
        if ds_id == input_ds_id or input_ds_id == 0:
            r_server.delete("pickled_cumulative_matricks_"+str(ds_id))
            print('starting to load cumulative ds_id '+str(ds_id) + ' files')
            read_cumulative_filename = base_dir+data_file


            read_cumulative_filename = base_dir+data_file
            m_dataset = Matricks(read_cumulative_filename)
            temp_labels = m_dataset.getLabels()
            labels = temp_labels[1:]
            label_name = 'cumulative_labels'+delimiter+str(ds_id)
            
            label_value = delimiter.join(labels)
            
            # print label_value[1:]
            r_server.set(label_name,label_value)
            
            test = r_server.get(label_name)
           
            print "Sample IDs in loaded cumulative data (if none, something didn't go well): \n" + ', '.join(test.split(delimiter))
            
            for r in m_dataset:
                probe = r[0]
                row_name = 'cumulative_values'+delimiter+str(ds_id) +delimiter+ probe
                #print row_name
                expression_columns = r[1:]
                row_value = delimiter.join(map( str, expression_columns ))
                #print row_value
                r_server.set(row_name,row_value)
            
 

            print('finished to load cumulative ds_id '+str(ds_id) + ' files')

# r_server.set("datasets",datasets[:-1])
        



