from matricks import *
import redis
import cPickle
import os
import re

config = ConfigParser.RawConfigParser()
config.read('../../../dataset_scripts/s4m/etc/pgconf/RM_local.conf')

redis_server = config.get("redis","redis_server")
ds_id = 5003

r_server = redis.Redis(redis_server)
r_server.delete("pickled_matricks_"+str(ds_id))
