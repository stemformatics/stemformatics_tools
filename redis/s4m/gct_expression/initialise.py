# initialise.py /data/redis/redis.sock /var/www/pylons-data/SHARED/GCTFiles/ 0
# initialise.py /data/redis/redis.sock /var/www/pylons-data/SHARED/GCTFiles/ 5012

import redis
import cPickle
import os
import re
import sys
from matricks import *

input_redis_server = sys.argv[1] # path to socket
input_base_dir = sys.argv[2] # should be the GCT files dir (needs trailling slash)
input_ds_id = int(sys.argv[3])
 
base_dir = input_base_dir
r_server = redis.Redis(unix_socket_path=input_redis_server)
dataset_files = os.listdir(base_dir)

delimiter = '|'
datasets= ''
for data_file in dataset_files:

    if data_file is None:
        continue

    if '.gct' in data_file:
        m = re.search('[0-9]+',data_file)
        ds_id = int(m.group(0))

        if ds_id == input_ds_id or input_ds_id == 0:
            print('starting to load ds_id '+str(ds_id) + ' gct files')
            
            r_server.delete("pickled_matricks_"+str(ds_id))
            
            read_gct_filename = base_dir+data_file
            ## TODO: Don't use Matricks - we can process the input GCT line by line without loading
            ##   the entire thing in memory -OK
            m_dataset = Matricks(read_gct_filename)
            temp_labels = m_dataset.getLabels()
            labels = temp_labels[2:]
            label_name = 'gct_labels'+delimiter+str(ds_id)
            
            label_value = delimiter.join(labels)
            
            # print label_value[1:]
            r_server.set(label_name,label_value)
            
            test = r_server.get(label_name)
            
            #print test.split(delimiter)
            for r in m_dataset:
                probe = r[0]
                row_name = 'gct_values'+delimiter+str(ds_id) +delimiter+ probe
                #print row_name
                expression_columns = r[2:]
                row_value = delimiter.join(map( str, expression_columns ))
                #print row_value
                r_server.set(row_name,row_value)
            
            print('finished to load ds_id '+str(ds_id) + ' gct files')
