#!/bin/bash

source /var/www/pylons/virtualenv/bin/activate

re='^0$'
if ! [[ $3 =~ $re ]] ; then
    echo "this is not 0 - removing current redis values for gct "
    /data/repo/git-working/stemformatics_tools/redis/s4m/remove_dataset_from_gct_redis.sh "$3"
else
    echo " this is 0 - cannot reset redis. do this manually"
fi


MYDIR="$(dirname "$(which "$0")")"
python $MYDIR/initialise.py "$1" "$2" "$3"
ret=$?

## Get number of key/vals loaded into Redis
keys_found=`redis-cli -s "$1" --scan --pattern "gct_values|$3|*" | wc -l`
echo "DEBUG: Number of GCT expression rows for dataset $3 loaded in Redis: $keys_found" 1>&2

if [ $ret -ne 0 ]; then
  echo "$0: Error: Script 'initialise.py' exited with non-zero status!
" 1>&2
fi
exit $ret
