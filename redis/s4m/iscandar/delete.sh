#!/bin/bash
ds_id="$1"
FILE="/data/redis/redis.sock"
redis-cli -s "$FILE" --scan --pattern "iscandar_values|$ds_id|*" | xargs redis-cli -s "$FILE" DEL
redis-cli -s "$FILE" DEL "iscandar_labels|$ds_id"
