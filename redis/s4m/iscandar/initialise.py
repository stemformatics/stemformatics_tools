# initialise.py localhost /var/www/pylons-data/SHARED/GCTFiles 0
# initialise.py localhost /var/www/pylons-data/SHARED/GCTFiles 5012

import redis
import cPickle
import os
import re
import sys
from matricks import *


input_redis_server = sys.argv[1] # should be localhost
input_base_dir = sys.argv[2] # should be the GCT files
ds_id = int(sys.argv[3])
 
#~ config = ConfigParser.RawConfigParser()
#~ config.read('../../../dataset_scripts/s4m/etc/pgconf/RM_local.conf')

#~ redis_server = config.get("redis","redis_server")
#~ base_dir = config.get("redis","DatasetGCTFiles")

base_dir = input_base_dir

r_server = redis.Redis(unix_socket_path=input_redis_server)
    
iscandar_file = base_dir + '/' + str(ds_id) + '/expression.tsv'

delimiter = '|'

print('starting to load ds_id '+str(ds_id) + ' iscandar gene expression from input: "' + iscandar_file + '"')

m_dataset = Matricks(iscandar_file)
labels = m_dataset.getLabels()
label_name = 'iscandar_labels'+delimiter+str(ds_id)
label_value = delimiter.join(labels)
r_server.set(label_name,label_value)

for r in m_dataset:
    gene = r[0]
    row_name = 'iscandar_values'+delimiter+str(ds_id) +delimiter+ gene
    #print row_name
    expression_columns = r[1:]
    row_value = delimiter.join(map( str, expression_columns ))
    #print row_value
    r_server.set(row_name,row_value)
print('finished to load ds_id '+str(ds_id) + ' iscandar expression files')

