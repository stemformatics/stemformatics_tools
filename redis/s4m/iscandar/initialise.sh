#!/bin/bash

source /var/www/pylons/virtualenv/bin/activate

re='^0$'
if ! [[ $3 =~ $re ]] ; then
    echo "this is not 0 - removing current redis values for iscandar "
    /data/repo/git-working/stemformatics_tools/redis/s4m/iscandar/delete.sh "$3"
else
    echo " this is 0 - cannot reset redis. do this manually"
fi


MYDIR="$(dirname "$(which "$0")")"
python $MYDIR/initialise.py "$1" "$2" "$3"

