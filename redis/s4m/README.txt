The two main scripts are:

/data/repo/git-working/stemformatics_tools/redis/s4m/load_redis_from_scratch.sh

and 

/data/repo/git-working/stemformatics_tools/redis/s4m/check_redis.sh


To run load_redis_from_scratch, you should clear Redis first. This will leave all the keys removed and Stemformatics will not work. The command is:
redis-cli -s /data/redis/redis.sock FLUSHALL
