# initialise.py localhost /var/www/pylons-data/SHARED/GCTFiles 0
# initialise.py localhost /var/www/pylons-data/SHARED/GCTFiles 5012

import redis
import cPickle
import os
import re
import sys
from matricks import *

input_redis_server = sys.argv[1] # should be localhost
input_base_dir = sys.argv[2] # should be the GCT files
 

base_dir = input_base_dir

r_server = redis.Redis(unix_socket_path=input_redis_server)
    
dataset_files = os.listdir(base_dir)

delimiter = '|'
datasets= ''
for data_file in dataset_files:

    
    m = re.search('[0-9]+',data_file)

    ds_id = int(m.group(0))

    if ds_id != 0:
        print('starting to load ds_id '+str(ds_id) + ' sd files')
        
        
        f = open(base_dir+data_file,'r')

        for raw_line in f:
            line = raw_line.replace('\n','')
        
            temp = line.split("\t")
            ds_id = int(temp[0])
            chip_type = temp[1]
            chip_id = temp[2]
            probe_id = temp[3]
            replicate_group_id=temp[4]
            standard_deviation=float(temp[6])
            
            
            row_name = 'standard_deviation'+delimiter+str(ds_id)+delimiter+chip_id+delimiter+ probe_id
            row_value = standard_deviation
            r_server.set(row_name,row_value)
            
            print('set sd row for ' + row_name + ' with value: ' + str(row_value)  )
            
        f.close()
    
    
        #=============================================
        #~ read_sd_filename = base_dir+data_file
        #~ m_dataset = Matricks(read_sd_filename)
        #~ temp_labels = m_dataset.getLabels()
        #~ labels = temp_labels[2:]
        #~ label_name = str(ds_id)+delimiter+'labels'
        #~ 
        #~ label_value = delimiter.join(labels)
        #~ 
        #~ # print label_value[1:]
        #~ r_server.set(label_name,label_value)
        #~ 
        #~ test = r_server.get(label_name)
        #~ 
        #~ #print test.split(delimiter)
        #~ 
        #~ for r in m_dataset:
            #~ probe = r[0]
            #~ row_name = str(ds_id) +'|'+ probe
            #~ #print row_name
            #~ expression_columns = r[2:]
            #~ row_value = delimiter.join(map( str, expression_columns ))
            #~ #print row_value
            #~ r_server.set(row_name,row_value)
        #~ 
        
