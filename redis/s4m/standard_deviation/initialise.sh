#!/bin/bash

source /var/www/pylons/virtualenv/bin/activate

MYDIR="$(dirname "$(which "$0")")"
python $MYDIR/initialise.py "$1" "$2"

