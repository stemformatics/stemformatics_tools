#!/bin/bash
ds_id="$1"
FILE="/data/redis/redis.sock"
redis-cli -s "$FILE" --scan --pattern "gct_values|$ds_id|*" | xargs redis-cli -s "$FILE" DEL
redis-cli -s "$FILE" DEL "gct_labels|$ds_id"
