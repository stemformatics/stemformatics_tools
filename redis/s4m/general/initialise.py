# Live querying
from guide.model.stemformatics import *
from pylons import config

def test():
    set_redis_generally()

def set_redis_generally():
    Stemformatics_Dataset.triggers_for_change_in_dataset(db)
    Stemformatics_Auth.triggers_for_change_in_user(db)
