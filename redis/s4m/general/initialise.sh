#!/bin/bash
source /var/www/pylons/virtualenv/bin/activate
MYDIR="$(dirname "$(which "$0")")"
WORKING_DIR=/var/www/pylons/prod
cd $WORKING_DIR

echo "nosetests --with-pylons="$2" /data/repo/git-working/stemformatics_tools/redis/s4m/general/initialise.py"
nosetests --with-pylons="$2" /data/repo/git-working/stemformatics_tools/redis/s4m/general/initialise.py

