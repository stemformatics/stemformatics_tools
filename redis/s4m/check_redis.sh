#!/bin/bash
clear
INSTANCE="$1"
INI="$2"
INTERACTIVE="$3"
ERROR="FALSE"
LOAD_SCRIPT="/data/repo/git-working/stemformatics_tools/redis/s4m/load_redis_from_scratch.sh $INSTANCE $INI"
NUMBER_OF_CHECKS=5



help(){

   echo "check_redis.sh prod /var/www/pylons/prod/qfab-production-local.ini TRUE"
   echo "where TRUE is for interactive"
}

if [ "$#" -eq 0 ]
    then
        help
        exit
fi

if [ "$INSTANCE" == "prod" ]; then
    PSQL_DETAILS=" -U portaladmin portal_prod -h localhost"
else
    PSQL_DETAILS=" -U portaladmin portal_beta -h localhost"
fi    


function run_or_skip (){

    echo -n " (r)un or (s)kip $1  "
    read run_or_skip
    if [ "$run_or_skip" == "r" ]; then
        return 0
    fi
    return 1
}

function check_redis(){

    RESULT=`redis-cli -s /data/redis/redis.sock get $1`
    if [ -z "$RESULT" ]; then
        echo "Error with no $1."
        ERROR="TRUE"
    fi


}




function check_headers(){
    check_redis "HEADER_DATASETS"
}

function check_choose_dataset_details(){
    check_redis "choose_dataset_details"
}


function check_user_dataset_availability(){

    USER_DUMP_FILE="/tmp/redis_check_users.dump"

    psql $PSQL_DETAILS -c "copy (select uid from stemformatics.users) to '$USER_DUMP_FILE';" 
    for i in `cat $USER_DUMP_FILE`;do 
        check_redis "user_dataset_availability|$i"
    done

    i="0"
    check_redis "user_dataset_availability|$i"


}

function check_gct_files (){
    
    DATASETS_DUMP_FILE="/tmp/redis_check_datasets.dump"

    psql $PSQL_DETAILS -c "copy (select id from datasets) to '$DATASETS_DUMP_FILE';" 
    for DS_ID in `cat $DATASETS_DUMP_FILE`;do 
        check_redis "gct_labels|$DS_ID"
        GCT_FILE="/var/www/pylons-data/$INSTANCE/GCTFiles/dataset$DS_ID.gct"

        if test -f "$GCT_FILE" 
        then
            OUTPUT_FILE="/tmp/redis_check_gct_$DS_ID.txt"
            tail -n "$NUMBER_OF_CHECKS" "$GCT_FILE" |  awk '{print $1}' > "$OUTPUT_FILE"

            for probe in `cat $OUTPUT_FILE`;do 
                check_redis "gct_values|$DS_ID|$probe"
            done

 
        fi
        
    done



}


function check_yugene_files (){
    
    DATASETS_DUMP_FILE="/tmp/redis_check_datasets_yugene.dump"

    psql $PSQL_DETAILS -c "copy (select id from datasets where show_yugene = True) to '$DATASETS_DUMP_FILE';" 
    for DS_ID in `cat $DATASETS_DUMP_FILE`;do 
        check_redis "cumulative_labels|$DS_ID"
        YUGENE_FILE="/var/www/pylons-data/$INSTANCE/CUMULATIVEFiles/dataset$DS_ID.cumulative.txt"

        if test -f "$YUGENE_FILE" 
        then
            OUTPUT_FILE="/tmp/redis_check_yugene_$DS_ID.txt"
            tail -n "$NUMBER_OF_CHECKS" "$YUGENE_FILE" |  awk '{print $1}' > "$OUTPUT_FILE"

            for probe in `cat $OUTPUT_FILE`;do 
                check_redis "cumulative_values|$DS_ID|$probe"
            done

 
        fi
        
    done


}

function check_standard_deviation(){
    SD_DIR="/var/www/pylons-data/$INSTANCE/StandardDeviationFiles/"
    
    for i in "$SD_DIR"/* 
    do

        if test -f "$i" 
        then
            DS_ID=`echo $i | grep "[0-9]\+" -o`
            OUTPUT_FILE="/tmp/redis_check_sd_$DS_ID.txt"
            head -n "$NUMBER_OF_CHECKS" "$i" | awk '{print $3 "|" $4}' > "$OUTPUT_FILE"
            tail -n "$NUMBER_OF_CHECKS" "$i" | awk '{print $3 "|" $4}' >> "$OUTPUT_FILE"

            for probe_and_chip in `cat $OUTPUT_FILE`;do 
                check_redis "standard_deviation|$DS_ID|$probe_and_chip"
            done

 
        fi
        
    done
        

}

function check_probe_mappings(){
    OUTPUT_FILE="/tmp/redis_check_probe_mappings.txt"
    FEATURE_MAPPING_FILE="/var/www/pylons-data/$INSTANCE/feature_mapping.raw" 
    
    head -n "$NUMBER_OF_CHECKS" $FEATURE_MAPPING_FILE | awk '{print $1 "_" $6}' > "$OUTPUT_FILE"
    tail -n "$NUMBER_OF_CHECKS" $FEATURE_MAPPING_FILE| awk '{print $1 "_" $6}' >> "$OUTPUT_FILE"

    for db_id_and_probe in `cat $OUTPUT_FILE`;do 
        check_redis "probe_mappings_$db_id_and_probe"
    done

}

function check_gene_mappings(){
    #Only look at symbols and entrez for spot checks

    GENE_MAPPING_FILE="/var/www/pylons-data/$INSTANCE/gene_mapping.raw" 
    
    #symbols
    OUTPUT_FILE="/tmp/redis_check_gene_mappings_symbols.txt"
    head -n "$NUMBER_OF_CHECKS" $GENE_MAPPING_FILE | awk -F "\t" '{print $1 "_" tolower($3)}' > "$OUTPUT_FILE"
    tail -n "$NUMBER_OF_CHECKS" $GENE_MAPPING_FILE| awk -F "\t" '{print $1 "_" tolower($3)}' >> "$OUTPUT_FILE"

    for item in `cat $OUTPUT_FILE`;do 
        check_redis "symbol_mappings_$item"
    done


    #entrez
    OUTPUT_FILE="/tmp/redis_check_gene_mappings_entrez.txt"
    head -n "$NUMBER_OF_CHECKS" $GENE_MAPPING_FILE | awk -F "\t" '{print $1 "_" tolower($5)}' > "$OUTPUT_FILE"
    tail -n "$NUMBER_OF_CHECKS" $GENE_MAPPING_FILE| awk -F "\t" '{print $1 "_" tolower($5)}' >> "$OUTPUT_FILE"

    for item in `cat $OUTPUT_FILE`;do 
        if [ -z "$item" ]; then
            check_redis "entrez_mappings_$item"
        fi
    done


}

echo "checking HEADER_DATASETS, user_dataset_availability|uid, choose_dataset_details,gct_labels,cumulative_labels,standard_deviation,probe_mappings_,gene_mappings_,alias_mappings_,entrez_mappings_,refseq_mappings_ "

#check_headers
check_user_dataset_availability
check_choose_dataset_details
check_gct_files
check_yugene_files
check_standard_deviation
check_probe_mappings
check_gene_mappings

if [ "$ERROR" == "TRUE" ]; then
    if [ "$INTERACTIVE" == "TRUE" ]; then
        echo "Error reported, so running the load script"
        cd /var/www/pylons/$INSTANCE
        $LOAD_SCRIPT $INSTANCE $INI
    else
        echo "Error reported. Run $LOAD_SCRIPT"
    fi
fi
