# Live querying
from guide.model.stemformatics import *
from pylons import config

def test():
    set_mappings_into_redis()

def set_mappings_into_redis():
    gene_mapping_raw_file_base_name = config['gene_mapping_raw_file_base_name']
    feature_mapping_raw_file_base_name = config['feature_mapping_raw_file_base_name']
    result = Stemformatics_Gene.setup_bulk_import_manager_mappings(gene_mapping_raw_file_base_name,feature_mapping_raw_file_base_name)

