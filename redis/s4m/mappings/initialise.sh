#!/bin/bash
source /var/www/pylons/virtualenv/bin/activate
MYDIR="$(dirname "$(which "$0")")"
redis-cli -s /data/redis/redis.sock KEYS "*_mappings_*" | xargs -d '\n' redis-cli -s /data/redis/redis.sock DEL
WORKING_DIR=/var/www/pylons/prod
cd $WORKING_DIR

#nosetests --with-pylons="$1" guide/model/stemformatics/tests/test_mappings_speed.py
nosetests --with-pylons="$2" /data/repo/git-working/stemformatics_tools/redis/s4m/mappings/initialise.py

