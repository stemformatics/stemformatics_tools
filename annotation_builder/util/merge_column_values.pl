#!/usr/bin/perl

#use Data::Dumper;

$script_name = (! $ARGV[0]) ? $0 : $ARGV[0];

sub Usage {
   print "\nUsage: $script_name <input_TSV_file> <output_merged_TSV_file> <merge_column_number> [<merge_delimiter>]\n\n";
   print "Note: Expects first column to contain the row 'key'.\n";
   print "      Default merge delimiter is a space.\n\n";
   exit 1
}

Usage() unless scalar(@ARGV) == 3;

$input_file = shift @ARGV;
$output_file = shift @ARGV;
$column = int(shift @ARGV);
if ($column =~ /^1$/) {
   print "Error: Cannot specify first column as merge column as it contains the row key!\n\n";
   exit 1
}
$merge_delim = ' ';
$arg_delim = shift @ARGV;
if ($arg_delim) {
   $merge_delim = $arg_delim;
}

open INFILE, "<", $input_file or die $!;
open OUTFILE, ">", $output_file or die $!;

my %rowHash = ();

$input_col_count = 0;

while (<INFILE>) {
   chomp $_;
   ## The "-1" length arg to split() prevents trailing blank fields from being stripped.
   my @tokens = split(/\t/, $_, -1);
   unless (scalar(@tokens)) {
      @tokens = ($_);
   }

   my $key = $tokens[0];
   ## The input file's column count is determined from the first input row.
   ## ALL subsequent rows must have the same column count.
   if ($input_col_count == 0) {
      $input_col_count = scalar(@tokens);
   } else {
     if (scalar(@tokens) != $input_col_count) {
       close(INFILE) && die "Error: Input row with key '$key' has column count mismatch (" . scalar(@tokens) . " vs. expected $input_col_count)\n\n";
     }
   } 
   my $replaceToken = $tokens[$column-1];

   if (! exists($rowHash{$key})) {
     $rowHash{$key} = \@tokens;
   } else {
     $rowHash{$key}[$column-1] = $rowHash{$key}[$column-1] . $merge_delim . $tokens[$column-1];
   }

}
close(INFILE);

foreach (keys(%rowHash)) {
   my @tokens = @{$rowHash{$_}};
   ### DEBUG ###
   #if (scalar(@tokens) != $input_col_count) {
   #  close OUTFILE && die "Error: Output row with key '$_' has column count mismatch (" . scalar(@tokens) . " vs. input $input_col_count)\n\n";
   #}
   ### END ###
   print OUTFILE join("\t", @tokens) . "\n";
}
