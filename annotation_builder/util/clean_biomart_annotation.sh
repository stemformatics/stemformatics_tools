#!/bin/sh
pid="$$"
mart_export_minus_header="$1"
output_dir="$2"
file_base=`basename $1`
## Remove "[Source: ...]" tag from description field
cat "$mart_export_minus_header" | sed -r -e 's/\[Source\:.*\]//g' > "$output_dir/$pid.tmp"

## Remove "LRG" gene IDs
grep -v -P "^LRG" "$output_dir/$pid.tmp" > "$output_dir/$file_base.clean"
rm -f "$output_dir/$pid.tmp"

## Remove transcript column 
cat "$output_dir/$file_base.clean" | sed -r -e 's/\t(ENST|ENSMUST)[0-9]+\t/\t/g' > "$output_dir/$file_base.clean.without.transcripts"
sort "$output_dir/$file_base.clean.without.transcripts" | uniq > "$output_dir/$file_base.clean.without.transcripts.uniq"

