#!/bin/sh
DOWNLOAD_DIR=""
INCLUDE_DIR="./include"
ENTREZ_FTP="ftp://ftp.ncbi.nlm.nih.gov"
GENE_INFO_PATH="/gene/DATA/GENE_INFO/Mammalia"
## Default species to download annotations for
SPECIES_DOWNLOAD="Homo_sapiens Mus_musculus"


Usage () {
  echo
  echo "$SCRIPTNAME --outdir <dir> [<options>]"; echo
  echo "options:
    --help
      This help

    --outdir <download directory>
  "
}

if [ ! -z "$1" ]; then
  while [ ! -z "$1" ]; do
    arg="$1"
    case $arg in
      --help)
        Usage
        exit
        ;;
      --outdir)
        shift
        DOWNLOAD_DIR="$1"
        ;;
    esac
    shift
  done
fi

if [ -z "$DOWNLOAD_DIR" -o ! -w "$DOWNLOAD_DIR" ]; then
  echo "Error: Must provide a valid path for downloads!"; echo
  Usage
  exit 1
fi

## Check we have everything we need to run this script, set up paths  etc
. "$INCLUDE_DIR/check_env.sh"
if [ $? -ne 0 ]; then
  exit $?
fi


### MAIN ###

echo
echo "Entrez FTP host: $ENTREZ_FTP"
echo "Species: $SPECIES_DOWNLOAD"
echo -n "Proceed to download? [Y/n] "
if [ -z "$ans" ]; then
  ans="Y"
fi
read ans
case $ans in
  N|n)
    echo "Skipping Entrez download."
    exit
    ;;
esac

### DOWNLOAD ###

for s in $SPECIES_DOWNLOAD
do
  echo "Downloading Entrez annotation for '$s' .."
  wget "${ENTREZ_FTP}${GENE_INFO_PATH}/$s.gene_info.gz" -O "$DOWNLOAD_DIR/$s.gene_info.gz"
  if [ $? -ne 0 ]; then
    echo "Error: Failed download for '$s', skipping"
    continue
  fi
  if [ -s "$DOWNLOAD_DIR/$s.gene_info.gz" ]; then
    gunzip "$DOWNLOAD_DIR/$s.gene_info.gz"
  fi
  echo "Done"
done

