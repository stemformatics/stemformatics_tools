#!/bin/sh

## 1. Fetch UniProt annotation
	## Mouse
	wget ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/idmapping/by_organism/MOUSE_10090_idmapping_selected.tab.gz
	gunzip MOUSE_10090_idmapping_selected.tab.gz

	## Human
	wget ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/idmapping/by_organism/HUMAN_9606_idmapping_selected.tab.gz
	gunzip HUMAN_9606_idmapping_selected.tab.gz


## 2. Output UniProt ID to Ensembl Transcript and Gene mappings

	### Mouse ###

	cut -f 1,20 MOUSE_10090_idmapping_selected.tab | awk -F'\t' '{print $2"\t"$1}' | sort | uniq | grep ENSMUSG > ENSMUSG_uniprot.txt.tmp
	grep ";" ENSMUSG_uniprot.txt.tmp | while read line
	do
	  uniprot=`echo "$line" | awk '{print $NF}' | tr -d [:cntrl:]`
	  genes=`echo "$line" | cut -d';' -f1- | tr "; " "\n" | sed -r -e 's/\t.*$//' | grep ENSMUSG`
	  for g in $genes
	  do
	    echo -e "$g\t$uniprot"
	  done
	done > ENSMUSG_uniprot.txt.multi_per_line
	grep -v ";" ENSMUSG_uniprot.txt.tmp > ENSMUSG_uniprot.txt.tmp.minus_multi
	cat ENSMUSG_uniprot.txt.tmp.minus_multi ENSMUSG_uniprot.txt.multi_per_line | sort | uniq > ENSMUSG_uniprot.txt
	#rm ENSMUSG_uniprot.txt.tmp
	## How many genes have mappings to UniProt IDs?
	cut -f 1 ENSMUSG_uniprot.txt | sort | uniq | wc -l
	#22557


	cut -f 1,21 MOUSE_10090_idmapping_selected.tab | awk -F'\t' '{print $2"\t"$1}' | sort | uniq | grep ENSMUST > ENSMUST_uniprot.txt.tmp
        grep ";" ENSMUST_uniprot.txt | while read line
	do
	  uniprot=`echo "$line" | awk '{print $NF}' | tr -d [:cntrl:]`
	  trans=`echo "$line" | cut -d';' -f1- | tr "; " "\n" | sed -r -e 's/\t.*$//' | grep ENSMUST`
	  for t in $trans
	  do
	    echo -e "$t\t$uniprot"
	  done
	done > ENSMUST_uniprot.txt.multi_per_line
	grep -v ";" ENSMUST_uniprot.txt.tmp > ENSMUST_uniprot.txt.tmp.minus_multi
	cat ENSMUST_uniprot.txt.tmp.minus_multi ENSMUST_uniprot.txt.multi_per_line | sort | uniq > ENSMUST_uniprot.txt
	#rm ENSMUST_uniprot.txt.tmp
        ## How many transcripts have mappings to UniProt IDs?
        cut -f 1 ENSMUST_uniprot.txt | sort | uniq | wc -l
        #50240


	### Human ###

	cut -f 1,20 HUMAN_9606_idmapping_selected.tab | awk -F'\t' '{print $2"\t"$1}' | sort | uniq | grep ENSG > ENSG_uniprot.txt.tmp
        grep ";" ENSG_uniprot.txt.tmp | while read line
	do
	  uniprot=`echo "$line" | awk '{print $NF}' | tr -d [:cntrl:]`
	  genes=`echo "$line" | cut -d';' -f1- | tr "; " "\n" | sed -r -e 's/\t.*$//' | grep ENSG`
	  for g in $genes
	  do
	    echo -e "$g\t$uniprot"
	  done
	done > ENSG_uniprot.txt.multi_per_line
        grep -v ";" ENSG_uniprot.txt.tmp > ENSG_uniprot.txt.tmp.minus_multi
        cat ENSG_uniprot.txt.tmp.minus_multi ENSG_uniprot.txt.multi_per_line | sort | uniq > ENSG_uniprot.txt
        #rm ENSG_uniprot.txt.tmp
        ## How many genes have mappings to UniProt IDs?
        cut -f 1 ENSG_uniprot.txt | sort | uniq | wc -l
	#21684


	cut -f 1,21 HUMAN_9606_idmapping_selected.tab | awk -F'\t' '{print $2"\t"$1}' | sort | uniq | grep ENST > ENST_uniprot.txt.tmp
        grep ";" ENST_uniprot.txt.tmp | while read line
	do
	  uniprot=`echo "$line" | awk '{print $NF}' | tr -d [:cntrl:]`
	  trans=`echo "$line" | cut -d';' -f1- | tr "; " "\n" | sed -r -e 's/\t.*$//' | grep ENST`
	  for t in $trans
	  do
	    echo -e "$t\t$uniprot"
	  done
	done > ENST_uniprot.txt.multi_per_line
        grep -v ";" ENST_uniprot.txt.tmp > ENST_uniprot.txt.tmp.minus_multi
        cat ENST_uniprot.txt.tmp.minus_multi ENST_uniprot.txt.multi_per_line | sort | uniq > ENST_uniprot.txt
        #rm ENST_uniprot.txt.tmp
        ## How many transcripts have mappings to UniProt IDs?
        cut -f 1 ENST_uniprot.txt | sort | uniq | wc -l
	#94336
