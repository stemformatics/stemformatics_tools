#!/bin/sh
## Downloads gene annotations from Ensembl Biomart using XML API.

SCRIPTNAME="$0"
DOWNLOAD_DIR=""
WORK_DIR="/tmp"
## Get Human and Mouse annotations by default
DOWNLOAD_SPECIES="homo_sapiens mus_musculus"
INCLUDE_DIR="include"
ENSEMBL_HOST="http://ensembl.org"
QUERY_XML_DIR="query-files"
QUERY_XMLS=""
for species in $DOWNLOAD_SPECIES
do
  QUERY_XMLS="$QUERY_XMLS,$species/${species}_s4m_all_LATEST.xml"
done


Usage () {
  echo
  echo "$SCRIPTNAME [<options>] [ensembl_archive_release]"; echo
  echo "options:
    --help
      This help

    --outdir <download directory>

    --tmpdir <temp working directory>
      If not given, default is /tmp

    --queryxml <species/xml_query_file1.xml,[species/xml_query_file2.xml,]>
      Override query XML files using only these specific ones, comma-separated
  "
  echo "NOTE: 'ensembl_archive_release' corresponds to Ensembl archive site name"
  echo "e.g. 'may2012' for v67 or 'jun2011' for v63."
  echo "See the full list at: http://www.ensembl.org/Help/ArchiveList"; echo
}

ARCHIVE_RELEASE=""

if [ ! -z "$1" ]; then
  while [ ! -z "$1" ]; do
    arg="$1"
    case $arg in
      --help)
        Usage
        exit
        ;;
      --outdir)
        shift
        DOWNLOAD_DIR="$1"
        ;;
      --tmpdir)
        shift
        WORK_DIR="$1"
        ;;
      --queryxml)
        shift
        QUERY_XMLS="$1"
        ;;
      jan*|feb*|mar*|apr*|may*|jun*|jul*|aug*|sep*|oct*|nov*|dec*)
        ARCHIVE_RELEASE="$1"
        ;;
    esac
    shift
  done
fi

if [ -z "$DOWNLOAD_DIR" ]; then
  echo "Error: Must provide path for downloads!"; echo
  Usage
  exit 1
fi



QUERY_XMLS=`echo "$QUERY_XMLS" | sed -r -e 's/\,/ /g'`

echo "DEBUG: Using QUERY_XMLS=[$QUERY_XMLS]"; sleep 2

## Check we have everything we need to run this script, set up paths etc
. "$INCLUDE_DIR/check_env.sh"
if [ $? -ne 0 ]; then
  exit $?
fi

if [ ! -z "$ARCHIVE_RELEASE" ]; then
  ENSEMBL_HOST="http://$ARCHIVE_RELEASE.archive.ensembl.org"
  ## check host validity
  host $ENSEMBL_HOST > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo "Error: Host '$ENSEMBL_HOST' doesn't appear to be valid!"; echo
    exit 1
  fi
fi

release="latest"
if [ ! -z "$ARCHIVE_RELEASE" ]; then
  release="$ARCHIVE_RELEASE"
fi 
echo "Ensembl host: '$ENSEMBL_HOST'"
echo "Release: $release"

BIOMART_SERVICE="$ENSEMBL_HOST/biomart/martservice"

echo -n "Start download? [Y/n] "
read ans
if [ -z "$ans" ]; then
  ans="Y"
fi
case $ans in
  N|n)
    exit
    ;;
esac


### DOWNLOAD ###

rm -f "$WORK_DIR/ens_download.log"

for qx in $QUERY_XMLS
do
  ## Build result file name
  resultfile=`echo "$qx" | sed -r -e 's/\..*$/\.dat/' -e "s|/|/${release}_|"`

  ## Check if target result file already exists
  if [ -f "$DOWNLOAD_DIR/$resultfile" -a -s "$DOWNLOAD_DIR/$resultfile" ]; then
    echo -n "Existing Biomart data file "$DOWNLOAD_DIR/$resultfile" would be clobbered, is that okay? [y/N] "
    read ans
    if [ -z "$ans" ]; then
      ans="N"
    fi
    case $ans in
      n|N)
        echo "Please check or remove old .dat files in $DOWNLOAD_DIR."
        echo "Skipping.."
        continue
        ;;
    esac
  fi
  xmlquery=`cat "$QUERY_XML_DIR/$qx" | tr -d [:cntrl:]`

  #echo "DEBUG: xmlquery=[$xmlquery]"; sleep 2

  ## Fetch using wget
  echo "Downloading Ensembl gene annotation '$resultfile' .."
  wget "$BIOMART_SERVICE?query=$xmlquery" -O "$DOWNLOAD_DIR/$resultfile"
  if [ $? -ne 0 ]; then
    echo "Error: Download failed."; echo
    exit 1
  fi
  ## Log downloaded file name; build script needs to know
  echo "$resultfile" >> "$WORK_DIR/ens_download.log"
  echo "Annotation download '$resultfile' complete."; echo; echo
done

echo "All done."
