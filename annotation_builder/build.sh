#!/bin/sh
## Default build pipeline for latest gene annotation (current version of Ensembl
## and Entrez annotations).

BUILD_DIR="$1"
SPECIES_LIST="homo_sapiens,mus_musculus"
ENS_RELEASE=""

Usage () {
  echo
  echo "Usage: $0 [options] <output_directory> [ensembl_archive_release]"; echo
  echo "   Options:"
  echo 
  echo "   --help                    This help text"; echo
  echo "   --species <SPECIES_LIST>  (default \"homo_sapiens,mus_musculus\")"; echo
  echo
  echo "   NOTE: 'ensembl_archive_release' corresponds to Ensembl archive site name"
  echo "   e.g. 'may2012' for v67 or 'jun2011' for v63."
  echo "   See the full list at: http://www.ensembl.org/Help/ArchiveList"; echo
  echo "   If not supplied, assumes CURRENT Ensembl release."
  echo
  exit
}

if [ ! -z "$1" ]; then
  while [ ! -z "$1" ]; do
    arg="$1"
    case $arg in
      --help)
        Usage
        exit
        ;;
      ## TODO: Not yet used
      --species)
        shift
        SPECIES_LIST="$1"
        ;;
      jan*|feb*|mar*|apr*|may*|jun*|jul*|aug*|sep*|oct*|nov*|dec*)
        ENS_RELEASE="$1"
        ;;
      ## TODO: Not supported yet. Must supply archive spec
      ##       for any release not the LATEST Ensembl version.
      4*|5*|6*|7*|8*|9*)
        echo "DEBUG: Got release version arg=[$1]"
        ENS_RELEASE="$1"
        ;;
    esac
    shift
  done
fi


if [ -z "$BUILD_DIR" ]; then
  Usage
fi

timestamp=`date +"%Y%m%d-%H%M%S"`

mkdir -p "$BUILD_DIR/$timestamp"
workdir="$BUILD_DIR/$timestamp"

### DEBUG ###
#if [ ! -z "$false" ]; then

## Download Ensembl biomart gene annotations
./download_ensembl_annotation.sh --outdir "./data/ensembl" --tmpdir "$workdir" "$ENS_RELEASE"
if [ $? -ne 0 ]; then
  exit $?
fi

## Set up list of Ensembl data files to be cleaned and processed
clean_files=""
if [ -s "$workdir/ens_download.log" ]; then
  echo "Processing downloaded Ensembl Biomart annotations.."
  clean_files=`cat "$workdir/ens_download.log"`
else
  echo "Processing EXISTING Ensembl Biomart annotations.."
  for s in `echo $SPECIES_LIST | sed -r -e 's/\,/ /g'`
  do
    for f in `ls ./data/ensembl/$s | grep .dat`; do
      if [ ! -z "$ENS_RELEASE" ]; then
        echo "$f" | grep "$ENS_RELEASE" > /dev/null 2>&1
        if [ $? -eq 0 ]; then
          clean_files="$clean_files $s/$f"
        fi
      else
        echo "$f" | grep latest > /dev/null 2>&1
        if [ $? -eq 0 ]; then
          clean_files="$clean_files $s/$f"
        fi
      fi
    done
  done
fi

if [ -z "$clean_files" ]; then
  echo "Warning: No annotations to clean; nothing downloaded and nothing existing"
  echo "  that maches build target."; echo
  sleep 3
else

  ## Clean and process biomart annotations
  for f in $clean_files
  do
    ./util/clean_biomart_annotation.sh "./data/ensembl/$f" "$workdir"
  done
  echo "done"
fi


## Download Entrez gene annotations
## TODO: Dynamic species usage vs. hard coded Human and Mouse
./download_entrez_annotation.sh --outdir "./data/entrez"
if [ $? -ne 0 ]; then
  exit $?
fi


echo "Collapsing annotations (merging RefSeq IDs, one gene per row).."
## Collapse RefSeq IDs
for f in `ls "$workdir" | grep "clean.without.transcripts.uniq"`
do
  ## RefSeq column is one less than position in XML query file (we have removed transcript column)
  ./util/merge_column_values.pl "$workdir/$f" "$workdir/$f.refseq_merged" 13
done
echo "done"


### DEBUG ###
#fi

entrez_files=`ls ./data/entrez | grep ".gene_info"`
if [ -z "$entrez_files" ]; then
  echo "Error: Cannot merge Entrez IDs (no downloaded annotations) with Biomart annotations."
  echo "    Re-run build script and choose 'Y' to download from Entrez"; echo
  exit 1  
fi


echo "Extracting Entrez aliases.."
entrez_species="Homo_sapiens Mus_musculus"
for es in $entrez_species
do
  if [ -s "./data/entrez/$es.gene_info" ]; then
    lines=`cat "./data/entrez/$es.gene_info" | wc -l`
    tail -`expr $lines - 1` "./data/entrez/$es.gene_info" | awk -F'\t' '{print $2"\t"$5}' | sed -r -e 's/\|/ /g' > "$workdir/$es.entrez_aliases"
    sed -i -r -e 's/\t-/\t/g' "$workdir/$es.entrez_aliases"
  fi
done
echo "done"


echo "Merging Entrez aliases with Ensembl gene annotations.."

for f in `ls "$workdir" | grep ".refseq_merged"`
do
  ## TODO: Dynamic species usage vs. hard coded Human and Mouse
  echo "$f" | grep "hom" > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    es="Homo_sapiens"
  else
    es="Mus_musculus"
  fi

  ## Sort Ensembl annotations by Entrez ID column, numeric ascending.
  ## We don't need to sort the Entrez aliases file before following "join"
  ## as it is already in correct numeric sort order.
  sort -t"`/bin/echo -e '\t'`" -k 12n,12 "$workdir/$f" > "$workdir/$f.entrez_sorted"
  ## Join on Entrez ID with aliases. Lines without Entrez IDs are kept as-is.
  join --nocheck-order -t"`/bin/echo -e '\t'`" -1 12  -2 1 -a 1 -o 1.1,2.2,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,1.10,1.11,1.12,1.13,1.14,1.15  "$workdir/$f.entrez_sorted" "$workdir/$es.entrez_aliases" > "$workdir/$f.alias_merged"

done
echo "done"


echo "Post-processing and creating combined (species merged) annotation table.."
## Add trailing "<br />" tags to Ensembl gene description (last column)
## and create Ensembl ID sorted version
for f in `ls "$workdir" | grep ".alias_merged"`
do
  sed -i -r -e 's/$/\<br \/\>/'  "$workdir/$f"

  ## Determine DB_ID for Stemformatics annotation loading
  ## (TODO: Dynamic species usage vs. hard coded Human and Mouse)
  dbid=""
  sp=""
  echo "$f" | grep "hom" > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    sp="hom"
    dbid="56"
  else
    sp="mus"
    dbid="46"
  fi

  awk -F'\t' -v dbid="$dbid" '{print  dbid"\t"$0}' "$workdir/$f" | sort  > "$workdir/gene_annotation_${sp}_final.dat"
  cat "$workdir/gene_annotation_${sp}_final.dat" >> "$workdir/gene_annotations_combined_final.dat"
done
echo "done"

## Symlink latest results to current output
(cd $workdir/../; unlink ./latest 2>&1; ln -s $timestamp latest)
echo

