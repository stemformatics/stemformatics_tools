#!/bin/sh

mkdir -p "$DOWNLOAD_DIR"
if [ $? -ne 0 -o ! -w "$DOWNLOAD_DIR" ]; then
  echo "Error: Failed to create or write path '$DOWNLOAD_DIR'. Please check write perms."; echo
  exit 1
fi

if [ ! -z "$DOWNLOAD_SPECIES" ]; then
  for s in $DOWNLOAD_SPECIES
  do
    mkdir -p "$DOWNLOAD_DIR/$s"
  done
fi

which wget > /dev/null 2>&1
if [ $? -ne 0 ]; then
  echo "Error: Failed to find 'wget' command in environment!"; echo
  exit 1
fi

which gunzip > /dev/null 2>&1
if [ $? -ne 0 ]; then
  echo "Error: Failed to find 'gunzip' command in environment!"; echo
  exit 1
fi

which host > /dev/null 2>&1
if [ $? -ne 0 ]; then
  echo "Error: Failed to find 'host' command in environment!"; echo
  exit 1
fi
