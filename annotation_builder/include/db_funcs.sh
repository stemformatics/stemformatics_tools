#!/bin/sh
## Note: This file only intended to be 'source'd from parent script.


if [ -z $TMPDIR ]; then
   TMPDIR="/tmp"
fi


load_database_settings () {
  pgloader_conf="$1"
  if [ -f "$pgloader_conf" ]; then
    while read pg_line
    do
      echo $pg_line | grep -P "^host|^port|^base|^user|^pass" > /dev/null 2>&1
      if [ $? -eq 0 ]; then
         var=`echo $pg_line | sed -r -e 's/\s*\=.*$//'`
         val=`echo $pg_line | sed -r -e 's/^\w+\s*\=\s*//'`
         eval "pg_$var=\"$val\""
      fi
    done < "$pgloader_conf"

    ## Create temporary "PGPASSFILE" for 'psql' automatic password usage.
    ## See: http://www.postgresql.org/docs/current/static/libpq-pgpass.html
    timestamp=`date +"%Y%m%d-%H%M%S"`
    export PGPASSFILE="$TMPDIR/.pgpass_$timestamp"
    echo "$pg_host:$pg_port:$pg_base:$pg_user:$pg_pass" > "$PGPASSFILE"
    ## If we don't set mode 600, 'psql' will ignore our password file.
    chmod 600 $PGPASSFILE

  else
    echo "db_funcs.sh: Error: pgloader configuration '$pgloader_conf' not found!"
    return 1
  fi
}


database_exec () {
  sql_input="$1"
  ## Don't continue if DB settings not loaded
  if [ -z "$pg_host" -o -z "$pg_user" -o -z "$pg_port" -o -z "$pg_base" ]; then
    echo "db_funcs.sh: Error: database_exec: Cannot execute queries, database settings not loaded!" 1>&2
    return 1
  fi

  if [ -f "$sql_input" ]; then
    result=`cat "$sql_input" | psql -U $pg_user -h $pg_host -p $pg_port $pg_base 2>&1`
  else
    echo "$sql_input" | grep -i -P "^copy.*stdout" > /dev/null 2>&1
    ## Don't intercept STDOUT if we're dealing with a COPY TO STDOUT command.
    ## Simply run the statement and exit
    if [ $? -eq 0 ]; then
       echo "$sql_input" | psql -U $pg_user -h $pg_host -p $pg_port $pg_base 2>&1
       return
    else
      result=`echo "$sql_input" | psql -U $pg_user -h $pg_host -p $pg_port $pg_base 2>&1`
    fi
  fi
    
  ## psql doesn't return non-zero error codes, so we try to intercept errors here.
  echo "$result" | grep -P -i "error|fatal|not known|help" > /dev/null 2>&1
  if [ "$?" -eq 0 ]; then
    echo "$result" 1>&2
    return 1
  else
    echo "$result"
  fi
}

