#!/bin/sh
## Load combine gene annotations table into a target Stemformatics DB

annotation_file="$1"
output_dir="$2"
conf_file="$3"
db_table="$4"
species_list="hom,mus"
## NOTE: Species list not yet used. Assume Human AND Mouse.
#if [ $# -gt 1 ]; then
#  species_list="$2"
#fi

if [ -z "$db_table" ]; then
  db_table="genome_annotations"
fi

SESSION="$$"
TMPDIR="/tmp"
OUTDIR="$output_dir"
LOGDIR="$output_dir"
PGLOG="$LOGDIR/pgloader_$SESSION.log"


CONF_BODY="./conf/template/pgloader.conf.body"
if [ ! -f "$CONF_BODY" ]; then
  echo "Fatal Error: Required file '$CONF_BODY' not found! Aborting."
  echo "(ensure running out of top-level annotation builder directory)"; echo
  exit 1
fi

## Include DB routines
. "./include/db_funcs.sh"


Usage () {
  ## NOTE: Species list not yet used. Assume Human AND Mouse by default.
  echo
  echo "Usage: $0 <annotation_file_to_load> <output_dir> <pgloader_conf_file> [target_table_name]"; echo
  echo "Note: Default target table name is 'genome_annotations', if not given."; echo
  echo "Examples:"
  echo "	$0 build/20121225-000001/gene_annotations_combined_final.dat  build/20121225-000001  QFAB_prod.conf"; echo
  echo "	$0 build/20121225-000001/gene_annotations_combined_final.dat  build/20121225-000001  AIBN_beta.conf genome_annotations_new"; echo; echo
  echo "NOTE: Requires 'pgloader' executable in PATH"; echo
  exit
}


if [ -z "$annotation_file" -o -z "$output_dir" -o -z "$conf_file" ]; then
  Usage
fi

## If path to conf file not given, prepend standard conf file path
echo "$conf_file" | grep "/" > /dev/null 2>&1
if [ $? -ne 0 ]; then
  conf_file="conf/$conf_file"
fi

if [ ! -f "$annotation_file" ]; then
  echo "Error: Input annotation file "$annotation_file" not found or not readable!"; echo
  exit 1
fi

if [ ! -f "$conf_file" ]; then
  echo "Error: Cannot find pgloader host configuration file '$conf_file'!"; echo
  exit 1
fi

## pgloader check
echo "Looking for 'pgloader'.."
which pgloader > /dev/null 2>&1
if [ $? -ne 0 ]; then
  ## Look in a commonly used virtual environment location
  if [ -f "/var/www/pylons/virtualenv/bin/activate" ]; then
    . "/var/www/pylons/virtualenv/bin/activate"
    which pgloader > /dev/null 2>&1
    if [ $? -ne 0 ]; then
      echo "Error: No 'pgloader' in PATH or virtual environment, cannot proceed. Aborting."; echo
      exit 1
    fi
  else
    echo "Error: No 'pgloader' in PATH, cannot proceed. Aborting."; echo
    exit 1
  fi
fi
echo "found"


## Create "full" pgloader file from template and host-specific parts.
## Replace table name and input annotation data file with actuals.
cat "$conf_file" "$CONF_BODY" > "$OUTDIR/pgloader_anno_builder_$SESSION.conf"
sed -i -r -e "s|%GENOME_ANNOTATIONS_TABLE%|${db_table}|g" "$OUTDIR/pgloader_anno_builder_$SESSION.conf"
sed -i -r -e "s|%GENE_ANNOTATION_FILE%|${annotation_file}|g" "$OUTDIR/pgloader_anno_builder_$SESSION.conf"
echo "Created session pgloader configuration file:  '$OUTDIR/pgloader_anno_builder_$SESSION.conf'"


echo "Loading database settings from supplied configuration.."
load_database_settings "$conf_file"
if [ $? -ne 0 ]; then
  echo "Error: Failed to load database settings! Exiting."; echo
  exit 1
fi
echo "done"

## TODO: Backup and clear target annotation table
database_exec "COPY (SELECT * FROM $db_table) TO STDOUT" > "$OUTDIR/${db_table}_${SESSION}.dump.txt"
#database_exec "COPY (SELECT * FROM genome_annotations) TO STDOUT" > "$OUTDIR/genome_annotations_${SESSION}.dump.txt"
if [ ! -s "$OUTDIR/${db_table}_${SESSION}.dump.txt" ]; then
  echo "Error: Failed to create backup dump ('$OUTDIR/${db_table}_${SESSION}.dump.txt')"
  echo "(May be expected if target table is currently empty)"; echo
  echo -n "Continue with reload? [y/N] "
  read ans
  if [ -z "$ans" ]; then
    ans="N"
  fi
  case $ans in
    y|Y) 
      ;;
    *)
      echo "Aborting."; echo; exit 1
      ;;
  esac
fi

echo "Clearing target table '${db_table}'.."
## Attempt to clear table if it has any existing data
rowcount=`database_exec "COPY (SELECT count(*) FROM $db_table) TO STDOUT"`
rowcount=`echo $rowcount | tr -d [:cntrl:]`
if [ $rowcount -gt 0 ]; then
  result=`database_exec "DELETE FROM $db_table"`
  if [ $? -eq 0 ]; then
    echo "done"
  else
    echo "Error: Failed to delete rows from table '$db_table'. Aborting."; echo
    exit 1
  fi
else
  echo "Table '$db_table' has no rows, skipping.."
  sleep 2
fi 

echo; echo "Loading annotation data into $pg_host:/$pg_base/$db_table .."
sleep 2
## Load annotation data into target gene annotation table
pgloader -v -s -D --logfile="$PGLOG" -c "$OUTDIR/pgloader_anno_builder_$SESSION.conf"
echo "done"


### DEBUG ###
#echo "DEBUG: Early exit.."
#exit
### END ###


## Re-build FTS lexemes (and other post-load SQL tasks)
echo "Running post-load SQL (re-building FTS lexemes etc).."
ftsupdate=`grep -P "^post_load_sql" "$OUTDIR/pgloader_anno_builder_$SESSION.conf" | sed -r -e 's/^post_load_sql\s=\s//g' | tr -d [:cntrl:]`

#echo "DEBUG: post_load_sql=[$ftsupdate]"; echo

result=`database_exec "$ftsupdate"`
if [ $? -ne 0 ]; then
  echo "  Error: Failed to execute post-load SQL:"
  echo "  $result"
else
  echo "  done"
fi

echo "All Done."
