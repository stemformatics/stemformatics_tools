
Annotation Builder for Stemformatics HOWTO
=============================================================================
Created: Oct 2012
Updated: Mar 2018
Author: O.Korn

OVERVIEW

The Annotation Builder suite of tools is intended to provide a first-step
into automation of core annotation updates e.g. Ensembl upgrade and 
platform probe sequence alignments to Ensembl-annotated genome model, vs. the
previous labour-intensive manual process/es involved.


STEPS

1. Download and build Ensembl (with Entrez aliases) gene annotation:

	$ ./build.sh <build_output_dir>

   By default, uses the Biomart XML query files in the ./query-files/ subdirectory
   (See Appendix 1).

   Creates a finished, Human and Mouse merged annotation data file ready for 
   Stemformatics loading:

	<build_output_dir>/<date_time>/gene_annotations_combined_final.dat


2. Check/update "db_id" field (first column) in output annotation file.

   Historical db_ids:
	- v67 Mouse = 46
	- v69 Human = 56

   Current (Mar 2018) db_ids:
	- v86 Human = 57
	- v86 Mouse = 58
	- v91 Human = 59
	- v91 Mouse = 60 

   e.g.

	$ sed -i -r -e 's|^56|59|g' -e 's|^46|60|g' <build_output_dir>/<date_time>/gene_annotations_combined_final.dat


3. Load completed Human and Mouse merged gene annotation table into a Stemformatics
   database instance:

	$ ./load.sh <path/to/gene_annotations_combined_final.dat>  <output_dir> <pgloader_conf_file> [<target_table_name>]

   The 'output_dir' is used to store log files and backup of existing target DB table.

   The 'pgloader_conf_file' is a configuration file name corresponding to host config
   in ./conf/ subdirectory (create one if necessary based on an existing one).

   The 'target_table_name' by default is 'genome_annotations'. Supply an alternate
   table name if desired (e.g. for testing).



APPENDICES
==========

APPENDIX 1 - Biomart XML Query Files:

Annotation data are fetched from Ensembl's Biomart instance using the XML API.
The XML query strings are stored in the ./query-files/ subdirectory, one query
to a file.

The default query files are named like '<species>_s4m_all.xml' and will download
all gene annotation fields required for Stemformatics.

The XML query files were built simply by manually visiting Ensembl Biomart,
selecting all required attributes, and using the 'XML' export button to
retrieve an XML API query representation of the composed query.

In a similar fashion, you may add new Biomart queries in this way by composing
the query and exporting the XML code and saving to a new query file under the
'query-files' path.

To specify the query file/s for the downloader to use, refer to the help
information:

	./download_ensembl_annotation.sh --help


