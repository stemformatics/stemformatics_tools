#!/bin/sh
## Downloads gene sequence data (transcript FASTA)
## from Ensembl FTP

SCRIPTNAME="$0"
DOWNLOAD_DIR=""
WORK_DIR="/tmp"
## Get Human and Mouse annotations by default
DOWNLOAD_SPECIES="homo_sapiens mus_musculus"
INCLUDE_DIR="include"
ENSEMBL_HOST="ftp://ftp.ensembl.org"

Usage () {
  echo
  echo "$SCRIPTNAME [<options>] --outdir <download_dir> <ensembl_release_number>"; echo
  echo "options:
    --help
      This help

    --tmpdir <temp working directory>
      If not given, default is /tmp
  "
}

RELEASE_VER=""

if [ ! -z "$1" ]; then
  while [ ! -z "$1" ]; do
    arg="$1"
    case $arg in
      --help)
        Usage
        exit
        ;;
      --outdir)
        shift
        DOWNLOAD_DIR="$1"
        ;;
      --tmpdir)
        shift
        WORK_DIR="$1"
        ;;
      4*|5*|6*|7*|8*|9*)
        echo "DEBUG: Got release version arg=[$1]"
        RELEASE_VER="$1"
        ;;
    esac
    shift
  done
fi

if [ -z "$DOWNLOAD_DIR" ]; then
  echo "Error: Must provide path for downloads (--outdir X)!"; echo
  Usage
  exit 1
fi

if [ -z "$RELEASE_VER" ]; then
  echo "Error: Must provide Ensembl release version!"; echo
  Usage
  exit 1
fi


## Check we have everything we need to run this script, set up paths etc
. "$INCLUDE_DIR/check_env.sh"
if [ $? -ne 0 ]; then
  exit $?
fi

FTP_PATH="pub/release-$RELEASE_VER/fasta"

wget "$ENSEMBL_HOST/$FTP_PATH" 2>&1 | grep -P "CWD.*done" > /dev/null 2>&1
## That directory exists (CWD command was successful), FTP URI looks okay
if [ $? -ne 0 ]; then
  echo "Error: FTP download path '$ENSEMBL_HOST/$FTP_PATH' doesn't appear to be valid!"
  echo "(Check release version validity etc.)"; echo
  exit 1
fi

echo "FTP base path: '$ENSEMBL_HOST/$FTP_PATH'"
echo "Release: $RELEASE_VER"


echo -n "Start download? [Y/n] "
read ans
if [ -z "$ans" ]; then
  ans="Y"
fi
case $ans in
  N|n)
    exit
    ;;
esac


### DOWNLOAD ###

for species in $DOWNLOAD_SPECIES
do
  echo "Attempting to download cDNA and ncRNA FASTA sequences for species '$species' .."
  sleep 3

  DLDIR="$DOWNLOAD_DIR/$species"
  ## Get cDNA
  (cd $DLDIR && wget -nc -nd -r -l1 --no-parent -A.all.fa.gz "$ENSEMBL_HOST/$FTP_PATH/$species/cdna/")
  if [ $? -ne 0 ]; then
    echo "Error: Download cDNA sequences failed."; echo
  fi
  ## Get ncRNA
  (cd $DLDIR && wget -nc -nd -r -l1 --no-parent -A.fa.gz "$ENSEMBL_HOST/$FTP_PATH/$species/ncrna/") 
  if [ $? -ne 0 ]; then
    echo "Error: Download ncRNA sequences failed."; echo
  fi

  echo "FASTA sequence download complete."; echo; echo
done

echo "All done."

