my $baseDir = '/tmp/kegg/';
my $db_id = "56";
my $uid = 0;
my $gene_pathway_type = "Kegg";
my $base_description = "";
my $gene_set_id = 10000;
my $gene_set_items_id = 100000;


my $raw_gene_sets_filename = $baseDir.'c2.cp.kegg.v3.0.entrez.gmt';
my $ensembl_to_entrez_filename = $baseDir.'ensembl_to_entrez_'.$db_id.'.txt';
my $gene_set_file = ">".$baseDir."gene_set_".$db_id.".tsv";
my $gene_set_items_file = ">".$baseDir."gene_set_items_".$db_id.".tsv";

open my $raw_gene_sets_file, $raw_gene_sets_filename;
open my $ensembl_to_entrez_file, $ensembl_to_entrez_filename;


open(GENESETOUTFILE, $gene_set_file); #open for write
open(GENESETITEMSOUTFILE, $gene_set_items_file); #open for write


%entrez_list = ();

while (<$ensembl_to_entrez_file>){

    $row = $_;
    
    @row = split(" ",$row);
    
    $gene_id = @row[0];
    
    # remove the first element
    shift(@row);
    
    for my $entrez_id (@row) {
        $entrez_list {$entrez_id } = $gene_id;
    }
    
}


while (<$raw_gene_sets_file>){
    
    $row = $_;
    
    @row = split("\t",$row);
    
    $gene_set_name = @row[0];
    $gene_set_url = @row[1];
    
    $description = $base_description."Specific URL for this pathway is ".$gene_set_url;
    
    $gene_set_id++;
    
    print GENESETOUTFILE "$gene_set_id\t$gene_set_name\t$db_id\t$description\t$uid\t$gene_pathway_type\n"; 
    
    # remove the first two elements 
    shift(@row);
    shift(@row);
    
    %genes_used = ();
    
    for my $entrez_id (@row) {
        $gene_id = $entrez_list {$entrez_id};
        
        # skip over genes that are not found
        if ( $gene_id eq "" ){
            next;
        }
        
        $used_before = $genes_used{$gene_id};
        # print $used_before." ===== ".$gene_id."\n";
        if ($used_before == 0 ){
        
            $gene_set_items_id++;
            print GENESETITEMSOUTFILE "$gene_set_items_id\t$gene_set_id\t$gene_id\n";
            $genes_used{$gene_id} = 1;
        }
        
    }
    
}

# cat gene_set_56.tsv | psql -h localhost -U portaladmin portal_beta -c "COPY stemformatics.gene_sets FROM STDIN"
# cat gene_set_items_56.tsv | psql -h localhost -U portaladmin portal_beta -c "COPY stemformatics.gene_set_items FROM STDIN"
























