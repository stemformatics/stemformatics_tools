use 5.010;

my $ensemblVersion = 'v63';
$baseDir = '/tmp/transcripts/';

# check if directory available
mkdir '/tmp/transcripts';

my @db_ids = ("46","56");

for $db_id (@db_ids){

    if ($db_id eq "46"){
        $organism = "mouse";
    } else {
        $organism = "human";
    }
    
    open my $all_transcripts, $baseDir.'raw_all_transcripts_'.$ensemblVersion.'_'.$organism.'.txt';
    open my $protein_transcripts, $baseDir.'raw_protein_transcripts_'.$ensemblVersion.'_'.$organism.'.txt';
    open my $cds_length, $baseDir.'raw_protein_sequence_'.$ensemblVersion.'_'.$organism.'.txt';

    @all_tx = <$all_transcripts>;
    @protein_tx = <$protein_transcripts>;
    @protein_seq = <$cds_length>;

    # remove first row
    my $all_header = splice (@all_tx,0,1);
    my $protein_header = splice (@protein_tx,0,1);

    my $test_length = scalar @all_tx;
    my $test_length_2 = scalar @protein_tx;

    open(MYOUTFILE, ">".$baseDir."transcript_annotations_".$ensemblVersion."_$organism.tsv"); #open for write, append
    open(ERRORFILE, ">".$baseDir."transcript_annotations_".$ensemblVersion."_$organism.err"); #open for write, append


    %main_tx = ();

    foreach (@all_tx){
        
        $row = $_;
        
        @row = split("\t",$row);
        
        $gene_id = @row[0]; 
        $tx_id = @row[1];
        $name = @row[2];
        
        # remove any special characters
        $name =~ s/[^a-zA-Z0-9\-]*//g; 

        $main_tx { $tx_id } = { gene_id => $gene_id, name => $name, tm_domain => "False", sp => "False" };

    }

    my $test = $main_tx{"ENST00000392132"}{name};
    # say "$test";

    foreach (@protein_tx){
        
        $row = $_;
        
        @row = split("\t",$row);
        
        $gene_id = @row[0]; 
        $tx_id = @row[1];
        $raw_tm_domain = @row[2];
        $raw_sp = @row[3];

        # remove any special characters
        $raw_sp =~ s/[^a-zA-Z0-9]*//g; 

        if ($raw_sp eq "Sigp"){
            $sp = "True";
        } else {
            $sp = "False";
        }
        
        if ($raw_tm_domain eq "Tmhmm"){
            $tm_domain = "True";
        } else {
            $tm_domain = "False";
        }
        

        # reassign individually
        $main_tx { $tx_id }{ gene_id }= $gene_id;
        $main_tx { $tx_id }{ tm_domain }= $tm_domain;
        $main_tx { $tx_id }{ sp }= $sp;
        

    }

    $old_tx_id = "";
    $old_tx_length = 0;

    foreach (@protein_seq){
        
        my $raw_row = $_;
        
        # my $header = grep />/, $raw_row;
        my $header = substr($raw_row,0,1);
        
        if ($header eq ">" ){
            
            # save old tx id details into hash
            if ($old_tx_id ne "" ){
                # print " $old_tx_id $old_tx_length\n";
                $main_tx { $old_tx_id } { protein_length } = $old_tx_length;
            }
            
            # setup updated old_tx_id
            my @new_row = split(/\|/,$raw_row);        
            $old_tx_id = $new_row[1];
            
            # remove any special characters but keep _ for LRG_24_t1
            $old_tx_id =~ s/[^a-zA-Z0-9\_\-]*//g; 
            $old_tx_length = 0;
            

        } else {
        
            
                
                
            $raw_row =~ s/[^a-zA-Z0-9]*//g; 
            
            # spaces are removed from raw_row using the above regular expression
            if ($raw_row ne "Sequenceunavailable"){
                $current_length = length($raw_row);
            
                $old_tx_length += $ current_length;    
            } else {
                $old_tx_length = 0;
            }
            
            
                
        }
        
        
    }

    # right at end save the last tx_id
    # save old tx id details into hash
    # print "$old_tx_id $old_tx_length";
    $main_tx { $old_tx_id } { protein_length } = $old_tx_length;



    # Now save to file
    while(($key, $value) = each(%main_tx)) { 
        
        my $tx_id = $key;
        my $gene_id = $main_tx{$key}{gene_id};
        my $name = $main_tx{$key}{name};
        my $tx_start = $main_tx{$key}{tx_start};
        my $tx_end = $main_tx{$key}{tx_end};
        my $protein_length = $main_tx{$key}{protein_length};
        my $sp = $main_tx{$key}{sp};
        my $tm_domain = $main_tx{$key}{tm_domain};
        my $mirna = "False";
        
        if ($gene_id ne ""){
            print MYOUTFILE "$db_id\t$tx_id\t$gene_id\t$name\t$protein_length\t$sp\t$tm_domain\t$mirna\n"; 
        } else{
            print ERRORFILE "$db_id\t$tx_id\t$gene_id\t$name\t$protein_length\t$sp\t$tm_domain\t$mirna\n"; 
        }
    }

}

exit;


# magama on ncascr-dev
# cat transcript_annotations_v63_human.tsv | psql -h ncascr-dev.rcs.griffith.edu.au -U ascc_dev magma -c "COPY stemformatics.transcript_annotations FROM STDIN"
# cat transcript_annotations_v63_mouse.tsv | psql -h ncascr-dev.rcs.griffith.edu.au -U ascc_dev magma -c "COPY stemformatics.transcript_annotations FROM STDIN"


# portal beta 
# cat transcript_annotations_v63_human.tsv | psql -h localhost -p 5433 -U portaladmin -d portal_beta -c "COPY stemformatics.transcript_annotations FROM STDIN"
# cat transcript_annotations_v63_mouse.tsv | psql -h localhost -p 5433 -U portaladmin -d portal_beta -c "COPY stemformatics.transcript_annotations FROM STDIN"



