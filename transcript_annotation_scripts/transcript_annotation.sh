#! /bin/bash

# Database options

# ####################################### local rowland database ####################################### 
# psql_options="-U portaladmin portal_beta "

# #######################################  ncascr dev ####################################### 
# have to run this first : ssh s2776403@ncascr.griffith.edu.au -L 5433:ncascr-dev.rcs.griffith.edu.au:5432

# psql options when at uq
#psql_options="-h 127.0.0.1 -p 5433 -U ascc_dev magma "

# options when at GU
# psql_options="-h ncascr-dev.rcs.griffith.edu.au -U portaladmin portal_beta "


# ####################################### portal beta ####################################### 
# have to run this first : 
# ssh r.mosbergen@stemformatics.org -L 5433:127.0.0.1:5433
# psql_options="-h 127.0.0.1 -p 5433 -U portaladmin portal_beta "


# ####################################### portal prod ####################################### 
# have to run this first : 
# ssh r.mosbergen@stemformatics.org -L 5433:127.0.0.1:5433
psql_options="-h 127.0.0.1 -p 5433 -U portaladmin portal_prod "



# clear out all other data
rm -fR /tmp/transcripts

mkdir /tmp/transcripts

# copy transcript files from my local machine
cp /home/s2776403/Downloads/Ensembl/raw*v63*.txt /tmp/transcripts

perl transcript_annotation_pre_load.pl

# output some quality control error files
ls -alh /tmp/transcripts

head -n 1 /tmp/transcripts/transcript_annotations_v63_mouse.tsv
tail -n 1 /tmp/transcripts/transcript_annotations_v63_mouse.tsv

head -n 1 /tmp/transcripts/transcript_annotations_v63_human.tsv
tail -n 1 /tmp/transcripts/transcript_annotations_v63_human.tsv

cat /tmp/transcripts/raw_all_transcripts_v63_human.txt | wc -l 
cat /tmp/transcripts/transcript_annotations_v63_human.tsv | wc -l
echo "--------------------------------------------------------------------"
cat /tmp/transcripts/raw_all_transcripts_v63_mouse.txt | wc -l 
cat /tmp/transcripts/transcript_annotations_v63_mouse.tsv | wc -l

echo "--------------------------------------------------------------------"

# now upload these 
psql $psql_options -c "truncate table stemformatics.transcript_annotations"
cat /tmp/transcripts/transcript_annotations_v63_human.tsv | psql $psql_options -c "COPY stemformatics.transcript_annotations FROM STDIN"
cat /tmp/transcripts/transcript_annotations_v63_mouse.tsv | psql $psql_options -c "COPY stemformatics.transcript_annotations FROM STDIN"

psql $psql_options -c "select count(*) from stemformatics.transcript_annotations"



