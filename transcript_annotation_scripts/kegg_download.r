#!/usr/bin/env Rscript
# source("http://www.bioconductor.org/biocLite.R")
# biocLite("KEGG.db")
library(KEGG.db)
ls("package:KEGG.db")
dir.create('/tmp/kegg',showWarnings = FALSE)

pathway_names = dbGetQuery(KEGG_dbconn(), "SELECT path_id,path_name FROM pathway2name")
all_genes_mouse = dbGetQuery(KEGG_dbconn(), "SELECT pathway_id,gene_or_orf_id FROM pathway2gene where pathway_id like 'mmu%' ")
all_genes_human = dbGetQuery(KEGG_dbconn(), "SELECT pathway_id,gene_or_orf_id FROM pathway2gene where pathway_id like 'hsa%' ")
    
write.table(pathway_names,'/tmp/kegg/pathway_names.txt')
write.table(all_genes_mouse,'/tmp/kegg/all_genes_46.txt')
write.table(all_genes_human,'/tmp/kegg/all_genes_56.txt')


