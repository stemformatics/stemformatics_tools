


@files = <gene_sets/*>;



open my $gene_f, "all_gene_list_human.tsv"; 

# my $gene_id = 'ENSG00000115415';
my $gene_id = 'ENSG00000115762';

open(OUTFILE, ">results.tsv"); #open for write

%gene_list = ();

while (<$gene_f>){

    $gene_id = $_;
    chomp($gene_id);
    my @files_found = ();    
    
    
    print OUTFILE "$gene_id";
    
    foreach $file (@files) {
        
        open my $f, $file;
        
        while (<$f>) {
            $line = $_;
            if ($line ==  $gene_id){
                $gene_set_id = $file;
                $gene_set_id =~s/gene_sets\///g;
                $gene_set_id =~s/\.txt//g;
                push (@files_found,$gene_set_id);
                print OUTFILE "\t$gene_set_id";
                last;
            }
        }
        
    }
    
    print OUTFILE "\n";
    
    $gene_list { $gene_id } = @files_found;
    
    
    
        
    
    
}
    
my $total = scalar @files_found;
print $total. "\n";
print $gene_id;
close OUTFILE


