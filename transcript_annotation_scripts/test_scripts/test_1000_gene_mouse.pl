open my $all_transcripts, '1000_gene_list_mouse.tsv';
@all_tx = <$all_transcripts>;
my $all_header = splice (@all_tx,0,1);

%genes = ();
my $count = 0;
my $both = 0;
my $only_sp = 0;
my $only_tm = 0;
my $neither = 0;

foreach (@all_tx){
    
    $gene_id = @row[0]; 
    $tx_id = @row[1];
    $raw_tm_domain = @row[2];
    $raw_sp = @row[3];
    
    
    # remove any special characters
    $raw_sp =~ s/[^a-zA-Z0-9]*//g; 

    if ($raw_sp eq "Sigp" and $raw_tm_domain eq "Tmhmm"){
        $both++;
    } 
    
    if ($raw_sp eq "Sigp" and $raw_tm_domain eq ""){
        $only_sp++;
    } 
    
    if ($raw_sp eq "" and $raw_tm_domain eq "Tmhmm"){
        $only_tm++;
    } 
    
    if ($raw_sp eq "" and $raw_tm_domain eq ""){
        $neither++;
    }
    
    
    
}


print "both: $both     only sp: $only_sp     only tm: $only_tm neither $neither \n";
