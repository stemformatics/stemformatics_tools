open my $all_transcripts, 'raw_protein_transcripts_v59_human.txt';
@all_tx = <$all_transcripts>;
my $all_header = splice (@all_tx,0,1);


# This is getting all the genes we want

%genes = ();
%gene_list = ();
my $count = 0;
my $size = 500;

foreach (@all_tx){
    
    @row = split("\t");
    $gene_id = $row[0];
    $genes{$gene_id}{gene_id} = $gene_id;    
}    

while(($key, $value) = each(%genes)) { 

    $count++;
    $gene_id = $key;
    
    $gene_list{$gene_id} = $genes{$gene_id};
    
    if ($count == $size){
        last;
    }
    
}

print "$count";


# setup gene sets
my $start_id = 10001;
my $end_id = 15000;
my $start_gene_set_items_id = 1000000;

my $db_id = 56; # human only
my $uid = 0; # public

foreach ($start_id .. $end_id){
    
    $id = $_;
    $gene_set_name = "load_test_".$id;
    $description = "load_test_".$id;
    
    open(GENESETOUTFILE, ">gene_sets/$id.txt");
    
    while(($key, $value) = each(%gene_list)) { 
        $gene_id = $key;
        
        print GENESETOUTFILE "$gene_id\n";
        
    }
    
}










# cat gene_set.tsv | psql -h localhost -U portaladmin portal_beta -c "COPY stemformatics.gene_sets FROM STDIN"
# cat gene_set_items.tsv | psql -h localhost -U portaladmin portal_beta -c "COPY stemformatics.gene_set_items FROM STDIN"
