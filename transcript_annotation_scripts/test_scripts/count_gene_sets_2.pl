


@files = <gene_sets/*>;


open my $gene_set, "10000_gene_list_human.tsv";
open my $gene_sets, "gene_and_gene_sets.tsv"; 

open(OUTFILE, ">results.tsv"); #open for write


%gene_list = ();


while (<$gene_set>){
    chomp($_);
    $gene_id = $_;
    
    $gene_list{$gene_id} = 1;
    
}


while (<$gene_sets>){

    $row = $_;
    chomp($row);
    
    my @values = split(/\t/,$row);
    
    $gene_id = @values[0];
    
    
    if (exists $gene_list{$gene_id}) {
        print OUTFILE $row."\n";
        
    }
    
    
    # last;
    
    
}
    
close OUTFILE


