my $dataDir = '/home/s2776403/Downloads/Ensembl/';
my $data_file = $dataDir.'raw_protein_transcripts_v59_human.txt';
my $baseDir = '/tmp/';
my $gene_set_file = ">".$baseDir."gene_set.tsv";
my $gene_set_items_file = ">".$baseDir."gene_set_items.tsv";


open my $all_transcripts, $data_file;

@all_tx = <$all_transcripts>;
my $all_header = splice (@all_tx,0,1);




open(GENESETOUTFILE, $gene_set_file); #open for write
open(GENESETITEMSOUTFILE, $gene_set_items_file); #open for write



# This is getting all the genes we want

%genes = ();
%gene_list = ();
my $count = 0;
my $size = 500;

# get all the genes that have transcript annotations
foreach (@all_tx){
    
    @row = split("\t");
    $gene_id = $row[0];
    $genes{$gene_id}{gene_id} = $gene_id;    
}    

@genes = ();

# now just create an array of genes
while(($key, $value) = each(%genes)) { 

    $gene_id = $key;
    push(@genes,$gene_id);
    
}


# setup gene sets
my $start_id = 10001;
my $end_id = 15000;
my $start_gene_set_items_id = 1000000;

my $db_id = 56; # human only
my $uid = 0; # public

my $range = @genes;

foreach ($start_id .. $end_id){
    
    $id = $_;
    $gene_set_name = "load_test_".$id;
    $description = "load_test_".$id;
    
    my $random_number = int(rand(100));
    
    if ($random_number > 66){
        $type = 'Kegg';
    }
    if ($random_number > 33 and $random_number < 66){
        $type = 'Publications';
    }
    if ($random_number >= 0 and $random_number < 33){
        $type = 'Kegg2';
    }
    
    print GENESETOUTFILE "$id\t$gene_set_name\t$db_id\t$description\t$uid\t$type\n"; 

    my $count = 0;
    
    while($count <500) { 
        
        #randomise gene
        $leave = 0;
        while ($leave == 0){
            
            my $random_number = int(rand($range));
            
            # print $random_number."\n";
            
            $gene_id = $genes[$random_number] ;
            
            my $used = $genes{$gene_id}{$id}{used};
            
            if ($used == 0){
                $leave = 1;
                $genes{$gene_id}{$id}{used} = 1;
            }
        }
        
        # print $id." -- ".$count."--".$gene_id."--".$genes{$gene_id}{used}."\n";
        
        print GENESETITEMSOUTFILE "$start_gene_set_items_id\t$id\t$gene_id\n";
        $count++;
        $start_gene_set_items_id++;
        
    }
    
}










# cat gene_set.tsv | psql -h localhost -U portaladmin portal_beta -c "COPY stemformatics.gene_sets FROM STDIN"
# cat gene_set_items.tsv | psql -h localhost -U portaladmin portal_beta -c "COPY stemformatics.gene_set_items FROM STDIN"
