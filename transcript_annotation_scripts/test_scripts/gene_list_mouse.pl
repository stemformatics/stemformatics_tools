open my $all_transcripts, 'raw_protein_transcripts_v59_mouse.txt';
@all_tx = <$all_transcripts>;
my $all_header = splice (@all_tx,0,1);

open(MYOUTFILE, ">1000_gene_list_mouse.tsv"); #open for write

%genes = ();
my $count = 0;

foreach (@all_tx){
    
    @row = split("\t");
    $gene_id = $row[0];
    $genes{$gene_id}{gene_id} = $gene_id;
     
    $tx_id = @row[1];
    $raw_tm_domain = @row[2];
    $raw_sp = @row[3];
    
    
    # remove any special characters
    $raw_sp =~ s/[^a-zA-Z0-9]*//g; 

    if ($raw_sp eq "Sigp" and $raw_tm_domain eq "Tmhmm"){
        $genes{$gene_id}{both} = $genes{$gene_id}{both}  + 1;
    } 
    
    if ($raw_sp eq "Sigp" and $raw_tm_domain eq ""){
        $genes{$gene_id}{sp_only} = $genes{$gene_id}{sp_only}  + 1;
    } 
    
    if ($raw_sp eq "" and $raw_tm_domain eq "Tmhmm"){
        $genes{$gene_id}{tm_only} = $genes{$gene_id}{tm_only}  + 1;
    } 
    
    if ($raw_sp eq "" and $raw_tm_domain eq ""){
        $genes{$gene_id}{neither} = $genes{$gene_id}{neither}  + 1;
    }
    
      
    
    
    
    
    
}    



my $count = 0;
my $both = 0;
my $only_sp = 0;
my $only_tm = 0;
my $neither = 0;

print MYOUTFILE "[";
while(($key, $value) = each(%genes)) { 

    $count++;
    
    $gene = $key;
    print MYOUTFILE "'$gene',";
        
    $both +=  $genes{$gene}{both};
    $only_sp +=  $genes{$gene}{sp_only};
    $only_tm +=  $genes{$gene}{tm_only};
    $neither +=  $genes{$gene}{neither};
    $total = $both + $only_sp + $only_tm + $neither;
    
    # print MYOUTFILE "$gene\t$genes{$gene}{both}\t$genes{$gene}{sp_only}\t$genes{$gene}{tm_only}\t$genes{$gene}{neither}\n";
    
    if ($count ==1000){
        last;
    }
}

print MYOUTFILE "]\n";







print "both: $both     only sp: $only_sp     only tm: $only_tm neither $neither  total: $total\n";


