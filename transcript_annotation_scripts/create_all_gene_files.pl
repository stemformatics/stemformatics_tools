my $baseDir = '/tmp/kegg/';
my $db_id = "46";
my $split = "\t";



my @db_ids = ("46","56");


# for each db_id

for $db_id (@db_ids){

    my $gene_set_items_filename = $baseDir.'gene_set_items_'.$db_id.'.tsv';
    open my $gene_set_item_file, $gene_set_items_filename;

    my $output_file = ">".$baseDir."db_id_".$db_id."_all_genes.tsv";

    open(OUTFILE, $output_file); #open for write

    %gene_list = ();

    while (<$gene_set_item_file>){

            $row = $_;
            
            @row = split($split,$row);
            
            $gene_set_id = @row[1];
            $gene_id = @row[2];
            
            chomp($gene_set_id);
            chomp($gene_id);
            
            if (!exists($gene_list{$gene_id})){
                
                $gene_list{$gene_id} = ();
            }
            
            push(@{$gene_list{$gene_id}},$gene_set_id);
            
            
            
        
    }


    while (($gene_id,$list) = each %gene_list){
        
        $rest_of_row = join("\t",@{$list});
        
        print OUTFILE "$gene_id\t$rest_of_row\n";
        
    }

    close OUTFILE

} #end of each db_id
