#! /bin/bash

# Database options

# ####################################### local rowland database ####################################### 
# psql_options="-U portaladmin portal_beta "

# #######################################  ncascr dev ####################################### 

# have to run this first at uq in separate window: ssh s2776403@ncascr.griffith.edu.au -L 5433:ncascr-dev.rcs.griffith.edu.au:5432
# ncascr-dev options when at uq
# psql_options="-h 127.0.0.1 -p 5433 -U ascc_dev magma "

# ncascr-dev options when at GU
# psql_options="-h ncascr-dev.rcs.griffith.edu.au -U portaladmin portal_beta "


# ####################################### portal beta ####################################### 
# have to run this first : 
# ssh r.mosbergen@stemformatics.org -L 5433:127.0.0.1:5433
# psql_options="-h 127.0.0.1 -p 5433 -U portaladmin portal_beta "

# ####################################### portal prod ####################################### 
# have to run this first : 
# ssh r.mosbergen@stemformatics.org -L 5433:127.0.0.1:5433
psql_options="-h 127.0.0.1 -p 5433 -U portaladmin portal_prod "




# these will have the highest values for gene_set_id and gene set itmes id
tail -n 1 /tmp/kegg/gene_set_56.tsv
tail -n 1 /tmp/kegg/gene_set_items_56.tsv


# clear out old Kegg gene sets
psql $psql_options -c "delete from stemformatics.gene_set_items where gene_set_id in (select id from stemformatics.gene_sets where gene_set_type = 'Kegg');" 
psql $psql_options -c "delete from stemformatics.gene_sets where gene_set_type = 'Kegg';"


# upload to database
cat /tmp/kegg/gene_set_46.tsv | psql $psql_options -c "COPY stemformatics.gene_sets FROM STDIN"
cat /tmp/kegg/gene_set_items_46.tsv | psql $psql_options -c "COPY stemformatics.gene_set_items FROM STDIN"

cat /tmp/kegg/gene_set_56.tsv | psql $psql_options -c "COPY stemformatics.gene_sets FROM STDIN"
cat /tmp/kegg/gene_set_items_56.tsv | psql $psql_options -c "COPY stemformatics.gene_set_items FROM STDIN"


# now update the sequences

psql $psql_options -c "select setval('stemformatics.gene_sets_id_seq',max(id)) from stemformatics.gene_sets;"

psql $psql_options -c "select setval('stemformatics.gene_set_items_id_seq',max(id)) from stemformatics.gene_set_items;"


# check they are uploaded
psql $psql_options -c "select count(*) from stemformatics.gene_sets where gene_set_type = 'Kegg';"
psql $psql_options -c "select gene_set_id,max(gene_set_name),max(db_id),count(*) from stemformatics.gene_sets left join stemformatics.gene_set_items on stemformatics.gene_set_items.gene_set_id = stemformatics.gene_sets.id where gene_set_type = 'Kegg' and db_id = 46 group by gene_set_id ;" > /tmp/kegg/database_dump_confirm_46.txt
psql $psql_options -c "select gene_set_id,max(gene_set_name),max(db_id),count(*) from stemformatics.gene_sets left join stemformatics.gene_set_items on stemformatics.gene_set_items.gene_set_id = stemformatics.gene_sets.id where gene_set_type = 'Kegg' and db_id = 56 group by gene_set_id ;" > /tmp/kegg/database_dump_confirm_56.txt
head -n 5 /tmp/kegg/database_dump_confirm_46.txt
head -n 5 /tmp/kegg/database_dump_confirm_56.txt


# copy over db_id_*_all_genes.tsv to the right spot



