my $baseDir = '/tmp/';
my $db_id = "46";
my $uid = 0;
my $gene_pathway_type = "Kegg";
my $base_description = "";
my $gene_set_id = 258;
my $gene_set_items_id = 22379;



my $ensembl_to_symbols_filename = $baseDir.'ensembl_to_symbols_'.$db_id.'.txt';
my $gene_set_file = ">".$baseDir."gene_set_".$db_id.".tsv";
my $gene_set_items_file = ">".$baseDir."gene_set_items_".$db_id.".tsv";


my $raw_gene_sets_dir = $baseDir.'kegg/';


open my $ensembl_to_symbols_file, $ensembl_to_symbols_filename;


open(GENESETOUTFILE, $gene_set_file); #open for write
open(GENESETITEMSOUTFILE, $gene_set_items_file); #open for write



%symbol_list = ();
%gene_set_list = ();

@file = <$ensembl_to_symbols_file>;
my $header = splice (@file,0,1);
my $header = splice (@file,0,2);

foreach (@file){

    $row = $_;
    
    @row = split(/\|/,$row);
    
    $symbols = @row[0];
    $names = @row[1];
    $gene_id = @row[2];
    
    
    # $names =~ s/[^a-zA-Z0-9\_\-]*//g; 
    # $symbols =~ s/[^a-zA-Z0-9\_\-]*//g; 
    $gene_id =~ s/[^a-zA-Z0-9]*//g; 
    
    
    @symbols = split(" ",$symbols);
    
    foreach (@symbols){
        $temp_symbol = $_;
        
        # print $temp_symbol."\n";
        $symbol_list {$temp_symbol} =  $gene_id;
    }
    
    @names = split(" ",$names);
    
    foreach (@names){
        $temp_name = $_;
        $symbol_list {$temp_name} =  $gene_id;
        # print $temp_name . "|". $symbol_list{$temp_name}."\n";
        
    }
    
    
    
    
}



opendir(DIR, $raw_gene_sets_dir);
@FILES= readdir(DIR);


foreach $kegg_filename (@FILES) {
    
    $kegg_filename = $raw_gene_sets_dir.$kegg_filename;
    
    # print $kegg_filename."\n";
    
    open my $kegg_file, $kegg_filename;
    
    @file = <$kegg_file>;
    
    my $header = splice (@file,0,1);
    
    $header =~ s/[^a-zA-Z0-9\-\_]*//g; 
    
    # print $header."\n";
    
    
    if ($header ne ""){
        
        $gene_set_id++;
        $gene_set_name = $header;
        $description = $header;

        print GENESETOUTFILE "$gene_set_id\t$gene_set_name\t$db_id\t$description\t$uid\t$gene_pathway_type\n"; 

        %genes_used = ();
        
        foreach (@file){
            
            
            
            $row = $_;
            
            @row = split(" ",$row);
            
            my $symbol = @row[1];
            
            $symbol =~ s/[^a-zA-Z0-9\-\_]*//g; 
            
            if ($symbol ne "") {
                
                $gene_id = $symbol_list { $symbol };
                
                $used_before = $genes_used{$gene_id};
                # print $used_before." ===== ".$gene_id."\n";
                if ($used_before == 0 ){
                    $gene_set_items_id++;
                    print GENESETITEMSOUTFILE "$gene_set_items_id\t$gene_set_id\t$gene_id\n";
                    $genes_used{$gene_id} = 1;
                }
                #print $gene_id . "|". $symbol."\n";
            }
        }
        # print $header."\n";
        
    }
    
    
}


# cat gene_set_56.tsv | psql -h localhost -U portaladmin portal_beta -c "COPY stemformatics.gene_sets FROM STDIN"
# cat gene_set_items_56.tsv | psql -h localhost -U portaladmin portal_beta -c "COPY stemformatics.gene_set_items FROM STDIN"
