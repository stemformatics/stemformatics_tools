#! /bin/bash

# Database options

# ####################################### local rowland database ####################################### 
# psql_options="-U portaladmin portal_beta "

# #######################################  ncascr dev ####################################### 
# have to run this first at uq: ssh s2776403@ncascr.griffith.edu.au -L 5433:ncascr-dev.rcs.griffith.edu.au:5432

# psql options when at uq
#psql_options="-h 127.0.0.1 -p 5433 -U ascc_dev magma "

# options when at GU
# psql_options="-h ncascr-dev.rcs.griffith.edu.au -U portaladmin portal_beta "


# ####################################### portal beta ####################################### 
# have to run this first : 
# ssh r.mosbergen@stemformatics.org -L 5433:127.0.0.1:5433
# psql_options="-h 127.0.0.1 -p 5433 -U portaladmin portal_beta "

# ####################################### portal prod ####################################### 
# have to run this first : 
# ssh r.mosbergen@stemformatics.org -L 5433:127.0.0.1:5433
psql_options="-h 127.0.0.1 -p 5433 -U portaladmin portal_prod "


rm -fR /tmp/kegg


# Download and dump kegg pathways to text files
R --slave < kegg_download.r > /tmp/kegg_download.log


# dump entrez to ensembl file from sql to command line
psql $psql_options -c "copy (select gene_id,entrezgene_id from genome_annotations where db_id = 56) To STDOUT;" > /tmp/kegg/ensembl_to_entrez_56.txt
psql $psql_options -c "copy (select gene_id,entrezgene_id from genome_annotations where db_id = 46) To STDOUT;" > /tmp/kegg/ensembl_to_entrez_46.txt


# dump sequences for gene set id and gene set items id into separate files

psql $psql_options -c "COPY (select nextval('stemformatics.gene_sets_id_seq')) TO STDOUT;" > /tmp/kegg/next_gene_set_id.txt
psql $psql_options -c "COPY (select nextval('stemformatics.gene_set_items_id_seq')) TO STDOUT;" > /tmp/kegg/next_gene_set_item_id.txt


# run perl to create tsv to upload to sql
next_gene_set_id=`cat /tmp/kegg/next_gene_set_id.txt`
next_gene_set_item_id=`cat /tmp/kegg/next_gene_set_item_id.txt`

echo "Next gene set id is" $next_gene_set_id 
echo "Next gene set items id is " $next_gene_set_item_id

perl create_gene_sets_from_R_kegg_download.pl $next_gene_set_id $next_gene_set_item_id


# run perl to create genes to gene set file to be used for gene set annotations
perl create_all_gene_files.pl


# output some quality control error files
ls -alh /tmp/kegg
head -n 1 /tmp/kegg/gene_set_46.tsv
tail -n 1 /tmp/kegg/gene_set_46.tsv

head -n 1 /tmp/kegg/gene_set_items_46.tsv
tail -n 1 /tmp/kegg/gene_set_items_46.tsv

head -n 1 /tmp/kegg/gene_set_56.tsv
tail -n 1 /tmp/kegg/gene_set_56.tsv
head -n 1 /tmp/kegg/gene_set_items_56.tsv
tail -n 1 /tmp/kegg/gene_set_items_56.tsv
echo "----------------46 all genes --------------"
head -n 2 /tmp/kegg/db_id_46_all_genes.tsv 
tail -n 2 /tmp/kegg/db_id_46_all_genes.tsv 

echo "----------------56 all genes --------------"
head -n 2 /tmp/kegg/db_id_56_all_genes.tsv 
tail -n 2 /tmp/kegg/db_id_56_all_genes.tsv 



# now have to upload the files into the database
# and then have to move the db_id_*_all_genes.tsv files to the appropriate directory
