

# expecting the following files
# in /tmp/kegg
# all_genes_46.txt  all_genes_56.txt  ensembl_to_entrez_46.txt  ensembl_to_entrez_56.txt  pathway_names.txt



my $baseDir = '.';

my $uid = 0;
my $gene_pathway_type = "Kegg";
my $base_description = "";

my @db_ids = ("46","56");



# get the following from the input if it is there

my $gene_set_id = 674;
my $gene_set_items_id = 50725;

if ($ARGV[0] ne ""){
    $gene_set_id = int($ARGV[0] || 0);
} 

if ($ARGV[1] ne ""){
    $gene_set_items_id = int($ARGV[1] || 0);
} 


# for each db_id

for $db_id (@db_ids){

    my $all_genes = $baseDir.'all_genes_'.$db_id.'.txt';
    my $confirm_entrez_pathways = ">".$baseDir."confirm_pathways".$db_id.".txt";
    my $error_file = ">".$baseDir."error_file".$db_id.".txt";

    open my $all_genes_file, $all_genes;

    open(CONFIRMOUTFILE, $confirm_entrez_pathways); #open for write
    open(ERRORFILE, $error_file); #open for write

    @all_genes = <$all_genes_file>;

    my $header = splice (@all_genes,0,1);


    %pathway_entrez_genes = ();
    %pathway_genes = ();
    foreach (@all_genes){
        
        $row = $_;
        
        @row = split(" ",$row);
        
        $pathway_id = @row[1];
        $entrez_id = @row[2];
        
        $pathway_id =~ s/"//g;
        $entrez_id =~ s/"//g;
        
        
        
        $gene_id = $entrez_list {$entrez_id};
        
        if ($gene_id eq ""){
        
            print ERRORFILE $entrez_id.":::No ensembl gene found for entrez id ".$entrez_id . " for pathway ". $pathway_id."\n";
        }
           
        #print $pathway_id. " " . $gene_id."\n";
        
        $pathway_genes {$pathway_id} = $pathway_genes {$pathway_id}. " " . $gene_id;
        
        $pathway_entrez_genes {$pathway_id} = $pathway_entrez_genes {$pathway_id}. " " . $entrez_id;
        
        # print $pathway_id . " " . $pathway_genes {$pathway_id}."\n";
        
    }

    $description = "";

    while(($key, $value) = each(%pathway_genes)) { 
        
        
        @row = split(" ",$value);    
        
        $path_id = substr($key,3);
        
        $gene_set_name = $pathway_names {$path_id};
        
        
        $gene_set_id++;
            
        print GENESETOUTFILE "$gene_set_id\t$gene_set_name\t$db_id\t$description\t$uid\t$gene_pathway_type\n"; 
        
        %genes_used = ();
            
        foreach (@row){
            $gene_id = $_;
        
            # skip over genes that are not found
            if ( $gene_id eq "" ){
                next;
            }
            
            $used_before = $genes_used{$gene_id};
            # print $used_before." ===== ".$gene_id."\n";
            if ($used_before == 0 ){
            
                $gene_set_items_id++;
                print GENESETITEMSOUTFILE "$gene_set_items_id\t$gene_set_id\t$gene_id\n";
                $genes_used{$gene_id} = 1;
            }
            
            
        }
        
    }

    print "Final next gene_set_id is " . $gene_set_id . ' : next gene_set_items_id is '. $gene_set_items_id."\n";


    # cat gene_set_56.tsv | psql -h localhost -U portaladmin portal_beta -c "COPY stemformatics.gene_sets FROM STDIN"
    # cat gene_set_items_56.tsv | psql -h localhost -U portaladmin portal_beta -c "COPY stemformatics.gene_set_items FROM STDIN"



    while(($key, $value) = each(%pathway_entrez_genes)) { 
        
        @genes = split(" ",$value);
        
        
        $path_id = substr($key,3);
        $gene_set_name = $pathway_names {$path_id};
        
        print CONFIRMOUTFILE $gene_set_name." - ".$key." - ".scalar(@genes)." ".$value."\n";
        
    }


} # end for each db_id


















