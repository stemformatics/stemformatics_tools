#! /bin/bash

# Database options

# ####################################### local rowland database ####################################### 
# psql_options="-h localhost -U portaladmin portal_beta "

# #######################################  ncascr dev ####################################### 
# have to run this first at uq: ssh s2776403@ncascr.griffith.edu.au -L 5433:ncascr-dev.rcs.griffith.edu.au:5432

# psql options when at uq
#psql_options="-h 127.0.0.1 -p 5433 -U ascc_dev magma "

# options when at GU
# psql_options="-h ncascr-dev.rcs.griffith.edu.au -U portaladmin portal_beta "


# ####################################### portal beta ####################################### 
# have to run this first : 
# ssh r.mosbergen@stemformatics.org -L 5433:127.0.0.1:5433
# psql_options="-h 127.0.0.1 -p 5433 -U portaladmin portal_beta "

# ####################################### portal prod ####################################### 
# have to run this first : 
# ssh r.mosbergen@stemformatics.org -L 5433:127.0.0.1:5433
psql_options="-h 127.0.0.1 -p 5433 -U portaladmin portal_prod "


base_dir="/tmp/bulk_import_manager/"

gene_mapping_raw_file_base_name="gene_mapping.raw"
gene_mapping_raw_file_name=$base_dir$gene_mapping_raw_file_base_name

feature_mapping_raw_file_base_name="feature_mapping.raw"
feature_mapping_raw_file_name=$base_dir$feature_mapping_raw_file_base_name


rm -fR $base_dir

mkdir $base_dir


# Dump gene mappings
psql $psql_options -c "copy (select db_id,gene_id,associated_gene_name,associated_gene_synonym,entrezgene_id,refseq_dna_id,description from genome_annotations) To STDOUT;" > $gene_mapping_raw_file_name

# Dump probe mappings
psql $psql_options -c "copy (select * from stemformatics.feature_mappings) To STDOUT;" > $feature_mapping_raw_file_name


# Quick check that this has worked
ls -alh $base_dir
head $feature_mapping_raw_file_name
head $gene_mapping_raw_file_name


# have to copy them into the appropriate directory eg. production
# gene_mapping_raw_file_base_name=/var/www/pylons-data/prod/gene_mapping.raw
# feature_mapping_raw_file_base_name=/var/www/pylons-data/prod/feature_mapping.raw


# Actually get these files parsed by pylons and into the format that is wanted


exit
