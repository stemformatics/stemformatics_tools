#!/bin/bash
#should run every 15 minutes as portaladmin
# sudo su -c "/data/scripts/cronjob/restore_regular_backup_from_production.sh" portaladmin
REGULAR_BACKUP_DIR=/data/db_scheduled_backups/portal_prod/regular_backup_to_mirror/
DUMP_REGULAR_TABLES="-t stemformatics.jobs -t stemformatics.users -t stemformatics.gene_sets -t stemformatics.gene_set_items -t stemformatics.users_metadata -t stemformatics.annotation_filters -t stemformatics.group_configs -t stemformatics.group_users -t stemformatics.groups -t stemformatics.override_private_datasets -t stemformatics.shared_resources -t public.datasets"
MIRROR_PSQL_USER="-h localhost -p 5432 -U portaladmin "
MIRROR_PSQL="$MIRROR_PSQL_USER portal_beta"
DUMP_MIRROR_BEFORE_FILE=/tmp/before_restore_of_regular_on_portal_beta.dump
DROP_TABLES_SQL="drop table stemformatics.jobs cascade;
drop table stemformatics.users cascade;
drop table stemformatics.users_metadata cascade;
drop table stemformatics.gene_sets cascade;
drop table stemformatics.gene_set_items cascade;
drop table stemformatics.annotation_filters cascade;
drop table stemformatics.group_configs cascade;
drop table stemformatics.group_users cascade;
drop table stemformatics.groups cascade;
drop table stemformatics.override_private_datasets cascade;
drop table stemformatics.shared_resources cascade;
drop table datasets cascade;"
REGULAR_TAR_FILES=portal_prod_regular_job_files_backup_*.tgz
PROD_DIR=/var/www/pylons-data/prod/
cd $REGULAR_BACKUP_DIR 
LAST_DUMP_FILE=`ls *.dump | sort | tail -n1`
BACKUP_RESTORED_FILES_TAR=/tmp/$LAST_DUMP_FILE.tgz
BACKUP_SERVER=130.102.48.83
BACKUP_SERVER=stemformatics.qern.qcif.edu.au
BACKUP_LOCATION=/data/backup_server/restored_regular_backups

echo "stop pylons"
/data/scripts/admin/portal-dev-cmd.sh stop

#remove dump files that are more than 12 hours old
echo "only keeping files that are 12 hours old or less "
find . -name "*.dump" -cmin +720 -exec rm -f {} \;


echo "dump out the database tables first"
pg_dump -Fc $MIRROR_PSQL $DUMP_REGULAR_TABLES > $DUMP_MIRROR_BEFORE_FILE


echo "drop the database tables"
psql $MIRROR_PSQL -c "$DROP_TABLES_SQL"

echo "restore using the dump file $LAST_DUMP_FILE"
pg_restore -v --disable-triggers --clean $MIRROR_PSQL_USER -d portal_beta $LAST_DUMP_FILE &> /tmp/$LAST_DUMP_FILE.log


echo "extract the tar files for jobs"
find . -name "$REGULAR_TAR_FILES" -exec tar zxvf {} -C / \;  -exec mv {} {}.restored \;

cd $PROD_DIR
cp -fR jobs/* ../dev/jobs/
rm -fR $PROD_DIR/jobs

echo "tar gzip files that worked to backup server"
cd $REGULAR_BACKUP_DIR 

message=`ls *.restored`
exit_success=$?
if [ $exit_success -eq 0 ]
then
	tar zcvf $BACKUP_RESTORED_FILES_TAR $LAST_DUMP_FILE *.restored
else
	tar zcvf $BACKUP_RESTORED_FILES_TAR $LAST_DUMP_FILE 
	
fi

exit_success=$?
if [ $exit_success -eq 0 ]
then
	echo "copy over the files that worked to backup server"
	scp $BACKUP_RESTORED_FILES_TAR $BACKUP_SERVER:$BACKUP_LOCATION
	exit_success=$?
	if [ $exit_success -eq 0 ]
	then
		echo "successful copy to backup server. delete files that worked to backup server"
		rm -f $LAST_DUMP_FILE
		rm -f *.restored 
	fi
fi

echo "starting pylons"
/data/scripts/admin/portal-dev-cmd.sh start

echo "validate that this worked?"

