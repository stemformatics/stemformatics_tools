#!/bin/bash
# should be run as portaladmin?
INSTANCE_FILES_DIR=/var/www/pylons-data/dev/
REGULAR_BACKUP_DIR=/data/db_scheduled_backups/portal_prod/regular_backup_to_mirror/
BACKUP_DIR=/data/backup_server/daily_backup_from_mirror
DATE=`date +"%Y%m%d"`
EXCLUDE_FILES="webcache"
BACKUP_FILE=$BACKUP_DIR/"$DATE"_daily_backup_of_mirror_files.tgz
DUMP_FILE=$BACKUP_DIR/"$DATE"_daily_db_dump_of_mirror.dump
REGULAR_BACKUP_FILE=$BACKUP_DIR/"$DATE"_daily_regular_backup_from_prod.tgz

#rm -fR $BACKUP_DIR
#mkdir $BACKUP_DIR

# Get all the files for the mirror
cd $INSTANCE_FILES_DIR
tar zcvf $BACKUP_FILE $INSTANCE_FILES_DIR -h --exclude $EXCLUDE_FILES


# Get the dump of the entire database
echo "dumping out the database"
pg_dump -h localhost -p 5432 -U portaladmin -Fc portal_beta > $DUMP_FILE

echo "tar gzip all the regular backup files to $BACKUP_DIR"
# Get all the regular backups from the production server that have been saved to the mirror
tar zcvf $REGULAR_BACKUP_FILE $REGULAR_BACKUP_DIR/* 

# Now scp this bastard
backup_server=stemformatics.qern.qcif.edu.au
echo "copying portal_data files tgz to backup server"
scp $BACKUP_FILE portaladmin@$backup_server:$BACKUP_FILE
echo "copying database dump file to backup server"
scp $DUMP_FILE portaladmin@$backup_server:$DUMP_FILE
echo "copying regular backup from production to backup server"
scp $REGULAR_BACKUP_FILE portaladmin@$backup_server:$REGULAR_BACKUP_FILE

