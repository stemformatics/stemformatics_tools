#!/bin/sh
if [ `whoami` != "root" ]; then
  echo "Error: Backup script must be run as root (or sudo)!"; echo
  exit 1
fi
DB="$1"
if [ "$DB" != "portal_beta" -a "$DB" != "portal_prod" ]; then
  echo "Error: Bad database target!"; echo
  exit 1
fi
export PGPASSFILE=".pgpass_${DB}"
DATETIME=`date +"%Y%m%d-%H%M"`
BACKUP_DIR="/data/db_scheduled_backups/$DB/regular_backup_to_mirror"
BACKUP_FILE="$BACKUP_DIR/portal_prod_regular_backup_$DATETIME.dump"
BACKUP_JOBS_DIR="/var/www/pylons-data/prod/jobs/"
BACKUP_JOBS_FILE="$BACKUP_DIR/portal_prod_regular_job_files_backup_$DATETIME.tgz"
#TABLES="-t stemformatics.users -t stemformatics.jobs -t stemformatics.gene_sets -t stemformatics.gene_set_items"
TABLES="-t stemformatics.jobs -t stemformatics.users -t stemformatics.gene_sets -t stemformatics.gene_set_items -t stemformatics.users_metadata -t stemformatics.annotation_filters -t stemformatics.group_configs -t stemformatics.group_users -t stemformatics.groups -t stemformatics.override_private_datasets -t stemformatics.shared_resources -t public.datasets"
if [ -d "$BACKUP_DIR" ]; then
  su portaladmin -m -c "pg_dump -Fc -p 5433 $TABLES -f $BACKUP_FILE $DB"
  if [ -f "$BACKUP_FILE" -a ! -z "$BACKUP_FILE" ]; then
    chown portaladmin:portaladmin "$BACKUP_FILE"
    chmod 640 "$BACKUP_FILE"
  fi
else
  echo "Error: Cannot write to backup directory! Aborted."; echo
  exit 1
fi
ls -alh $BACKUP_FILE
mirror=stemformatics.aibn.uq.edu.au
su portaladmin -m -c "scp $BACKUP_FILE portaladmin@$mirror:$BACKUP_FILE"


tar zcvf $BACKUP_JOBS_FILE `find $BACKUP_JOBS_DIR -type f -mmin -10`
su portaladmin -m -c "scp $BACKUP_JOBS_FILE portaladmin@$mirror:$BACKUP_JOBS_FILE"

