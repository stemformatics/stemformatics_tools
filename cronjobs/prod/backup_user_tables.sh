#!/bin/sh
if [ `whoami` != "root" ]; then
  echo "Error: Backup script must be run as root (or sudo)!"; echo
  exit 1
fi
DB="$1"
if [ "$DB" != "portal_beta" -a "$DB" != "portal_prod" ]; then
  echo "Error: Bad database target!"; echo
  exit 1
fi
export PGPASSFILE=".pgpass_${DB}"
DATETIME=`date +"%Y%m%d-%H%M"`
BACKUP_DIR="/data/db_scheduled_backups/$DB/user_data_daily"
BACKUP_FILE="$BACKUP_DIR/user_data_$DATETIME.sql"
TABLES="-t stemformatics.users -t stemformatics.jobs -t stemformatics.gene_sets -t stemformatics.gene_set_items"
if [ -d "$BACKUP_DIR" ]; then
  su portaladmin -m -c "pg_dump -h localhost -p 5433 -a $TABLES -f $BACKUP_FILE $DB"
  if [ -f "$BACKUP_FILE" -a ! -z "$BACKUP_FILE" ]; then
    chown portaladmin:portaladmin "$BACKUP_FILE"
    chmod 640 "$BACKUP_FILE"
  fi
else
  echo "Error: Cannot write to backup directory! Aborted."; echo
  exit 1
fi

