#!/bin/sh
SCRIPTDIR="/data/scripts/admin/local"

if [ `whoami` != "root" ]; then
  echo "Must be root (or sudo)!"; echo
  exit 1
fi

check_restart_postgres () {
  ## Make sure postgres is running before we fire up pylons.
  ## If it isn't, fire it up.
  /etc/init.d/postgresql-local status > /tmp/postgresql-local-status 2>&1
  grep -i "is running" /tmp/postgresql-local-status > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo "DEBUG: Portal Prod watchdog discovered postgresql-local not running, attempting to start now."
    logger -t PORTAL "Portal Prod watchdog discovered postgresql-local not running, attempting to start now."
    /etc/init.d/postgresql-local start
    ## Postgres did not restart, quit
    if [ $? -ne 0 ]; then
      echo "DEBUG: postgresql-local could not be restarted, abandoning attempt to restart Portal Prod."
      logger -t PORTAL "postgresql-local could not be restarted, abandoning attempt to restart Portal Prod."
      exit 1
    fi
  fi
}

check_restart_redis () {
  /etc/init.d/redis-server status > /tmp/redis-server-status 2>&1
  grep -i "is running" /tmp/redis-server-status > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo "DEBUG: Portal Prod watchdog discovered redis-server not running, attempting to start now."
    logger -t PORTAL "Portal Prod watchdog discovered redis-server not running, attempting to start now."
    /etc/init.d/redis-server start
    ## Postgres did not restart, quit
    if [ $? -ne 0 ]; then
      echo "DEBUG: redis-server could not be restarted, abandoning attempt to restart Portal Prod."
      logger -t PORTAL "redis-server could not be restarted, abandoning attempt to restart Portal Prod."
      exit 1
    fi
  fi

  ## Task #705 - Independent check that redis is running according to TCP connectivity,
  ## as sometimes "running" status is falsely reported
  netstat -an | grep -P "0\.0\.0\.0\:6379.*LISTEN" > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo "DEBUG: Portal Prod watchdog discovered redis-server not running on port 6379, attempting to (re)start now."
    logger -t PORTAL "Portal Prod watchdog discovered redis-server not running on port 6379, attempting to (re)start now."
    /etc/init.d/redis-server restart
  fi
}


### MAIN ###

if [ -d $SCRIPTDIR ]; then
  cd $SCRIPTDIR
  su portaladmin -c "./portal-prod-local-cmd.sh status > /tmp/portal-prod-status 2>&1"
  grep -i "not running" /tmp/portal-prod-status > /dev/null 2>&1

  if [ $? -eq 0 ]; then
    echo "DEBUG: Portal Prod watchdog discovered pylons not running, will attempt restart."
    logger -t PORTAL "Portal Prod watchdog discovered pylons not running, will attempt restart."

    check_restart_postgres
    check_restart_redis

    sleep 5
    ## Not running, attempt pylons re-deployment
    su portaladmin -c "./portal-prod-local-cmd.sh start"

  else
    ## Portal is running, but how about redis? It can be restarted at any time and we
    ## should check it in isolation
    check_restart_redis
  fi

fi

