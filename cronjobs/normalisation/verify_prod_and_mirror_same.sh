
#psql -h ascc.qfab.org -p 5433 -U portaladmin -d portal_prod
#psql -U portaladmin -h stemformatics.aibn.uq.edu.au portal_beta
PROD="www1.stemformatics.org"
MIRROR="www2.stemformatics.org"
PROD_DETAILS="-h ascc.qfab.org -U portaladmin -d portal_prod"
MIRROR_DETAILS="-U portaladmin -h www2.stemformatics.org portal_prod"
TMP_DIR=/tmp/verify_prod_and_mirror

TABLES="stemformatics.jobs stemformatics.gene_sets stemformatics.users datasets stemformatics.users_metadata  stemformatics.group_users stemformatics.groups  stemformatics.group_configs stemformatics.override_private_datasets stemformatics.shared_resources dataset_metadata biosamples_metadata stemformatics.gene_set_items stemformatics.feature_mappings stemformatics.transcript_annotations"

PROD_FILE=$TMP_DIR/sql_prod.txt
MIRROR_FILE=$TMP_DIR/sql_mirror.txt

rm -fR $TMP_DIR
mkdir $TMP_DIR

echo " < means in www2 but not www1"
echo " > means in www1 but not www2"

TABLES="stemformatics.jobs"
for TABLE in  $TABLES
do
	echo "diff between prod/mirror for $TABLE"

	SQL="copy (select * from $TABLE) to stdout"
	psql $PROD_DETAILS -c "$SQL" | sort > $PROD_FILE
	psql $MIRROR_DETAILS -c "$SQL" | sort > $MIRROR_FILE
	diff $MIRROR_FILE $PROD_FILE
done

function external_check {
	CMD="$1"
	PROD_FILE=$TMP_DIR/prod_file.txt
	MIRROR_FILE=$TMP_DIR/mirror_file.txt
	ssh $PROD "$CMD" | sort > $PROD_FILE 
	ssh $MIRROR "$CMD" | sort > $MIRROR_FILE
	diff $MIRROR_FILE $PROD_FILE
}

# now check datasets gct/cumulative
CMD="ls /var/www/pylons-data/prod/GCTFiles/dataset*.gct"
external_check "$CMD" 

CMD="ls /var/www/pylons-data/prod/CUMULATIVEFiles/dataset*.txt"
external_check "$CMD" 

CMD="find /var/www/pylons-data/prod/jobs/ -type d "
external_check "$CMD" 


CMD="redis-cli -s /tmp/redis.sock KEYS gct_labels*"
external_check "$CMD" 


CMD="redis-cli -s /tmp/redis.sock KEYS cumulative_labels*"
external_check "$CMD" 

