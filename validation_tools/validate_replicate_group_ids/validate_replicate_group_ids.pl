sub trim($)
{
	my $string = shift;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	return $string;
}



# get the following from the input if it is there
if ($ARGV[0] ne ""){
    $biosamples_filename = $ARGV[0];
} 

if ($ARGV[1] ne ""){
    $gct_output_files_dir = $ARGV[1];
} 

if ($ARGV[2] ne ""){
    $cumulative_output_files_dir = $ARGV[2];
} 
print "$gct_output_files_dir\n";



open my $biosamples_file, $biosamples_filename;



my %replicate_table;
while (<$biosamples_file>){
    
    $row = trim($_);
    
    
    @row = split("\t",$row);
    # print "\n";
    $ds_id = trim(@row[0]);
    $replicate_group_id = trim(@row[1]);
    
    #~ print $ds_id."\n";
    #~ print $replicate_group_id."\n";
    $replicate_table{$ds_id}{$replicate_group_id} = 'b';
    
}

#~ 
#~ while (<$probe_expression_file>){
    #~ 
    #~ $row = trim($_);
    #~ # print $row."\n";
    #~ @row = split(/\|/,$row);
    #~ 
    #~ 
    #~ $ds_id = trim(@row[0]);
    #~ 
    #~ if ( $ds_id =~ /^[0-9]+/) {
        #~ $replicate_group_id = trim(@row[1]);
        #~ # print "$ds_id $replicate_group_id \n";
        #~ $replicate_table{$ds_id}{$replicate_group_id} = $replicate_table{$ds_id}{$replicate_group_id}.'p';  
    #~ } 
    #~ 
#~ }


# go through all the dataset gct files
opendir (DIR, $gct_output_files_dir);
while (my $file = readdir(DIR)) {

    if ($file =~ m/gct/) {
      
        my @ds_id = $file =~ /(\d+)/g;
        $ds_id = @ds_id[0];
        
        
        $full_filename = $gct_output_files_dir.$file;
        #print "$full_filename\n";
        open FILE, $full_filename;
        
        $row = 0;
        while (<FILE>) 
        {
            $row = $row + 1;
            if ($row == 3){
                $row = trim($_);
                @row = split("\t",$row);
                
                # remove first two elements
                splice @row, 0, 2;
                
                foreach (@row) {
                    $replicate_group_id = $_;
                    
                    $replicate_table{$ds_id}{$replicate_group_id} = $replicate_table{$ds_id}{$replicate_group_id}.'g';  
                 } 
                
                
                last;
            }
        }
        close(FILE);
        
    } 
        

}

closedir(DIR);

# go through the list now


# go through all the dataset gct files
opendir (DIR, $cumulative_output_files_dir);
while (my $file = readdir(DIR)) {

    #print $file;

    if ($file =~ m/cumulative/) {
      
        my @ds_id = $file =~ /(\d+)/g;
        $ds_id = @ds_id[0];
        
        
        $full_filename = $cumulative_output_files_dir.$file;
        #print "$full_filename\n";
        open FILE, $full_filename;
        #print $full_filename;
        $row = 0;
        while (<FILE>) 
        {
            $row = $row + 1;
            if ($row == 1){
                $row = trim($_);
                @row = split("\t",$row);
                
                # remove first two elements
                splice @row, 0, 1;
                #print @row;
                foreach (@row) {
                    $replicate_group_id = $_;
                    #print "$replicate_group_id\n";
                    $replicate_table{$ds_id}{$replicate_group_id} = $replicate_table{$ds_id}{$replicate_group_id}.'c';  
                    #print "$replicate_table{$ds_id}{$replicate_group_id}\n";
                 } 
                
                
                last;
            }
        }
        close(FILE);
        
    } 
        

}

closedir(DIR);

$list_of_datasets='';
$count_datasets = 0;
$problem_datasets = 0;
for $ds_id ( keys %replicate_table){

    # print $ds_id;
    $issue = 0;
    $count_datasets = $count_datasets + 1;
    for $y ( keys %{ $replicate_table{$ds_id} } ){
        
        
        $value = $replicate_table{$ds_id}{$y};
        if ($value ne "bgc"){
            print "$ds_id|$y|$value\n";
            $issue = 1;
        }
        
        
    }
    
    if ( $issue == 1){
        $list_of_datasets=$ds_id. ",".$list_of_datasets;
        $problem_datasets = $problem_datasets + 1;
    }
    
}
print "$problem_datasets/$count_datasets\n";
print "$list_of_datasets\n";
