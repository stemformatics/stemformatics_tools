#! /bin/bash

# Database options

GCT_DIR="$1"
DB="$3"
download="$4"
CUMULATIVE_DIR="$2"

if [ "$DB" == "local" ]; then
    psql_options="-U portaladmin portal_beta "
elif [ "$DB" == "aibn" ]; then
    psql_options="-h stemformatics.aibn.uq.edu.au -U portaladmin portal_beta "
elif [ "$DB" == "prod" ]; then
    psql_options="-h ascc.qfab.org -p 5433 -U portaladmin portal_prod "
fi

output_dir="/tmp/replicate_group_id_validation/"
bs_md_output_file="/tmp/replicate_group_id_validation/biosamples_metadata"

if [ $download == "y" ]; then
    echo $psql_options
    echo "download"
    rm -fR $output_dir
    mkdir $output_dir
    # dump sql to command line
    psql $psql_options -c "copy (select ds_id,md_value from biosamples_metadata where md_name = 'Replicate Group ID') To STDOUT;" > /tmp/replicate_group_id_validation/biosamples_metadata
    #psql $psql_options -c "select ds_id,replicate_group_id from stemformatics.probe_expressions_avg_replicates group by replicate_group_id, ds_id ;" > /tmp/replicate_group_id_validation/probe_expression_avg_replicates

fi

perl validate_replicate_group_ids.pl /tmp/replicate_group_id_validation/biosamples_metadata "$GCT_DIR" "$CUMULATIVE_DIR"



