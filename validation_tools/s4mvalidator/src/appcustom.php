<?php

/**
  File: appcustom.php
  Synopsis: Custom application code that extends class 'appbase' goes here.
  Author: O.Korn
  Created: Feb 2008
  Updated: Oct 2011
**/


// Import base web app class
include_once( 'appbase.php' );
// Custom app settings
include_once( 'config_custom.inc.php' );

// optional flag
define('S4M_OPT', 2);
// mandatory flag
define('S4M_MAN', 1);
// empty flag
define('S4M_EMPTY', 0);


/**
	Class:    appcustom 
	Synopsis: Specific methods for the S4M Validator tool
**/

class appcustom extends appbase {

	// annotation file => data (file lines)
	private $_annotationData = null;
	private $_annotationFiles = null;
	// name of annotation file for display purposes in validation output
	private $_fileName = null;
	private $_validationErrors = null;
	private $_validationWarnings = null;
	// s4m format version 1.3 attribs
	private $_metastoreAttributes = null;
	private $_instrumentTypes = null;
	private $_biosamplesAttributes = null;
	// if TRUE, we return different content template in some situations
	private $_is_api = null;


	function __construct () {
		parent::__construct();
		global $_LOG, $_ERROR_LOG, $_ACTIVITY_LOG;
		if ($_LOG) {
			if (strlen($_ACTIVITY_LOG) and strlen($_ERROR_LOG)) {

				$logConfig = array(
						'activity' => array(
								'type' => 'file',
								'location' => $_ACTIVITY_LOG,
								'autotime' => 1, 
								'autonewline' => 1
						),
						'error' => array(
								'type' => 'file',
								'location' => $_ERROR_LOG,
								'autotime' => 1, 
								'autonewline' => 1
						)
				);
				$this->setLog($logConfig);
				// set which log should be the error log
				$this->setErrorLogID('error');
			}
		}

		$this->_annotationData = array();
		$this->_annotationFiles = array();
		$this->_fileName = '';
		$this->_validationErrors = array();
		$this->_validationWarnings = array();

		// s4m format version 1.3 attribs and mandatory, null or optional status
		$this->_metastoreAttributes = array(
			'accession_id'			=> S4M_MAN,
			'array'				=> S4M_EMPTY,
			'array_manufacturer'		=> S4M_EMPTY,
			'array_platform'		=> S4M_EMPTY,
			'array_probe_count'		=> S4M_OPT,
			'array_version'			=> S4M_EMPTY,
			's4m_curation_date'		=> S4M_MAN,
			'dataset_accession_ae'		=> S4M_OPT,
			'dataset_accession_geo'		=> S4M_OPT,
			'dataset_contributor'		=> S4M_MAN,
			'dataset_contributor_affiliation'	=> S4M_MAN,
			'dataset_contributor_email'	=> S4M_OPT,
			'dataset_handle'		=> S4M_MAN,
			'dataset_release_date'		=> S4M_MAN,
			'dataset_title'			=> S4M_MAN,
			'experiment_type'		=> S4M_MAN,
			'max_replicates'		=> S4M_MAN,
			'min_replicates'		=> S4M_MAN,
			'postnorm_expression_median'	=> S4M_OPT,
			'postnorm_expression_threshold' => S4M_OPT,
			'postnorm_probes_detected'	=> S4M_OPT,
			'publication_authors'		=> S4M_OPT,
			'publication_citation'		=> S4M_OPT,
			'publication_contact'		=> S4M_OPT,
			'publication_contact_email'	=> S4M_OPT,
			'publication_description'	=> S4M_MAN,
			'publication_title'		=> S4M_MAN,
			'publication_pubmed_id'		=> S4M_OPT,
			's4m_chip_type'			=> S4M_EMPTY,
			's4m_curator'			=> S4M_MAN,
			's4m_dataset_id'		=> S4M_EMPTY,
			'species_long'			=> S4M_OPT,
			'species_short'			=> S4M_OPT,
			'technology_used'		=> S4M_MAN,
			'top_genes'			=> S4M_OPT,
			'genePatternAnalysisAccess'	=> S4M_OPT,
			'lineGraphOrdering'		=> S4M_OPT,
			'sampleTypeDisplayOrder'	=> S4M_MAN,
			'sampleTypeDisplayGroups'	=> S4M_MAN,
			'limitSortBy'			=> S4M_MAN,
			'cell_surface_antigen_expression'	=> S4M_OPT,
			'cellular_protein_expression'	=> S4M_OPT,
			'nucleic_acid_extract'		=> S4M_MAN,
			'cells_samples_assayed'		=> S4M_MAN
		);

		// NOTE: Enable other types if/when supported
		$this->_instrumentTypes = array(
			'microarray',	//OBI
			'high throughput sequencer',	//EFO
			//'DNA sequencer'	//EFO
			'single molecule sequencing assay'
		);
		// NOTE: Enable other types if/when supported
		$this->_experimentTypes = array(
			//'transcription profiling assay',	//OBI
			'transcription profiling by array',	//EFO
			'transcription profiling by high throughput sequencing',	//EFO
			'cap analysis gene expression assay'
		);

		$this->_biosamplesAttributes = array(
			'Age'				=> S4M_OPT,
			'Cell Line'			=> S4M_OPT,
			'Cell Line Reference'		=> S4M_OPT,
			'Cell Type'			=> S4M_MAN,
			'Derived From'			=> S4M_OPT,
			'Differentiated To'		=> S4M_OPT,
			'Developmental Stage'		=> S4M_MAN,
			'Disease State'			=> S4M_MAN,
			'FACS Profile'			=> S4M_OPT,
			'Gender'			=> S4M_OPT,
			'GEO Sample Accession'		=> S4M_OPT,
			'Hybridization Name'		=> S4M_OPT,
			'iPS Derivation Method'		=> S4M_OPT,
			'Labelling'			=> S4M_MAN,
			'Material Type'			=> S4M_OPT,	//TODO: remove?
			'Organism'			=> S4M_MAN,
			'Organism Part'			=> S4M_OPT,
			'Patient'			=> S4M_OPT,
			'Pregnancy Status'		=> S4M_OPT,
			'Replicate'			=> S4M_MAN,
			'Replicate Group ID'		=> S4M_MAN,
			'Reprogramming Method'		=> S4M_OPT,
			'Sample Description'		=> S4M_MAN,
			'Sample Type'			=> S4M_MAN,
			'Scan Name'			=> S4M_OPT,	//TODO: remove?
			'Sorted For'			=> S4M_OPT,
			'Source Name'			=> S4M_MAN,
			'Tissue'			=> S4M_MAN
		);

		$this->_biosamplesAttributesDisplayOrder = array(
			'Replicate Group ID',
			'GEO Sample Accession',
			'Hybridization Name',
			'Source Name',
			'Scan Name',
			'Sample Type',
			'Labelling',
			'Replicate',
			'Organism',
			'Organism Part',
			'Tissue',
			'Cell Type',
			'Cell Line',
			'Cell Line Reference',
			'Age',
			'Gender',
			'Disease State',
			'Patient',
			'Developmental Stage',
			'Reprogramming Method',
			'Derived From',
			'Differentiated To',
			'Sorted For',
			'Sample Description',
			'iPS Derivation Method',
			'FACS Profile',
			'Material Type',
			'Pregnancy Status'
		);
	}


	/** Application Methods **/

	function logFail ($failMessage) {
		array_push($this->_validationErrors, $failMessage);
	}

	function logWarn ($warnMessage) {
		array_push($this->_validationWarnings, "WARNING: $warnMessage");
	}

	function failCount () {
		return count($this->_validationErrors);
	}

	function warnCount () {
		return count($this->_validationWarnings);
	}

	function getFailString () {
		$failString = '';
		foreach ($this->_validationErrors as $fail) {
			$failString .= "$fail<br />";
		}
		return $failString;
	}

	function getWarnString () {
		$warnString = '';
		foreach ($this->_validationWarnings as $warn) {
			$warnString .= "$warn<br />";
		}
		return $warnString;
	}

	function emptyOrNull ($value) {
		return empty($value) or $value == 'NULL';
	}

	function logFailShowMatches ($message, $matches, $filename = '') {
		$badLines = "<blockquote><pre>===\n" . join("\n", $matches) . "\n===</pre></blockquote>";
		$this->logFail($filename . ': ' . count($matches) . " $message:" . $badLines);
	}

	function get_biosamples_metadata_attribute_names ($specificity = null) {
		if ($specificity) {
			$matchAttributes = array();
			foreach ($this->_biosamplesAttributes as $name => $status) {
				if ($status == $specificity) {
					array_push($matchAttributes, $name);
				}
			}
			return $matchAttributes;
		}
		return array_keys($this->_biosamplesAttributes);
	}

	function validate_metastore_lines_attributes ($lines) {
		// check recognised attribute names only
		$unrecognisedAttribs = array();
		foreach ($lines as $line) {
			$tokens = preg_split('/\=/', $line);
			$var = trim($tokens[0]);
			if (! empty($var)) {
				if (! in_array($var, array_keys($this->_metastoreAttributes))) {
					array_push($unrecognisedAttribs, $var);
				}
			}
		}
		if (count($unrecognisedAttribs)) {
			$this->logFailShowMatches('unrecognised attributes', $unrecognisedAttribs, $this->_fileName);
		}
	}

	function validate_metastore_lines_specificity ($lines) {
		$missingMandatoryAttribs = array();
		$nonEmptyAttribs = array();

		foreach ($lines as $line) {
                        $equalSignIndex = strpos($line, '=');
                        $var = substr($line, 0, $equalSignIndex);
                        $val = substr($line, $equalSignIndex+1);

			if (! empty($var)) {
				if (in_array($var, array_keys($this->_metastoreAttributes))) {
					$attribRequirement = $this->_metastoreAttributes[$var];
					if ($attribRequirement == S4M_MAN) {
						if ($this->emptyOrNull($val)) {
							array_push($missingMandatoryAttribs, $var);
						}
					} else if ($attribRequirement == S4M_EMPTY) {
						if (! $this->emptyOrNull($val)) {
							array_push($nonEmptyAttribs, $var);
						}
					}
				}
			}
		}
		if (count($missingMandatoryAttribs)) {
			$this->logFailShowMatches('missing values for mandatory attributes', $missingMandatoryAttribs, $this->_fileName);
		}
		// OK: Ignore these - this is an issue for s4m.sh validation, not end-user.
		/*
		if (count($nonEmptyAttribs)) {
			$this->logFailShowMatches('attributes set where they ought to be unspecified (blank or NULL)', $nonEmptyAttribs, $this->_fileName);
		}
		*/
	}

	function validateDatasetAccessionGEO ($val) {
		return preg_match('/^GSE\d+$/', $val);
	}

	function validateDateYYYMMDD ($val) {
		return preg_match('/^\d{4}\-\d{2}\-\d{2}$/', $val);
	}

	function validate_top_genes ($val) {
		$genes = split(',', $val);
		$ret = false;
		if (count($genes) == 5) {
			foreach ($genes as $gene) {
				if (! preg_match('/^ENS[GM]{1}/', $gene)) {
					return false;
				}
			}
			$ret = true;
		}
		return $ret;
	}

	function validate_sampleTypeDisplayOrder ($val) {
		$sampleTypes = split(',', $val);
		$ret = false;
		if (count($sampleTypes) >= 1) {
			foreach ($sampleTypes as $st) {
				if (preg_match('/^\s+/', $st) or preg_match('/\s+$/', $st)) {
					return false;
				}
			}
			$ret = true;
		}
		return $ret;
	}

	function validate_sampleTypeDisplayGroups ($val) {
		$regexValid = preg_match('/^\{\".*\d\}$/', $val);
		if (strpos($val, ',') > 0) {
			$tokens = split(',', $val);
			foreach ($tokens as $token) {
				$token = trim($token, '{}');
				if (! preg_match('/^\".+\"\:\d+$/', $token)) {
					return false;
				}
			}
		}
		return $regexValid;
	}

	function validate_limitSortBy ($val) {
		$gotSampleType = preg_match('/Sample Type/', $val);
		if (strpos($val, ',')) {
			if (count(split(',', $val)) > 5) {
				return false;
			}
			if (preg_match('/\,$/', $val)) {
				return false;
			}
		}
		return $gotSampleType;
	}

	function validate_technology_used ($val) {
		return in_array($val, $this->_instrumentTypes);
	}

	function validate_experiment_type ($val) {
		return in_array($val, $this->_experimentTypes);
	}

	function validate_nucleic_acid_extract ($val) {
		return ($val == 'total RNA extract');
	}

	function validate_metastore_lines_specific_format ($lines) {
		$badDates = array();
		$unrecognisedValues = array();
		
		foreach ($lines as $line) {
			$equalSignIndex = strpos($line, '=');
			$var = substr($line, 0, $equalSignIndex);
			$val = substr($line, $equalSignIndex+1);

			if (! $this->emptyOrNull($val)) {

			switch ($var){
				case 'dataset_accession_geo':
					if (! $this->validateDatasetAccessionGEO($val)) {
						$this->logFailShowMatches(
							'bad dataset_accession_geo format. Expecting ID like \'GSExxxx\' where x is a digit',
							array($line),
							$this->_fileName
						);
					}
					break;
				case 'dataset_release_date':
					if (! $this->validateDateYYYMMDD($val)) {
						array_push($badDates, $line);
					}
					break;
				case 'experiment_type':
					if (! $this->validate_experiment_type($val)) {
						$this->logFailShowMatches(
							'bad experiment_type value. Expecting one of: ' . join(', ', $this->_experimentTypes),
							array($line),
							$this->_fileName
						);

					}
					break;
				case 'limitSortBy':
					if (! $this->validate_limitSortBy($val)) {
						$this->logFailShowMatches(
							"bad limitSortBy format. Expecting at least 'Sample Type' or 'Sample Type,OtherType1[,OtherType2..OtherTypeN]' (not tested for N >= 5 but in theory should be fine to proceed)",
							array($line),
							$this->_fileName
						);

					}
					break;
				case 'nucleic_acid_extract':
					if (! $this->validate_nucleic_acid_extract($val)) {
						$this->logFailShowMatches(
							"bad nucleic_acid_extract value. Expecting 'total RNA extract'",
							array($line),
							$this->_fileName
						);
					}
					break;
				case 's4m_curation_date':
					if (! $this->validateDateYYYMMDD($val)) {
						array_push($badDates, $line);
					}
					break;
				case 'sampleTypeDisplayOrder':
					if (! $this->validate_sampleTypeDisplayOrder($val)) {
						$this->logFailShowMatches(
							'bad sampleTypeDisplayOrder format. Expecting comma-separated Sample Type values (no spaces)',
							array($line),
							$this->_fileName
						);
					}
					break;
				case 'sampleTypeDisplayGroups':
					if (! $this->validate_sampleTypeDisplayGroups($val)) {
						$this->logFailShowMatches(
							'bad sampleTypeDisplayGroups format. Expecting: {"sampleOne":0,"sampleTwo":1,"sampleThree":2}',
							array($line),
							$this->_fileName
						);

					}
					break;
				case 'technology_used':
					if (! $this->validate_technology_used($val)) {
						$this->logFailShowMatches(
							'bad technology_used value. Expecting one of: ' . join(', ', $this->_instrumentTypes),
							array($line),
							$this->_fileName
						);
					
					}
					break;
				case 'top_genes':
					if (! $this->validate_top_genes($val)) {
						$this->logFailShowMatches(
							'bad top_genes format. Expecting 5 comma-separated human or mouse Ensembl IDs (no spaces)',
							array($line),
							$this->_fileName
						);
					}
					break;
			}

			} //! empty(val)
		}

		if (count($badDates)) {
			$this->logFailShowMatches('date values not in YYYY-MM-DD format', $badDates, $this->_fileName);
		}
		if (count($unrecognisedValues)) {
			$this->logFailShowMatches('', $unrecognisedValues, $this->_fileName);
		}
	}

	function validate_metastore_sample_consistency ($lines) {
		$sampleTypeDisplayOrder = preg_grep('/sampleTypeDisplayOrder=/', $lines);
		$sampleTypeDisplayGroups = preg_grep('/sampleTypeDisplayGroups=/', $lines);

		$displayOrderVal = split('=', array_shift($sampleTypeDisplayOrder));
		$displayGroupsVal = split('=', array_shift($sampleTypeDisplayGroups));

		$orderSamples = split(',',$displayOrderVal[1]);
		$groupSamples = split(',',$displayGroupsVal[1]);

		if (count($orderSamples) != count($groupSamples)) {
			$this->logFail($this->_fileName. ': Sample count mismatch (' . count($orderSamples) . ' vs ' . count($groupSamples) . ') between sampleTypeDisplayOrder and sampleTypeDisplayGroups.<br />');
		}

		$orderSamplesCleaned = array();
		$groupSamplesCleaned = array();
		foreach ($orderSamples as $sample) {
			$st = trim($sample, ' ');
			array_push($orderSamplesCleaned, $st);
		}
		foreach ($groupSamples as $group) {
			$st = preg_replace('/\:\d+/', '', $group);
			$st = trim($st, '"{}');
			array_push($groupSamplesCleaned, $st);
		}
		sort($groupSamplesCleaned);
		sort($orderSamplesCleaned);
		if ($orderSamplesCleaned != $groupSamplesCleaned) {
			$this->logFail($this->_fileName. ': Sample Type mismatch between sampleTypeDisplayOrder (' . join(',',$orderSamplesCleaned) . ') and sampleTypeDisplayGroups (' . join(',',$groupSamplesCleaned) . ')<br />');

		}
	}

	function validate_metastore_lines_value_consistency ($lines) {
		$this->validate_metastore_sample_consistency($lines);
	}

	function validate_metastore_lines_regex ($lines) {
		// check for blank lines
		$matches = preg_grep('/^\s*$/', $lines);
		array_shift($matches);	//Remove first false-positive match, true count is minus one
		if (count($matches)) {
			$this->logFail($this->_fileName. ': There are ' . count($matches) . ' blank lines.<br />');
		}

		// check each line is a "foo=bar" assignment
		$matches = preg_grep('/^[^\=]+\=([^\=].*|)$/', $lines, PREG_GREP_INVERT);
		array_shift($matches);	//Remove first false-positive match, true count is minus one
		if (count($matches)) {
			$this->logFailShowMatches('invalid lines (not "A=B" format)', $matches, $this->_fileName);
		}

		// check for leading or trailing spaces within lines
		$matches = preg_grep('/^ +\w+/', $lines);
		if (count($matches)) {
			$this->logFailShowMatches('lines with leading spaces', $matches, $this->_fileName);
		}
		$matches = preg_grep('/ $/', $lines);
		if (count($matches)) {
			$this->logFailShowMatches('lines with trailing spaces', $matches, $this->_fileName);
		}

		$matches = preg_grep('/^[A-Za-z\_]+ +\=/', $lines);
		if (count($matches)) {
			$this->logFailShowMatches('attribute names with trailing spaces', $matches, $this->_fileName);
		}
		$matches = preg_grep('/^[A-Za-z\_]+\= +/', $lines);
		if (count($matches)) {
			$this->logFailShowMatches('values with leading spaces', $matches, $this->_fileName);
		}
	}

	function warn_metastore_accession_specification () {
		// If no AE accession provided, warn that AE is preferenced ahead of GEO
		$accession_id = $this->get_metastore_item('accession_id');
		$dataset_accession_ae = $this->get_metastore_item('dataset_accession_ae');
 		$dataset_accession_geo = $this->get_metastore_item('dataset_accession_geo');
		if (! $this->emptyOrNull($accession_id) and $this->emptyOrNull($dataset_accession_ae) and $this->emptyOrNull($dataset_accession_geo)) {
			$this->logWarn($this->_fileName . ": If accession ID '$accession_id' is an <a href=\"http://www.ebi.ac.uk/arrayexpress\" target=\"_ae\">Array Express</a> or <a href=\"http://www.ncbi.nlm.nih.gov/geo/\">GEO</a> accession, please ensure fields 'dataset_accession_ae' and/or 'dataset_accession_geo' are set accordingly.<br />");
		} else if ($this->emptyOrNull($dataset_accession_ae)) {
			$this->logWarn($this->_fileName . ": If dataset has an <a href=\"http://www.ebi.ac.uk/arrayexpress\" target=\"_ae\">Array Express</a> ID, please set field 'dataset_accession_ae' accordingly.<br />");
		}
	}

	function validate_metastore_lines_warnings ($lines) {
		$this->warn_metastore_accession_specification();
	}

	function validate_metastore_lines ($lines) {
		$this->validate_metastore_lines_regex($lines);
		$this->validate_metastore_lines_attributes($lines);
		$this->validate_metastore_lines_specificity($lines);
		$this->validate_metastore_lines_specific_format($lines);
		$this->validate_metastore_lines_value_consistency($lines);
		$this->validate_metastore_lines_warnings($lines);
	}


	function validate_biosamples_metadata_lines_regex ($lines) {
		// check for blank lines
		$matches = preg_grep('/^\s*$/', $lines);
		// remove first false-positive match, true count is minus one. Don't know why.
		array_shift($matches);	
		if (count($matches)) {
			$this->logFail($this->_fileName . ': There are ' . count($matches) . ' blank lines.<br />');
		}

		// check number of tab-separated columns correct for each line
		$misMatches = array();
		$count = 1;
		foreach ($lines as $line) {
			$matched = array();
			// there should be 5 columns per line (4 TABs)
			if (preg_match_all('/\t/', $line, $matched, PREG_SET_ORDER) != 4) {
				array_push($misMatches, $line);
			}
			if ($count == 1) {
				// remove first false-positive match, true count is minus one. Don't know why.
				array_shift($misMatches);
			}
			$count++;
		}
		if (count($misMatches)) {
			$this->logFailShowMatches('invalid lines; expecting 5 tab-separated columns per line', $misMatches, $this->_fileName);
		}

		// check for leading or trailing spaces within lines
		$matches = preg_grep('/^\ +\w+/', $lines);
		if (count($matches)) {
			$this->logFailShowMatches('lines with leading spaces', $matches, $this->_fileName);
		}
		$matches = preg_grep('/\ +$/', $lines);
		if (count($matches)) {
			$this->logFailShowMatches('lines with trailing spaces', $matches, $this->_fileName);
		}
		$matches = preg_grep('/\ +\t/', $lines);
		if (count($matches)) {
			$this->logFailShowMatches('trailing spaces between column values', $matches, $this->_fileName);
		}
		$matches = preg_grep('/\t\ +/', $lines);
		if (count($matches)) {
			$this->logFailShowMatches('leading or trailing spaces in column values', $matches, $this->_fileName);
		}

		// check for non-empty required column values
		$emptyChipIDs = array();
		$emptyAttributes = array();
		foreach ($lines as $line) {
			if (! empty($line)) {
				$tokens = split("\t", $line);
				if (empty($tokens[2])) {
					array_push($emptyChipIDs, $line);
				}
				if (empty($tokens[3])) {
					array_push($emptyAttributes, $line);
				}
			}
		}
		if (count($emptyChipIDs)) {
			$this->logFailShowMatches('lines without chip IDs', $emptyChipIDs, $this->_fileName);
		}
		if (count($emptyAttributes)) {
			$this->logFailShowMatches('lines with missing metadata attribute names', $emptyAttributes, $this->_fileName);
		}
	}

	function get_biosamples_metadata_non_empty_lines ($lines) {
		$ret = preg_grep('/\w+/', $lines);
		if ($this->isDebug()) {
			assert( count($ret) >= 1 );
		}
		return $ret;
	}

	function get_biosamples_metadata_chip_ids ($lines) {
		static $chipIDs = array();

		if (! count($chipIDs)) {
			foreach ($lines as $line) {
				$tokens = split("\t", $line);
				// skip lines with bad column count
				if (count($tokens) == 5) {
					// only process non-empty chip IDs
					if (! in_array(trim($tokens[2]), $chipIDs) and ! empty($tokens[2])) {
						array_push($chipIDs, trim($tokens[2]));
					}
				}
			}
		}
		return $chipIDs;
	}

	function get_biosamples_metadata_attribute_values ($lines, $attributeName, $unique = true) {
		$attribValues = array();

		foreach ($lines as $line) {
			$tokens = split("\t", $line);
			// skip lines with bad column count
			if (count($tokens) == 5) {
				if (trim($tokens[3]) == $attributeName) {
					if ($unique) {
						if (! in_array(trim($tokens[4]), $attribValues)) {
							array_push($attribValues, trim($tokens[4]));
						}
					} else {
						array_push($attribValues, trim($tokens[4]));
					}
				}
			}
		}
		return $attribValues;
	}

	function validate_biosamples_metadata_chipid_rep_group_counts ($lines) {
		$chipIDs = $this->get_biosamples_metadata_chip_ids($lines);
		$repGroups = $this->get_biosamples_metadata_attribute_values($lines, 'Replicate Group ID');

		// check that number of unique chip IDs matches number of unique "Replicate Group ID" values
		if (count($chipIDs) != count($repGroups)) {
			$this->logFail($this->_fileName. ': Chip ID and Replicate Group ID count mismatch (' . count($chipIDs) . ' vs ' . count($repGroups) . ').<br />');
		}
	}

	function validate_biosamples_metadata_attribute_counts ($lines) {
		$chipIDs = $this->get_biosamples_metadata_chip_ids($lines);
		$nonEmptyLines = $this->get_biosamples_metadata_non_empty_lines($lines);
		// check total lines divides evenly by chip ID count
		// (assuming number of chip IDs is correct, this works).
		if (count($nonEmptyLines) % count($chipIDs) != 0) {
			$this->logFail($this->_fileName. ': Inconsistent metadata #rows between samples (at least one sample has more / less attributes than others).<br />');
		}
	}

	function validate_biosamples_metadata_unique_values ($lines) {
		$repGroups = array();
		$duplicateRepGroupLines = array();
		$sampleDesc = array();
		$duplicateSampleDescLines = array();

		foreach ($lines as $line) {
			$tokens = split("\t", $line);
			// don't look at lines with bad column count
			if (count($tokens) == 5) {
				switch (trim($tokens[3])) {
					// check uniqueness of Replicate Group IDs
					case 'Replicate Group ID':
						if (in_array(trim($tokens[4]), $repGroups)) {
							array_push($duplicateRepGroupLines, $line);
						}
						array_push($repGroups, trim($tokens[4]));
						break;
					// check uniqueness of Sample Description values
					case 'Sample Description':
						if (in_array(trim($tokens[4]), $sampleDesc)) {
							array_push($duplicateSampleDescLines, $line);
						}
						array_push($sampleDesc, trim($tokens[4]));
						break;
				}	
			}
		}
		if (count($duplicateRepGroupLines)) {
			$this->logFailShowMatches('duplicate Replicate Group IDs', $duplicateRepGroupLines, $this->_fileName);
		}
                //OK 2012-07-17: Duplicate sample descriptions no longer an issue
                /*
                if (count($duplicateSampleDescLines)) {
                        $this->logFailShowMatches('duplicate Sample Descriptions', $duplicateSampleDescLines, $this->_fileName);
                }
                */
	}

	// As of Stemformatics v3.1, max unique Sample Type values supported is 16
	// due to front-end graphing constraints
	function validate_biosamples_metadata_max_unique_sample_types ($lines) {
		$uniqueSampleTypes = array_unique($this->get_biosamples_metadata_attribute_values($lines, 'Sample Type'));
		if (count($uniqueSampleTypes) > 16) {
			$this->logFailShowMatches('Too many unique Sample Type values (' . count($uniqueSampleTypes) . '), current maximum is 16 (Stemformatics v3.1) due to graphing restrictions', $uniqueSampleTypes, $this->_fileName);
		}
	}

	// TODO: more?
	function validate_biosamples_metadata_value_consistency ($lines) {
		$this->validate_biosamples_metadata_chipid_rep_group_counts($lines);
		$this->validate_biosamples_metadata_attribute_counts($lines);
		$this->validate_biosamples_metadata_unique_values($lines);
		$this->validate_biosamples_metadata_max_unique_sample_types($lines);
	}

	function validate_biosamples_metadata_attribute_specificity ($lines) {
		$chipIDs = $this->get_biosamples_metadata_chip_ids($lines);
		$lines = $this->get_biosamples_metadata_non_empty_lines($lines);
		$missingMandatoryAttribs = array();

		foreach ($chipIDs as $chipID) {
			foreach ($this->get_biosamples_metadata_attribute_names(S4M_MAN) as $attrib) {
				if (! preg_grep("/$chipID\t$attrib\t.+$/", $lines)) {
					array_push($missingMandatoryAttribs, "$chipID\t$attrib");
				}
			}
		}

		if (count($missingMandatoryAttribs)) {
			$this->logFailShowMatches('missing or empty values for mandatory attributes', $missingMandatoryAttribs, $this->_fileName);
		}
	}

	// Check that sample attributes are not duplicated for a given sample
	function validate_biosamples_metadata_no_duplicate_attributes ($lines) {
		$chipIDs = $this->get_biosamples_metadata_chip_ids($lines);
		$lines = $this->get_biosamples_metadata_non_empty_lines($lines);
		$duplicateAttribs = array();

                foreach ($chipIDs as $chipID) {
                        foreach ($this->get_biosamples_metadata_attribute_names() as $attrib) {
                                if (count(preg_grep("/$chipID\t$attrib\t/", $lines)) >= 2) {
                                        array_push($duplicateAttribs, "$chipID\t$attrib");
                                }
                        }
                }

		if (count($duplicateAttribs)) {
			$this->logFailShowMatches('sample/s with duplicate attributes (each attribute should only be specified once per sample)', $duplicateAttribs, $this->_fileName);
		}
	}

	function validate_biosamples_metadata_lines ($lines) {
		$this->validate_biosamples_metadata_lines_regex($lines);
		$this->validate_biosamples_metadata_value_consistency($lines);
		$this->validate_biosamples_metadata_attribute_specificity($lines);
		$this->validate_biosamples_metadata_no_duplicate_attributes($lines);
	}

	function validate_metastore_format ($metastoreFileContents) {
		$lines = $metastoreFileContents;
		sort($lines);
		assert( count($lines) );
		$startFailCount = $this->failCount();

		$this->validate_metastore_lines($lines);

		return ($this->failCount() <= $startFailCount);
	}

	function validate_biosamples_metadata_is_sorted ($lines) {
		$chipIDs = array();
		foreach ($lines as $line) {
			$tokens = split("\t", $line);
			$chipid = $tokens[2];
			if (strlen($chipid)) {
				array_push($chipIDs, $chipid);
			}
		}
		$sortedChipIDs = $chipIDs;
		sort($sortedChipIDs,SORT_STRING);
		if ($chipIDs !== $sortedChipIDs) {
			$this->logFail($this->_fileName . ': does not appear to be sample-sorted (by chip ID)!<br />');
		}
	}

	function validate_biosamples_metadata_format ($biosamplesMetadataFileContents) {
		$lines = $biosamplesMetadataFileContents;
		assert( count($lines) );
		$startFailCount = $this->failCount();

		// Uncomment to enable check for biosamples metadata sort order
		//$this->validate_biosamples_metadata_is_sorted($lines);

		sort($lines);
		$this->validate_biosamples_metadata_lines($lines);

		return ($this->failCount() <= $startFailCount);
	}

	function get_metastore_item ($attributeName) {
		assert( isset($this->_annotationData['metastore']) );
		if ($this->isDebug()) {
			//print "<br />get_metastore_item(): _annotationData['metastore'] = [" . print_r($this->_annotationData['metastore'], true) . ']<br />';
		}
		$matches = preg_grep("/$attributeName/", $this->_annotationData['metastore']);

		//assert( count($matches) == 1 );

		if (count($matches)) {
			$ret = trim(array_shift($matches));
			if (strpos($ret, '=') > 0) {
				return array_pop(split('=', $ret));
			}
		}
		return null;
	}

	function validate_metastore_biosamples_cross_check () {
		$sampleTypeDisplayOrder = $this->get_metastore_item('sampleTypeDisplayOrder');
		$sampleTypeDisplayGroups= $this->get_metastore_item('sampleTypeDisplayGroups');

		if ($this->isDebug()) {
			//print '<br />get_metastore_item(sampleTypeDisplayOrder) = [' . $sampleTypeDisplayOrder . ']<br />';
			//print '<br />get_metastore_item(sampleTypeDisplayGroups) = [' . $sampleTypeDisplayGroups . ']<br />';
		}

		$orderSamples = split(',', $sampleTypeDisplayOrder);
		$orderSamplesNonEmpty = array();
		foreach ($orderSamples as &$os) {
			$os = trim($os, ' ');
			if (! empty($os)) {
				array_push($orderSamplesNonEmpty, $os);
			}
		}
		$groupSamples = split(',', $sampleTypeDisplayGroups);
		$groupSamplesNonEmpty = array();
		foreach ($groupSamples as &$gs) {
			$gs = trim($gs, '{} ');
			$gs = preg_replace('/\:\d+$/', '', $gs);
			if ($gs) {
				$gs = trim($gs, '" ');
			}
			if (! empty($gs)) {
				array_push($groupSamplesNonEmpty, $gs);
			}
		}
		if ($this->isDebug()) {
			//print '<br />orderSamples:<br />'; print_r($orderSamples);
			//print '<br />groupSamples:<br />'; print_r($groupSamples);
			//print '<br />orderSamplesNonEmpty:<br />'; print_r($orderSamplesNonEmpty);
			//print '<br />groupSamplesNonEmpty:<br />'; print_r($groupSamplesNonEmpty);
		}

		$sampleTypes = $this->get_biosamples_metadata_attribute_values($this->_annotationData['biosamples_metadata'], 'Sample Type');

		assert( count($sampleTypes) );

		$badOrderSamples = array();
		foreach ($orderSamples as $sample) {
			//print "<br />DEBUG: Processing order sample '$sample'..<br />";
			if (! empty($sample)) {
				if (! in_array($sample, $sampleTypes)) {
					//print "<br />DEBUG: Sample '$sample' not in Sample Types!<br />";
					array_push($badOrderSamples, $sample);
				} else {
					//print "<br />DEBUG: Sample '$sample' in Sample Types<br />";
				}
			}
		}
		$badGroupSamples = array();
		foreach ($groupSamples as $sample) {
			//print "<br />DEBUG: Processing group sample '$sample'..<br />";
			if (! empty($sample)) {
				if (! in_array($sample, $sampleTypes)) {
					//print "<br />DEBUG: Sample '$sample' not in Sample Types!<br />";
					array_push($badGroupSamples, $sample);
				} else {
					//print "<br />DEBUG: Sample '$sample' in Sample Types<br />";
				}
			}
		}

		//print_r($badOrderSamples);
		//print_r($badGroupSamples);

		if (count($badOrderSamples)) {
			$this->logFailShowMatches('unknown sample type/s in sampleTypeDisplayOrder (no matching Sample Type in biosamples_metadata.txt found)', $badOrderSamples, $this->_fileName);
		}
		if (count($badGroupSamples)) {
			$this->logFailShowMatches('unknown sample type/s in sampleTypeDisplayGroups (no matching Sample Type in biosamples_metadata.txt found)', $badGroupSamples, $this->_fileName);
		}
		$countMisMatch = false;
		$bsSampleTypeCount = count(array_unique($sampleTypes));
		$msSampleTypeCount = count(array_unique(array_merge($orderSamplesNonEmpty, $groupSamplesNonEmpty)));
		if ($bsSampleTypeCount != $msSampleTypeCount) {
			$countMisMatch = true;
			$this->logFail($this->_fileName . ": Mis-match in number of unique Sample Types across sampleTypeDisplayOrder and sampleTypeDisplayGroups ($msSampleTypeCount) vs. annotated in biosamples_metadata.txt 'Sample Type' fields ($bsSampleTypeCount)<br />");
		}
		return (empty($badOrderSamples) and empty($badGroupSamples) and ! $countMisMatch);
	}


	function validate_annotation_file_contents ($fileContentsHash) {
		$validationResults = array();
		foreach ($fileContentsHash as $file => $fileLines) {
			switch ($file) {
				case 'metastore':
					$this->_fileName = 'METASTORE';
					$validationResults['metastore'] = $this->validate_metastore_format($fileLines) ? 'true' : 'false';
					break;
				case 'biosamples_metadata':
					$this->_fileName = 'biosamples_metadata.txt';
					$validationResults['biosamples_metadata'] = $this->validate_biosamples_metadata_format($fileLines) ? 'true' : 'false';
					break;
			}
		}
		// cross-check
		if (isset($fileContentsHash['metastore']) and isset($fileContentsHash['biosamples_metadata'])) {
			/*
			if ($this->isDebug()) {
				print '<br />DEBUG: Performing cross-annotation check between METASTORE and biosamples_metadata.txt<br />';
			}
			*/
			$this->_fileName = 'cross-check';
			$validationResults['combined'] = $this->validate_metastore_biosamples_cross_check() ? 'true' : 'false';
		}
		return ! preg_grep('/false/', array_values($validationResults));
	}

	// Returns array of file lines. Intended to be DOS, UNIX and Mac aware
	// with respect to EOF encodings.
	function get_file_lines_array ($fileContentsRaw) {
		$retLines = null;
		if (strpos("\r", $fileContentsRaw) >= 0) {
			if (strpos("\r\n", $fileContentsRaw) >= 0) {
				$retLines = str_replace("\r\n", "\n", $fileContentsRaw);
			} else {
				$retLines = str_replace("\r", "\n", $fileContentsRaw);
			}
		}
		return $retLines ? split("\n", $retLines) : array();
	}

	function is_biosamples_metadata_table_format ($bsMetaLines) {
		$headerTokens = split("\t", $bsMetaLines[0]);
		return is_array($headerTokens) and count($headerTokens) >= 4 and empty($headerTokens[0]) and in_array('ds_id', $headerTokens) and in_array('chip_type', $headerTokens);
	}

	function is_biosamples_metadata_long_format ($bsMetaLines) {
		$headerTokens = split("\t", $bsMetaLines[0]);
		return is_array($headerTokens) and count($headerTokens) == 5;
	}

	function convert_biosamples_metadata_to_table ($bsMetaLines) {
		$retLines = array();
		$chipIDMetaHash = array();
		$mdNames = array();
		$ds_id = '';
		$chip_type = '';

		foreach ($bsMetaLines as $line) {
			$tokens = split("\t", $line);
			list ($dsid,$chiptype,$chipid,$mdname,$mdvalue) = array($tokens[0], $tokens[1], $tokens[2], $tokens[3], $tokens[4]);
			$mdvalue = rtrim($mdvalue);
			// NOTE: strlen() test for mdvalue otherwise would ignore legitimate values of '0' which empty() returns as true
			if (! empty($dsid) and ! empty($chiptype) and ! empty($chipid) and ! empty($mdname) and strlen($mdvalue)) {
				if (empty($ds_id)) {
					$ds_id = $dsid;
				}
				if (empty($chip_type)) {
					$chip_type = $chiptype;
				}

				if (! isset($chipIDMetaHash[$chipid])) {
					$chipIDMetaHash[$chipid] = array();
				}
				$chipIDMetaHash[$chipid][$mdname] = $mdvalue;
				if (! in_array($mdname, $mdNames)) {
					array_push($mdNames, $mdname);
				}
			}
		}

		// sort metadata attributes according to list in this->_biosamplesAttributesDisplayOrder
		usort($mdNames, array($this,'md_cmp_md_only'));

		// print header
		array_push($retLines, "\t" . join("\t", $mdNames) . "\tds_id\tchip_type");
		// print per-sample metadata rows
		foreach ($chipIDMetaHash as $sample => $metaHash) {
			$mdValuesSorted = array();
			foreach ($mdNames as $md) {
				array_push($mdValuesSorted, $metaHash[$md]);
			}
			array_push($retLines, "$sample\t" . join("\t", $mdValuesSorted) . "\t$ds_id\t$chip_type");
		}
		return $retLines;
	}

	// Callback sort routine for usort() on input metadata attribute name
	function md_cmp_md_only ($mdA, $mdB) {
		$aPos = array_search($mdA, $this->_biosamplesAttributesDisplayOrder);
		$bPos = array_search($mdB, $this->_biosamplesAttributesDisplayOrder);
		if ($aPos === false and $bPos === false) {
			if ($mdA == $mdB) {
				return 0;
			}
			// Do a default alpha-numeric sort on md name
			return ($mdA < $mdB) ? -1 : 1;
		} else if ($aPos === false) {
			return 1;
		} else if ($bPos === false) {
			return -1;
		}
		if ($aPos == $bPos) {
			return 0;
		}
		return ($aPos < $bPos) ? -1 : 1;
	}

	// Callback sort routine for usort() on input "long" biosamples_metadata lines
	function md_cmp_long ($lineA, $lineB) {
		$aTokens = split("\t", $lineA);
		$bTokens = split("\t", $lineB);
		$aChip = $aTokens[2];
		$bChip = $bTokens[2];
		// If chip IDs are not the same, return their comparison result ignoring metadata attribute ordering
		if ($aChip != $bChip) {
			return ($aChip < $bChip) ? -1 : 1;
		}
		$aMDName = $aTokens[3];
		$bMDName = $bTokens[3];
		// If chip IDs are the same, compare the two lines by metadata attribute order
		$aPos = array_search($aMDName, $this->_biosamplesAttributesDisplayOrder);
		$bPos = array_search($bMDName, $this->_biosamplesAttributesDisplayOrder);

		if ($aPos === false and $bPos === false) {
			if ($aMDName == $bMDName) {
				return 0;
			}
			// Do a default alpha-numeric sort on md name
			return ($aMDName < $bMDName) ? -1 : 1;
		} else if ($aPos === false) {
			return 1;
		} else if ($bPos === false) {
			return -1;
		}
		if ($aPos == $bPos) {
			return 0;
		}
		return ($aPos < $bPos) ? -1 : 1;
	}


	function convert_biosamples_metadata_to_long ($bsMetaLines) {
		$retLines = array();
		$dsid = '';
		$chip_type = '';

		$header = rtrim($bsMetaLines[0]);
		$headerTokens = split("\t", $header);
		if (is_array($headerTokens) and count($headerTokens)) {
			$ds_id_pos = array_search('ds_id', $headerTokens);
			$chip_type_pos = array_search('chip_type', $headerTokens);
			assert( $ds_id_pos >= 4 );
			assert( $chip_type_pos >= 4 );
			array_shift($bsMetaLines);
			/// DEBUG
			//echo 'DEBUG: convert_biosamples_metdata_to_long(): Got row count = [' . count($bsMetaLines) . '], ds_id_pos = [' . $ds_id_pos . '], chip_type_pos = [' . $chip_type_pos . ']<br />';
			/// END DEBUG
			foreach ($bsMetaLines as $row) {
				$rowTokens = split("\t", $row);
				if (is_array($rowTokens) and count($rowTokens) >= 4) {
					$chip_id = $rowTokens[0];
					$ds_id = rtrim($rowTokens[$ds_id_pos]);
					$chip_type = rtrim($rowTokens[$chip_type_pos]);
					for ($md_item = 1; $md_item < min($ds_id_pos, $chip_type_pos); $md_item++) {
						$md_name = $headerTokens[$md_item];
						$md_value = $rowTokens[$md_item];
						//echo "DEBUG: Calling array_push(retLines, '$ds_id\t$chip_type\t$chip_id\t$md_name\t$md_value')<br />";
						array_push($retLines, "$ds_id\t$chip_type\t$chip_id\t$md_name\t$md_value");
					}
				} else {
					/// DEBUG
					//echo "DEBUG: Skipping row = [$row]<br />";
					/// END DEBUG
				}
			}
		} else {
			$this->append_token('ERROR: Cannot convert biosamples metadata; bad file header');
		}
		// Sort biosamples metadata attributes by specific sort order as per $this->_biosamplesAttributesDisplayOrder
		usort($retLines, array($this,'md_cmp_long')); 
		return $retLines;
	}

	function convert_biosamples_metadata ($bsMetaLines, $fromType, $toType) {
		if ($fromType == 'table' and $toType == 'long') {
			//echo "DEBUG: Calling convert_biosamples_metadata_to_long(bsMetaLines)<br />";
			//$ret = $this->convert_biosamples_metadata_to_long($bsMetaLines);
			//echo "Got: " . print_r($ret, true);
			//return $ret;
			return $this->convert_biosamples_metadata_to_long($bsMetaLines);
		} elseif ($fromType == 'long' and $toType == 'table') {
			//echo "DEBUG: Calling convert_biosamples_metadata_to_table(bsMetaLines)<br />";
			return $this->convert_biosamples_metadata_to_table($bsMetaLines);
		}
		/// DEBUG
		echo "Internal Error: Bad from-spec '$fromType' or to-spec '$toType' in convert_biosamples_metadata()<br />";
		/// END DEBUG
		return array();
	}

	function is_biosamples_metadata_format ($bsMetaLines, $formatType = 'long') {
		switch ($formatType) {
			case 'long':
				return $this->is_biosamples_metadata_long_format($bsMetaLines);
			case 'table':
				return $this->is_biosamples_metadata_table_format($bsMetaLines);
			default:
				return false;
		}
	}


	// TODO
	function email_validated_files () {
	}

	function validate_annotation_files () {
		if (isset($this->_annotationFiles['metastore'])) {
			$msRaw = file_get_contents($this->_annotationFiles['metastore']);
			if ($msRaw === false) {
				$this->logError("Internal Error: Could not read contents of METASTORE!");
				return false;
			}
			$this->_annotationData['metastore'] = $this->get_file_lines_array($msRaw);
		}
		if (isset($this->_annotationFiles['biosamples_metadata'])) {
			$bsRaw = file_get_contents($this->_annotationFiles['biosamples_metadata']);
			if ($bsRaw === false) {
				$this->logError("Internal Error: Could not read contents of biosamples_metadata.txt!");
				return false;
			}
			$this->_annotationData['biosamples_metadata'] = $this->get_file_lines_array($bsRaw);
		}

		if (isset($this->_annotationData['metastore'])) {
			assert( (is_array($this->_annotationData['metastore']) and count($this->_annotationData['metastore'])) );
			//print "<br />DEBUG: Uploaded METASTORE:<br /><pre>===\n" . join("\n", $this->_annotationData['metastore']) . "===</pre>";
			if (! $this->_is_api) {
				$upload_debug = $this->get_xhtml_from_file('templates/debug_toggle_output.xhtml.php');
				$upload_debug = $this->replace_tokens($upload_debug, array('DEBUG_ELEMENT_NAME' => 'Uploaded_METASTORE', 'DEBUG_DATA' => '<pre>'.join("\n", $this->_annotationData['metastore']).'</pre>'));
				$this->append_token('MESSAGE', $upload_debug);
			}
		}
		if (isset($this->_annotationData['biosamples_metadata'])) {
			assert( is_array($this->_annotationData['biosamples_metadata']) and count($this->_annotationData['biosamples_metadata']) );
			if (! $this->_is_api) {
				$upload_debug = $this->get_xhtml_from_file('templates/debug_toggle_output.xhtml.php');
				$upload_debug = $this->replace_tokens($upload_debug, array('DEBUG_ELEMENT_NAME' => 'Uploaded_biosamples_metadata', 'DEBUG_DATA' => '<pre>'.join("\n", $this->_annotationData['biosamples_metadata']).'</pre>'));
				$this->append_token('MESSAGE', $upload_debug);
			}
		}

		if ($this->is_biosamples_metadata_format($this->_annotationData['biosamples_metadata'], 'table')) {
			//echo 'DEBUG: Got uploaded biosamples_metadata "table" format<br />';
			$bsMetaLongFormat = $this->convert_biosamples_metadata($this->_annotationData['biosamples_metadata'], 'table', 'long');
			$upload_debug = $this->get_xhtml_from_file('templates/debug_toggle_output.xhtml.php');
			$bsMD5 = preg_replace('/^.*biosamples_metadata/', '', $this->_annotationFiles['biosamples_metadata']);
			$downloadLink = '<a href="index.php?download='.$bsMD5.'&type=BS&format=long"><small>Download biosamples metadata</small></a>';
			$debug_data = "<br />$downloadLink<pre>" . join("\n", $bsMetaLongFormat) . "</pre>$downloadLink<br />";
			$upload_debug = $this->replace_tokens($upload_debug, array('DEBUG_ELEMENT_NAME' => 'Converted_biosamples_metadata_standard_format', 'DEBUG_DATA' => $debug_data));
			if (! $this->_is_api) {
				$this->append_token('MESSAGE', $upload_debug);
			}
			$this->_annotationData['biosamples_metadata_table'] = $this->_annotationData['biosamples_metadata'];
			$this->_annotationData['biosamples_metadata'] = $bsMetaLongFormat;
		}

		$validationResult = $this->validate_annotation_file_contents($this->_annotationData);
		if ($validationResult == true) {
			$this->email_validated_files();
		}
		if (isset($this->_annotationData['biosamples_metadata']) and ! isset($this->_annotationData['biosamples_metadata_table'])) {
			$bsMetaTableFormat = $this->convert_biosamples_metadata($this->_annotationData['biosamples_metadata'], 'long', 'table');
			$upload_debug = $this->get_xhtml_from_file('templates/debug_toggle_output.xhtml.php');
			$bsMD5 = preg_replace('/^.*biosamples_metadata/', '', $this->_annotationFiles['biosamples_metadata']);
			$downloadLink = '<a href="index.php?download='.$bsMD5.'&type=BS"><small>Download this table</small></a>';
			$debug_data = "<br />$downloadLink<pre>" . join("\n", $bsMetaTableFormat) . "</pre>$downloadLink<br />";
			$upload_debug = $this->replace_tokens($upload_debug, array('DEBUG_ELEMENT_NAME' => 'Converted_biosamples_metadata_table', 'DEBUG_DATA' => $debug_data));
			if (! $this->_is_api) {
				$this->append_token('MESSAGE', $upload_debug);
			}
		}
		return $validationResult;
	}


	function get_biosamples_metadata_JSON () {
		global $_S4M_API_URI;
		$bsmeta_json = file_get_contents($_S4M_API_URI . '/biosamples_metadata_unique');
		return $bsmeta_json;	
	}


	function get_dataset_metadata_JSON () {
		global $_S4M_API_URI;
		$dsmeta_json = file_get_contents($_S4M_API_URI . '/dataset_metadata_unique');
		return $dsmeta_json;	
	}

	function download_metastore_file ($md5) {
		if (! empty($md5) and preg_match('/^[A-Za-z0-9]+$/', $md5)) {
			$msFileContents = file_get_contents('/tmp/METASTORE'.$md5);
			if ($msFileContents) {
				header('Content-Type: application/octet-stream');
				header('Content-Disposition: attachment; filename=METASTORE_'.$md5);
				echo $msFileContents;
				exit();
				//return;
			}
		}
                $this->set_token('MESSAGE', $this->error_span('Error: Failed to find or load the file!'));
                return $this->render_template_html('content_main');

	}

	function download_biosamples_metadata_file ($md5, $format = null) {
		$format = empty($format) ? 'table' : $format;
		if (! empty($md5) and preg_match('/^[A-Za-z0-9]+$/', $md5)) {
			$bsFileContents = file_get_contents('/tmp/biosamples_metadata'.$md5);
			if ($bsFileContents) {
				$bsFileTableLines = array();
				$to = $format;
				$from = 'long';
				if ($format == 'long') {
					$to = 'long';
					$from = 'table';
				}
				$bsFileTableLines = $this->convert_biosamples_metadata(explode("\n", $bsFileContents), $from, $to);
				if (count($bsFileTableLines)) {
					header('Content-Type: application/octet-stream');
					header('Content-Disposition: attachment; filename=biosamples_metadata_'.$md5.'.txt');
					echo join("\n", $bsFileTableLines);
					exit();
				} else {
                			$this->append_token('MESSAGE', $this->error_span('Error: Failed to convert file for download!'));
				}
			} else {
                		$this->set_token('MESSAGE', $this->error_span('Error: Failed to retrieve data for the requested file!'));

			}
		} else {
                	$this->set_token('MESSAGE', $this->error_span('Error: Cannot return file, the file ID is invalid or unknown!'));
		}

                return $this->render_template_html('content_main');
	}

	function download_file ($md5, $type, $format = null) {
		if ($type == 'BS') {
			return $this->download_biosamples_metadata_file($md5, $format);
		} else if ($type == 'MS') {
			return $this->download_metastore_file($md5);
		}
		$this->set_token('MESSAGE', $this->error_span('Error: Download file type invalid!'));
		return $this->render_template_html('content_main');
	}


        // OVERRIDE parent::handle_request()
        // Main controller. Must implement this.
        public function handle_request () {

		$TEMPLATE = 'content_main';

                // Process POST
                if ($this->isPOST()) {

                        // Based on POST'ed attributes, do app specific work here.
                        // (Set 'MESSAGE' token at least)

			// API flag check
			if (isset($_GET['action'])) {
				if ($_GET['action'] == 'apivalidation') {
					$this->_is_api = true;
					$TEMPLATE = 'validation_message_only';
				}
			}

			//$annotationFiles = array();
			$this->_annotationFiles = array();

			if (! empty($_FILES['metastore']['name'])) {
				if ($this->isDebug()) {
					assert( isset($_FILES['metastore']['tmp_name']) );
				}
				$mspath = '/tmp/METASTORE'.md5(gettimeofday(true));
				$ret = move_uploaded_file($_FILES['metastore']['tmp_name'], $mspath);
				if ($ret === false) {
					return $this->error_span('Internal Error: Failed to move uploaded METASTORE file!');
				}
				$this->_annotationFiles['metastore'] = $mspath;
			}

			if (! empty($_FILES['biosamples_metadata']['name'])) {
				if ($this->isDebug()) {
					assert( isset($_FILES['biosamples_metadata']['tmp_name']) );
				}
				$bspath = '/tmp/biosamples_metadata'.md5(gettimeofday(true));
				$ret = move_uploaded_file($_FILES['biosamples_metadata']['tmp_name'], $bspath);
				if ($ret === false) {
					return $this->error_span('Internal Error: Failed to move uploaded biosamples_metadata.txt file!');
				}
				$this->_annotationFiles['biosamples_metadata'] = $bspath;
			}

			if (count(array_keys($this->_annotationFiles))) {
				//if (! $this->validate_annotation_files($annotationFiles)) {
				if (! $this->validate_annotation_files()) {
					$errors = $this->getError() . '<br />' . $this->getFailString();
					$this->set_token('MESSAGE', $this->error_span($errors) . $this->get_token('MESSAGE'));
				} else {
					$this->set_token('MESSAGE', $this->success_span('Validation success!') . $this->get_token('MESSAGE'));
				}
				// Prepend warning messages, if any
				if ($this->warnCount()) {
					/// DEBUG
					//echo 'DEBUG: Got ' . $this->warnCount() . ' warnings.<br />';
					/// END DEBUG
					$this->set_token('MESSAGE', $this->message_span($this->getWarnString()) . $this->get_token('MESSAGE'));
				}

			} else {
				$this->set_token('MESSAGE', $this->error_span('Sorry, at least one annotation file must be supplied!') . $this->get_token('MESSAGE'));
			}


		// Process GET
		} elseif ($this->isGET()) {
			if (isset($_GET['action'])) {
				if ($_GET['action'] == 'biosamples_metadata') {
					$this->set_token('MESSAGE', '');
					$this->set_token('BSMETA_JSON', $this->get_biosamples_metadata_JSON());
					return $this->render_template_html('bsmeta');

				} else if ($_GET['action'] == 'dataset_metadata') {
					$this->set_token('MESSAGE', '');
					$this->set_token('DSMETA_JSON', $this->get_dataset_metadata_JSON());
					return $this->render_template_html('dsmeta');

				}
			} else if (isset($_GET['download'])) {
				$md5target = $_GET['download'];
				if (isset($_GET['type'])) {
					if ($_GET['type'] == 'BS') {
						$format = isset($_GET['format']) ? (in_array($_GET['format'],array('long','table')) ? $_GET['format'] : 'table') : 'table';
						return $this->download_file($md5target, 'BS', $format);
					} else if ($_GET['type'] == 'MS') {
						return $this->download_file($md5target, 'MS');
					}
				}
			}

		} else {
			$message = $this->get_xhtml_from_file('templates/content_instructions.xhtml.php');
        	        $this->set_token('MESSAGE', $message);
		}

		return $this->render_template_html($TEMPLATE);
	}


} //class appcustom 

?>
