<?php

/**
  File: appbase.php
  Synopsis: Base code for custom PHP LAMP applications.
  Author: O.Korn
  Created: Feb 2008
  Updated: May 2012
**/


// Import site settings
require_once( 'config.inc.php' );
// Import logger
require_once( 'logger.inc.php' );
// Import database connection settings
//require_once( 'database.inc.php' );      


/**
   Class:    appbase
   Synopsis: Base class for simple OO PHP5 + MySQL web app.
             Intended to be extended by custom application class.
   Features:
             - Local or LDAP authentication
             - User privileges
             - User sessions (using cookies)
             - MySQL connectivity via PEAR::MDB2
             - Load XHTML templates from file
             - Dynamic content via token replacement 
**/

abstract class appbase
{
        // the currently logged-in user
        var $_user;
        // last known error message
        var $_error;
        // boolean "to debug or not to debug"
        var $_debug;
        // log facility
        var $_logger;
        // name of default (non-error) log
        var $_defaultlog;
        // name of default error log
        var $_errorlog;
        // template token replacement map
        var $_templateTokens;


        // Constructor
	function __construct() {

		global $_MAINTENANCE;
		if ($_MAINTENANCE == true) {
			$this->_error = 'Site currently under maintenance, please try again later.';
			return;
		}
                $this->_user = '';
                $this->_templateTokens = array();

                global $_SITENAME, $_BASEURL;
                $this->_templateTokens['SITENAME'] = $_SITENAME;
                $this->_templateTokens['BASEURL'] = $_BASEURL;

		// Set application time zone
		global $_TIME_ZONE;
		if (isset($_TIME_ZONE) and strlen($_TIME_ZONE)) {
			date_default_timezone_set($_TIME_ZONE);
		}
	}

	// Destructor
	function __destruct () {
	}


	/** Public Methods **/

	function isDebug () {
		global $_DEBUGMODE;
		if (isset($_DEBUGMODE) and $_DEBUGMODE === true) {
			return true;
		}
		return false;
	}


	// --------------------------------------------------------------------------------------------
	// Return the latest error message
	// --------------------------------------------------------------------------------------------
	function getError () {
		return $this->_error;
	}


        // --------------------------------------------------------------------------------------------
        // Clean a string of elements that could inject XSS attacks etc.
        // Adapted from: http://css-tricks.com/snippets/php/sanitize-database-inputs/
        // --------------------------------------------------------------------------------------------
        function cleanString ($userString) {
                $search_regex = array(
                        // remove JS
                        '@<script[^>]*?>.*?</script>@si',
                        // remove HTML
                        '@<[\/\!]*?[^<>]*?>@si',
                        // remove style tags
                        '@<style[^>]*?>.*?</style>@siU',
                        // remove multi-line commends
                        '@<![\s\S]*?--[ \t\n\r]*>@'
                );
                return preg_replace($search_regex, '', $userString);
        }

        // --------------------------------------------------------------------------------------------
        // Sanitise a user input string (clean and escape).
        // Adapted from: http://css-tricks.com/snippets/php/sanitize-database-inputs/
        // --------------------------------------------------------------------------------------------
        function sanitise ($userString) {
                $ret = null;
                if (is_array($userString)) {
                        foreach ($userString as $var => $val) {
                                $ret[$var] = $this->sanitise($val);
                        }
                } else {
                        if (get_magic_quotes_gpc()) {
                                $userString = stripslashes($userString);
                        }
                        $userString = $this->cleanString($userString);
                        $ret = $this->escape($userString);
                }
                return $ret;
        }


        // --------------------------------------------------------------------------------------------
        // Escape a string to make it database-friendly.
        // Tries to use charset aware MySQL escape (requires active DB connection).
        // Fallback is to use older, non-charset aware mysql_escape_string().
        //
        // !! A better solution is needed !!
        // 
        // NOTE: "mysql_escape_string()" is DEPRECATED as of PHP 5.3.0
        // --------------------------------------------------------------------------------------------
        function escape ($string) {
                if (function_exists('mysql_real_escape_string')) {
                        // Maybe there is no active DB connection..
                        if (@mysql_real_escape_string($string) !== false) {
                                return @mysql_real_escape_string($string);
                        }
                }
                // fallback
                return @mysql_escape_string($string);
        }


	// --------------------------------------------------------------------------------------------
	// Set logger facility 
	// --------------------------------------------------------------------------------------------
	function setLog ($logConfig) {
		assert( isset($logConfig) and is_array($logConfig) );

		$this->_logger = new logger($logConfig);
		if ($this->_logger->isError()) {
			if ($this->_debug) {
				$this->_error = 'Failed to initialise logger! (' . $this->_logger->getError() . ')';
			} else {
				$this->_error = 'Internal Error (B009)';
			}
			return false;
		}
		/// set default log facility
		$this->_defaultlog = $this->_logger->getDefaultLogID();
		$this->_errorlog = $this->_defaultlog;
		return true;
	}

	// --------------------------------------------------------------------------------------------
	// Set error log by name.
	// Precondition: Have set log config using setLog() method.
	// --------------------------------------------------------------------------------------------
	function setErrorLogID ($logName) {
		if ($this->_logger and strlen($logName)) {
			$this->_errorlog = $logName;
			return true;
		}
		return false;
	}
   	

	// --------------------------------------------------------------------------------------------
	// Log a message using logger facility.
	// --------------------------------------------------------------------------------------------
	function logMessage ($message) {
		$user = $this->_user;
		if (empty($user)) {
			global $_SERVER;
			$user = $_SERVER['REMOTE_ADDR'];
		}
		if ($this->_logger) {
			return $this->_logger->log($this->_defaultlog, '['.$user.'] '.$message);
		}
		return false;
	}


	// --------------------------------------------------------------------------------------------
	// Log an error message using logger facility.
	// Debug message only used if application debug level is set.
	// --------------------------------------------------------------------------------------------
	function logError ($message, $errorCode = '', $debugMessage = '') {
		$user = $this->_user;
		if (empty($user)) {
			global $_SERVER;
			$user = $_SERVER['REMOTE_ADDR'];
		}
		$this->_error .= ' ' . $message;
		if (! empty($errorCode)) { 
			$this->_error .= ' ('. $errorCode . ') ';
		}
		if ($this->_logger) {
			if ($this->_debug) {
				$this->_error .= ' : ' . $debugMessage;
			}
			return $this->_logger->log($this->_errorlog, '['.$user.'] '.$this->_error);
		}
		return false;
	}
   

	// --------------------------------------------------------------------------------------------
	// Load presentational markup from text file
	// --------------------------------------------------------------------------------------------
	function get_xhtml_from_file ($filename) {
		$page_xhtml = '';

		if (file_exists($filename)) {
			// Get the contents
			$page_xhtml = file_get_contents($filename);
		} else {
			$page_xhtml = '<p>Sorry, the content appears to be missing!</p>';
		}
		return $page_xhtml;
	}


	// --------------------------------------------------------------------------------------------
	// Fetch the current user name
	// --------------------------------------------------------------------------------------------
	function current_user () {
		return $this->_user;
	}


	// --------------------------------------------------------------------------------------------
	// Perform token replacement on given string, where a key/val array provides the search
	// and replace strings respectively.
	// --------------------------------------------------------------------------------------------
	function replace_tokens ($text, $tokenHash) {

		$newtext = $text;
		foreach ($tokenHash as $token => $value) {
			$newtext = str_replace("%$token%", $value, $newtext);
		}
		return $newtext;
	}


        // --------------------------------------------------------------------------------------------
        // Get a value for a given template token
        // --------------------------------------------------------------------------------------------
        function get_token ($token) {
                if (isset($this->_templateTokens[$token])) {
                        return $this->_templateTokens[$token];
                }
                return null;
        }

        // --------------------------------------------------------------------------------------------
        // Set a value for a given template token
        // --------------------------------------------------------------------------------------------
        function set_token ($token, $value) {
                $this->_templateTokens[$token] = $value;
        }

        // --------------------------------------------------------------------------------------------
        // Set a value for a given template token
        // --------------------------------------------------------------------------------------------
        function append_token ($token, $stringToAppend) {
                if (isset($this->_templateTokens[$token]) and strlen($this->_templateTokens[$token])) {
                        $this->_templateTokens[$token] .= $stringToAppend;
                } else {
                        $this->set_token($token, $stringToAppend);
                }
        }


        // --------------------------------------------------------------------------------------------
        // Loads and returns XHTML template as string
        // --------------------------------------------------------------------------------------------
        function load_template ($templateName) {
                if (strlen($templateName)) {
                        if (file_exists("templates/$templateName")) {
                                return $this->get_xhtml_from_file("templates/$templateName");
                        } elseif (file_exists("templates/$templateName.tpl.php")) {
                                return $this->get_xhtml_from_file("templates/$templateName.tpl.php");
                        }
                }
                $this->_error = "Internal: Template $templateName not found!";
                return false;
        }

        // --------------------------------------------------------------------------------------------
        // Loads AND renders (replaces tokens in) a template
        // --------------------------------------------------------------------------------------------
        function render_template ($templateName) {
                $tplOut = $this->load_template($templateName);
                if ($tplOut) {
                        return $this->replace_tokens($tplOut, $this->_templateTokens);
                }
                return 'Template not loaded.';
        }

	// --------------------------------------------------------------------------------------------
	// Loads AND renders (replaces tokens in) a HTML template
	// Emits HTML content-type header.
	// --------------------------------------------------------------------------------------------
        function render_template_html ($templateName) {
		header('Content-Type: text/html; charset=utf-8');
		return $this->render_template($templateName);
        }


	// --------------------------------------------------------------------------------------------
	// Return an XHTML span with "error" styling containing the given message
	// --------------------------------------------------------------------------------------------
	function error_span ($message) {
		return "<div class=\"error\"><strong>$message</strong></div>";
	}


	// --------------------------------------------------------------------------------------------
	// Return an XHTML span with "success" styling containing the given message
	// --------------------------------------------------------------------------------------------
	function success_span ($message) {
		return "<div class=\"success\"><strong>$message</strong></div>";
	}


	// --------------------------------------------------------------------------------------------
	// Return an XHTML span with no styling containing the given message
	// --------------------------------------------------------------------------------------------
	function message_span ($message) {
		return "<div class=\"normal\">$message</div>";
	}


        // --------------------------------------------------------------------------------------------
        // Determine if a GET request has been made
        // --------------------------------------------------------------------------------------------
        function isGET () {
                if (isset($_GET) and ! empty($_GET)) {
                        foreach ($_GET as $key => $val) {
                                if (strlen($val)) {
                                        return true;
                                }
                        }
                }
                return false;
        }

        // --------------------------------------------------------------------------------------------
        // Determine if a POST request has been made
        // --------------------------------------------------------------------------------------------
        function isPOST () {
                if (isset($_POST) and ! empty($_POST)) {
                        foreach ($_POST as $key => $val) {
                                if (strlen($val)) {
                                        return true;
                                }
                        }
                }
                return false;
        }

        // --------------------------------------------------------------------------------------------
        // Entry point into GET/POST processing and application control.
        // VIRTUAL: Override in child class.
        // --------------------------------------------------------------------------------------------
        function handle_request () {
                $message = $this->error_span('Default request handler invoked (override in application class).');
                $this->set_token('MESSAGE', $message);
                return $this->render_template('content_main');
        }


} /// class appbase 
   
?>
