<?php

   /**
      File:     config.inc.php
      Synopsis: Configuration file for web app.
      Author:   O.Korn
      Created:
      Updated:  May 2012
   **/


   // Name of site
   $_SITENAME = 'Stemformatics Annotation Validator';

   // Version string
   $_VERSION = 'v0.68-130107 (for s4m dataset format v1.5+)';

   // Base URI of site
   $_BASEURL = 'http://stemformatics.aibn.uq.edu.au/validator/';

   // Base file system path
   $_BASEPATH = '/var/www/html/validator/';

   // Toggle maintenance mode (lock out)
   $_MAINTENANCE = false;

   // If enabled, gives extended error messages and output
   $_DEBUGMODE = true;

   // Do logging?
   $_LOG = false;

   // Application timezone
   $_TIME_ZONE = 'Australia/Brisbane';

   // Server-side user session expiry (number of seconds)
   //$_SESSION_EXPIRE = 60 * 45;

   // Login cookie name
   //$_COOKIE_NAME = '';

   // How long to keep login cookie on client machine.
   // Zero seconds = expire at end of browser session.
   //$_COOKIE_EXPIRE = 60 * 60 * 24;

   // Auth mode. Currently supported options are:
   //    "LDAP"	LDAP only
   //    "Local"       Local DB auth only
   //    "LDAP_Local"  LDAP first, then local DB fall through
   //
   //$_AUTH_MODE = 'Local';


   // LDAP server host name or IP address (including protocol specifier)
   //$_LDAP_SERVER = '';


   // LDAP server port
   //$_LDAP_PORT = '';


   // Map of LDAP options and their values to use in connection
   //$_LDAP_OPT = array(
   //      LDAP_OPT_PROTOCOL_VERSION => 3,
   //      LDAP_OPT_DEREF => 3
   //);


   // The base DN attributes for an LDAP user.
   // This is concatenated with the user ID attribute (see below)
   // to form the bind DN for LDAP authentication.
   //$_LDAP_BINDDN_BASE = '';


   // The unique user ID attribute.
   // This forms the first part of LDAP bind DN, e.g. if unique ID
   // attribute is 'uid', then the string "uid=<login_name>" is prepended
   // onto the _LDAP_BINDDN_BASE string (see above)
   //$_LDAP_USER_ATTRIB = '';

?>
