<h1><a href="index.php">Stemformatics Annotation Validator</a></h1>

<div id="message">%MESSAGE%</div>

<div id="startPage">
   <form name="importform" id="importform" action="index.php" enctype="multipart/form-data" method="POST">
	    <div class="Step"></div>
		<fieldset class="section">
		  <!-- <input type="hidden" name="MAX_FILE_SIZE" value="1048576" /> -->
		  <label for="metastore">METASTORE file</label>
		  <input name="metastore" id="metastore" type="file" /><br />
		  <label for="biosamples_metadata">biosamples_metadata.txt file</label>
		  <input name="biosamples_metadata" id="biosamples_metadata" type="file" /><br />
		  <label for="annotation-submit"></label>
		  <input type="submit" name="annotation-submit" id="annotation-submit" value="Validate Annotation Files" />
	  </fieldset>
	  <br />     
  </form>
</div>
