<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-au">
<head>
	<meta http-equiv="pragma" content="no-cache" /> 
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta http-equiv="content-language" content="en-au" />
	<meta http-equiv="imagetoolbar" content="no" />
	<meta name="mssmarttagspreventparsing" content="true" />
	<meta name="Description" content="Stemformatics Validator"/>

        <title><?php echo $_SITENAME; ?></title>

        <base href="<?php echo $_BASEURL; ?>" />

        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="css/app.css" media="screen" title="content-style" />   
        
        <!-- JavaScript -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js" type="text/javascript" charset="utf-8"></script>
        <script src="https://raw.github.com/DataTables/DataTables/RELEASE_1_8_2/media/js/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/json2.js" type="text/javascript" charset="utf-8"></script>
        <script src="js/app.js" type="text/javascript" charset="utf-8"></script>      
</head>
<body >
        <div id="page">
                <div id="banner">
                        <div class="br"></div>
                </div><!-- end of banner -->

                <div id="content">
                        <noscript>
                                <div class="warning major">
                                        <p>Your browser either has Javascript turned off or is not Javascript-capable. This is a required standard for the service you are attempting to access. Please enable Javascript or upgrade your browser.</p>
                                </div>
                        </noscript>
                        <?php echo $_CONTENT; ?>
                </div><!-- end of content -->
                <div id="footer">
                        <small><?php echo $_VERSION; ?></small>
                </div>
                        <!-- end of footer -->
        </div><!-- end of page -->
</body>

