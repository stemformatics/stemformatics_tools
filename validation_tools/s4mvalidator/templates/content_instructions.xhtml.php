<div style="margin-left: 2em; font-size: 0.9em;">
<p>Upload annotation files to begin validation.</p>
<p>Annotation files can be validated singly or together. NOTE: Cross-validation requires both files.</p>
<h4>Annotation Support:</h4>
<ul>
  <li>List all unique <a href="index.php?action=biosamples_metadata" target="_bm">Biosamples Metadata</a> and <a href="index.php?action=dataset_metadata" target="_dm">Dataset Metadata</a> terms currently in use in Stemformatics (Beta)</li>
  <li>Jill's annotation <a href="https://docs.google.com/document/d/1CuZq189fWr_Kk0iou4lbXeVGOx8SdKn6wAzssfFurEs/view" target="_jillhowto">HOWTO doc</a> <em>(Google Doc, ask for access if you don't have it already).</em></li>
  <li>Download annotation files <a href="http://stemformatics.aibn.uq.edu.au/agile_org/datasets">templates</a> for your dataset</li>
  <li>Full-blown <a href="https://docs.google.com/document/d/1RT5tWbM5g-rP-3aLN5UPYALsYIe9EgGJyTkYks9GP1o/view" target="_s4mhowto">&quot;Stemformatics Normalization &amp; Annotation&quot;</a> internal doc <em>(Google Doc, ask for access if you don't have it already).</em></li>
</ul>
</div>

