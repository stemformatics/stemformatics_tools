                  <h2>s4m Biosamples Metadata List</h2>
                  <p>All unique biosamples metadata name and value pairs are shown here. These results are generated in real-time from Stemformatics Beta database.</p>
                  <div id="bstable_div"></div>
                  <noscript>
                  <div class="warning major">
                    <p>Your browser either has Javascript turned off or is not Javascript-capable. This is a required standard for the service you are attempting to access. Please enable Javascript or upgrade your browser.</p>
                  </div>
                  </noscript>
                  <script type="text/javascript">
                     $(document).ready(function() {
                        var bsmeta = %BSMETA_JSON%;
                        $('#bstable_div').html( '<table cellpadding="1em" cellspacing="0" border="0" class="display" id="bstable"><thead><th style="text-align:left">Name</th><th style="text-align:left">Value</th></thead><tbody id="bstablebody"></tbody></table>' );
                        for (b in bsmeta) {
                           var tuple = bsmeta[b];
                           $("#bstablebody").append('<tr><td>'+tuple['md_name']+'</td><td>'+tuple['md_value']+"</td></tr>\n");
                        }
                        $('#bstable').dataTable({
                           "bPaginate": false,
                           "oLanguage": { "sSearch": "Filter" }
                        });
                     });
                  </script>

