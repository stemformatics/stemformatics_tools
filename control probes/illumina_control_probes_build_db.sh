#!/bin/sh
FILE="$1"
DBOUTPUTFILE="$FILE.sql"
PROBESFOUNDFILE="$FILE.found"

psql_options="$2"

HUMAN_CHECK=`echo $FILE | grep -ic HUMAN`

if [ "$HUMAN_CHECK" -gt "0" ] 
then
    SPECIES="Human"
    db_id="56"

else
    SPECIES="Mouse"
    db_id="46"
fi

echo "----------------------------------------------------------------------------"
cat ../dataset_scripts/s4m/etc/assay_platforms.txt | grep Illumina | grep -i $SPECIES
echo $FILE

echo "Enter in the chip type number"
read chip_type


rm -fR $DBOUTPUTFILE

QUERY="Select db_id,chip_type,from_type,from_id,to_type,to_id from stemformatics.feature_mappings where db_id= $db_id and chip_type=$chip_type and to_type='Probe' and to_id in ("

while read line
do
QUERY="$QUERY'$line',"
done < $FILE

QUERY=`echo $QUERY | sed 's/.$//'`");"

echo $QUERY > $DBOUTPUTFILE

psql $psql_options < $DBOUTPUTFILE | grep Probe > $PROBESFOUNDFILE


if [ -s $PROBESFOUNDFILE ]
then

    DELETEQUERY="Delete from stemformatics.feature_mappings where db_id= $db_id and chip_type=$chip_type and to_type='Probe' and to_id in ("
    while read line
    do
    probe=`echo $line | awk ' { print $11}'`
    DELETEQUERY="$DELETEQUERY'$probe',"
    done < $PROBESFOUNDFILE

    DELETEQUERY=`echo $DELETEQUERY | sed 's/.$//'`");"

    echo $DELETEQUERY  > $DBOUTPUTFILE

    echo $DBOUTPUTFILE
    psql $psql_options < $DBOUTPUTFILE 
else
    echo "$PROBESFOUNDFILE is empty, no control probes found"
fi





