#!/bin/sh
BASEDIR="/home/s2776403/Downloads/Illumina"
EXTRACTDIR="/tmp/extracted"
OUTPUTDIR="/tmp/output_control_probes"

# Database options

# ####################################### local rowland database ####################################### 
# psql_options="-U portaladmin portal_beta "


# #######################################  ncascr dev ####################################### 
# have to run this first at uq : ssh s2776403@ncascr.griffith.edu.au -L 5433:ncascr-dev.rcs.griffith.edu.au:5432

# psql options when at uq
psql_options="-h 127.0.0.1 -p 5433 -U ascc_dev magma "

# options when at GU
# psql_options="-h ncascr-dev.rcs.griffith.edu.au -U portaladmin portal_beta "


# ####################################### portal beta ####################################### 
# have to run this first : 
# ssh r.mosbergen@stemformatics.org -L 5433:127.0.0.1:5433
# psql_options="-h 127.0.0.1 -p 5433 -U portaladmin portal_beta "

clear

rm -fR $EXTRACTDIR
mkdir $EXTRACTDIR
find $BASEDIR -name "*.zip" -exec unzip {} -d $EXTRACTDIR \;
# ls -alh $EXTRACTDIR


find $EXTRACTDIR -name "*.txt" -exec ./illumina_control_probes.sh {} \;


rm -fR $OUTPUTDIR
mkdir $OUTPUTDIR

# This is the list
clear


find $EXTRACTDIR -name "*.txt.control" -exec ./illumina_control_probes_build_db.sh {} "$psql_options" \;


find $EXTRACTDIR -name "*.sql" -exec cp {} $OUTPUTDIR \;
find $EXTRACTDIR -name "*.found" -exec cp {} $OUTPUTDIR \;


# find $OUTPUTDIR -name "*.found" -exec cat {}  \;
