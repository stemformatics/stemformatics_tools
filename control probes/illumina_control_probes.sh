#!/bin/sh
FILE="$1"

OUTPUT="$FILE.control"

# http://nixtip.wordpress.com/2010/10/12/print-lines-between-two-patterns-the-awk-way/
# Last sed removes the first file
# awk '/\[Columns\]/{flag=0}flag;/\[Controls\]/{flag=1}' "$FILE" | awk '{ print $1}' | sed '1d' > "$OUTPUT"

awk '/\[Columns\]/{flag=0}flag;/\[Controls\]/{flag=1}' "$FILE" | awk '{ print $1}' > "$OUTPUT"
sed -i '1d' "$OUTPUT"


# Quality control
NUMBER_CONTROLS=`grep "Number of Controls" $FILE | awk '{ print $4}'`
OUTPUT_LINE_COUNT=`cat $OUTPUT | wc -l`

if [ "$NUMBER_CONTROLS" = "$OUTPUT_LINE_COUNT" ] 
then
    echo "Output file $OUTPUT is valid"
else
    echo "Error in output file $OUTPUT. Number of Controls $NUMBER_CONTROLS not equal to output file line count $OUTPUT_LINE_COUNT"

fi
