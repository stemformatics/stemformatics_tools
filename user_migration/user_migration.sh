#! /bin/bash

# Database options

# ####################################### local rowland database ####################################### 
old_psql_options="-h 127.0.0.1 -U portaladmin portal_beta "
new_psql_options="-h stemformatics.aibn.uq.edu.au -U portaladmin portal_beta "
stemformatics_data_folder=/home/rowlandm/workspace_data/Stemformatics

# from old portal beta to portal prod
#~ old_psql_options="-h 127.0.0.1 -p 5433 -U portaladmin portal_beta "
#~ new_psql_options="-h 127.0.0.1 -p 5433 -U portaladmin portal_prod "
#~ stemformatics_data_folder=/var/www/pylons-data/dev/jobs


############## NOTES: ###########################
# If you don't have the datasets that a job is linked to in the new database, you won't see that job
# You also have to copy the files over manually.


uid=$1
base_dir=/tmp/$uid

rm -fR $base_dir
mkdir $base_dir
chmod 777 $base_dir

psql $old_psql_options -c "COPY (select * from stemformatics.users where uid = $uid) to '$base_dir/db_users.txt';" 
psql $old_psql_options -c "COPY (select * from stemformatics.jobs where uid = $uid) to '$base_dir/db_jobs.txt';" 
psql $old_psql_options -c "COPY (select * from stemformatics.gene_sets where uid = $uid) to '$base_dir/db_gene_sets.txt';" 

gene_set_ids=""
for gene_set_id in `cat $base_dir/db_gene_sets.txt | awk '{print $1}'`
do
    gene_set_ids=$gene_set_id,$gene_set_ids
done
gene_set_ids=${gene_set_ids%?}

#echo $gene_set_ids
sql_query="COPY (select * from stemformatics.gene_set_items where gene_set_id in ($gene_set_ids)) to '$base_dir/db_gene_set_items.txt';"
#echo "$sql_query"

psql $old_psql_options -c  "$sql_query"

mkdir $base_dir/StemformaticsQueue
mkdir $base_dir/GPQueue

# now retrieve the GPQUEUE from jobs
# and the stemformatics job id
for job_id in `cat $base_dir/db_jobs.txt | awk '{print $1}'`
do
    cp -fR $stemformatics_data_folder/StemformaticsQueue/$job_id $base_dir/StemformaticsQueue
done

find $base_dir/StemformaticsQueue

for gp_job_id in `cat $base_dir/db_jobs.txt | awk '{print $12}'`
do
    if [ $gp_job_id != "\N" ] 
        then
        cp -fR $stemformatics_data_folder/GPQueue/$gp_job_id $base_dir/GPQueue
    fi
done

# tar zcvf /tmp/user_migration_$uid.tgz $base_dir

# want to check if we can safely use the same job id, gene set id, gene set item ids, uid etc.
# check that no record exists and that the setval is higher than the current item like uid


# ############################################ Now get the user ids #############################################
max_user_id=`psql $new_psql_options -c  "COPY (select max(uid) from stemformatics.users) to STDOUT;"`
new_user_id=`echo $max_user_id+1 | bc`

# echo $new_user_id
old_user_id=`cat $base_dir/db_users.txt | awk '{print $1}'`
#echo $old_user_id

cat $base_dir/db_users.txt | sed s/"$old_user_id"/"$new_user_id"/ > $base_dir/db_users_new.txt
chmod 777 $base_dir/db_users_new.txt


# upload to database
# cat /tmp/kegg/gene_set_46.tsv | psql $psql_options -c "COPY stemformatics.gene_sets FROM STDIN"

# cat $base_dir/new_db_users.txt

# echo $new_psql_options
# psql $new_psql_options -c  "COPY (select uid,username from stemformatics.users order by uid desc) to '/tmp/test.txt';" 


# ############################################ Now get the gene sets #############################################
max_gene_set_id=`psql $new_psql_options -c  "COPY (select max(id) from stemformatics.gene_sets) to STDOUT;"`
max_gene_set_items_id=`psql $new_psql_options -c  "COPY (select max(id) from stemformatics.gene_set_items) to STDOUT;"`
max_jobs_id=`psql $new_psql_options -c  "COPY (select max(job_id) from stemformatics.jobs) to STDOUT;"`

# echo $max_gene_set_id
#echo $max_gene_set_items_id
#echo $max_jobs_id


# use perl to get this done now for changing gene sets/geneset items and jobs
perl user_migration_extra.pl $base_dir $max_gene_set_id $max_gene_set_items_id $max_jobs_id $new_user_id $old_user_id

head -n 3 $base_dir/*.txt

find $base_dir/StemformaticsQueue -type d


# ########################### upload the data ###########################
#~ cat $base_dir/db_users_new.txt | psql $new_psql_options -c  "COPY stemformatics.users FROM STDIN;" 
#~ cat $base_dir/db_gene_sets_new.txt | psql $new_psql_options -c  "COPY stemformatics.gene_sets FROM STDIN;" 
#~ cat $base_dir/db_gene_set_items_new.txt | psql $new_psql_options -c  "COPY stemformatics.gene_set_items FROM STDIN;" 
#~ 
#~ vi $base_dir/db_jobs_new.txt
#~ cat $base_dir/db_jobs_new.txt | psql $new_psql_options -c  "COPY stemformatics.jobs FROM STDIN;" 

# then need to copy over the files manually....


psql $psql_options -c "select setval('stemformatics.gene_sets_id_seq',max(id)) from stemformatics.gene_sets;"
psql $psql_options -c "select setval('stemformatics.gene_set_items_id_seq',max(id)) from stemformatics.gene_set_items;"
psql $psql_options -c "select setval('stemformatics.jobs_job_id_seq',max(job_id)) from stemformatics.jobs;"
