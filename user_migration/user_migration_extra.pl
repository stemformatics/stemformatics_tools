
$base_dir=$ARGV[0];
$max_gene_set_id = $ARGV[1];
$max_gene_set_items_id = $ARGV[2];

$max_jobs_id = $ARGV[3];
$new_user_id = $ARGV[4];
$old_user_id = $ARGV[5];


print "$ARGV[0]\n";

open my $gene_set_file, $base_dir.'/db_gene_sets.txt';
open(NEWGENESET, ">".$base_dir."/db_gene_sets_new.txt"); 


open my $gene_set_items_file, $base_dir.'/db_gene_set_items.txt';
open(NEWGENESETITEMS, ">".$base_dir."/db_gene_set_items_new.txt"); 


open my $jobs_file, $base_dir.'/db_jobs.txt';
open(NEWJOBS, ">".$base_dir."/db_jobs_new.txt"); 



@gene_sets = <$gene_set_file>;

$new_gene_set_id = $max_gene_set_id;

%map_old_new_gene_set_ids = ();
foreach (@gene_sets){
    
    $row = $_;
    
    @row = split("\t",$row);
    
    $old_gene_set_id = @row[0];
    
    $new_gene_set_id = $new_gene_set_id + 1;
    
    $row[0] = $new_gene_set_id;
    $row[4] = $new_user_id;
    $new_row = join("\t",@row);
    
    $map_old_new_gene_set_ids {$old_gene_set_id} = $new_gene_set_id;
    
    print NEWGENESET "$new_row";
    
}
close NEWGENESET;
# Now do the same for gene set items



@gene_set_items = <$gene_set_items_file>;
$new_gene_set_items_id = $max_gene_set_items_id;
foreach (@gene_set_items){
    
    $row = $_;
    
    @row = split("\t",$row);
    
    $old_gene_set_items_id = @row[0];
    $old_gene_set_id = @row[1];
    $gene_id = @row[2];
    
    chomp($gene_id);
    
    $new_gene_set_items_id = $new_gene_set_items_id + 1;
    
    $new_gene_set_id = $map_old_new_gene_set_ids{$old_gene_set_id};
    
    $row = "$new_gene_set_items_id\t$new_gene_set_id\t$gene_id\n";
    
    print NEWGENESETITEMS "$row";
    
}
close NEWGENESETITEMS;


%map_old_new_job_ids = ();
@jobs = <$jobs_file>;
$new_jobs_id = $max_jobs_id;
foreach (@jobs){
    
    $row = $_;
    
    print "$row\n";
    
    @row = split("\t",$row);
    
    $old_job_id = @row[0];
    $old_gene_set_id = @row[4];
    $old_uid = @row[5];
    
    $new_jobs_id = $new_jobs_id + 1;
    
    if ($old_gene_set_id != 0 ){
        $new_gene_set_id = $map_old_new_gene_set_ids{$old_gene_set_id};
    } else {
        $new_gene_set_id = 0;
    }
    
    $row[0] = $new_jobs_id;
    $row[4] = $new_gene_set_id;
    $row[5] = $new_user_id;
       
    
    $new_row = join("\t",@row);
    
    # $row = "$new_gene_set_items_id\t$new_gene_set_id\t$gene_id\n";
    
    print NEWJOBS "$new_row";
    
    $map_old_new_job_ids{$old_job_id} = $new_jobs_id;
    
    system("mv $base_dir/StemformaticsQueue/$old_job_id $base_dir/StemformaticsQueue/$new_jobs_id")
    
}
close NEWJOBS;
