#!/bin/sh

emails=$1
dataset_ids=$2
instance=$3
echo $instance

usage () {
    echo
    echo "Usage: s4m_admin.sh <uids_comma_separated> <ds_ids_comma_separated> <instance eg. QFAB_prod.conf or AIBN_beta.conf> "
    echo 
}


if [ "$instance" = "QFAB_prod.conf" ]; then
    psql_options="-h ascc.qfab.org -p 5433 -U portaladmin portal_prod "
fi 
if [ "$instance" = "AIBN_beta.conf" ]; then
    psql_options="-h stemformatics.aibn.uq.edu.au -U portaladmin portal_beta "
fi 

if [ -z "$psql_options" ] ; then
     echo "No instance option found"
     usage
     exit
fi
echo $psql_options
email_arr=$(echo $emails | tr "," "\n")
ds_arr=$(echo $dataset_ids | tr "," "\n")

for user_email in $email_arr
do
    uid=`psql $psql_options -c "copy (select uid from stemformatics.users where username = '$user_email') TO STDOUT;"` 
    echo "---------------------------------------------------------------"
    echo "$user_email has a uid of  $uid"
    if [ -z $uid ] 
    then
       echo "$user_email needs to be created"
       sql_query="insert into stemformatics.users(username,status) values('$user_email',2);"
       output=`psql $psql_options -c "$sql_query"`
       echo $sql_query
       echo $output
       uid=`psql $psql_options -c "copy (select uid from stemformatics.users where username = '$user_email') TO STDOUT;"` 
       if [ -z $uid ] 
       then 
           echo "Error trying to create $user_email"
       else 
           echo "New uid for $user_email is $uid"
       fi
    fi

    for ds_id in $ds_arr
    do 
        check_available=`psql $psql_options -c "copy (select ds_id from stemformatics.override_private_datasets where ds_id = $ds_id and uid = $uid) TO STDOUT;"` 
        if [ -z $check_available ] ; then 
            sql_query="insert into stemformatics.override_private_datasets values($ds_id,$uid,'view');" 
            output=`psql $psql_options -c "$sql_query"`
            echo $sql_query 
            echo $output
        else 
            echo "Dataset id $ds_id already has been overriden for $user_email"
        fi
    done

done

#echo $emails
#echo $dataset_ids

