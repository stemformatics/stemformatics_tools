$mir_name = $ARGV[0];
$ds_id = $ARGV[1];
$gct_base_dir = $ARGV[2];
$mir_clean_file = 'cleanup_hsa.txt';
$tmp_dir = '/tmp/';
$mir_raw_file = 'hsa.gff3';



$result =`cat  $mir_raw_file | awk '{print \$1 "\t" \$4 "\t" \$5 "\t" \$7 "\t" \$9}' > $mir_clean_file`;

@lines = `grep $mir_name $mir_clean_file`;

%mir_list = ();
for $line (@lines){
    chomp($line);
    $split = "\t";
    @row = split($split,$line);
    $chr = @row[0];
    $start = @row[1];
    $end = @row[2];
    $str = @row[3];
    @others = split(';',@row[4]);
    $temp_name = @others[2];
    @name_split = split('=',$temp_name); 
    $name = @name_split[1];

    $mir_list{$name} = ();
    $mir_list{$name}{'chr'}=$chr;
    $mir_list{$name}{'start'}=$start;
    $mir_list{$name}{'end'}=$end;
    $mir_list{$name}{'str'}=$str;
}


my $gct_file_name = $gct_base_dir.'dataset'.$ds_id.'.gct';
$cmd="awk -F '\t' '{ if( NR > 3 ) { print \$1} }' $gct_file_name";
#print "$cmd \n";


@lines = `$cmd`;
for $probe_id (@lines){
    #print $probe_id;    
    chomp($probe_id); 

    $temp = $probe_id;
    $temp =~ s/[:.,]+/#/g;
    #print "$temp\n";
    
    @remove_5pr_and_3pr = split("_",$temp);
    if (scalar(@remove_5pr_and_3pr) == 2){
        $temp = @remove_5pr_and_3pr[1];         
    }
    #print "$temp\n"; 
    @items = split('#',$temp);
    $temp_chr = @items[0];

    $start = @items[1];
    $end = @items[2];
    $strand = @items[3];

    while (($mir_name,$list) = each %mir_list){
        #print "$mir_name\n";
        #print "$mir_list{$mir_name}{'chr'}\n";
        
        $mir_chr = $mir_list{$mir_name}{'chr'};
        $mir_start = $mir_list{$mir_name}{'start'};
        $mir_end = $mir_list{$mir_name}{'end'};
        $mir_strand = $mir_list{$mir_name}{'str'};

        if (($mir_chr eq $chr)&& ($mir_strand eq $strand)){
            if (($mir_start > $end) || ($mir_end < $start)){
    
            } else {
                print "$mir_name\t$probe_id\n";
            }
        }
    }

} 

exit;

my $baseDir = '/tmp/kegg/';
my $db_id = "46";
my $split = "\t";



my @db_ids = ("46","56");


# for each db_id

for $db_id (@db_ids){

    my $gene_set_items_filename = $baseDir.'gene_set_items_'.$db_id.'.tsv';
    open my $gene_set_item_file, $gene_set_items_filename;

    my $output_file = ">".$baseDir."db_id_".$db_id."_all_genes.tsv";

    open(OUTFILE, $output_file); #open for write

    %gene_list = ();

    while (<$gene_set_item_file>){

            $row = $_;
            
            @row = split($split,$row);
            
            $gene_set_id = @row[1];
            $gene_id = @row[2];
            
            chomp($gene_set_id);
            chomp($gene_id);
            
            if (!exists($gene_list{$gene_id})){
                
                $gene_list{$gene_id} = ();
            }
            
            push(@{$gene_list{$gene_id}},$gene_set_id);
            
            
            
        
    }


    while (($gene_id,$list) = each %gene_list){
        
        $rest_of_row = join("\t",@{$list});
        
        print OUTFILE "$gene_id\t$rest_of_row\n";
        
    }

    close OUTFILE

} #end of each db_id
