my $mirna_dat_file = $ARGV[0];
print $mirna_dat_file."\n";
open my $mirna_dat, $mirna_dat_file;
@mirna_dat_lines = <$mirna_dat>;

%track_identifiers = ();
$aliases = "";
$sequence = "";
foreach (@mirna_dat_lines){
    $row = $_;
    chomp($row);
    @row = split("   ",$row);
    #print "@row\n";

    if ($row eq "//"){
        $aliases =~ s/,$//g;
        if ($species eq "MMU;"){
            $species_name = "Mus musculus";
            print "$species_name\tmiRNA\t$identifier\t$aliases\t$description\t$sequence\t\n";
        }
        if ($species eq "HSA;"){
            $species_name = "Homo sapiens";
            print "$species_name\tmiRNA\t$identifier\t$aliases\t$description\t$sequence\t\n";
        }
        # %track_identifiers = ();
        $sequence = "";
        $aliases = "";
    } else {

        $type = @row[0];
        if ($type eq "ID"){
            $last_type = "ID";
            # check for MMU or HSA otherwise ignore til the //
            @species = split(" ",@row[3]);
            $species = @species[2];
            #print "$species\n";
            $identifier = @row[1];
        }
        if ($type eq "AC"){
            $last_type = "AC";
            $accession_id = @row[1];
            $accession_id =~ s/;//g;
            $aliases = $aliases.$accession_id.",";
        }
        if ($type eq "DE"){
            $last_type = "DE";
            $description = @row[1];
        }

        if ($type eq "FT"){
            $last_type = "FT";
            $row =~ s/"//g;
            @product = split("product=",$row);
            @accession = split("accession=",$row);
            $temp_product = @product[1];
            $temp_accession = @accession[1];
            if ($temp_product ne ""){
                $aliases = $aliases.$temp_product.",";
            }
            if ($temp_accession ne ""){
                $aliases = $aliases.$temp_accession.",";
            }
        }

        if ($type eq "SQ"){
            $last_type = $type;

        } else {
            if ($last_type eq "SQ"){
                $row =~ s/[0-9]*//g; 
                $row =~ s/\ //g; 
                $sequence = $sequence.$row; 
            }
            
        }
    }
} 
 
