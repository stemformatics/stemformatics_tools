


$starting_colour_hex = ''.$ARGV[0];


my $starting_colour_decimal = hex($starting_colour_hex);
my $end_colour_decimal = hex('000000');
my $number_of_steps = 12;

my $step_in_decimal = int(($starting_colour_decimal - $end_colour_decimal) / $number_of_steps);

$count = 0;

$output = "[";

while ($count <= 9){
    
    $new_colour = $starting_colour_decimal - $count*$step_in_decimal;
    
    $new_hex_colour = sprintf("%X", $new_colour);
    
    $output = $output . "'#$new_hex_colour',";
    
    $count ++;
}

$output = substr($output,0,-1) . "]";

print "\n\n$output\n\n"

