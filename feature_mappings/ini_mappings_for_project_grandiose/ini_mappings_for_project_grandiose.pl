

# ftp://ftp.ebi.ac.uk/pub/databases/IPI/current/ipi.genes.MOUSE.xrefs.gz
# when you untar
# cat ipi.genes.MOUSE.xrefs | awk -F "\t" '{print $7 "\t" $11}' > basic_mapping.txt



$db_id = 46;
$mapping_id = 105;

my $inputDir = '/home/rowlandm/mappings/uniprot/from_ipi/';
my $outputDir = $inputDir.'output/';


open(MYOUTFILE, ">".$outputDir."ini_feature_mappings.tsv"); #open for write, append
open my $probe_mappings, $inputDir.'basic_mapping.txt';

@probe_mappings = <$probe_mappings>;


%track_mappings = ();

foreach (@probe_mappings){
    
    $row = $_;
    
    chomp($row);
    
    @row = split("\t",$row);
    
    my $proteins = @row[1];
    my $gene_id = @row[0];
    
    
    $gene_id =~ s/[^a-zA-Z0-9]*//g;
    # $proteins =~ s/[^a-zA-Z0-9]*//g;
    
    @proteins = split(';',$proteins);
    
    
    
    
    foreach (@proteins){
        
        @temp_protein = split("-",$_);
    
        $protein_id = @temp_protein[0];
    
        $mapping = $gene_id.$protein_id;
        
        
        if ($gene_id ne "" and $protein_id ne "" and $track_mappings{$mapping} ne 'done'){
            $protein_id =~ s/[^a-zA-Z0-9]*//g;
            
            $track_mappings{$mapping} = 'done';    
            
            print MYOUTFILE "$db_id\t$mapping_id\tGene\t$gene_id\tProbe\t$protein_id\n"; 
        }

    }
}



# remove the last two lines of the output file
# cat import_feature_mappings.tsv | psql -h localhost -U portaladmin portal_beta -c "COPY stemformatics.feature_mappings FROM STDIN"
