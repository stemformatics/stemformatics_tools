my $inputDir = '/tmp/';
my $outputDir = '/tmp/';


open(MYOUTFILE, ">".$outputDir."import_feature_mappings.tsv"); #open for write, append
open my $probe_mappings, $inputDir.'probe_mappings';

@probe_mappings = <$probe_mappings>;

my $header = splice (@probe_mappings,0,1);
splice (@probe_mappings,0,1);

foreach (@probe_mappings){
    
    $row = $_;
    
    chomp($row);
    
    @row = split(/\|/,$row);
    my $chip_type = @row[0];
    my $probe_id = @row[1];
    my $db_id = @row[2];
    my $gene_id = @row[3];
    
    $chip_type =~ s/[^a-zA-Z0-9]*//g;
    $probe_id =~ s/[^a-zA-Z0-9\_]*//g;
    $db_id =~ s/[^a-zA-Z0-9]*//g;
    $gene_id =~ s/[^a-zA-Z0-9]*//g;
    
    print MYOUTFILE "$db_id\t$chip_type\tGene\t$gene_id\tProbe\t$probe_id\n"; 
    
}

# remove the last two lines of the output file
# cat import_feature_mappings.tsv | psql -h localhost -U portaladmin portal_beta -c "COPY stemformatics.feature_mappings FROM STDIN"
