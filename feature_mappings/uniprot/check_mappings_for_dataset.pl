#copy (select to_id from stemformatics.feature_mappings where mapping_id = 105) to '/tmp/test.dat';
#cat /tmp/test.dat | sort | uniq > /tmp/uniprot.txt
#!/usr/local/bin/perl
$dir = '/var/www/pylons-data/dev/ProbeFiles/';
my $mapping = ();
$ds_id = @ARGV[0];
open (MYFILE, '/tmp/uniprot.txt');
 while (<MYFILE>) {
    chomp;
    $mapping {$_} = 1;
 }
 close (MYFILE); 


my $count = 0;
my $total = 0;
my $probes = ();
 open (MYFILE,$dir.$ds_id.'.probes');
 while (<MYFILE>) {
    chomp;
    $probe = $_;
    if (exists $mapping{$probe}){
        $count = $count + 1;
    }
    $total = $total +1;
 }
 close (MYFILE); 
    print "$count / $total\n";




