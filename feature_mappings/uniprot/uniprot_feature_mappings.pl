


$filename = $ARGV[0];
$db_id = $ARGV[1];
$mapping_id = $ARGV[2];


# $working_dir = "/home/rowlandm/";
$uniprot = $filename;

open (INPUTFILE, $uniprot);

while (<INPUTFILE>){
    
    $row = $_;
    
    @row = split("\t",$row);
    
    $uniprot_code = @row[0];
    
    @genes = split(";",@row[1]);
    
    #print "$row";
    
    for my $gene (@genes){
        
        chomp($gene);
        
        $gene =~ s/[^a-zA-Z0-9]*//g;
        
        if ($gene eq ""){
            next;
        }
        print $db_id."\t".$mapping_id."\tGene\t".$gene."\tProbe\t".$uniprot_code."\n";
    }
    
    # print $uniprot_code."\n";
    
}
