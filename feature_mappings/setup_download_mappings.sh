#!/bin/bash

help () {

  echo "Usage: setup_download_mappings.sh dev localhost portal_beta \"-p 5433\""

}

runsql(){
    if [ "$#" -eq 4 ]
    then
        psql -U portaladmin -h "$DBSERVER" "$DB" "$PORT" -c "$SQL"
    else
        psql -U portaladmin -h "$DBSERVER" "$DB" -c "$1"
        
    fi
}



if [ "$#" -eq 0 ]
    then
        help
        exit
fi


INSTANCE="$1"
DBSERVER="$2"
DB="$3"
PORT="$4"
BASE_DIR="/var/www/pylons/$INSTANCE/guide/public/mappings"
BASE_DIR="/var/www/html/mappings"
mkdir -p "$BASE_DIR"
chmod 777 "$BASE_DIR"
ASSAY_PLATFORM_LIST="$BASE_DIR/assay_platform.txt"
SQL="copy (select mapping_id from assay_platforms) to '$ASSAY_PLATFORM_LIST';"


runsql "$SQL"

if [ -f $ASSAY_PLATFORM_LIST ];
then
    cat "$ASSAY_PLATFORM_LIST" | while read this_line; do
        MAPPING_ID=`echo $this_line | awk '{print $1}'`
        MAPPING_FILE="$BASE_DIR/mapping_$MAPPING_ID.txt"
        SQL="copy (select to_id,from_id from stemformatics.feature_mappings where mapping_id = $MAPPING_ID) to '$MAPPING_FILE';"
        runsql "$SQL"
    done
fi
