
$probes_list_in_dataset = $ARGV[0];
$main_mapping_file_name = $ARGV[1];
#~ $uniprot_to_entrez_file_name = $ARGV[2];
#~ $entrez_to_ensembl_file_name = $ARGV[3];
$outfile_name = $ARGV[2];

open my $probe_file, $probes_list_in_dataset;
open my $main_mapping_file,$main_mapping_file_name;
open my $uniprot_to_entrez_file,$uniprot_to_entrez_file_name;
open my $entrez_to_ensembl_file,$entrez_to_ensembl_file_name;
open OUTFILE , ">", $outfile_name;


%mapProbe = ();
while (<$main_mapping_file>) {
    $row = $_;
    $split = "\t";
    @row = split($split,$row);
    
    $protein = @row[5];
    
    $gene = @row[3];
    
    chomp $protein;
    chomp $gene;
    
    $mapProbe{$protein} = $gene;  
    
    # print "$protein\t$gene\n";
}
close $main_mapping_file;
#~ 
#~ %entrez_to_ensembl =  ();
#~ while (<$entrez_to_ensembl_file>) {
    #~ $row = $_;
    #~ $split = "\t";
    #~ @row = split($split,$row);
    #~ 
    #~ $entrez = @row[0];
    #~ $gene = @row[1];
    #~ chomp $gene;
    #~ $entrez_to_ensembl{$entrez} = $gene;      
    #~ 
#~ }
#~ close $entrez_to_ensembl_file;
#~ 
#~ %mapProbeViaEntrez =  ();
#~ while (<$uniprot_to_entrez_file>) {
    #~ $row = $_;
    #~ $split = "\t";
    #~ @row = split($split,$row);
    #~ 
    #~ $protein = @row[0];
    #~ $entrez = @row[1];
    #~ chomp $entrez;
    #~ $gene = $entrez_to_ensembl{$entrez};
    #~ 
    #~ 
    #~ $mapProbeViaEntrez{$protein} = $gene;      
    #~ 
    #~ 
#~ }
#~ close $uniprot_to_entrez_file;



$count = 0;
$count_first = 0;
$count_second = 0;
$count_none = 0;
while (<$probe_file>) {
    
    chomp $_;
    $protein = $_;
    
    $first = $mapProbe{$protein};
    
    #~ $second = $mapProbeViaEntrez{$protein};
    
    if ($first ne ""){
        $count_first ++;
    } else {
        
        #~ if ($second ne ""){
            #~ $count_second ++;
        #~ } else {
        $count_none ++;
        print OUTFILE "$protein\n";
        #~ }
        
    }
    
    
    $count ++;
    
    
    
    
}
close OUTFILE;
close $probe_file;


print "$count\t$count_first\t$count_none\n";
