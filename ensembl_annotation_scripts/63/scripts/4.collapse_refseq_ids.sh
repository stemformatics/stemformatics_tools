#!/bin/sh
input_file="$1"

last_gene_id=""
refseq_ids=""
last_line=""

while read line
do
   gene_id=`echo "$line" | awk -F'\t' '{print $1}'`
   ref_id=`echo "$line" | awk -F'\t' '{print $13}'`
   if [ "$gene_id" = "$last_gene_id" ]; then
      refseq_ids="$refseq_ids $ref_id"
   else
      refseq_ids=`echo "$refseq_ids" | sed -r -e 's/^\s+//g' -e 's/\s+$//g' -e 's/\s\s+//g'`
      echo "$last_line" | awk -v ref="$refseq_ids" -F'\t' '{print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10"\t"$11"\t"$12"\t"ref"\t"$14"\t"$15}'
      refseq_ids="$ref_id"
   fi
   last_gene_id="$gene_id"
   last_line="$line"
done < $input_file

## If we dont't do this, we miss the last gene
refseq_ids=`echo "$refseq_ids" | sed -r -e 's/^\s+//g' -e 's/\s+$//g' -e 's/\s\s+//g'`
echo "$last_line" | awk -v ref="$refseq_ids" -F'\t' '{print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10"\t"$11"\t"$12"\t"ref"\t"$14"\t"$15}'

