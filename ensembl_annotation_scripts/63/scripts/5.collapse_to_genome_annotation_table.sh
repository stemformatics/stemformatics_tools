#!/bin/sh
## Takes collapsed RefSeq ID Ensembl annotations file and converts to "genome_annotations" table format
## ready for load into Stemformatics database.
## Also adds Entrez ID and synonyms.

input="$1"
file_base=`basename $input`
output="../processed/genome_annotations_${file_base}.txt"

echo "$file_base" | grep hom > /dev/null 2>&1
if [ $? -eq 0 ]; then
  dbid=56
  entrez_annotation="../../../entrez/hom_gene_info"
else
  dbid=46
  entrez_annotation="../../../entrez/mus_gene_info"
fi

## Add database_id column at the front "56" for Human genes
awk -F'\t' -v dbid=$dbid '{print dbid"\t"$1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10"\t"$11"\t"$12"\t"$13"\t"$14"\t"$15}' $input > /tmp/genome_annotations_with_dbid.txt

## Add Entrez IDs and synonyms (where available)
while read line
do
   desc=`echo "$line" | awk -F'\t' '{print $16}'`
   ensid=`echo "$line" | awk -F'\t' '{print $2}'`
   matching_entrez_records=`grep -P "$ensid[\|\t]" $entrez_annotation`
   match_count=`echo "$matching_entrez_records" | wc -l`
   if [ $match_count -eq 1 ]; then
      entrez_id=`echo "$matching_entrez_records" | awk -F'\t' '{print $2}'`
      entrez_syn=`echo "$matching_entrez_records" | awk -F'\t' '{print $5}'`
   else
      echo "DEBUG: Found $match_count Entrez records for ensembl ID $ensid, concatenating synonyms.."
      entrez_id=`echo "$matching_entrez_records" | awk -F'\t' '{print $2}' | head -1`
      entrez_syn=`echo "$matching_entrez_records" | awk -F'\t' '{print $5}'`
   fi

   ## Process gene synonyms
   if [ ! -z "$entrez_syn" ]; then
     ## convert synonym newlines and pipes to spaces
     syn=`echo "$entrez_syn" | sed -r -e 's/\n/ /g' -e 's/\|/ /g'`
     ## remove duplicate synonyms
     syn=`echo "$syn" | sed -r -e 's/\s/\n/g' | sort | uniq | tr [:cntrl:] " " | sed -r -e 's/\s+$//g'`
   else
     syn=""
   fi

   ## Add required HTML line break to description field.
   desc="$desc<br />"

   ## output
   echo "$line" | awk -v syn="$syn" -v entrez="$entrez_id" -v desc="$desc" -F'\t' '{print $1"\t"$2"\t"$3"\t"syn"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10"\t"$11"\t"$12"\t"entrez"\t"$14"\t"$15"\t"desc}' >> $output

done < /tmp/genome_annotations_with_dbid.txt

