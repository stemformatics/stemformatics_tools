#!/bin/sh
## Takes collapsed RefSeq ID Ensembl annotations file and converts to "genome_annotations" table format
## ready for load into Stemformatics database.
##
## Also adds gene synonyms from Entrez annotation, by exact match on Entrez ID.

input="$1"
file_base=`basename $input`
output="../processed/genome_annotations_${file_base}.txt"

echo "$file_base" | grep hom > /dev/null 2>&1
if [ $? -eq 0 ]; then
  dbid=56
  species=9606
  entrez_annotation="../../../entrez/hom_gene_info"
else
  dbid=46
  species=10090
  entrez_annotation="../../../entrez/mus_gene_info"
fi

## Add database_id column at the front "56" for Human genes
awk -F'\t' -v dbid=$dbid '{print dbid"\t"$1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10"\t"$11"\t"$12"\t"$13"\t"$14"\t"$15}' $input | grep "ENS" > /tmp/genome_annotations_with_dbid_$dbid.txt

## Add Entrez IDs and synonyms (where available)
while read line
do
   desc=`echo "$line" | awk -F'\t' '{print $3}'`
   ensid=`echo "$line" | awk -F'\t' '{print $2}'`
   ## Second last field in input (last is HGNC or MGI ID)
   entrezid=`echo "$line" | awk -F'\t' '{print $15}'`

   matching_entrez_records=`grep -P "^$species\t$entrezid\t" $entrez_annotation`
   match_count=`echo "$matching_entrez_records" | wc -l`

   if [ $match_count -eq 1 ]; then
      entrez_syn=`echo "$matching_entrez_records" | awk -F'\t' '{print $5}'`
   elif [ $match_count -gt 1 ]; then
      echo "DEBUG: Found $match_count Entrez records for ensembl ID $ensid, concatenating synonyms.."
      entrez_syn=`echo "$matching_entrez_records" | awk -F'\t' '{print $5}'`
   else
     echo "DEBUG: ENS gene with Entrez ID $entrezid not found in Entrez annotation!"
   fi

   ## Process gene synonyms
   if [ ! -z "$entrez_syn" ]; then
     ## convert synonym newlines and pipes to spaces
     syn=`echo "$entrez_syn" | sed -r -e 's/\n/ /g' -e 's/\|/ /g'`
     ## remove duplicate synonyms
     syn=`echo "$syn" | sed -r -e 's/\s/\n/g' | sort | uniq | tr [:cntrl:] " " | sed -r -e 's/\s+$//g'`
   else
     syn=""
   fi

   ## Add required HTML line break to description field.
   desc="$desc<br />"

   ## output
   echo "$line" | awk -v syn="$syn" -v entrez="$entrezid" -v desc="$desc" -F'\t' '{print $1"\t"$2"\t"syn"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10"\t"$11"\t"$12"\t"$13"\t"entrez"\t"$16"\t"$14"\t"desc}' >> $output

done < /tmp/genome_annotations_with_dbid_$dbid.txt

