#!/bin/sh
mart_export_minus_header="$1"
output_dir="../processed/"
file_base=`basename $1`
## Remove "[Source: ...]" tag from desription field
cat $mart_export_minus_header | sed -r -e 's/\[Source\:.*\]//g' > $output_dir/$file_base.clean

## Remove transcript column 
cat $output_dir/$file_base.clean | sed -r -e 's/\t(ENST|ENSMUST)[0-9]+\t/\t/g' > $output_dir/$file_base.clean.without.transcripts
sort $output_dir/$file_base.clean.without.transcripts | uniq > $output_dir/$file_base.clean.without.transcripts.uniq

