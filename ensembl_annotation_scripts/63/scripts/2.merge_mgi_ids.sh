#!/bin/sh
## Input is typically the output of the "1.clean_mart_export.sh" script.
input="$1"
## The BioMart export file containing Gene, Transcript, MGI ID, Name, Desc
mgi_mart="$2"

while read line
do
  ens_id=`echo "$line" | awk -F'\t' '{print $1}'`
  mgi_id=`grep -P "^$ens_id" $mgi_mart | head -1 | awk -F'\t' '{print $3}'`
  ## Uncomment this to remove the "MGI:" namespace from numerical IDs.
  #if [ ! -z $mgi_id ]; then
  #  mgi_id=`echo "$mgi_id" | sed -r -e 's/^MGI\://g'`
  #fi
  echo "$line" | awk -F'\t' -v mgi=$mgi_id '{print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10"\t"$11"\t"$12"\t"$13"\t"$14"\t"$15"\t"mgi}'
done < $input
