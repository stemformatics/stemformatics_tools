#!/bin/sh
mart_export_clean_merged="$1"
output_dir="../processed/"
file_base=`basename $1`

## Remove transcript column 
cat $output_dir/$file_base | sed -r -e 's/\t(ENST|ENSMUST)[0-9]+\t/\t/g' > $output_dir/$file_base.without.transcripts
sort $output_dir/$file_base.without.transcripts | uniq > $output_dir/$file_base.without.transcripts.uniq

