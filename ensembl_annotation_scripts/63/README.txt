
These scripts convert Ensembl biomart-downloaded genome / transcript annotations to
Stemformatics genome_annotations table-ready data.



Processing Steps
================

1. Download from biomart

2. Remove header and create "minus.header" versions

3. Clean downloaded annotations
	$ cd scripts && ./1.clean_mart_export.sh ../mart_exports/hom_gene_annotation_attribs_plus_refseq_entrez_MINUS-HEADER.txt
	$ cd scripts && ./1.clean_mart_export.sh ../mart_exports/mus_gene_annotation_attribs_plus_refseq_entrez_MINUS-HEADER.txt

4. Merge HGNC IDs / MGI IDs
	$ cd scripts && ./2.merge_hgnc_ids.sh ../processed/hom_gene_annotation_attribs_plus_refseq_entrez_MINUS-HEADER.txt.clean ../mart_exports/hom_gene_transcript_hgnc_id_syn_MINUS-HEADER.txt > ../processed/hom_gene_annotation_attribs_plus_refseq_entrez_MINUS-HEADER.txt.clean.hgnc.merged
	$ cd scripts && ./2.merge_mgi_ids.sh ../processed/mus_gene_annotation_attribs_plus_refseq_entrez_MINUS-HEADER.txt.clean ../mart_exports/mus_gene_transcript_mgi_id_name_desc_MINUS-HEADER.txt > ../processed/mus_gene_annotation_attribs_plus_refseq_entrez_MINUS-HEADER.txt.clean.mgi.merged

5. Remove transcripts column
	$ cd scripts && ./3.remove_transcripts.sh ../processed/hom_gene_annotation_attribs_plus_refseq_entrez_MINUS-HEADER.txt.clean.hgnc.merged
	$ cd scripts && ./3.remove_transcripts.sh ../processed/mus_gene_annotation_attribs_plus_refseq_entrez_MINUS-HEADER.txt.clean.mgi.merged

6. Collapse RefSeq IDs per gene
	$ cd scripts && nohup ./4.collapse_refseq_ids.sh ../processed/hom_gene_annotation_attribs_plus_refseq_entrez_MINUS-HEADER.txt.clean.hgnc.merged.without.transcripts.uniq > ../processed/hom_gene_annotation_attribs_plus_refseq_entrez_MINUS-HEADER.txt.clean.hgnc.merged.without.transcripts.uniq.collapsed &
	$ cd scripts && nohup ./4.collapse_refseq_ids.sh ../processed/mus_gene_annotation_attribs_plus_refseq_entrez_MINUS-HEADER.txt.clean.mgi.merged.without.transcripts.uniq > ../processed/mus_gene_annotation_attribs_plus_refseq_entrez_MINUS-HEADER.txt.clean.mgi.merged.without.transcripts.uniq.collapsed &


7. Add gene synonyms from Entrez
	$ cd scripts && nohup ./5.collapse_to_genome_annotation_table_match_by_entrez_id.sh ../processed/hom_gene_annotation_attribs_plus_refseq_entrez_MINUS-HEADER.txt.clean.hgnc.merged.without.transcripts.uniq.collapsed > ../processed/5.hom_collapse.log &
	$ cd scripts && nohup ./5.collapse_to_genome_annotation_table_match_by_entrez_id.sh ../processed/mus_gene_annotation_attribs_plus_refseq_entrez_MINUS-HEADER.txt.clean.mgi.merged.without.transcripts.uniq.collapsed > ../processed/5.mus_collapse.log &

(Success)

