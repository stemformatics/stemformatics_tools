#!/bin/bash
# copy (select id,mapping_id from datasets as d left join assay_platforms as ap on ap.chip_type = d.chip_type) to '/tmp/map_ds_id_to_mapping_id.map';
# copy (select ds_id,chip_id,md_value from biosamples_metadata where md_name = 'Replicate Group ID' order by ds_id desc) to '/tmp/map_chip_id_to_rep_group_id.map';
# copy (select to_id,mapping_id,gene_id,associated_gene_synonym from stemformatics.feature_mappings as fm left join genome_annotations as ga on ga.gene_id = fm.from_id ) to '/tmp/map_probe_to_gene.map'  ;

YUGENE_FILES_DIR="/data/temp_yugene"

cd "$YUGENE_FILES_DIR"
rm -f *.prev
rm -f *.temp
for FILE in dataset*.cumulative.txt;
do
    ds_id=`echo $FILE | grep "[0-9]\+" -o`
    chip_type=`grep $ds_id map_ds_id_to_mapping_id.map | cut -f2`
    FILE_PREV="$FILE".prev
    FILE_TEMP="$FILE".temp
    cp "$FILE" "$FILE_PREV"
    sed -i '1!b;s/Probe\t//' "$FILE_PREV" 
    sed -i '1!b;s/Probe.\t//' "$FILE_PREV" 
    /data/repo/git-working/stemformatics_tools/dataset_scripts/s4m/dataset_scripts/normalization/util/unpack_expression_table.pl "$FILE_PREV" > "$FILE".temp
    sed -i "s/^/$ds_id\t$chip_type\t/" "$FILE_TEMP"
    echo "finished $FILE_TEMP"
done

# call perl to replace the chip_id with the replicate_group_id
# call perl to replace the mapping_id and the probe_id with the gene_id and associated_gene_name
#perl replace_values.pl
