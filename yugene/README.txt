Create directory /data/temp_yugene

Copy all the files from here to /data/temp_yugene

Get the three mapping data filesby running queries similar to the ones shown at the top of yugene_merge.sh and saving the files 
map_chip_id_to_rep_group_id.map  
map_ds_id_to_mapping_id.map  
map_probe_to_gene.map

Copy all the datasetXXXX.cumulative.txt files to /data/temp_yugene

Run the yugene_merge.sh script

Check none of the .temp files has a Probe accidetally by using: head -1 *.temp | grep Probe

Run the perl file replace_values.pl eg: nohup perl replace_values.pl &

Ask Rowland for more details - although he probably won't remember.
