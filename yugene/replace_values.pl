#$file_name = @ARGV[0] ;

print "staring mapping chip_id to replicate group id\n";
# do the mapping for the chip_id to replicate group id
my $map_chip_id_to_replicate_group_id_file_name = 'map_chip_id_to_rep_group_id.map';
my %hash_chip_id_to_replicate_group_id = ();

open (MYFILE, $map_chip_id_to_replicate_group_id_file_name);
 while (<MYFILE>) {
    chomp;
    my @row = split("\t",$_);
    my $ds_id = @row[0];
    my $chip_id = @row[1];
    my $probe_id = @row[2];

    $hash_chip_id_to_replicate_group_id{$ds_id}{$chip_id} = $probe_id;

 }
close (MYFILE); 



print "starting mapping probe to gene\n";

# do the mapping for the probe to gene
my $map_probe_to_gene_file_name = 'map_probe_to_gene.map';
my %hash_probe_to_gene = ();

open (MYFILE, $map_probe_to_gene_file_name);
 while (<MYFILE>) {
    chomp;
    my @row = split("\t",$_);
    my $probe_id = @row[0];
    my $mapping_id = @row[1];
    my $gene_id = @row[2];
    my $gene_symbol = @row[3];

    $hash_probe_to_gene{$probe_id}{$mapping_id}{$gene_id}{'gene_id'} = $gene_id;
    $hash_probe_to_gene{$probe_id}{$mapping_id}{$gene_id}{'gene_symbol'} = $gene_symbol;

 }
close (MYFILE); 

print "starting reading temp files\n";

my $dir = '/data/temp_yugene/';
open (MYFILE, '>/data/temp_yugene/merged.final');
foreach my $fp (glob("$dir/*.temp")) {
  open my $fh, "<", $fp or die "can't read open '$fp': $OS_ERROR";
  while (<$fh>) {
    chomp;
    @row = split("\t",$_);
    my $ds_id = @row[0];
    my $mapping_id = @row[1];
    my $chip_id = @row[2];
    my $probe_id = @row[3];
    my $value = 1 - @row[4];
    while (my ($key, $val) = each %{ $hash_probe_to_gene{$probe_id}{$mapping_id} } ) {
        my $gene_id = $hash_probe_to_gene{$probe_id}{$mapping_id}{$key}{'gene_id'};
        my $gene_name = $hash_probe_to_gene{$probe_id}{$mapping_id}{$key}{'gene_symbol'};
        my $sample_id = $hash_chip_id_to_replicate_group_id{$ds_id}{$chip_id};

        print MYFILE "$ds_id\t$gene_id\t$gene_name\t$sample_id\t$probe_id\t$value\n";
    }


  }
  close $fh or die "can't read close '$fp': $OS_ERROR";
}
close (MYFILE);
