# MageTab2sql.py
#
# To be able to take a experiment specified by its ArrayExpress code
# and parse it into sql 
#
#
# Written by Amanda Miotto - amanda.miotto.1@gmail.com
# 24-Aug-2010
##########################################################################



import urllib
import rpy2
import rpy2.robjects as robjects
#import pgloader

##############
# Functions
##############


# will get raw expression data, normalise it and spit it back out.
def getexpression(infilename_expression):
	robjects.r('''
		normalizing<-function(fexpression){
			exptable<-read.table(fexpression, header=TRUE, sep="\t")
			exptable2<-cbind(exptable[,1:4],exptable[,9] ,exptable[,47:82], exptable[,155:190])
			#needs to normalize data here
			probes<-exptable2[,5]
			for (i in 6:(dim(exptable2)[1])){ 
				exptable3<-rbind(cbind(rep(names(exptable2)[i],dim(exptable2)[2]),probes,exptable2[,i]));
			} 
			write.table(exptable2, file="exp2.tester.txt")
			exptable3<-sendtogp(exptable2)
			write.table(exptable3, file="exp3.tester.txt")
			return(exptable3)
			}
		sendtogp<-function(inputfile){
			# need to make sure library Genepattern is installed , and point it towards a workign GP server
			# Also need file in res or gct format. Prob gct.
			# save as file or just parse it?
			library(GenePattern)
			username<-"AmandaMiotto"
			password<-"93427417"
			servername<-"http://genepattern.broadinstitute.org/"			
			gp.client <- gp.login(servername, username, password)
			input.ds <- "ftp://ftp.broadinstitute.org/pub/genepattern/all_aml/all_aml_train.res"
			preprocess.jobresult <- run.analysis(gp.client, "PreprocessDataset", input.filename=input.ds)
			download.directory <- job.result.get.job.number(preprocess.jobresult)
			download.directory <- as.character(download.directory)
			preprocess.out.files <- job.result.download.files(preprocess.jobresult, download.directory)
			data<-read.dataset(as.character(preprocess.out.files))
		}
		''')
	r_normalizing=robjects.globalenv['normalizing']
	return r_normalizing(infilename_expression)

getexpression('/home/s2686739/ArrayExpress/Tools/head.Fibro_Sz_Vs_Controls_Sample_Probe_Profile.txt.proc')

def getexpressionusingrsp(infilename_expression):
	__RPythonInitArgs = ["--vanilla", "--silent"]
	import RS
	RS.call(’print("hi")’)

# Download data
def getMetadata(infilename_idf):
	import urllib
	filehandle = urllib.urlopen(infilename_idf)
	return filehandle.read()
	filehandle.close()

# Prepare idf file to publication metadata
def pubMetadataProcessing(pubMetadata):
	pubMetadataLines=pubMetadata.split("\n")
	pubMetadataEntries=[i.split("\t") for i in pubMetadataLines]
	return pubMetadataEntries

def printarray(array):
	l=len(array)
	for i in range(l):
		print array[i]

		
###################
#  Script
####################
def main():
	getexpression('/home/s2686739/ArrayExpress/Tools/head.Fibro_Sz_Vs_Controls_Sample_Probe_Profile.txt.proc')
	sampleMetadata=getMetadata("http://www.ebi.ac.uk/microarray-as/ae/files/E-TABM-724/E-TABM-724.sdrf.txt")
	pubMetadata=getMetadata("http://www.ebi.ac.uk/microarray-as/ae/files/E-TABM-724/E-TABM-724.idf.txt")
	infilename_expression=getMetadata("http://www.ebi.ac.uk/microarray-as/ae/files/E-MEXP-31/E-MEXP-31.raw.1.zip")
	pubMetadataProcessing(pubMetadata)


