#!/bin/sh
## ----------------------------------------------------------------------------
## Script:	s4m.sh
## Description:	Stemformatics expression dataset tool.
##		Create datasets, normalize datasets, update metadata etc.
## Author:	O.Korn
## NOTE:	Do not mix dataset versions! Bad things might happen.
## 
## Format Changelog
## =================
## v2.0  (Sept 2018) T#2914
##	- Remove no longer needed table-based pgconf loader files (used to
##	  specify order of fields to load from text files e.g. dataset.txt,
##	  biosamples.txt) since this is now being handled by the dataset
##	  upload script which now uses psql directly, completely bypassing
##	  pgloader (which was found to cause Postgres to stop responding while
##	  waiting to acquire semaphore on table being loaded. Postgres server
##	  version running in CentOS 7).
##
## v1.95 (Aug 2018) T#2914
##	- Add "data_type_id" to dataset.txt
##
## v1.94 (Apr 2018) T#2422
##	- Disabled Rohart score generation by default
##
## v1.93 (Apr 2017) T#2626
##	- Now storing mapping_id,log_2 in dataset.txt
##      - Now storing yAxisLabel and probeName in dataset_metadata.txt
##
## v1.92 (Apr 2015)
##	- Now generating Rohart score files in dataset finalisation. Does not
##	  affect dataset format. Requires '-C' flag usage with '-f'
##	- The "rohartMSCAccess" flag in METASTORE toggled based on Rohart file
##	  existence
##	- New '-rh' flag for stand-alone Rohart score file generation
##
## v1.91 (Jan 2015)
##	- dataset.txt now addes "min_y_axis" and "show_yugene" fields.
##	- METASTORE template now has "minGraphValue" field.
##
## v1.9 (May 2014)
##	- Unrecognised fields in METASTORE now transferred to dataset_metadata.txt
##	  "as-is", allowing for easy extension of METASTORE by external means;
##	  at the cost of NO VALIDATION on these fields but this is now handled
##	  within Stemformatics itself.
##	- New 'db_id' field for datasets.txt
##	- New "-qc" flag for QC plot regeneration without normalization
##
## v1.81 (Dec 2013)
##	- lineGraphSampleOrder in METASTORE => lineGraphOrdering
##	- New "-dp" flag to generate detection threshold plots automatically
##	  for a hard-coded range, calling same function as "-d" being invoked.
##	  PDF generated at each "-d" subroutine call with any/all plots available.
##	- Install command "--install" now takes "-C" argument to get install path
##	  from given install profile target <foo.conf> (etc/profile/<foo.conf>)
##
## v1.8  (Oct 2013)
##	- Removed assay_platform.txt and assay_platform.conf (too difficult
##	  to maintain) - target chip_type must exist in assay_platforms DB table
##	  prior to dataset load
##	- This version also corresponds to inclusion of support for GCT-only
##	  validation (no "probe_expression_avg_replicates.txt" necessary).
##	- Removed manual (CLI) metadata annotation (upon init and by -a flag)
##
## v1.72 (Apr 2013):
##	- New 'maxGraphValue' attribute
##
## v1.71 (Mar 2013):
##	- Extending assay_platforms.txt to 10 columns as per SQL table spec.
##
## v1.7 (Oct 2012):
##	- New METASTORE attributes:
##	  genepattern_analysis_enabled (ds_meta: genePatternAnalysisAccess)
##	  lineGraphSampleOrder (ds_meta: lineGraphOrdering)
##
## v1.62 (Aug 2012):
##	- Prepending "[pgsql]" section header to pgloader configuration header
##	  functions to allow recent (and future) custom sections to host-specific
##	  pgloader conf templates (in /etc/pgconf) to work as expected, when
##	  creating targeted configurations by appending conf files.
##	  Otherwise, global options such as client_encoding are being ignored.
##
## v1.61 (Aug 2012):
##	- New file "sample_pheno.txt.template" intended for annotator to
##	  save as "sample_pheno.txt" with sample ID to pheno groups required
##	  as input to PCA plotting during dataset normalisation & QC.
##
## v1.6 (July 2012):
##	- New fields in default biosamples_metadata.txt:
##	  Reprogramming Method, Derived From, Differentiated To, Cells Sorted For.
##	- Metadata items re-ordered according to A.Vitale instructions #583
##	- On update, pgloader templates re-written regardless of format version
##	  to ensure consistency with latest DB requirements
##	- No longer loading 'probe_expressions_avg_replicates' table
##
## v1.51 (July 2012):
##	- biosamples.conf now with 'utf-8' encoding to support Unicode chars
##	  in biosamples metadata (e.g. Greek "alpha" or "beta" symbols).
##	  Otherwise identical to v1.5 format.
##	- dataset_metadata.txt now using 'NULL' in place of blank since some 
##	  pgloader versions seem to ignore config directive to treat blanks
##	  as "empty string" for DB loading.
##
## v1.5 (May/June 2012):
##	- Added 'chip_type' field to dataset.txt with update to dataset.conf
##	  accordingly.
##	- Removed "CurationDate" and "Curator" from VERSION file as these
##	  are tracked in dataset_metadata (via METASTORE)
##	- "--update" preserves VERSION file and only replaces the version number
##	  for 'FormatVersion' attribute vs. overwriting file
##	- Better INSTALL.txt instructions
##
## v1.41 (May 2012):
##	- New "--install" and "--unload" options.
##	- Validation supports "high throughput sequencer" type for
##	  'technologyUsed' METASTORE field.
##
## v1.4  (April 2012):
##	- New "--reload" option to reload dataset and biosamples metadata for
##	  a dataset already loaded in target Stemformatics DB.
##	- Finally remove superfluous 'probe_expression.txt'
##
## v1.33 (Feb 2012):
##	- Fix: "Geo Accession" in dataset_metadata.txt now "GEO Accession" and
##	  is now replaced correctly from METASTORE
##
## v1.32 (Feb 2012):
##	- Re-added "null = NULL" pgloader setting in dataset.conf template to
##	  fix issue with incorrect treatment of blank strings in
##	  dataset_metadata.txt. This setting was erroneously misplaced during
##	  v1.31 edits, quite possibly.
##
## v1.31 (October 2011):
##	- biosamples.conf - removed pgloader "null = NULL" setting so
##	  biosamples_metadata values annotated as string "NULL" are treated as
##	  empty strings which don't break the "not null" constraints on those
##	  tables.
##	- Dataset Contributor Email and Assay Version can now be blank (optional)
##	  in dataset_metadata.txt
##	- biosamples_metadata.txt now auto-sorted upon --update
##
## v1.3 (August 2011):
##	- METASTORE - Multiple field name changes, additions and subtractions
##	  as part of dataset annotation overhaul process. See agile_org task #246
##	  and task #249.
##	- Now takes "--format" flag to force creation of dataset in particular
##	  format version. Only applies for version 1.2 and above.
##	  Usage:	s4m.sh --format 1.2 ..
##      - Backwards compatible with format version 1.2, for example the annotation
##        and status features recognised the older format.
##	- Now auto-generates files "biosamples.txt" and "chip_id_replicate_group_map.txt"
##	  from supplied "biosamples_metadata.txt" file, as all required metadata
##	  fields to accomplish this are already available.
##
## v1.2 (July 2011):
##	- dataset_metadata.txt:
##	  Attribs "cellTypeDisplayGroups", "cellTypeDisplayOrder" now:
##	  "sampleTypeDisplayGroups", "sampleTypeDisplayOrder" (respectively)
##	- biosamples_metadata.txt: renamed "Cell Type" to "Sample Type".
##	  "Cell Type" retained and becomes curated, ontology-like cell type definition.
##
## v1.1 (June 2011):
##	- Added missing chip_type column to probe_expression.conf template
##	  (defect #150)
##	- Now honouring "S4M_HOME" environment for script execution
##	  (removed "scripts" sub-directory)
##
## v1.01 (March 2011):
##	- File 'VERSION' now differentiates dataset revision (DatasetVersion) vs.
##	  Stemformatics dataset format version (FormatVersion).
##      - New "source/raw" and "source/normalized" subdirectories.
##      - New 'METASTORE' file which centralises all dataset metadata used to
##        generate and update attributes in Stemformatics dataset files.
##
## v1.0 (Dec 2010):
## 	- Initial format used in datasets corresponding to December 2010 release.
## ----------------------------------------------------------------------------

export S4M_FORMAT_LATEST_VERSION="2.0"
## NOTE: This variable is set to contents of VERSION file from dataset on disk
##       during "set_dataset_path" routine early in script execution.
export S4M_FORMAT_VERSION="$S4M_FORMAT_LATEST_VERSION"
modified_date=`stat $0 | grep -i change | awk '{print $2}'`
export S4M_SCRIPT_VERSION="v$S4M_FORMAT_VERSION $modified_date"
SCRIPT_NAME="$0"

if [ -z "$S4M_HOME" ]; then
   export S4M_HOME=`dirname $SCRIPT_NAME`
fi

timestamp=`date +"%Y%m%d-%H%M%S"`
export S4M_LOG="/tmp/.s4m_$timestamp.log"

## Minutes within which dataset can be loaded since last validation
export VALIDATION_TIMEOUT_MINS=10
## Force update of conf files on every --update ?
export FORCE_UPDATE_CONF_FILES="true"



verify_environment () {
    if [ ! -d "$S4M_HOME/dataset_scripts" -o ! -d "$S4M_HOME/etc" ]; then
        echo "Error: Stemformatics scripts bundle does not appear to be complete (items missing or not found)."
        echo "Run '`basename $SCRIPT_NAME`' from containing directory, or set environment and try again:"
        echo "    export S4M_HOME=\"/path/to/s4m_bundle\""
        exit 1
    fi
}

set_assay_platform_for_chip_type () {
    chip_type="$1"
    optional_format_version="$2"
    platforms_file="$S4M_HOME/etc/assay_platforms.txt"
    if [ ! -z "$chip_type" ]; then
        export S4M_ARRAY_MAN=`grep -P "^$chip_type\t" $platforms_file | awk -F'\t' '{print $3}'`
        export S4M_ARRAY_PLAT=`grep -P "^$chip_type\t" $platforms_file | awk -F'\t' '{print $4}'`
        export S4M_ARRAY_VER=`grep -P "^$chip_type\t" $platforms_file | awk -F'\t' '{print $5}'`
        #echo "DEBUG: Got S4M_ARRAY_MAN=[$S4M_ARRAY_MAN] for chip type=[$chip_type]"
        #echo "DEBUG: Got S4M_ARRAY_PLAT=[$S4M_ARRAY_PLAT] for chip type=[$chip_type]"
        #echo "DEBUG: Got S4M_ARRAY_VER=[$S4M_ARRAY_VER] for chip_type=[$chip_type]"
    else
        #echo "DEBUG: Setting array metadata in ENV to null (empty string)"
        export S4M_ARRAY_MAN=""
        export S4M_ARRAY_PLAT=""
        export S4M_ARRAY_VER=""
    fi
    #echo "DEBUG: set_meta \"array_manufacturer\" \"$S4M_ARRAY_MAN\""
    set_meta "array_manufacturer" "$S4M_ARRAY_MAN"
    #echo "DEBUG: set_meta \"array_platform\" \"$S4M_ARRAY_PLAT\""
    set_meta "array_platform" "$S4M_ARRAY_PLAT"
    #echo "DEBUG: set_meta \"array_version\" \"$S4M_ARRAY_VER\""
    set_meta "array_version" "$S4M_ARRAY_VER"

    ## NOTE: Do we need this platform field? It needs to be hand edited because this default sucks.
    #echo "DEBUG: set_meta \"platform\" \"$S4M_ARRAY_MAN $S4M_ARRAY_PLAT $S4M_ARRAY_VER\""
    set_meta "array" "$S4M_ARRAY_MAN $S4M_ARRAY_PLAT $S4M_ARRAY_VER"
}

## Output common header section for pgloader config files
pg_config_header () {
    echo "[pgsql]
client_encoding = 'latin1'
copy_every      = 10000
commit_every    = 10000
max_parallel_sections = 1
"
}

pg_config_header_utf8 () {
    echo "[pgsql]
client_encoding = 'utf-8'
copy_every      = 10000
commit_every    = 10000
max_parallel_sections = 1
"
}


get_metastore_path () {
  metastore=""
  if [ -z "$S4M_DATASET" ]; then
    metastore="./METASTORE"
  else
    metastore="$S4M_DATASET/METASTORE"
  fi
  echo "$metastore"
}

get_metadata_field () {
    field="$1"
    metastore=`get_metastore_path`
    row=`grep -P "^$field\=" "$metastore" 2>> "$S4M_LOG"`
    #echo "DEBUG: Fetched METASTORE row [$row] for field [$field]. Returned value:"  >> "$S4M_LOG"
    if [ "$?" -eq 0 ]; then
        ## Everything after the FIRST '=' sign is the field value (i.e. values CAN contain '=')
        echo "$row" | sed -r -e 's/^\w+\=//g' -e 's/\n//g' >> "$S4M_LOG"
        echo "$row" | sed -r -e 's/^\w+\=//g' -e 's/\n//g'
    else
        echo -n ""
    fi
}

set_meta () {
    field="$1"
    value="$2"

    grep -P "^$field\=" "$S4M_DATASET/METASTORE" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
        sed -i -re "s|^$field\=.*$|$field\=$value|" "$S4M_DATASET/METASTORE"
    else
        echo "$field=$value" >> "$S4M_DATASET/METASTORE"
    fi
}

get_meta_chip_type () {
    echo `get_metadata_field s4m_chip_type`
}


## NOTE: Used for gauging metadata completeness for metadata status output.
##       Should be kept up-to-date with metastore attributes defined in "init_metastore"
## TODO: This is ugly. Needs a severe update.
##
## TODO: May 2014 - Need to get rid of this ASAP as all annotation now happens
##		in Stemformatics.
##
get_metastore_mandatory_annotation_count () {
    ## Okay, this is the "actual" mandatory count since it's hard to compute
    ## with cases like "accession_id", "dataset_accession_ae", "dataset_accession_geo" etc
    echo "40"
    #init_metastore "/tmp/.s4mmeta"
    #count=`wc -l "/tmp/.s4mmeta" | awk '{print $1}'`
    #rm "/tmp/.s4mmeta"
    #echo "$count"
}

get_metastore_annotated_count () {
    metastore="$S4M_DATASET/METASTORE"
    if [ -f "$metastore" ]; then
        annotated_count=`awk -F'=' '{print $2}' "$metastore" | grep -P "^\w+" | grep -v grep | wc -l`
        if [ $? -eq 0 ]; then
            echo $annotated_count 2>/dev/null
            return
        fi
    fi
    echo "0"
}


## METASTORE "current" version.
## Always corresponds to whatever the current dataset format version is.
##
## NOTE: As of 2014 the METASTORE file is initiated externally and hence
##       this function is likely to be obsoleted and cannot be trusted as
##       a reference.
##
## Metadata attributes below cease to be current as of 29th April 2014.
##
## Kept here as a REFERENCE only.
##
init_metastore () {
    if [ ! -z "$1" ]; then
        metastore="$1"
    else
        metastore="$S4M_DATASET/METASTORE"
    fi
    echo "accession_id=
array=
array_manufacturer=
array_platform=
array_probe_count=
array_version=
dataset_accession_ae=
dataset_accession_geo=
dataset_contributor=
dataset_contributor_affiliation=
dataset_contributor_email=
dataset_handle=
dataset_release_date=
dataset_title=
experiment_type=transcription profiling assay
max_replicates=1
min_replicates=1
postnorm_expression_median=
postnorm_expression_threshold=
postnorm_probes_detected=
publication_authors=
publication_citation=
publication_contact=
publication_contact_email=
publication_description=
publication_title=
publication_pubmed_id=
s4m_chip_type=
s4m_curator=
s4m_curation_date=
s4m_dataset_id=
species_long=
species_short=
technology_used=microarray
top_genes=
lineGraphOrdering=
sampleTypeDisplayOrder=
sampleTypeDisplayGroups=
limitSortBy=Sample Type
cells_samples_assayed=
cell_surface_antigen_expression=
cellular_protein_expression=
nucleic_acid_extract=total RNA extract
genepattern_analysis_enabled=
maxGraphValue=
minGraphValue=" > $metastore
}


update_dataset_metadata_from_metastore () {
    metastore=`get_metastore_path`
    if [ ! -f "$metastore" -o ! -f "$S4M_DATASET/dataset_metadata.txt" ]; then
        return
    fi
    while read line
    do
       field=`echo "$line" | awk -F'=' '{print $1}'`
       value=`echo "$line" | sed -r -e 's/^\w+\=//g'`
       ## Set "NULL" or "IGNORE" values to blank so NULL strings don't get propagated to dataset_metadata.txt
       if [ "$value" = "NULL" -o "$value" = "IGNORE" ]; then
          value=""
       else
          ## We use the '|' character as regex delimiter in sed statements below, so if any appear in "natural"
          ## strings within values, we need to escape them before they are passed in as replacement regex.
          value=`echo "$value" | sed -r -e 's/\|/\\\|/g'`
       fi

       if [ ! -z "$value" ]; then


       case $field in
           accession_id)
               sed -i -re "s|\tAccession\t.*|\tAccession\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           array_manufacturer)
               sed -i -re "s|Assay Manufacturer\t.*|Assay Manufacturer\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           array_platform)
               sed -i -re "s|Assay Platform\t.*|Assay Platform\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           array_version)
               sed -i -re "s|Assay Version\t.*|Assay Version\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           dataset_contributor_affiliation)
               sed -i -re "s|Affiliation\t.*|Affiliation\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           dataset_accession_ae)
               sed -i -re "s|\tAE Accession\t.*|\tAE Accession\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           dataset_accession_geo)
               sed -i -re "s|\tGEO Accession\t.*|\tGEO Accession\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           s4m_curator)
               sed -i -re "s|s4mCurator\t.*|s4mCurator\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           s4m_curation_date)
               sed -i -re "s|s4mCurationDate\t.*|s4mCurationDate\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           s4m_dataset_id)
               if [ ! -z "$value" ]; then
                 awk -F'\t' -v c="$value" '{print c"\t"$2"\t"$3}' "$S4M_DATASET/dataset_metadata.txt" > "$S4M_DATASET/.s4mtmp"
                 mv "$S4M_DATASET/.s4mtmp" "$S4M_DATASET/dataset_metadata.txt"
               fi
               ;;
           dataset_contributor)
               sed -i -re "s|Contributor Name\t.*|Contributor Name\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           dataset_contributor_email)
               sed -i -re "s|Contributor Email\t.*|Contributor Email\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           dataset_title)
               sed -i -re "s|\tTitle\t.*|\tTitle\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           dataset_release_date)
               sed -i -re "s|Release Date\t.*|Release Date\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           experiment_type)
               sed -i -re "s|Experimental Design\t.*|Experimental Design\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           postnorm_expression_threshold)
               sed -i -re "s|detectionThreshold\t.*|detectionThreshold\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           postnorm_expression_median)
               sed -i -re "s|medianDatasetExpression\t.*|medianDatasetExpression\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           min_replicates)
               sed -i -re "s|minReplicates\t.*|minReplicates\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           max_replicates)
               sed -i -re "s|maxReplicates\t.*|maxReplicates\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           array)
               sed -i -re "s|\tPlatform\t.*|\tPlatform\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           array_probe_count)
               sed -i -re "s|\tprobeCount\t.*|\tprobeCount\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           postnorm_probes_detected)
               if [ "$value" = "0" ]; then
                 value="NA"
               fi
               sed -i -re "s|\tprobesDetected\t.*|\tprobesDetected\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           publication_authors)
               sed -i -re "s|Authors\t.*|Authors\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           publication_citation)
               sed -i -re "s|Publication Citation\t.*|Publication Citation\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           publication_contact)
               sed -i -re "s|Contact Name\t.*|Contact Name\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           publication_contact_email)
               sed -i -re "s|Contact Email\t.*|Contact Email\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           publication_description)
               sed -i -re "s|\tDescription\t.*|\tDescription\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           publication_title)
               sed -i -re "s|Publication Title\t.*|Publication Title\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           publication_pubmed_id)
               sed -i -re "s|PubMed ID\t.*|PubMed ID\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           species_long)
               sed -i -re "s|Organism\t.*$|Organism\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           technology_used)
               sed -i -re "s|technologyUsed\t.*$|technologyUsed\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           top_genes)
               sed -i -re "s|topDifferentiallyExpressedGenes\t.*$|topDifferentiallyExpressedGenes\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           limitSortBy)
               sed -i -re "s|limitSortBy\t.*$|limitSortBy\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
		   ## For backwards compatibility
           lineGraphSampleOrder)
               sed -i -re "s|lineGraphOrdering\t.*$|lineGraphOrdering\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           lineGraphOrdering)
               sed -i -re "s|lineGraphOrdering\t.*$|lineGraphOrdering\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           sampleTypeDisplayOrder)
               sed -i -re "s|sampleTypeDisplayOrder\t.*$|sampleTypeDisplayOrder\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           sampleTypeDisplayGroups)
               sed -i -re "s|sampleTypeDisplayGroups\t.*$|sampleTypeDisplayGroups\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           species_long)
               sed -i -re "s|Organism\t.*$|Organism\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           cells_samples_assayed)
               sed -i -re "s|cellsSamplesAssayed\t.*$|cellsSamplesAssayed\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           yAxisLabel)
               sed -i -re "s|yAxisLabel\t.*$|yAxisLabel\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           probeName)
               sed -i -re "s|probeName\t.*$|probeName\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           cell_surface_antigen_expression)
               sed -i -re "s|cellSurfaceAntigenExpression\t.*$|cellSurfaceAntigenExpression\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           cellular_protein_expression)
               sed -i -re "s|cellularProteinExpression\t.*$|cellularProteinExpression\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           nucleic_acid_extract)
               sed -i -re "s|nucleicAcidExtract\t.*$|nucleicAcidExtract\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
           genepattern_analysis_enabled)
               if [ "$value" = "TRUE" ]; then
                 sed -i -re "s|genePatternAnalysisAccess\t.*$|genePatternAnalysisAccess\tAllow|" "$S4M_DATASET/dataset_metadata.txt"
               else
                 sed -i -re "s|genePatternAnalysisAccess\t.*$|genePatternAnalysisAccess\tDisable|" "$S4M_DATASET/dataset_metadata.txt"
               fi
               ;;
           maxGraphValue)
               ## TODO: Turn this check into a function and apply for ALL fields to create first
               ##       if the attribute does not exist
               grep -P "\tmaxGraphValue" "$S4M_DATASET/dataset_metadata.txt" > /dev/null 2>&1
               if [ $? -eq 0 ]; then
                 sed -i -re "s|maxGraphValue\t.*$|maxGraphValue\t$value|" "$S4M_DATASET/dataset_metadata.txt"
               else
                 dsid=`get_metadata_field s4m_dataset_id`
                 /bin/echo -e "$dsid\tmaxGraphValue\t$value" >> "$S4M_DATASET/dataset_metadata.txt"
               fi
               ;;

           ## New in v1.9 - unknown fields copied / updated into dataset_metadata.txt allowing for easy insertion of new fields
           *)
               if [ "$field" != "dataset_handle" -a "$field" != "s4m_chip_type" -a "$field" != "species_short" -a "$field" != "mapping_id" -a "$field" != "log_2" ]; then
                 dsid=`get_metadata_field s4m_dataset_id`
                 grep -P "\t\Q$field\E\t" "$S4M_DATASET/dataset_metadata.txt" > /dev/null 2>&1
                 if [ $? -eq 0 ]; then
                   ## http://stackoverflow.com/questions/407523/escape-a-string-for-a-sed-replace-pattern
                   searchFieldEscaped=`echo -n "$field" | sed -re 's/[]\/$*.^|[]/\\&/g'`
                   replaceFieldEscaped=`echo -n "$field" | sed -re 's/[\/&]/\\&/g'`
                   replaceValueEscaped=`echo -n "$value" | sed -re 's/[\/&]/\\&/g'`
                   sed -i -re "s/$searchFieldEscaped\t.*$/$replaceFieldEscaped\t$replaceValueEscaped/" "$S4M_DATASET/dataset_metadata.txt"
                 else
                   echo "$dsid	$field	$value" >> "$S4M_DATASET/dataset_metadata.txt"
                 fi
               fi
               ;;
       esac
       ## Do updates for METASTORE attributes that can be blank
       else 
          case $field in
             ## Assay version can be blank
             array_version)
               sed -i -re "s|Assay Version\t.*|Assay Version\tNULL|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
             ## Standard optional fields
             cell_surface_antigen_expression)
                sed -i -re "s|cellSurfaceAntigenExpression\t.*$|cellSurfaceAntigenExpression\tNULL|" "$S4M_DATASET/dataset_metadata.txt"
                ;;
             cellular_protein_expression)
                sed -i -re "s|cellularProteinExpression\t.*$|cellularProteinExpression\tNULL|" "$S4M_DATASET/dataset_metadata.txt"
                ;;
             ## If this isn't set in METASTORE, make sure we set NULL. This should fail validation if mandatory field.
             cells_samples_assayed)
               sed -i -re "s|cellsSamplesAssayed\t.*$|cellsSamplesAssayed\tNULL|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
             ## There are cases where the contributor has no email address
             dataset_contributor_email)
               sed -i -re "s|Contributor Email\t.*|Contributor Email\tNULL|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
             ## non-microarray data may not have a detection threshold
             postnorm_expression_threshold)
               sed -i -re "s|detectionThreshold\t.*|detectionThreshold\tNULL|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
             ## non-microarray data may not have an expression median
             postnorm_expression_median)
               sed -i -re "s|medianDatasetExpression\t.*|medianDatasetExpression\tNULL|" "$S4M_DATASET/dataset_metadata.txt"
               ;;
             ## Datasets can be non-published; in which case the following fields may be left blank
             publication_citation)
                sed -i -re "s|Publication Citation\t.*|Publication Citation\tNULL|" "$S4M_DATASET/dataset_metadata.txt"
                ;;
             publication_contact)
                sed -i -re "s|Contact Name\t.*|Contact Name\tNULL|" "$S4M_DATASET/dataset_metadata.txt"
                ;;
             publication_contact_email)
                sed -i -re "s|Contact Email\t.*|Contact Email\tNULL|" "$S4M_DATASET/dataset_metadata.txt"
                ;;
             publication_pubmed_id)
                sed -i -re "s|PubMed ID\t.*|PubMed ID\tNULL|" "$S4M_DATASET/dataset_metadata.txt"
                ;;
             publication_authors)
                sed -i -re "s|Authors\t.*|Authors\tNULL|" "$S4M_DATASET/dataset_metadata.txt"
                ;;
             ## AE and GEO accessions may both be blank
             dataset_accession_ae)
                sed -i -re "s|\tAE Accession\t.*|\tAE Accession\tNULL|" "$S4M_DATASET/dataset_metadata.txt"
                ;;
             dataset_accession_geo)
                sed -i -re "s|\tGEO Accession\t.*|\tGEO Accession\tNULL|" "$S4M_DATASET/dataset_metadata.txt"
                ;;
             top_genes)
                sed -i -re "s|topDifferentiallyExpressedGenes\t.*$|topDifferentiallyExpressedGenes\tNULL|" "$S4M_DATASET/dataset_metadata.txt"
                ;;
			 ## For backwards compatibility
             lineGraphSampleOrder)
                sed -i -re "s|lineGraphOrdering\t.*$|lineGraphOrdering\tNULL|" "$S4M_DATASET/dataset_metadata.txt"
                ;;
             lineGraphOrdering)
                sed -i -re "s|lineGraphOrdering\t.*$|lineGraphOrdering\tNULL|" "$S4M_DATASET/dataset_metadata.txt"
                ;;
             genepattern_analysis_enabled)
                sed -i -re "s|genePatternAnalysisAccess\t.*$|genePatternAnalysisAccess\tAllow|" "$S4M_DATASET/dataset_metadata.txt"
                ;;
             dataset_release_date)
                sed -i -re "s|Release Date\t.*|Release Date\tNULL|" "$S4M_DATASET/dataset_metadata.txt"
                ;;
             ## New in v1.9 - unknown fields copied / updated into dataset_metadata.txt allowing for easy insertion of new fields
             *)
                if [ "$field" != "dataset_handle" -a "$field" != "s4m_chip_type" -a "$field" != "species_short" ]; then
                  dsid=`get_metadata_field s4m_dataset_id`
                  grep -P "\t\Q$field\E\t" "$S4M_DATASET/dataset_metadata.txt" > /dev/null 2>&1
                  if [ $? -eq 0 ]; then
                    ## http://stackoverflow.com/questions/407523/escape-a-string-for-a-sed-replace-pattern
                    searchFieldEscaped=`echo -n "$field" | sed -re 's/[]\/$*.^|[]/\\&/g'`
                    replaceFieldEscaped=`echo -n "$field" | sed -re 's/[\/&]/\\&/g'`
                    sed -i -re "s/$searchFieldEscaped\t.*$/$replaceFieldEscaped\tNULL/" "$S4M_DATASET/dataset_metadata.txt"
                  else
                    echo "$dsid	$field	NULL" >> "$S4M_DATASET/dataset_metadata.txt"
                  fi
                fi
                ;;
          esac
       fi

    done < "$metastore"
}


update_dataset_from_metastore () {

    metastore=`get_metastore_path`
    if [ ! -f "$metastore" -o ! -f "$S4M_DATASET/dataset.txt" ]; then
        return
    fi

    ## Pre dataset v1.91 format check - legacy "dataset.txt" with < 7 columns
    dataset_cols=`awk -F'\t' '{print NF}' "$S4M_DATASET/dataset.txt" | tr -d [:cntrl:]`
    if [ $dataset_cols -lt 10 ]; then
        ## Changed in v1.93 for T#2626
	## Changed in v1.95 for T#2914
        echo "Error: dataset.txt has < 10 columns, expecting:"
        echo "  <ds_id>	<release_date>	<handle>	<chip_type>	<db_id>	<min_y_axis>	<show_yugene>	<mapping_id>	<log_2>	<data_type_id>"
        echo
        echo "Please check/add missing column(s) in dataset.txt and run:   s4m.sh --update."
        exit 1
    fi

    while read line
    do
       field=`echo "$line" | awk -F'=' '{print $1}'`
       value=`echo "$line" | sed -r -e 's/^\w+\=//g'`
       ## Set "NULL" values to blank so NULL strings don't get propagated to dataset.txt
       if [ "$value" = "NULL" ]; then
          value=""
       fi

       case $field in
           dataset_handle)
               if [ -z "$value" ]; then
                 echo "Error: METASTORE missing 'dataset_handle' field!"
                 exit 1
               fi
               awk -F'\t' -v c="$value" '{print $1"\t"$2"\t"c"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10}' "$S4M_DATASET/dataset.txt" > "$S4M_DATASET/.s4mtmp"
               mv "$S4M_DATASET/.s4mtmp" "$S4M_DATASET/dataset.txt"
               ;;
           dataset_release_date)
               if [ -z "$value" ]; then
                 ## Dummy date way into the future (can't be blank or NULL)
                 ## TODO: Remove from "datasets" table, serves no purpose
                 value="2030-01-01"
               fi
               awk -F'\t' -v c="$value" '{print $1"\t"c"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10}' "$S4M_DATASET/dataset.txt" > "$S4M_DATASET/.s4mtmp"
               mv "$S4M_DATASET/.s4mtmp" "$S4M_DATASET/dataset.txt"
               ;;
           s4m_dataset_id)
               if [ -z "$value" ]; then
                 echo "Error: METASTORE missing 's4m_dataset_id' field!"
                 exit 1
               fi
               awk -F'\t' -v c="$value" '{print c"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10}' "$S4M_DATASET/dataset.txt" > "$S4M_DATASET/.s4mtmp"
               mv "$S4M_DATASET/.s4mtmp" "$S4M_DATASET/dataset.txt"
               ;;
           s4m_chip_type)
               if [ -z "$value" ]; then
                 echo "Error: METASTORE missing 's4m_chip_type' field!"
                 exit 1
               fi
               awk -F'\t' -v c="$value" '{print $1"\t"$2"\t"$3"\t"c"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10}' "$S4M_DATASET/dataset.txt" > "$S4M_DATASET/.s4mtmp"
               mv "$S4M_DATASET/.s4mtmp" "$S4M_DATASET/dataset.txt"
               ;;
           mapping_id)
               if [ -z "$value" ]; then
                 echo "Error: METASTORE missing 'mapping_id' field!"
                 exit 1
               fi
               awk -F'\t' -v c="$value" '{print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"c"\t"$9"\t"$10}' "$S4M_DATASET/dataset.txt" > "$S4M_DATASET/.s4mtmp"
               mv "$S4M_DATASET/.s4mtmp" "$S4M_DATASET/dataset.txt"
               ;; 
           db_id)
               if [ -z "$value" ]; then
                 echo "Error: METASTORE file missing 'db_id' field!"
                 exit 1
               fi
               awk -F'\t' -v c="$value" '{print $1"\t"$2"\t"$3"\t"$4"\t"c"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10}' "$S4M_DATASET/dataset.txt" > "$S4M_DATASET/.s4mtmp"
               mv "$S4M_DATASET/.s4mtmp" "$S4M_DATASET/dataset.txt"
               ;;

           log_2)
               if [ -z "$value" ]; then
                 echo "Error: METASTORE missing 'log_2' field!"
                 exit 1
               fi
               awk -F'\t' -v c="$value" '{print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"c"\t"$10}' "$S4M_DATASET/dataset.txt" > "$S4M_DATASET/.s4mtmp"
               mv "$S4M_DATASET/.s4mtmp" "$S4M_DATASET/dataset.txt"
               ;;
           minGraphValue)
               if [ -z "$value" ]; then
                 floor=0
               else
                 floor=`echo "$value" | cut -d'.' -f 1`
                 echo "$value" | grep -P "^\-" > /dev/null 2>&1
                 # number is negative, subtract 1
                 if [ $? -eq 0 ]; then
                   floor=`expr "$floor" - 1`
                 # number is positive, make min = 0
                 else
                   floor=0
                 fi
               fi
               awk -F'\t' -v c="$floor" '{print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"c"\t"$7"\t"$8"\t"$9"\t"$10}' "$S4M_DATASET/dataset.txt" > "$S4M_DATASET/.s4mtmp"
               mv "$S4M_DATASET/.s4mtmp" "$S4M_DATASET/dataset.txt"
               ;;
           technology_used)
               do_yugene="false"
               if [ "$value" = "microarray" ]; then
                 do_yugene="true"
               fi
               awk -F'\t' -v c="$do_yugene" '{print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"c"\t"$8"\t"$9"\t"$10}' "$S4M_DATASET/dataset.txt" > "$S4M_DATASET/.s4mtmp"
               mv "$S4M_DATASET/.s4mtmp" "$S4M_DATASET/dataset.txt"
               ;;
           data_type_id)
               if [ -z "$value" ]; then
                 echo "Error: METASTORE file missing 'data_type_id' field!"
                 exit 1
               fi 
               awk -F'\t' -v c="$value" '{print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\t"c}' "$S4M_DATASET/dataset.txt" > "$S4M_DATASET/.s4mtmp"
               mv "$S4M_DATASET/.s4mtmp" "$S4M_DATASET/dataset.txt"
               ;;
       esac

    done < "$metastore"
}

update_biosamples_metadata_from_metastore () {
    metastore=`get_metastore_path`
    if [ ! -f "$metastore" -o ! -f "$S4M_DATASET/biosamples_metadata.txt" ]; then
        return
    fi
    while read line
    do
       field=`echo "$line" | awk -F'=' '{print $1}'`
       value=`echo "$line" | sed -r -e 's/^\w+\=//g'`
       ## Set "NULL" values to blank so NULL strings don't get propagated to dataset_metadata.txt
       if [ "$value" = "NULL" ]; then
          value=""
       fi

       if [ ! -z "$value" ]; then
       case $field in
           s4m_chip_type)
               if [ ! -z "$value" ]; then
                 awk -F'\t' -v c="$value" '{print $1"\t"c"\t"$3"\t"$4"\t"$5}' "$S4M_DATASET/biosamples_metadata.txt" > "$S4M_DATASET/.s4mtmp"
                 mv "$S4M_DATASET/.s4mtmp" "$S4M_DATASET/biosamples_metadata.txt"
               fi
               ;;
           s4m_dataset_id)
               if [ ! -z "$value" ]; then
                 awk -F'\t' -v c="$value" '{print c"\t"$2"\t"$3"\t"$4"\t"$5}' "$S4M_DATASET/biosamples_metadata.txt" > "$S4M_DATASET/.s4mtmp"
                 mv "$S4M_DATASET/.s4mtmp" "$S4M_DATASET/biosamples_metadata.txt"
               fi
               ;;
           species_long)
               sed -i -re "s|Organism\t.*$|Organism\t$value|" "$S4M_DATASET/biosamples_metadata.txt"
               ;;
          esac
       fi

    done < "$metastore"
}

update_biosamples_from_metastore () {
    metastore=`get_metastore_path`
    if [ ! -f "$metastore" -o ! -f "$S4M_DATASET/biosamples.txt" ]; then
        return
    fi
    while read line
    do
       field=`echo "$line" | awk -F'=' '{print $1}'`
       value=`echo "$line" | sed -r -e 's/^\w+\=//g'`
       ## Set "NULL" values to blank so NULL strings don't get propagated to dataset_metadata.txt
       if [ "$value" = "NULL" ]; then
          value=""
       fi

       if [ ! -z "$value" ]; then
       case $field in
           s4m_chip_type)
               if [ ! -z "$value" ]; then
                 awk -F'\t' -v c="$value" '{print $1"\t"c"\t"$3"\t"$4"\t"$5"\t"$6}' "$S4M_DATASET/biosamples.txt" > "$S4M_DATASET/.s4mtmp"
                 mv "$S4M_DATASET/.s4mtmp" "$S4M_DATASET/biosamples.txt"
               fi
               ;;
           s4m_dataset_id)
               if [ ! -z "$value" ]; then
                 awk -F'\t' -v c="$value" '{print $1"\t"$2"\t"c"\t"$4"\t"$5"\t"$6}' "$S4M_DATASET/biosamples.txt" > "$S4M_DATASET/.s4mtmp"
                 mv "$S4M_DATASET/.s4mtmp" "$S4M_DATASET/biosamples.txt"
               fi
               ;;
          esac
       fi

    done < "$metastore"
}


update_errata_files_from_metastore () {
    ## Update 'VERSION' and 'ACCESSION' (and similar) files from METASTORE as required
    metastore=`get_metastore_path`
    if [ ! -f "$metastore" ]; then
        return
    fi
    if [ ! -f "$S4M_DATASET/VERSION" ]; then
        init_version_file
    fi
    while read line
    do
       field=`echo "$line" | awk -F'=' '{print $1}'`
       value=`echo "$line" | sed -r -e 's/^\w+\=//g'`
       ## Set "NULL" values to blank so NULL strings don't get propagated to dataset_metadata.txt
       if [ "$value" = "NULL" ]; then
          value=""
       fi

       if [ ! -z "$value" ]; then
       case $field in
           accession_id)
               echo "$value" > "$S4M_DATASET/ACCESSION"
               ;;
           s4m_curator)
               sed -i -re "s|^Curator\=.*|Curator\=$value|" "$S4M_DATASET/VERSION"
               ;;
           s4m_curation_date)
               sed -i -re "s|^CurationDate\=.*|CurationDate\=$value|" "$S4M_DATASET/VERSION"
               ;;
          esac
       fi

    done < "$metastore"
} 


update_files_from_metastore () {
    update_dataset_from_metastore
    update_dataset_metadata_from_metastore
    update_biosamples_from_metastore
    update_biosamples_metadata_from_metastore
    update_errata_files_from_metastore
}


## TODO: Check and update, not tested in AGES.
dataset_status () {
    if [ -f "$S4M_DATASET/VERSION" ]; then
        created=`grep -P "^Date\=" "$S4M_DATASET/VERSION" | awk -F'=' '{print $2}'`
        s4m_ver=`grep -P "^FormatVersion\=" "$S4M_DATASET/VERSION" | awk -F'=' '{print $2}'`
        dataset_ver=`grep -P "^DatasetVersion\=" "$S4M_DATASET/VERSION" | awk -F'=' '{print $2}'`
        #curator=`grep -P "^Curator\=" "$S4M_DATASET/VERSION" | awk -F'=' '{print $2}'`
    ## Bad directory, or not a Stemformatics dataset?
    else
        created="N/A"
        s4m_ver="N/A"
        dataset_ver="N/A"
        echo; echo "** WARNING: Target does not appear to be a valid Stemformatics dataset (not found, or malformed) **"
    fi
    if [ -z "$curator" ]; then
        curator="N/A"
    fi

    raw_file_count=`ls -a "$S4M_DATASET/source/raw" | wc -l 2>/dev/null`
    has_raw="N/A"
    if [ $? -eq 0 ]; then
        if [ "$raw_file_count" -gt 2 ]; then
            has_raw="Yes" 
        else
            has_raw="No" 
        fi
    fi

    normalized="N/A"
    normalized_file_count=`ls -a "$S4M_DATASET/source/normalized" | wc -l 2>/dev/null`
    if [ $? -eq 0 ]; then
        if [ "$normalized_file_count" -gt 2 ]; then
            normalized="Yes" 
        else
            normalized="No" 
        fi
    fi

    thresholded="No"
    if [ -f "$S4M_DATASET/source/normalized/THRESHOLD" -a -f "$S4M_DATASET/source/normalized/MEDIAN_ABOVE_THRESHOLD" ]; then
      thresholded="Yes"
    fi

    annotation_count=`get_metastore_annotated_count`
    total_count=`get_metastore_mandatory_annotation_count`
    if [ "$annotation_count" -eq "$total_count" ]; then
        annotation_status="Complete ($annotation_count/$total_count)"
    else
        if [ "$annotation_count" -gt 0 ]; then
            annotation_status="Partially Complete ($annotation_count/$total_count)"
        else
            annotation_status="Not Present (0/$total_count)"
        fi
    fi

    if [ -f "$S4M_DATASET/VALIDATED" ]; then
        validation_timestamp=`cat "$S4M_DATASET/VALIDATED" | tr -d [:cntrl:]`
        validation_status="Validated (timestamp: $validation_timestamp)"
    else
        validation_status="Not Validated / Unknown"
    fi

    echo
    echo "  Stemformatics Dataset Format: $s4m_ver"
    echo "  Dataset Revision: $dataset_ver"
    echo "  Created: $created"
    echo "  Curated By: $curator"
    echo "  Raw Expression Data Present? $has_raw"
    echo "  Normalized: $normalized"
    echo "  Thresholded: $thresholded"
    echo "  Mandatory Annotation: $annotation_status"
    echo "  Validation Status: $validation_status"
    echo
}


init_accession_file () {
    ## ACCESSION
    echo -n "<accession_id>" > "$S4M_DATASET/ACCESSION"
    today=`date +"%Y-%m-%d"`
}


init_version_file () {
    ## VERSION
    echo "FormatVersion=$S4M_FORMAT_VERSION
DatasetVersion=1.0
Date=$today" > "$S4M_DATASET/VERSION"
}


## Updates format version in VERSION file to latest
update_version_file () {
    if [ -f "$S4M_DATASET/VERSION" ]; then
        sed -i -r -e "s/^FormatVersion\=.*/FormatVersion\=$S4M_FORMAT_LATEST_VERSION/" "$S4M_DATASET/VERSION"
    fi
}


init_install_file () {
    # INSTALL.txt
    /bin/echo -e -n "    DATASET UPLOAD HOWTO

 1. Ensure dataset is finalised (s4m.sh -f <dataset_dir>)

 2. Ensure dataset is validated (s4m.sh -v <dataset_dir>)

 3. If validation succeeded, load dataset into target host:
    s4m.sh --load <dataset_dir>  -C <profile_name>

    (See: etc/profile/*.conf for possible profiles)

 4. If dataset files were not installed at the end of the
    previous step, install it now:
    s4m.sh --install <dataset_dir> [user@]host:/path/to/SHARED]

 5. If dataset setup failed in step 4, manually run required setup
    routine via Stemformatics admin interface

" > "$S4M_DATASET/INSTALL.txt"

}


init_errata_files () {
    init_accession_file
    init_version_file
    init_install_file
}


init_biosamples_metadata_file () {
    # biosamples_metadata.txt
    /bin/echo -e -n "<ds_id>\t<chip_type>\t<chip_id>\tCell Line\t<cell_line_or_NULL>
<ds_id>\t<chip_type>\t<chip_id>\tCell Type\t<cell_type_ontology_term>
<ds_id>\t<chip_type>\t<chip_id>\tOrganism\t<Homo sapiens|Mus musculus>
<ds_id>\t<chip_type>\t<chip_id>\tOrganism Part\t<sample_anatomy_part_or_NULL>
<ds_id>\t<chip_type>\t<chip_id>\tDisease State\t<disease_name_or_normal>
<ds_id>\t<chip_type>\t<chip_id>\tLabelling\t<antibody labelling protein e.g. \"Cy3 dye\" or NULL>
<ds_id>\t<chip_type>\t<chip_id>\tAge\t<sample patient or specimen age_or_NULL>
<ds_id>\t<chip_type>\t<chip_id>\tGender\t<gender_if_available_or_NULL>
<ds_id>\t<chip_type>\t<chip_id>\tDevelopmental Stage\t<adult_or_embryo_etc_or_NULL>
<ds_id>\t<chip_type>\t<chip_id>\tSample Description\t<curated_sample_description>
<ds_id>\t<chip_type>\t<chip_id>\tSource Name\t<original_sample_name>
<ds_id>\t<chip_type>\t<chip_id>\tSample Type\t<curated_sample_type_name>
<ds_id>\t<chip_type>\t<chip_id>\tReplicate Group ID\t<curated_sample_id>
<ds_id>\t<chip_type>\t<chip_id>\tReplicate\t<replicate_number_usually_1>
<ds_id>\t<chip_type>\t<chip_id>\tReprogramming Method\t<cell_reprogramming_method_or_NULL>
<ds_id>\t<chip_type>\t<chip_id>\tDerived From\t<cell_derived_from_or_NULL>
<ds_id>\t<chip_type>\t<chip_id>\tDifferentiated To\t<cell_differentiated_to_or_NULL>
<ds_id>\t<chip_type>\t<chip_id>\tCells Sorted For\t<cells_sorted_for_or_NULL>
<ds_id>\t<chip_type>\t<chip_id>\tTime\t<time_point_or_remove>
<ds_id>\t<chip_type>\t<chip_id>\tDay\t<time_point_day_or_remove>
<ds_id>\t<chip_type>\t<chip_id>\tTissue\t<tissue_type_ontology_term_or_NULL>
<ds_id>\t<chip_type>\t<chip_id>\tGenetic Modification\t<genetic_modification_details_or_remove>
<ds_id>\t<chip_type>\t<chip_id>\tPregnancy Status\t<optional_if_appropriate_for_mouse_studies_or_remove>
<ds_id>\t<chip_type>\t<chip_id>\tGEO Sample Accession\t<optional_if_available_or_remove>
" > "$S4M_DATASET/biosamples_metadata.txt"
}


init_dataset_metadata_file () {
    # dataset_metadata.txt
    /bin/echo -e -n "<dataset_id>\tAccession\t<accession_id>
<dataset_id>\tAE Accession\t<optional_remove_if_not_applicable>
<dataset_id>\tGEO Accession\t<optional_remove_if_not_applicable>
<dataset_id>\tTitle\t<dataset_title>
<dataset_id>\tPublication Title\t<publication_title>
<dataset_id>\tPublication Citation\t<publication_citation>
<dataset_id>\tContact Name\t<publication_contact_person/s>
<dataset_id>\tContact Email\t<publication_contact_email/s>
<dataset_id>\tPubMed ID\t<pubmed_id>
<dataset_id>\tDescription\t<study_description>
<dataset_id>\tAuthors\t<authors_list>
<dataset_id>\tContributor Name\t<contributor_name>
<dataset_id>\tContributor Email\t<contributor_email>
<dataset_id>\tAffiliation\t<contributor_affiliation>
<dataset_id>\tRelease Date\t<dataset_release_date>
<dataset_id>\tOrganism\t<Homo sapiens|Mus musculus>
<dataset_id>\tExperimental Design\t<e.g. Transcription profiling by array>
<dataset_id>\tAssay Manufacturer\t<array_manufacturer>
<dataset_id>\tAssay Platform\t<array_platform>
<dataset_id>\tAssay Version\t<array_version>
<dataset_id>\tPlatform\t<full array platform description Man + Plat + Version>
<dataset_id>\ts4mCurator\t<dataset curator name/s>
<dataset_id>\ts4mCurationDate\t<dataset curation date (usually start date of metadata curation effort>
<dataset_id>\tlineGraphOrdering\t<Ordered JSON dict of biosamples meta 'Day' values for line graph {"Day 0":1,"Day 02":2,"Day 05":3}>
<dataset_id>\tsampleTypeDisplayOrder\t<Order of sample types for frontend graphing e.g. Sample Type 1[,Sample Type 2]>
<dataset_id>\tsampleTypeDisplayGroups\t<JSON dict of sample type groupings indexed by sample type name e.g. {\"Sample Type 1\":0,\"Sample Type 2\":0,\"Sample Type 3\":1}>
<dataset_id>\tlimitSortBy\t<Sample Type[,OtherAttributes]>
<dataset_id>\tprobeCount\t<probe_count>
<dataset_id>\tprobesDetected\t<detected_count>
<dataset_id>\tminReplicates\t1
<dataset_id>\tmaxReplicates\t<max_replicate_count>
<dataset_id>\ttopDifferentiallyExpressedGenes\t<comma_separated_ensembl_ids_atleast_5>
<dataset_id>\tdetectionThreshold\t<detection_threshold_value>
<dataset_id>\tmedianDatasetExpression\t<median_expression_value>
<dataset_id>\ttechnologyUsed\t<assay_technology> 
<dataset_id>\tcellsSamplesAssayed\t<Cell / sample type list for front-end experiment browser display>
<dataset_id>\tcellSurfaceAntigenExpression\t<cell_surface_antigen_expression> 
<dataset_id>\tcellularProteinExpression\t<cellular_protein_expression> 
<dataset_id>\tnucleicAcidExtract\t<nucleic_acid_extract>
<dataset_id>\tgenePatternAnalysisAccess\t<Disable/Allow (or leave blank for default enabled)>
<dataset_id>\tmaxGraphValue\t<dataset max expression value (for max Y axis)>
<dataset_id>\tyAxisLabel\t<y axis label>
<dataset_id>\tprobeName\t<probe name>
" > "$S4M_DATASET/dataset_metadata.txt"
}


init_biosamples_file () {
    /bin/echo -e "<chip_id>\t<chip_type>\t<dataset_id>\t<sample_alias>\t<sample_accession_id>\t<sample_id>" > "$S4M_DATASET/biosamples.txt"
}


init_dataset_file () {
    /bin/echo -e "<s4m_dataset_id>\t<dataset_release_date>\t<dataset_handle>\t<chip_type>\t<db_id>\t<min_y_axis>\t<show_yugene>\t<mapping_id>\t<log_2>\t<data_type_id>" > "$S4M_DATASET/dataset.txt"
}


init_sample_pheno_template_file () {
    /bin/echo -e "<chip_id>\t<pheno_group_name>
..." > "$S4M_DATASET/sample_pheno.txt.template"
}


## Output dataset format files for current revision.
## Interactive - prompts curator for some basic information to pre-populate file contents.
init_files () {
    if [ -d "$S4M_DATASET/source/raw" -o -d "$S4M_DATASET/source/normalized" ]; then
       echo "Error: Dataset directory '$S4M_DATASET' already initialised!"; echo
       exit 1
    fi
    mkdir -p "$S4M_DATASET/source/raw"
    mkdir -p "$S4M_DATASET/source/normalized"

    init_metastore

    echo -n "Writing files.. "

    init_errata_files
    init_biosamples_file
    init_dataset_file
    init_dataset_metadata_file
    init_biosamples_metadata_file
    init_sample_pheno_template_file
    ##  NOTE: There is no "chip_id_replicate_group_map.txt.template" creation any more.
    ##  Since v1.3+ we don't need it because it's auto-generated from biosamples_metadata.txt

    echo "Done"
}


normalize () {
    dataset_path="$1"
    chip_type=`get_metadata_field "s4m_chip_type"`

    array_type=""
    normalize_args=""

    ## Check that our platform / chip type data path is known
    if [ -z "$S4M_DATA" ]; then
      echo "Error: s4m data directory not set! Please set environment variable 'S4M_DATA' to data path, i.e."
      echo "    export S4M_DATA=\"/path/to/s4m_data\""; echo
      echo "NOTE: The installation script ($S4M_HOME/install.sh) normally sets this for you."; echo
      exit 1
    fi

    ## Check chip type is annotated
    if [ -z "$chip_type" ]; then
      echo "Error: Failed to determine array chip type from metadata store. Please annotate the array"
      echo "       platform and try again."; echo
      ## DEBUG ## 
      #echo "DEBUG: Got chip_type=[$chip_type]"; echo
      exit 1
    fi

    ## Check Accession ID dependency 
    accession=`get_metadata_field "accession_id"`
    if [ -z "$accession" ]; then
      echo "Error: Accession ID is not set! Please (re)annotate and try again."; echo
      exit 1
    fi

    ## Determine array type via chip type from master assay platforms table
    grep -P "^$chip_type\t" "$S4M_HOME/etc/assay_platforms.txt" | grep "Affymetrix" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
      grep -P "^$chip_type\t" "$S4M_HOME/etc/assay_platforms.txt" | awk -F'\t' '{print $4}' | grep -i st > /dev/null 2>&1
      if [ $? -eq 0 ]; then
        ## Got Affymetrix ST
        array_type=3
        echo ST $array_type
      else
        ## Got Affymetrix GeneChip (non-ST)
        array_type=2
        ## This control flow is a bit crude, theres probably a better way to implement this
        grep -P "^$chip_type\t" "$S4M_HOME/etc/assay_platforms.txt" | grep -i "PrimeView" > /dev/null 2>&1
        if [ $? -eq 0 ]; then
        ## Got Affymetrix PrimeView
          array_type=4
        fi
      fi
    else
      ## Got Illumina
      array_type=1
    fi

    case "$array_type" in
        ## Illumina
        1)
            ;;

        ## Affy GeneChip (APT or simpleaffy/GC-RMA)
        2)
            ## Force APT pipeline vs "simpleaffy" GC-RMA if APT given
            if [ ! -z "$S4M_APT" ]; then
              annotation_chip_types="$S4M_DATA/.AromaAffymetrix/annotationData/chipTypes"
              cdf_name=`grep -P "^$chip_type\t" "$S4M_HOME/etc/assay_platforms.txt" | awk -F'\t' '{print $6}'`
              annotation_dir="$annotation_chip_types/$cdf_name"
              if [ ! -d "$annotation_dir" ]; then
                echo "Error: Annotation 'cdf' directory for chip type $chip_type not found!"
                echo "Expected Path: $annotation_dir"
                echo "(APT and Aroma pipelines share common platform annotation data files)"
                exit 1
              fi
              normalize_args="$annotation_dir"

            ## "simpleaffy" args are set here.
            else
              if [ -d "$S4M_HOME/.RData" -a -w "$S4M_HOME/.RData" ]; then
                probe_affinity_store="$S4M_HOME/.RData"
              else
                mkdir -p "$HOME/.s4m/.RData"
                probe_affinity_store="$HOME/.s4m/.RData"
              fi
              normalize_args="$probe_affinity_store"
            fi
            ;;

        ## Affy ST (Gene and Ex)
        3)
            annotation_chip_types="$S4M_DATA/.AromaAffymetrix/annotationData/chipTypes"
            cdf_name=`grep -P "^$chip_type\t" "$S4M_HOME/etc/assay_platforms.txt" | awk -F'\t' '{print $6}'`
            annotation_dir="$annotation_chip_types/$cdf_name"
            if [ ! -d "$annotation_dir" ]; then
                echo "Error: Annotation 'cdf' directory for chip type $chip_type not found!"
                echo "Expected Path: $annotation_dir"
                echo "(APT and Aroma pipelines share common platform annotation data files)"
                exit 1
            fi
            normalize_args="$annotation_dir"
            ;;
        
        ## Affy PrimeView
        4)
            ## Unlike other Affymetrix platforms, no additional parameters necessary
            ## Currently does not support the APT alternate workflow
            ;;

        
        ## Unknown
        *)
            echo "Error: Unkown array type ID '$array_type'!"
            usage
            ;;
    esac

    if [ -w "$dataset_path" ]; then
        cd "$S4M_HOME/dataset_scripts" && ./normalize_dataset.sh "$dataset_path" "$chip_type" "$normalize_args"
        normreturn=$?
        cd $OLDPWD

        ## If Affymetrix ST check for ability to auto-set threshold from ANTIGENOMIC_MEDIAN value
        ## FIXME: is there a way to do a nested "and-or" statement in bash?
        ## eg if foo = 1 and (bar = 2 or spam = 3)
        ## brief search turned up nothing so ill just nest the "or" statement
        if [ $normreturn -eq 0 ]; then
           if [ $array_type -eq 3 -o $array_type -eq 4 ]; then
              if [ -f "$dataset_path/source/normalized/ANTIGENOMIC_MEDIAN" ]; then
                 echo "Auto-applying threshold based on antigenomic probe median.."
                 set_detection_threshold `cat "$dataset_path/source/normalized/ANTIGENOMIC_MEDIAN" | tr -d [:cntrl:]` TRUE
                 update_metastore_threshold
              fi
           fi
        fi
        ## If normalization completed successfully, remind user to finalize
        if [ $normreturn -eq 0 ]; then
           echo
           echo "Normalization and/or QC complete; don't forget to finalize:"
           echo "   $ s4m.sh --finalize <dataset_dir>"; echo
        else
          echo
          echo "There were error(s) during normalization and/or QC."; echo
          exit 1
        fi
    else
        echo "Error: Dataset path '$dataset_path' is not writeable, aborting Normalization."; echo
        exit 1
    fi
}


generate_cls () {
    if [ -f "$S4M_DATASET/probe_expression_avg_replicates.txt" ]; then
        if [ -w "$S4M_DATASET" ]; then
            accession=`get_metadata_field "accession_id"`
            if [ -z "$accession" ]; then
                echo "Error: Accession ID is not set! Please (re)annotate and try again."; echo
                exit 1
            fi
            echo -n "Generating CLS file/s.. "
            ### DEBUG ###
            #echo "DEBUG: Generating CLS file: "
            #echo "    pwd=[`pwd`]"
            #echo "    S4M_DATASET=[$S4M_DATASET]"
            #echo "    cmd=[$S4M_HOME/dataset_scripts/util/dataset_to_cls.pl $S4M_DATASET $S4M_DATASET/$accession.cls"; echo
            ### END DEBUG ###
            "$S4M_HOME/dataset_scripts/util/dataset_to_cls.pl" "$S4M_DATASET" "$S4M_DATASET/$accession.cls"
            echo "done"
        else
            echo "Error: Dataset path $S4M_DATASET is not writeable!"; echo
            exit 1
        fi
    else
        echo "WARNING: No 'probe_expression_avg_replicates.txt', skipping CLS generation.."; echo
    fi
}


generate_gct () {
    echo "Generating GCT.."
    data_type_id=`get_metadata_field "data_type_id"`
    if [ -z "$data_type_id" ]; then
        echo "Error: Failed to determine dataset type (data_type_id field in METASTORE)! Please (re)annotate and try again."; echo
        exit 1
    fi
    accession=`get_metadata_field "accession_id"`
    if [ -z "$accession" ]; then
        echo "Error: Accession ID is not set! Please (re)annotate and try again."; echo
        exit 1
    fi
    touch "$S4M_DATASET/${accession}.gct" > /dev/null 2>&1
    if [ $? -ne 0 ]; then
        echo "Error: Dataset path $S4M_DATASET, or existing GCT file is not writeable!"; echo
        exit 1
    fi

    ## Not a single cell dataset, create GCT using usual method
    if [ -f "$S4M_DATASET/probe_expression_avg_replicates.txt" -a "$data_type_id" != "3" ]; then
        "$S4M_HOME/dataset_scripts/util/dataset_to_gct.sh" "$S4M_DATASET" "$S4M_DATASET/$accession.gct"

    ## Single cell, generate GCT from normalized_expression.txt (fast implementation for large N cells/samples)
    elif [ "$data_type_id" = "3" ]; then
        if [ -f "$S4M_DATASET/$accession.gct" ]; then
            echo "Found existing GCT '$S4M_DATASET/$accession.gct', doing nothing.."
            echo "Done"
            return 0
        elif [ ! -f "$S4M_DATASET/source/normalized/normalized_expression.txt" ]; then
            echo "Error: No normalized expression found in path '$S4M_DATASET/source/normalized/normalized_expression.txt' (required for GCT creation)"; echo
            exit 1
        fi
        norm_file="$S4M_DATASET/source/normalized/normalized_expression.txt"
        gct_out="$S4M_DATASET/${accession}.gct"
        ngenes=`tail -n+2 "$norm_file" | wc -l`
        nsamples=`head -1 "$norm_file" | awk -F'\t' '{print NF}'`
        echo "#1.2" > "$gct_out"
        /bin/echo -e "$ngenes\t$nsamples" >> "$gct_out"
        samplenames=`head -1 "$norm_file"`
        /bin/echo -e "NAME\tDescription\t$samplenames" >> "$gct_out"
        tail -n+2 "$norm_file" | awk -F'\t' 'BEGIN{ ORS="" }; {print $1 "\tNA"; for (i=2;i<NF;i++) { print "\t" $i }; print "\n" }' >> "$gct_out"
    fi
    echo "Done"
    return 0
}


## YuGene transform from GCT file
generate_cumulative_gct () {
    if [ -f "$S4M_DATASET/$accession.gct" ]; then
        if [ -w "$S4M_DATASET" ]; then
            accession=`get_metadata_field "accession_id"`
            if [ -z "$accession" ]; then
                echo "Error: Accession ID is not set! Please (re)annotate and try again."; echo
                exit 1
            fi
	    "$S4M_HOME/dataset_scripts/x_platform/cumulative_gct.sh" "$S4M_HOME/dataset_scripts/x_platform/cumulative_gct.R" "$S4M_DATASET" "$accession.gct"
        else
            echo "Error: Dataset path $S4M_DATASET is not writeable!"; echo
            exit 1
        fi
    fi
}


## YuGene transform from bg-corrected raw_expression.txt file
generate_cumulative_bgonly () {
    technology_used=`get_metadata_field "technology_used"`

    if [ -f "$S4M_DATASET/source/normalized/raw_expression.txt" -a "$technology_used" = "microarray" ]; then
        if [ -w "$S4M_DATASET" ]; then
            accession=`get_metadata_field "accession_id"`
            if [ -z "$accession" ]; then
                echo "Error: Accession ID is not set! Please (re)annotate and try again."; echo
                exit 1
            fi
            (cd "$S4M_DATASET/source/normalized" && ln -s raw_expression.txt "${accession}_bgonly.txt" > /dev/null 2>&1)
            "$S4M_HOME/dataset_scripts/x_platform/cumulative_datatable.sh" "$S4M_HOME/dataset_scripts/x_platform/cumulative_datatable.R" "$S4M_DATASET" "./source/normalized/${accession}_bgonly.txt"
        else
            echo "Error: Dataset path $S4M_DATASET is not writeable!"; echo
            exit 1
        fi
    else
      echo "Skipping 'bgonly' cumulative (YuGene) file generation - not a microarray dataset"; echo
    fi
}


## Download probe mapping from Stemformatics and return file path OR
## error log if error.
##
## UPDATE: 2017-08-31: The "mapping_id" parameter replaces "chip_type" since we recently stopped
##	using assay_platforms table in Stemformatics to determine mappings - all datasets now have
##	a "mapping_id" METASTORE field.
##
get_probe_mapping_for_mapping_id () {
    mapping_id="$1"
    ## path to pgconfig file
    pgconf="$2"

    ## load database routines
    . "$S4M_HOME/dataset_scripts/include/funcs.sh"
    if ! check_pgloader; then
        return 1
    fi
    if ! load_database_settings "$pgconf"; then
        return 1
    fi
    if ! test_db "$pgconf"; then
        return 1
    fi

    mapdir="$S4M_DATA/tmp/probe_mappings"
    mkdir -p "$mapdir"
    database_exec "copy (select to_id, from_id from stemformatics.feature_mappings where mapping_id=$mapping_id order by from_id asc) to stdout"  > "$mapdir/${mapping_id}.tsv" 2> "$mapdir/${mapping_id}.err"
    if [ $? -ne 0 ]; then
        echo "Failed fetching probe mappings for mapping_id $mapping_id:" 1>&2
        cat "$mapdir/${mapping_id}.err" 1>&2
        return 1
    else
        rowsReturned=`grep "\w" "$mapdir/${mapping_id}.tsv" | wc -l`
        if [ $rowsReturned -lt 1 ]; then
            echo "Failed fetching probe mappings for mapping_id $mapping_id - there were no mappings in target database!" 1>&2
            return 1
        fi
    fi
    echo "$mapdir/${mapping_id}.tsv"
}


generate_rohart_from_cumulative_bgonly () {
    ## Project tag or label, it must match part of the target model RData file amongst other things
    project="$1"
    target_species="$2"
    ## Threshold above which a Rohart score for this project is said to be of target type
    upperThreshold="$3"
    ## Threshold below which a Rohart score for this project is said to be NOT of target type
    lowerThreshold="$4"
    ## Number of times to subsample in Rohart prediction function
    subsamples="$5"

    echo "Generating Rohart score file for '$project' project [NOTE: this can take several minutes or more] .."

    if [ -z "$project" -o -z "$target_species" -o -z "$upperThreshold" -o -z "$lowerThreshold" ]; then
        echo "Fatal Error: Internal: generate_rohart_from_cumulative_bgonly(): Not enough arguments!"
        exit 1
    fi
    ## Default subsampling rate if not supplied
    if [ -z "$subsamples" ]; then
        subsamples=100
    fi

    ## S4M data location must have Rdata model which matches input params
    if [ ! -f "$S4M_DATA/data/rohart/$target_species/$project.model.Rdata" ]; then
        echo "Error: Model Rdata file '$S4M_DATA/data/rohart/$target_species/$project.model.Rdata' not found!"
        return 1
    fi

    ds_species=`get_metadata_field "species_short"`
    if [ -z "$ds_species" ]; then
        echo "Error: Failed to determine species from dataset annotation, please (re)annotate and try again."
        return 1
    fi
    if [ $ds_species != "$target_species" ]; then
        echo "Notice: Skipping Rohart scores file generation because dataset species is not $target_species."
        return
    fi

    ## Need the "bgonly" cumulative file as input
    accession=`get_metadata_field "accession_id"`
    technology=`get_metadata_field "technology_used"`
    if [ ! -f "$S4M_DATASET/${accession}_bgonly.cumulative.txt" -a "$technology" = "microarray" ]; then
        echo "Error: Rohart scores file cannot be generated because input YuGene file '$S4M_DATASET/${accession}_bgonly.cumulative.txt' is missing."
        return 1
    elif [ ! -f "$S4M_DATASET/${accession}_bgonly.cumulative.txt" ]; then
        echo "Warning: Skipping Rohart score generation because YuGene file '$S4M_DATASET/${accession}_bgonly.cumulative.txt' is missing."
        echo "    This is probably okay for non-microarray platforms."
        return
    fi

    ROHART_DIR="$S4M_DATASET/rohart"
    mkdir -p "$ROHART_DIR"

    ## Either fetch probe mapping from a remote S4M database, or use locally supplied file
    ## NOTE: The latter should only be done for development/debugging purposes!

    if [ -z "$S4M_PROBE_MAP_FILE" ]; then
        ## Get mapping_id - needed to download probe mappings
        ds_mapping=`get_metadata_field "mapping_id"`
        if [ -z "$ds_mapping" ]; then
            echo "Error: Could not determine mapping_id, please (re)annotate and try again."; echo
            return 1
        fi
        ## Need various pgloader config (DB settings) funcs
        . "$S4M_HOME/dataset_scripts/include/funcs.sh"

        ## Check or install user copy of pgconf file
        check_install_user_pgconf "$S4M_LOAD_CONFIG"
        ret=$?
        if [ $ret -ne 0 ]; then
            echo "Error: Cannot generate Rohart scores because DB settings could not be configured
" 1>&2;
            exit $ret
        fi
        ## If we didn't exit due to some kind of error, set config file path to user's copy
        pgconf="$HOME/.s4m/pgconf/$S4M_LOAD_CONFIG"

        echo "  Fetching probe mappings for mapping_id $ds_mapping .."
        ## Returns mapping file path on success, error message otherwise
        probe_mapping=`get_probe_mapping_for_mapping_id "$ds_mapping" "$pgconf"`
        if [ $? -ne 0 ]; then
            echo "  Error: Cannot generate Rohart scores due to probe mapping issue :-"
            echo "  $probe_mapping"
            return 1
        else
            echo "  done"
        fi
        ## keep copy of probe mappings with dataset as definitive record
        cp "$probe_mapping" "$ROHART_DIR/probe_mapping_${ds_mapping}_for_rohart_downloaded.$timestamp.tsv"
        probe_mapping_file="$ROHART_DIR/probe_mapping_${ds_mapping}_for_rohart_downloaded.$timestamp.tsv"
    else
        echo "  Using locally supplied probe mapping file '$S4M_PROBE_MAP_FILE'.."
        ## keep copy of probe mappings with dataset as definitive record
        probe_mapping_file="$ROHART_DIR/probe_mapping_for_rohart_locally_supplied.$timestamp.tsv"
        cp "$S4M_PROBE_MAP_FILE" "$probe_mapping_file"
    fi

    echo "  Converting YuGene (cumulative file) Probe ID -> Ensembl .."

    ## Call the cumulative file to Ensembl IDs conversion script
    ## Args: <cumulative_to_ens.R> <bg_cumulative> <ens_cumulative_output> <probe_mapping_file> <R_PATH>
    "$S4M_HOME/dataset_scripts/rohart/cumulative_to_ensembl.sh" "$S4M_HOME/dataset_scripts/rohart/yugene_probeid_to_ensembl.R" "$S4M_DATASET/${accession}_bgonly.cumulative.txt" "$S4M_DATASET/rohart/${accession}_bgonly.ensembl.$timestamp.cumulative.txt" "$probe_mapping_file" "$S4M_R"
    if [ $? -ne 0 ]; then
        echo "Error: Failed to convert probe ID based YuGene (cumulative) file to Ensembl ID based version!"
        return 1
    fi
    echo "  done"

    echo "  Rohart score prediction function subsampling N=$subsamples times and writing output .."
    ## Args: <ens_yugene_to_rohart.R> <ens_cumulative> <output_rohart_file> <rohart_model_Rdata> <project_label> <upper_threshold> <lower_threshold> <number_subsamples> <R_PATH>
    "$S4M_HOME/dataset_scripts/rohart/ensembl_yugene_to_rohart.sh" "$S4M_HOME/dataset_scripts/rohart/yugene_to_rohart.R" "$S4M_DATASET/rohart/${accession}_bgonly.ensembl.$timestamp.cumulative.txt" "$S4M_DATASET/rohart/${accession}_rohart.$project.txt" "$S4M_DATA/data/rohart/$target_species/$project.model.Rdata" "$project" "$upperThreshold" "$lowerThreshold" "$subsamples" "$S4M_R"
    if [ $? -ne 0 ]; then
        echo "Error: Failed to create Rohart score file from Ensembl ID based YuGene file!"
        return 1
    fi
    cp "$S4M_DATASET/rohart/${accession}_rohart.$project.txt" "$S4M_DATASET/${accession}_rohart.$project.txt"
    echo "  Created: $S4M_DATASET/${accession}_rohart.$project.txt"
    echo "  done"
}


generate_mev () {
   accession=`get_metadata_field "accession_id"`
   if [ ! -f "$S4M_DATASET/$accession.gct" ]; then
       echo "Error: Need GCT file to generate MeV file! See '--do-gct' flag or '--finalize'."; echo
   else
      "$S4M_HOME/dataset_scripts/util/gct_to_mev.sh" "$S4M_DATASET/$accession.gct" "$S4M_DATASET/$accession.mev"
   fi
}


## If there are more sample expression columns in dataset than there are annotated
## samples information in biosamples_metadata file, ask user if they wish to
## automatically remove sample probe expressions for these non-annotated samples.
check_remove_non_annotated_sample_data () {
    bscount=0
    chipcount=0

    echo "Checking for absence of biosamples from annotation (compared to expression data).."

    if [ -f "$S4M_DATASET/biosamples_metadata.txt" ]; then
        bscount=`grep "Replicate Group ID" "$S4M_DATASET/biosamples_metadata.txt" | wc -l`
    else
        echo "WARNING: No 'biosamples_metadata.txt' file, finalization step will fail! Please create this file first."; echo
        sleep 1
    fi
    if [ -f "$S4M_DATASET/probe_expression_avg_replicates.txt" ]; then
        chipcount=`awk -F'\t' '{print $3}' "$S4M_DATASET/probe_expression_avg_replicates.txt" | sort -T "$S4M_DATASET" | uniq | wc -l` 
    else
        echo "WARNING: Dataset does not appear to be normalized, finalization step will most likely fail.."; echo
        sleep 1
    fi
    if [ $bscount -gt 0 -a $chipcount -gt 0 ]; then
        if [ $bscount -lt $chipcount ]; then
            echo; echo "WARNING: Annotated biosample count $bscount less than expression sample count $chipcount.

This might be an error (e.g. you forgot to remove unwanted samples from data, or did not annotate all of them).

Do you wish to remove un-annotated samples (if 'N', will likely cause dataset validation errors)? (Y/N) [Y]: "
            read ans
            if [ -z $ans ]; then
                ans="y"
            fi
            case $ans in
                Y|y) 
                    echo "Removing non-annotated samples from expression data.."; echo
                    ## Get sorted annotated sample chip ids
                    annotatedChipIDs=`grep "Replicate Group ID" "$S4M_DATASET/biosamples_metadata.txt" | awk -F'\t' '{print $3}' | sort -T "$S4M_DATASET" > "$S4M_DATASET/.annotatedChipIDs"`
                    ## Get sorted expression data chip ids (from probe_expression_avg_replicates.txt)
                    expressionChipIDs=`awk -F'\t' '{print $3}' "$S4M_DATASET/probe_expression_avg_replicates.txt" | sort -T "$S4M_DATASET" | uniq > "$S4M_DATASET/.expressionChipIDs"`
                    ## Compare the two chip ID lists, grep results and trim to get at missing sample chip ID/s as conditional match regex
                    chipIDsToRemoveRegex=`diff "$S4M_DATASET/.annotatedChipIDs" "$S4M_DATASET/.expressionChipIDs" | grep -P "^\>" | awk '{print $2}' | tr [:cntrl:] "\|"`
                    ## rtrim trailing '|'
                    chipIDsToRemoveRegex=`echo "$chipIDsToRemoveRegex" | sed -r -e 's/\|$//g'`
                    ### DEBUG ###
                    #echo "DEBUG: chipIDsToRemoveRegex=[$chipIDsToRemoveRegex]"
                    #sleep 2
                    ### END DEBUG ###
                    rm "$S4M_DATASET/.annotatedChipIDs" "$S4M_DATASET/.expressionChipIDs"
                    ## Remove non-annotated sample expressions from probe_expression_avg_replicates.txt
                    grep -P -v "\t($chipIDsToRemoveRegex)\t" "$S4M_DATASET/probe_expression_avg_replicates.txt" > "$S4M_DATASET/probe_expression_avg_replicates.txt.tmp"
                    mv "$S4M_DATASET/probe_expression_avg_replicates.txt" "$S4M_DATASET/probe_expression_avg_replicates.txt.all.samples"
                    mv "$S4M_DATASET/probe_expression_avg_replicates.txt.tmp" "$S4M_DATASET/probe_expression_avg_replicates.txt"
                    ;;
                *) echo "Keeping expression data for non-annotated samples (NOTE: This will likely cause dataset validation to fail)"; echo
                    ;;
            esac
            sleep 2
        elif [ $bscount -gt $chipcount ]; then
            ## T#2652: Don't error out if we have any technical reps as the sample count mismatch
            ##	is probably due to that.
            grep -P "Replicate\t2" "$S4M_DATASET/biosamples_metadata.txt" > /dev/null 2>&1
            if [ $? -ne 0 ]; then
                echo "Error: Got annotated biosamples count $bscount greater than expression sample count $chipcount!"
                echo "Check biosamples_metadata.txt and normalization outputs, and try again."; echo
                return 1 
            fi
        else
            echo "Annotated biosamples count matches expression data, skipping sample exclusion."; echo
        fi
    else
        echo "WARNING: Could not check if any sample annotations are missing compared to expression data."
        echo "         (GCT, CLS, MEV file creation may fail..)"; echo
        sleep 2
    fi
    return 0
}


## Just the one hard-coded MSC project for now
##
## *** NOTE: The MSC signature project is inherently tied to Ensembl v69 as the gene signature
##           was developed using probe mappings to that release, so these Rohart scores should
##           not be used if probe-to-gene mappings use any other Ensembl version.
## ***
##
## Can add more projects and species as they are developed
## See also: update_rohart_flags()
generate_rohart_files () {
    technology_used=`get_metadata_field "technology_used"`
    accession=`get_metadata_field "accession_id"`

    if [ -f "$S4M_DATASET/${accession}_bgonly.cumulative.txt" -a "$technology_used" = "microarray" ]; then
        generate_rohart_from_cumulative_bgonly "MSC" "Human" "0.5169" "0.4337" "200"
        if [ $? -ne 0 ]; then
            echo "Exiting."; echo
            exit 1
        else
            update_rohart_flags
        fi
    else
        echo "Warning: Skipping Rohart score generation - not a microarray dataset and/or 'bgonly' cumulative file not found "; echo
    fi
}


finalize_dataset () {
    echo "Preparing expression files for Stemformatics database loading.."
    echo "(This could take some time, especially for a dataset with many samples)"

    ## Create 'probe_expression_avg_replicates.txt' (for non single cell datasets)
    data_type_id=`get_metadata_field "data_type_id"`
    if [ "$data_type_id" != "3" ]; then
        cd "$S4M_HOME/dataset_scripts/util" && ./expression_detection_2_s4m.sh "$S4M_DATASET"
	if [ $? -eq 0 ]; then
	    check_remove_non_annotated_sample_data
	    if [ $? -eq 0 ]; then
		generate_cls
		time generate_gct
                echo "  setting max value from GCT.."
                time update_metastore_max_from_gct
                echo "  setting min value from GCT.."
                time update_metastore_min_from_gct
		time generate_cumulative_gct
		time generate_cumulative_bgonly
		generate_mev
	    else
		echo "Error: Could not proceed to create CLS, GCT and MeV files!"; echo
	    fi
	fi
    fi
    ## data_type_id 3 == single cell RNAseq
    ## We also skip a bunch of things we don't want or need (e.g. cumulative, MeV..)
    if [ "$data_type_id" = "3" ]; then
        generate_cls
        time generate_gct
        echo "  setting max value from GCT.."
        time update_metastore_max_from_gct
        echo "  setting min value from GCT.."
        time update_metastore_min_from_gct
    fi
}


update_metastore_max_from_gct () {
    accession=`cat "$S4M_DATASET/ACCESSION" | tr -d [:cntrl:]`
    gctfile="$S4M_DATASET/$accession.gct"
    if [ -f "$S4M_DATASET/$accession.gct" ]; then
      maxvalue=`$S4M_HOME/dataset_scripts/util/gct_max_value.sh "$gctfile"`
      set_metastore_maxvalue "$maxvalue" 
    fi
}


update_metastore_min_from_gct () {
    accession=`cat "$S4M_DATASET/ACCESSION" | tr -d [:cntrl:]`
    gctfile="$S4M_DATASET/$accession.gct"
    if [ -f "$S4M_DATASET/$accession.gct" ]; then
      minvalue=`$S4M_HOME/dataset_scripts/util/gct_min_value.sh "$gctfile"`
      set_metastore_minvalue "$minvalue" 
    fi
}


update_rohart_flags () {
    projects="MSC
"
    accession=`get_metadata_field "accession_id"`
    for p in $projects
    do
      if [ -f "$S4M_DATASET/${accession}_rohart.$p.txt" ]; then
        set_meta "Rohart${p}Access" True
      else
        set_meta "Rohart${p}Access" False
      fi
    done
}


update_metastore_project_flags () {
    update_rohart_flags
}


set_expression_threshold () {
    set_meta "postnorm_expression_threshold" "$1"
}


set_expression_median () {
    set_meta "postnorm_expression_median" "$1"
}


## Update METASTORE with max expression value
set_metastore_maxvalue () {
    set_meta "maxGraphValue" "$1"
}


## Update METASTORE with min expression value
set_metastore_minvalue () {
    set_meta "minGraphValue" "$1"
}


## Update METASTORE with threshold and median values, if available
update_metastore_threshold () {
    if [ -f "$S4M_DATASET/source/normalized/THRESHOLD" ]; then
        threshold=`cat "$S4M_DATASET/source/normalized/THRESHOLD"`
        set_expression_threshold "$threshold"
    fi
    if [ -f "$S4M_DATASET/source/normalized/MEDIAN_ABOVE_THRESHOLD" ]; then
        median=`cat "$S4M_DATASET/source/normalized/MEDIAN_ABOVE_THRESHOLD"`
        set_expression_median "$median"
    fi
}


set_probes_detected () {
    detected=""
    ## Determine from "probe_expression_avg_replicates.txt" first
    if [ -f "$S4M_DATASET/probe_expression_avg_replicates.txt" ]; then
        detected=`grep TRUE "$S4M_DATASET/probe_expression_avg_replicates.txt" | awk -F'\t' '{print $4}' | sort -T "$S4M_DATASET" | uniq | wc -l`
    ## .. OR if it's not there, try the GCT file
    else
        accession=`cat "$S4M_DATASET/ACCESSION"`
        if [ -f "$S4M_DATASET/$accession.gct" ]; then
            nlines=`cat "$S4M_DATASET/$accession.gct" | wc -l`
            totprobecount=`expr $nlines - 3`
            ## "detected" probes are those where at least one value exists in a row
            nondetprobecount=`tail -$totprobecount "$S4M_DATASET/$accession.gct" | grep -P "\tNA\s+$" | wc -l`
            detected=`expr $totprobecount - $nondetprobecount`
        fi
    fi
    set_meta "postnorm_probes_detected" "$detected"
}


set_probe_count () {
    probecount=""
    ## Determine from "probe_expression_avg_replicates.txt" first
    if [ -f "$S4M_DATASET/probe_expression_avg_replicates.txt" ]; then
        probecount=`awk -F'\t' '{print $4}' "$S4M_DATASET/probe_expression_avg_replicates.txt" | sort -T "$S4M_DATASET" | uniq | wc -l`
    ## .. OR if it's not there, try the GCT file
    else
        accession=`cat "$S4M_DATASET/ACCESSION"`
        if [ -f "$S4M_DATASET/$accession.gct" ]; then
            nlines=`cat "$S4M_DATASET/$accession.gct" | wc -l`
            probecount=`expr $nlines - 3`
        fi
    fi
    if [ ! -z "$probecount" ]; then
        set_meta "array_probe_count" "$probecount"
    fi
}


update_metastore_probecounts () {
    set_probe_count
    set_probes_detected
}


set_detection_threshold () {
    threshold="$1"
    quiet="FALSE"
    if [ ! -z "$2" ]; then
        if [ "$2" = "TRUE" -o "$2" = "FALSE" ]; then
            quiet="$2"
        fi
    fi
    if [ -f "$S4M_DATASET/source/normalized/normalized_expression_unpacked.txt" ]; then
        ## Last boolean flag (optional) tells R to be quiet
        cd "$S4M_HOME/dataset_scripts/normalization" && ./threshold.sh "$S4M_DATASET/source/normalized/normalized_expression_unpacked.txt" "$S4M_DATASET/source/normalized" "$threshold" "$quiet"

        ## compile all det. threshold plots into a PDF file
        which convert > /dev/null 2>&1
        if [ $? -ne 0 ]; then
            echo "WARNING: Utility 'convert' not found; det. threshold plots PDF will not be created."
            sleep 2
        else
           echo "   updating thresholded density plots PDF.."
           convert "$S4M_DATASET/source/normalized/"thresholded_density_plot_Probes_Above_Threshold_*.png "$S4M_DATASET/source/normalized/thresholded_density_plots.PDF"
        fi

        cd $OLDPWD
    else
        echo "Sorry, detection threshold can only be applied after normalization has been completed."; echo
        echo "(See: --normalize flag)"
        exit 1
    fi
}


## Auto-generate detection threshold plots between 3.4 and 8.0
generate_detection_threshold_plots () {
    input_thresholds="3.9
4.1
4.3
5.5
5.7
5.9
7.1
7.3
7.5"
    n=`echo "$input_thresholds" | wc -l`
    count=1
    echo "Creating auto-range detection threshold plots (this will take some time).."
    for i in $input_thresholds
    do
        echo "$count of $n  ($i +/- 0.5)"
        set_detection_threshold $i TRUE
        count=`expr $count + 1`
    done

    ## Remove THRESHOLD and MEDIAN_ABOVE_THRESHOLD files generated by "set_detection_threshold"
    ## function (above) - so these values don't get propagated to METASTORE on dataset
    ## update (we want a DT to be set by a person using "s4m.sh -d <value> ." ) !

    rm -f "$S4M_DATASET/source/normalized/THRESHOLD" "$S4M_DATASET/source/normalized/MEDIAN_ABOVE_THRESHOLD" > /dev/null 2>&1

    echo "All done."
}


set_dataset_path () {
    if [ -z "$1" ]; then
        echo "Error: Dataset directory invalid or not specified"; echo
        usage
    fi 
    dir="$1"
    create_if_not_exists="$2"
    if [ ! -d "$dir" -a "$create_if_not_exists" = "true" ]; then
        mkdir -p "$dir"
    elif [ ! -d "$dir" ]; then
        echo "Error: Dataset path '$dir' does not exist or is not writeable!"; echo
        exit 1
    fi
    dataset_path=`cd "$dir" && pwd`
    cd $OLDPWD
    export S4M_DATASET="$dataset_path"
    if [ ! -d "$S4M_DATASET" -o ! -w "$S4M_DATASET" ]; then
        echo "Error: Dataset path '$S4M_DATASET' does not exist or is not writeable!"; echo
        exit 1
    fi
    ## Load format version for existing dataset, if available
    if [ -f "$S4M_DATASET/VERSION" ]; then
        formatvers=`grep -P "^FormatVersion" "$S4M_DATASET/VERSION" | awk -F'=' '{print $2}'`
        export S4M_FORMAT_VERSION="$formatvers"
    fi
}


## Regenerate QC plots without performing normalization.
## If normalized data is not present, we cannot produce QC plots for
## post-normalized data.
regenerate_qc_plots_only () {
    dataset_path="$1"
    ## See if the "qconly" flag was already set - if not we add it otherwise ignore
    echo "$S4M_SET" | grep qconly > /dev/null 2>&1
    if [ $? -ne 0 ]; then
        if [ -z "$S4M_SET" ] ; then
            export S4M_SET="qconly"
        else
            export S4M_SET="$S4M_SET,qconly"
        fi
    fi
    normalize "$dataset_path"
}


validate_dataset () {
    cd "$S4M_HOME/dataset_scripts" && ./validate_dataset.sh "$S4M_DATASET"
}


force_annotation_files_unix () {
    which dos2unix > /dev/null 2>&1
    if [ $? -ne 0 ]; then
        echo "WARNING: Utility 'dos2unix' not found; please manually ensure input source files are Unix formatted."
        sleep 2
    else
        dos2unix -q "$S4M_DATASET/METASTORE"
        if [ -f  "$S4M_DATASET/biosamples_metadata.txt" ]; then
            dos2unix -q "$S4M_DATASET/biosamples_metadata.txt"
        fi
        if [ -f "$S4M_DATASET/ACCESSION" ]; then
            dos2unix -q "$S4M_DATASET/ACCESSION"
        fi
    fi
}


force_annotation_files_special_encoding () {
    which iconv > /dev/null 2>&1
    if [ $? -ne 0 ]; then
        echo "WARNING: Utility 'iconv' not found; please manually check annotation file encodings."
        sleep 2
    else
        ## Attempt to convert annotation files that are allowed to contain UTF-8 chars to
        ## UTF-8 if they contain some variant of ASCII or ISO-8859 etc.
        ## If they don't contain any UTF-8 chars, the file encodings will not be changed
        ## so we don't risk anything by doing this. I think.
        utf8_files="METASTORE dataset.txt dataset_metadata.txt"
        for f in $utf8_files
        do
            if [ -s "$S4M_DATASET/$f" ]; then
		## If Byte Order Marker (BOM) present, strip it out
		bomstatus=`file "$S4M_DATASET/$f"`
		echo "$bomstatus" | grep BOM > /dev/null 2>&1
		if [ $? -eq 0 ]; then
                    tail --bytes=+4 "$S4M_DATASET/$f" > "$S4M_DATASET/$f.tmp"
                    mv "$S4M_DATASET/$f.tmp" "$S4M_DATASET/$f"
		fi
                enc=`file -i "$S4M_DATASET/$f" | awk '{print $3}' | sed -r -e 's/^.*\=//g'`
                iconv -f $enc -t UTF-8 "$S4M_DATASET/$f" > "$S4M_DATASET/$f.tmp"
                mv "$S4M_DATASET/$f.tmp" "$S4M_DATASET/$f"
            fi
        done
    fi
}


check_update_conf_files_and_version_file () {
    formatversion="0"

    if [ -f "$S4M_DATASET/VERSION" ]; then
        formatversion=`grep "FormatVersion" $S4M_DATASET/VERSION 2>&1`
        if [ $? -eq 0 ]; then
            vers=`echo "$formatversion" | sed -r -e 's/^.*\=//g' | tr -d [:cntrl:]`
            ## Assume version mismatch means that dataset files are OLDER than current.
            ## (NOTE: An "-lt" comparator fails here because it only works for integers).
            if [ "$vers" != "$S4M_FORMAT_LATEST_VERSION" ]; then
                update_version_file
            fi
        else
            echo "FormatVersion=$S4M_FORMAT_LATEST_VERSION" >> "$S4M_DATASET/VERSION"
        fi
    else
        init_version_file
    fi
}


## Update dataset data / metadata
## NOTE: Output timings for processes that could take a long time
update_dataset () {
    echo "Refreshing dataset metadata.."; echo
    force_annotation_files_unix
    force_annotation_files_special_encoding
    update_metastore_probecounts
    update_metastore_threshold
    update_metastore_project_flags
    check_update_conf_files_and_version_file
    echo "  updating biosamples (refresh biosamples.txt and chip_id_replicate_group_map.txt ** if required **).."
    time update_biosamples_files
    update_files_from_metastore
}


## New in v1.3 - Create files:
##	biosamples.txt
##	chip_id_replicate_group_map.txt
##
## from biosamples_metadata.txt
update_biosamples_files () {
    data_type_id=`get_metadata_field "data_type_id"`
    ## For scRNA-seq, refreshing files could be unnecessary - skip if that's the case
    if [ "$data_type_id" = "3" ]; then
        if [ ! -f "$S4M_DATASET/biosamples_metadata.tsv" ]; then
            echo "Error: Require 'biosamples_metadata.tsv' before updating biosamples files!
" 1>&2
            return 1
        fi
        ## If biosamples_metadata.txt AND biosamples.txt exists and both are newer than biosamples_metadata.tsv, skip checks
        test -f "$S4M_DATASET/biosamples.txt" -a "$S4M_DATASET/biosamples.txt" -nt "$S4M_DATASET/biosamples_metadata.tsv"
        bs_newer=$?
        test -f "$S4M_DATASET/biosamples_metadata.txt" -a "$S4M_DATASET/biosamples_metadata.txt" -nt "$S4M_DATASET/biosamples_metadata.tsv"
        bs_meta_newer=$?
        if [ $bs_newer -eq 0 -a $bs_meta_newer -eq 0 ]; then
            echo "  SKIPPED (biosamples.txt AND biosamples_metadata.txt exist and both are newer than biosamples_metadata.tsv)"
            return
        fi
    fi
    if [ -f "$S4M_DATASET/biosamples_metadata.txt" ]; then
        backup_biosamples_files
        regenerate_biosamples_files
    fi
}


regenerate_biosamples_files () {
    bsmeta="$S4M_DATASET/biosamples_metadata.txt"

    ## Make sure biosamples_metadata.txt is sorted!
    sort -T "$S4M_DATASET" "$bsmeta" > "$bsmeta.sorted"
    mv "$bsmeta.sorted" "$bsmeta"

    ## NOTE: Added for v1.3x transition to v1.4 format for extra ds_id column in biosamples_metadata.txt
    bsmeta_col_count=`awk -F'\t' '{print NF}' "$bsmeta" | sort -T "$S4M_DATASET" | uniq | tr -d [:cntrl:]`
    if [ $bsmeta_col_count -ne 5 ]; then
        echo "Error: biosamples_metadata.txt expecting 5 columns but got $bsmeta_col_count columns (is first column 'ds_id' present?)"
        exit 1
    fi

    grep -P "\t\QSample Type\E\t" "$bsmeta" | awk -F'\t' '{print $3}' > "$S4M_DATASET/.s4m_chip_ids.txt"

    ## Create "biosamples.txt" and "chip_id_replicate_group_map.txt" from "biosamples_metadata.txt"
    while read chipid
    do
        grep -P "\t\Q$chipid\E\t" "$bsmeta" > "$S4M_DATASET/.s4m_sample_$chipid.txt"
        chip_type=`head -1 "$S4M_DATASET/.s4m_sample_$chipid.txt" | awk -F'\t' '{print $2}'`
        if [ -z "$chip_type" ]; then
            chip_type="<s4m_chip_type>"
        fi
        rep_group_id=`grep -P "\tReplicate Group ID\t" "$S4M_DATASET/.s4m_sample_$chipid.txt" | awk -F'\t' '{print $5}' | tr -d [:cntrl:]`
        sample_alias="$rep_group_id"
        sample_accession=`grep -P "\tGEO Sample Accession\t" "$S4M_DATASET/.s4m_sample_$chipid.txt" | awk -F'\t' '{print $5}' | tr -d [:cntrl:]`
        if [ -z "$sample_accession" ]; then
            sample_accession=`grep -P "\tSource Name\t" "$S4M_DATASET/.s4m_sample_$chipid.txt" | awk -F'\t' '{print $5}' | tr -d [:cntrl:]`
        fi
        /bin/echo -e "$chipid\t$chip_type\t<s4m_dataset_id>\t$sample_alias\t$sample_accession\t$rep_group_id" >> "$S4M_DATASET/biosamples.txt"
        /bin/echo -e "$chipid\t$rep_group_id" >> "$S4M_DATASET/chip_id_replicate_group_map.txt"

        rm "$S4M_DATASET/.s4m_sample_$chipid.txt"
    done < "$S4M_DATASET/.s4m_chip_ids.txt"

    rm "$S4M_DATASET/.s4m_chip_ids.txt"
}


backup_biosamples_files () {
    files="biosamples.txt biosamples_metadata.txt chip_id_replicate_group_map.txt"
    rmfiles="biosamples.txt chip_id_replicate_group_map.txt"
    for f in $files; do
        if [ -f "$S4M_DATASET/$f" ]; then
            ## overwrite backup
            cp "$S4M_DATASET/$f" "$S4M_DATASET/$f.BAK"
        fi
    done
    ## remove files we ought to remove before regeneration
    for f in $rmfiles; do
        rm -f "$S4M_DATASET/$f"
    done
}


is_validated () {
    if [ -f "$S4M_DATASET/VALIDATED" ]; then
        echo "true"
        return
    fi
    echo "false"
}

is_validated_recently () {
    if [ -f "$S4M_DATASET/VALIDATED" ]; then
        validated_timestamp=`cat "$S4M_DATASET/VALIDATED" | tr -d [:cntrl:]`
        current_timestamp_day=`date +"%Y%m%d"`
        current_timestamp_time=`date +"%H%M"`
        current_timestamp_mins=`date +"%M"`
        current_timestamp_hour=`date +"%H"`
        validated_timestamp_day=`echo "$validated_timestamp" | sed -r -e 's/\_.*$//g'`
        validated_timestamp_time=`echo "$validated_timestamp" | sed -r -e 's/^.*\_//g' -e 's/^(.*)[0-9]{2}$/\1/g'`
        validated_timestamp_mins=`echo "$validated_timestamp" | sed -r -e 's/^.*\_//g' -e 's/^[0-9]{2}([0-9]{2}).*$/\1/g'`
        validated_timestamp_hour=`echo "$validated_timestamp" | sed -r -e 's/^.*\_//g' -e 's/^([0-9]{2}).*$/\1/g'`

        #echo "DEBUG: validated_timestamp = [$validated_timestamp]"
        #echo "DEBUG: current_timestamp_day = [$current_timestamp_day]"
        #echo "DEBUG: current_timestamp_time = [$current_timestamp_time]"
        #echo "DEBUG: validated_timestamp_day = [$validated_timestamp_day]"
        #echo "DEBUG: validated_timestamp_time = [$validated_timestamp_time]"

        if [ "$current_timestamp_day" != "$validated_timestamp_day" ]; then
            echo "false"
            #echo "DEBUG: Current vs validated day not matching"
            return
        else
            curr_hours_as_mins=`expr $current_timestamp_hour \* 60`
            curr_mins=`expr $curr_hours_as_mins + $current_timestamp_mins`
            val_hours_as_mins=`expr $validated_timestamp_hour \* 60`
            val_mins=`expr $val_hours_as_mins + $validated_timestamp_mins`

            mins_diff=`expr $curr_mins - $val_mins`
            
            if [ $mins_diff -gt $VALIDATION_TIMEOUT_MINS ]; then
                echo "false"
                #echo "DEBUG: mins_diff = [$mins_diff]"
                #echo "DEBUG: curr_mins = [$curr_mins]"
                #echo "DEBUG: curr_hours_as_mins = [$curr_hours_as_mins]"
                #echo "DEBUG: val_mins = [$val_mins]"
                #echo "DEBUG: val_hours_as_mins = [$val_hours_as_mins]"
                return
            fi
        fi
        ## Validation time and date within validation timeout threshold
        echo "true"
        return
    fi
    echo "false"
    #echo "DEBUG: File $S4M_DATASET/VALIDATED not found"
}

dataset_metadata_warnings () {
    median=`get_metadata_field "postnorm_expression_median"`
    threshold=`get_metadata_field "postnorm_expression_threshold"`
    has_warnings="false"
    if [ "$median" = "NULL" -o -z "$median" ]; then
        has_warnings="true"
        echo "WARNING:  METASTORE field 'postnorm_expression_median' is not set"
    fi
    if [ "$threshold" = "NULL" -o -z "$threshold" ]; then
        has_warnings="true"
        echo "WARNING:  METASTORE field 'postnorm_expression_threshold' is not set"
    fi

    if [ "$has_warnings" = "true" ]; then
        echo
        echo "Try:"
        echo "- s4m.sh -d <threshold_value> .   for standard microarrays"
        echo "- s4m.sh -u .                     to update METASTORE"
        echo "- Check existence of files './source/normalized/THRESHOLD' and/or"
        echo "  './source/normalized/MEDIAN_ABOVE_THRESHOLD'."
        echo
    fi
}


## TODO: Deprecate
##
display_pgloader_configs () {
    echo "Available pgloader configurations in $S4M_HOME/etc/pgconf:
" 1>&2
    files=`ls -1 "$S4M_HOME/etc/pgconf/"*.conf | sort`
    for f in $files
    do
      dpc_host=`grep -P "^host =" "$f" | cut -d '=' -f 2 | tr -d " "`
      /bin/echo -e "   `basename $f`\t\t($dpc_host)" 1>&2
    done
    echo
    echo "(If required target not present, a new configuration file may be required)
" 1>&2
}


## Check that template pgloader config file is valid
## TODO: Deprecate
##
validate_pgloader_config () {
    pgloader_config="$1"
    if [ -z "$pgloader_config" ]; then
        echo "Error: Target pgloader (remote DB config for target server) file must be specified."
        echo "Specify using \"-C\" or \"--pgconfig\" flag."; echo
        display_pgloader_configs
        exit 1
    fi
    ## check config exists..

    ## Need 'find_pgconfig_file()'
    . "$S4M_HOME/dataset_scripts/include/funcs.sh"

    found_pgconf=`find_pgconfig_file "$pgloader_config"`
    if [ -z "$found_pgconf" ]; then
        echo "Error: Could not find target pgloader config template '$S4M_LOAD_CONFIG'
" 1>&2
        display_pgloader_configs
        exit 1
    fi
}


pre_dataset_load_checks () {
    validate_install_profile "$S4M_INSTALL_PROFILE"

    warn=`dataset_metadata_warnings`
    if [ ! -z "$warn" ]; then
        echo; echo "$warn"; echo
        echo -n "Continue loading? [y/N] "
        read ans
        cont="false"
        case $ans in
            y|Y)
                cont="true"
                ;;
            n|N)
                ;;
            *)
                ;;
        esac
        if [ $cont = "false" ]; then
            exit
        fi
    fi

    validated=`is_validated`
    validated_recently=`is_validated_recently`
    if [ "$validated" != "true" ]; then
        if [ -z "$FORCE" -o "$FORCE" = "false" ]; then
            echo "Error: Dataset must be validated before loading!"
            echo "(Can force with '--force' flag IF YOU KNOW WHAT YOU'RE DOING!)"; echo
            exit 1
        fi

    elif [ "$validated_recently" != "true" -a "$FORCE" != "true" ]; then
        echo; echo -n "WARNING: Dataset has not been validated recently (> $VALIDATION_TIMEOUT_MINS mins). Continue anyway? [y/N] "
        read ans
        cont="false"
        case $ans in
            y|Y)
                cont="true"
                ;;
            n|N)
                ;;
            *)
                ;;
        esac
        if [ $cont = "false" ]; then
            exit
        fi
    fi
}


set_install_target_from_profile () {
    profile="$1"
    if [ -f "$S4M_HOME/etc/profile/$profile" ]; then
        install_path=`grep -P "^ssh_install_uri" "$S4M_HOME/etc/profile/$profile" | sed -r -e 's/^.*\s=\s//g' | tr -d [:cntrl:]`
        echo "Got install path from config: [$install_path]"
        grep -P "^s4m_api_proto_host" "$S4M_HOME/etc/profile/$profile" > /dev/null 2>&1
        if [ $? -eq 0 ]; then
            s4m_api_proto_host=`grep -P "^s4m_api_proto_host" "$S4M_HOME/etc/profile/$profile" | sed -r -e 's/^.*\s=\s//g' | tr -d [:cntrl:]`
            echo "Got web API host spec from config: [$s4m_api_proto_host]"
            if [ ! -z "$s4m_api_proto_host" ]; then
                export S4M_API_PROTO_HOST="$s4m_api_proto_host"
            fi
        fi
    fi
    if [ -z "$install_path" ]; then
        echo "Failed to find 'user@host:/path/' install path in config '$S4M_HOME/etc/profile/$profile', please enter it now:"
        echo -n "> "
        read ans
        if [ -z "$ans" ]; then
            echo "Error: Must provide an install path spec, or add in '$S4M_HOME/etc/profile/$profile' as 'ssh_install_uri' variable"; echo
            exit 1
        fi 
        export INSTALL_TARGET="$ans"
    else
        u=`whoami`
        echo -n "If target SSH user is not '$u' please override user name now or ENTER to continue: "
        read ans
        if [ ! -z "$ans" ]; then
            u="$ans"
        fi
        export INSTALL_TARGET="$u@$install_path"
    fi
}


load_dataset () {
    pre_dataset_load_checks

    ## If we got this far, we can load the dataset now!
    "$S4M_HOME/dataset_scripts/load_dataset.sh" "$S4M_DATASET" "$S4M_INSTALL_PROFILE"
    ret=$?
    if [ $ret -ne 0 ]; then
      echo "Aborting dataset load."; echo
      exit $ret
    fi
 
    echo; echo -n "Perform dataset install now? [Y/n] "
    cont="true"
    read ans
    if [ -z "$ans" ]; then
        ans="y"
    fi
    case $ans in
        n|N)
            cont="false"
            ;;
        *)
            ;;
    esac
    if [ "$cont" = "true" ]; then
        set_install_target_from_profile "$S4M_INSTALL_PROFILE"
        if [ ! -z "$INSTALL_TARGET" ]; then
            install_dataset 
        else
            echo "Error: Cannot install dataset, target install path not found or supplied."
            exit 1
        fi
    fi
}

unload_dataset () {
    validate_install_profile "$S4M_INSTALL_PROFILE"
    
    if [ -z "$DATASETID_TARGET" ]; then
        echo "Error: Must specify the numeric dataset ID for target dataset."
        echo "e.g.   s4m.sh --unload 1234 -C <profile_name>"; echo
        exit 1
    fi

    ## Args: <dsid> <profile_name>
    "$S4M_HOME/dataset_scripts/unload_dataset.sh" "$DATASETID_TARGET" "$S4M_INSTALL_PROFILE"
    if [ $? -ne 0 ]; then
      echo "Error: Dataset unload failed."
      exit 1
    fi
}


## NOTE: "--reload" currently (crudely) disabled, so this function is not used
## TODO: Deprecate or refactor
##
reload_dataset () {
    pre_dataset_load_checks

    ## Sometimes METASTORE given as "metastore.txt" - if so, copy to METASTORE 
    if [ ! -f "$S4M_DATASET/METASTORE" -a -f "$S4M_DATASET/metastore.txt" ]; then
        cp "$S4M_DATASET/metastore.txt" "$S4M_DATASET/METASTORE"
    fi
    ## If dataset_metadata.txt not present but we have METASTORE, create it from METASTORE
    if [ ! -f "$S4M_DATASET/dataset_metadata.txt" -a -f "$S4M_DATASET/METASTORE" ]; then
        init_dataset_metadata_file
        update_dataset_metadata_from_metastore
    fi

    "$S4M_HOME/dataset_scripts/reload_dataset.sh" "$S4M_DATASET" "$S4M_LOAD_CONFIG"
}


display_install_profiles () {
    echo "Available install target profiles in $S4M_HOME/etc/profile:
" 1>&2
    files=`ls -1 "$S4M_HOME/etc/profile/"*.conf | sort`
    for f in $files
    do
      dip_host=`grep -P "^s4m_api_proto_host =" "$f" | cut -d '=' -f 2 | tr -d " "`
      /bin/echo -e "   `basename $f`\t\t($dip_host)" 1>&2
    done
    echo
    echo "(If required target not present, a new configuration file may be required)
" 1>&2
}

validate_install_profile () {
    install_profile="$1"

    if [ -z "$install_profile" ]; then
        echo "Error: Install target (web server data config) profile must be specified."
        echo "Specify using \"-C\" or \"--pgconfig\" flag."; echo
        display_install_profiles
        exit 1
    fi
    if [ ! -f "$S4M_HOME/etc/profile/$install_profile" ]; then
       echo "Error: Install target (web server data config) profile '$install_profile' not found!
" 1>&2
       display_install_profiles
       exit 1
    fi
}


install_dataset_with_host_target () {
    if [ -z "$INSTALL_TARGET" ]; then
        echo "Error: Target host:path string must be specified for dataset install."
        echo "e.g.   s4m.sh --install <dataset_dir> [user@]host:/shared/data/path"; echo
        exit 1
    fi
    ## NOTE: Optional 'S4M_API_PROTO_HOST' arg to install script only set automatically if
    ## following on from a --load command (reads config option from conf file)
    "$S4M_HOME/dataset_scripts/install_dataset.sh" "$S4M_DATASET" "$INSTALL_TARGET" "$S4M_API_PROTO_HOST"
}


install_dataset () {
    ## This is true when doing an --install command (not coming from --load)
    ## If this is the case, we need to validate the profile
    if [ ! -z "$S4M_INSTALL_PROFILE" ]; then
        validate_install_profile "$S4M_INSTALL_PROFILE"
        set_install_target_from_profile "$S4M_INSTALL_PROFILE"
    fi
    if [ -z "$INSTALL_TARGET" ]; then
        echo "Error: Cannot install dataset, target install path not found or supplied."
        exit 1
    fi
    install_dataset_with_host_target
}


## NOTE: "--dump" currently (crudely) disabled, so this function is not used
## TODO: Deprecate or refactor
##
dump_dataset () {
    ## Whole bunch of sanity checks first. Make sure we got all the inputs we needed,
    ## test that dataset target on disk doesn't already exist and that we can write
    ## to the input path.
    validate_pgloader_config "$S4M_LOAD_CONFIG"

    if [ -z "$DATASETID_TARGET" ]; then
        echo "Error: Must specify the numeric dataset ID for target dataset."
        echo "e.g.   s4m.sh --dump 1234 <output_path> -C <pgloader_config>"; echo
        exit 1
    fi
    if [ -z "$DUMP_TARGET" ]; then
        echo "Error: Must specify the dataset dump target path."
        echo "e.g.   s4m.sh --dump <dataset_id> /path/to/output/dataset_dir -C <pgloader_config>"; echo
    fi
    if [ -d "$DUMP_TARGET" ]; then
        echo "Target directory '$DUMP_TARGET' already exists!"
        echo -n "Create dataset files in this directory? [y/N] "
        read ans
        if [ -z $ans ]; then
            ans="N"
        fi
        case $ans in
            N|n) echo "Aborting."; echo; exit
                 ;;
        esac
    else
        touch "$DUMP_TARGET" > /dev/null 2>&1
        if [ $? -ne 0 ]; then
            echo "Error: The directory '$DUMP_TARGET' cannot be written, please check permissions."; echo
            exit 1
        fi
        rm "$DUMP_TARGET"
        mkdir -p "$DUMP_TARGET" > /dev/null 2>&1
        if [ $? -ne 0 ]; then
            echo "Error: Failed to create directory '$DUMP_TARGET'. Check permissions etc."; echo
            exit 1
        fi
    fi

    "$S4M_HOME/dataset_scripts/dump_dataset.sh" "$DATASETID_TARGET" "$DUMP_TARGET" "$S4M_LOAD_CONFIG"
    if [ ! -d "$DUMP_TARGET" ]; then
        echo "Dataset dump was not completed."
    else
        echo "Dataset dump completed."
    fi
}

set_R_binary () {
    export S4M_R="$1"
}

set_APT_path () {
    export S4M_APT="$1"
}

set_s4m_options () {
    export S4M_SET="$1"
}

set_dataset_format () {
    export S4M_FORMAT_VERSION="$1"
}

set_pgloader_config () {
    export S4M_LOAD_CONFIG="$1"
}

set_project () {
    export S4M_PROJECT_LABEL="$1"
}

## For "offline" Rohart score generation
set_probe_map_file () {
    export S4M_PROBE_MAP_FILE="$1"
}


usage () {
    echo
    echo "Usage:  $SCRIPT_NAME <options>"
    echo 
    echo "   options:"
    echo
    echo "   -h |--help                      -- Help (this screen)"
    echo "   [-V |--version]                 -- Show script version and exit."
    echo "   [-i |--init] <dataset_dir>      -- Initialise a new Stemformatics dataset in given directory (relative to current)"
    echo "   [-u |--update] <dataset_dir>    -- Regenerate dataset files using METASTORE attributes"
    echo "   [-n |--normalize] <dataset_dir> -- Normalize a Stemformatics dataset."
    echo "   [-d |--detection-threshold <threshold> <dataset_dir>"
    echo "                                   -- Set detection threshold (floor), post-normalization."
    echo "   [-dp|--detection-plots <dataset_dir>"
    echo "                                   -- Auto generate det. threshold plots in range log2(3.4) to log2(8.0)"
    echo "   [-qc|--qc-only <dataset_dir>    -- Regenerate QC plots ONLY. If no raw/normalized data, nothing is done."
    echo "                                      Add '-SET force=TRUE' flag if dataset is non-microarray (AT YOUR OWN RISK)"
    echo "   [-f |--finalize <dataset_dir> -C <pg_config_file>"
    echo "                                   -- Finalise dataset (do chip ID <-> sample expression merge, format conversion, GCT etc)"
    echo "                                      The pgloader config file is required for probe mapping download in Rohart file creation"
    echo "   [-c |--do-cls <dataset_dir>     -- Generate CLS file for normalized dataset (--finalize does this too)."
    echo "   [-g |--do-gct <dataset_dir>     -- Generate GCT file for normalized dataset (--finalize does this too)."
    echo "   [-rh|--do-rohart <dataset_dir> [-C <pg_config_file> | -pm <map_file>]"
    echo "                                   -- Generate Rohart score file(s) with probe mappings from given server OR"
    echo "                                      use mapping from a local file (*** use the latter ONLY for testing ***)"
    echo "   [-pm|--probe-map <map_file>     -- See above. Currently used only in combination with '-rh'"
    echo "   [-s |--status] <dataset_dir>    -- Print dataset status information"
    echo "   [-v |--validate] <dataset_dir>  -- Perform dataset validation, prior to database load."
    echo "   [-l |--load] <dataset_dir> -C <pg_config_file>"
    echo "                                   -- Load whole dataset into database whose configuration is specified in given pgloader config."
    echo "   [-U |--unload] <dsid> -C <pg_config_file>"
    echo "                                   -- Unload whole dataset from database whose configuration is specified in given pgloader config."
    ## Temporarily disabled 'reload' because downstream reload_dataset.sh needs user-based pgconf
    ## and password-injection update!
    #echo "   [-r |--reload] <dataset_dir> -C <pg_config_file>"
    #echo "                                   -- Reload dataset and biosamples METADATA ONLY using pgloader config specified (see below)."
    ## Temporarily disabled 'dump' because downstream reload_dataset.sh needs user-based pgconf
    ## and password-injection update!
    #echo "   [-D |--dump] <dsid> <output_dataset_dir> -C <pg_config_file>"
    #echo "                                   -- Dump dataset from database to disk, whose configuration is specified in given pgloader config."
    echo "   [-C |--pgconfig] <pg_config>    -- Name of pgloader.conf file (or profile.conf)."
    echo "                                      To list available options, just specify '-C' with no further value."
    echo "                                      This flag is required for: load, unload, reload, install, finalize."
    echo "   [-P|--project]                  -- Set environment to support non-Stemformatics project. Consult an admin for usage."
    echo "   [--force]                       -- Set 'force' mode. Switches on 'best effort' behaviour for various functions."
    echo "   [-I |--install] <dataset_dir> <[user@]host:target>  OR"
    echo "   [-I |--install] <dataset_dir> -C <pg_profile>"
    echo "                                         -- Install GCT, CLS etc files into given shared data files path on remote host via SSH"
    echo "                                         If -C flag, extracts remote path from given <pg_profile> (in \$S4M_HOME/etc/profile/<pg_profile>)"
    echo "   [-R |--with-R] <R binary >            -- Use custom 'R' executable for normalization"
    echo "   [-APT |--with-APT] <APT binary path>  -- Give path to APT binary directory"
    echo "   [-SET |--set-options] <a=b[,x=y]..>   -- Set s4m options (INI style key/val pairs, comma separated)"
    echo
    exit 1
}

if [ ${#@} = 0 ]; then
    usage
fi


## Some globals
ARRAY_TYPE=""
ANNOTATE=""
LOAD=""
UNLOAD=""
RELOAD=""
DUMP=""
INSTALL=""
INSTALL_TARGET=""
S4M_LOAD_CONFIG=""
S4M_INSTALL_PROFILE=""
S4M_API_PROTO_HOST=""
DATASETID_TARGET=""
DUMP_TARGET=""
NORMALIZE_TARGET=""
FORCE=""


until [ -z "$1" ]
do
    case $1 in 
        -h|--help)
            shift
            usage
            ;;

        -i|--init)
            verify_environment
            shift
            set_dataset_path "$1" "true"
            init_files
            exit
            ;;

        -u|--update)
            shift
            set_dataset_path "$1"
            update_dataset
            exit
            ;;

        -n|--normalize)
            verify_environment
            shift
            set_dataset_path "$1"
            NORMALIZE_TARGET="$S4M_DATASET"
            ## Uncomment to automatically update probe count / detected in METASTORE
            #update_metastore_probecounts
            ;;

        -t|--type)
            shift
            echo; echo "NOTE: The --type (-t) flag has been deprecated, ignoring.."; echo
            sleep 2
            ;;

        -d|--detection-threshold)
            shift
            DETECTION_THRESHOLD="$1"
            shift
            set_dataset_path "$1"
            set_detection_threshold "$DETECTION_THRESHOLD"
            update_metastore_threshold
            exit
            ;;

        -dp|--detection-plots)
            shift
            set_dataset_path "$1"
            generate_detection_threshold_plots
            exit
            ;;
 
        -qc|--qc-only)
            verify_environment
            shift
            set_dataset_path "$1"
            QC_ONLY_TARGET="$S4M_DATASET"
            ;;

        -rh|--do-rohart)
            shift
            set_dataset_path "$1"
            ROHART="true"
            ;;

        -pm|--probe-map)
            shift
            set_probe_map_file "$1"
            ;;

        -c|--do-cls)
            shift
            set_dataset_path "$1"
            generate_cls
            exit
            ;;
            
        -g|--do-gct)
            shift
            set_dataset_path "$1"
            generate_gct
            exit
            ;;

        -m|--do-mev)
            shift
            set_dataset_path "$1"
            generate_mev
            exit
            ;;

        -f|--finalize)
            shift
            set_dataset_path "$1"
            FINALIZE="true"
            ;;

        -s|--status)
            shift
            set_dataset_path "$1"
            dataset_status
            exit
            ;;

        -v|--validate)
            shift
            set_dataset_path "$1"
            update_dataset
            validate_dataset
            exit
            ;;

        -l|--load)
            shift
            set_dataset_path "$1"
            LOAD="true"
            ;;

        -I|--install)
            shift
            set_dataset_path "$1"
            INSTALL="true"
            shift
            INSTALL_TARGET="$1"
            if [ "$INSTALL_TARGET" = "-C" ]; then
                ## -C arg will get picked up next and config var set accordingly
                continue
            else
                shift
                ## Arg for <proto>://<host> to use for API calls.
                ## If target Stemformatics host is SSL enabled, this MUST be provided otherwise
                ## API update part of install might fail.
                if [ ! -z "$1" ]; then
                    S4M_API_PROTO_HOST="$1"
                fi
            fi
            ;;

        -r|--reload)
            echo "Sorry, the 'reload' command is currently disabled. Use 'unload' and then 'load' instead." 1>&2
            exit 1
            #shift
            #set_dataset_path "$1"
            #RELOAD="true"
            ;;

        -U|--unload)
            shift
            DATASETID_TARGET="$1"
            UNLOAD="true"
            ;;

        -D|--dump)
            echo "Sorry, the 'dump' command is currently disabled." 1>&2
            exit 1
            #if [ $# -gt 2 ]; then
            #    shift
            #    DATASETID_TARGET="$1"
            #    shift
            #    DUMP_TARGET="$1"
            #    DUMP="true"
            #else
            #    usage
            #fi
            ;;

        -C|--pgconfig)
            shift
            set_pgloader_config "$1"
            ;;

        --force)
            shift
            FORCE="true"
            ;;
        -P|--project)
            shift
            set_project "$1"
            ;;
        -R|--with-R)
            shift
            set_R_binary "$1"
            ;;

        -APT|--with-APT)
            shift
            set_APT_path "$1"
            ;;

        -SET|--set-options)
            shift
            set_s4m_options "$1"
            ;;

        ## NOTE: Only to be used in conjunction with --init !
        ## (Specify flag --format before --init)
        -F|--format)
            shift
            set_dataset_format "$1"
            ;;

        -V|--version)
            echo
            echo "  Stemformatics (\"s4m.sh\") expression dataset management tool"
            echo "  Version: $S4M_SCRIPT_VERSION"; echo
            exit
            ;;

        *) 
            usage
            ;;
    esac
    shift
done


## Do normalization after all related settings are captured from command line
if [ ! -z "$NORMALIZE_TARGET" ]; then
    normalize "$NORMALIZE_TARGET"
    exit
fi

## Do "qc-only" normalization (regen QC plots only)
if [ ! -z "$QC_ONLY_TARGET" ]; then
    regenerate_qc_plots_only "$QC_ONLY_TARGET"
    exit
fi

## Do Rohart score files generation only
if [ ! -z "$ROHART" ]; then
    generate_rohart_files
    update_dataset
    exit
fi

## Finalize
if [ ! -z "$FINALIZE" ]; then
    update_dataset
    finalize_dataset
    ## Removing because doing this again seems to be unnecessary (can also take a LONG
    ## time to run for some datasets)
    #update_dataset
    exit
fi

## Install
if [ ! -z "$INSTALL" ]; then
    export S4M_INSTALL_PROFILE="$S4M_LOAD_CONFIG"
    install_dataset
    exit
fi

## Load or reload dataset
if [ ! -z "$LOAD" -a ! -z "$RELOAD" ]; then
    echo "Error: Cannot specify both --load and --reload flags together!"; echo
    exit 1
fi
if [ ! -z "$LOAD" ]; then
    export S4M_INSTALL_PROFILE="$S4M_LOAD_CONFIG"
    load_dataset
    exit
fi
if [ ! -z "$RELOAD" ]; then
    reload_dataset
    exit
fi
if [ ! -z "$UNLOAD" ]; then
    export S4M_INSTALL_PROFILE="$S4M_LOAD_CONFIG"
    unload_dataset
    exit
fi
if [ ! -z "$DUMP" ]; then
    dump_dataset
    exit
fi

