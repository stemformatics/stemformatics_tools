#!/bin/sh
## Stemformatics s4m bundle annotation / data installation

if [ -z "$S4M_HOME" ]; then
  echo "Error: Required environment variable 'S4M_HOME' is not set! Please set it to your s4m scripts bundle directory."; echo
  exit 1
fi
echo -n "Enter full s4m data installation path or ENTER for default [$HOME/.s4m]: "
read instpath
if [ -z "$instpath" ]; then
  instpath="~/.s4m"
fi
#echo "DEBUG: instpath=[$instpath]"

if [ -d "$instpath" ]; then
  echo -n "WARNING: S4M installation already detected in given path ($instpath) - overwrite? [y/N]: "
  read ans
  case $ans in
    n|N)
      echo "Aborting installation."; echo
      exit 1
      ;;
    *)
      ;;
  esac
fi

echo -n "Setting up data directories and copying files.. "
mkdir -p "$instpath/.RData"

installdir=`dirname $instpath`
target=`basename $instpath`
cp -Rp "$S4M_HOME/etc/data" "$installdir"
if [ $target != "data" ]; then
  (cd $installdir && mv data $target)
fi

if [ $? -eq 0 ]; then
  echo "done"
fi

echo "$SHELL" | grep -i bash > /dev/null 2>&1
if [ $? -ne 0 ]; then
  echo "WARNING: You're not using Bash shell, environment will not be automatically updated."
  echo "Please manually set the following (or equivalent for your shell):"; echo
  echo "    export S4M_DATA=\"$instpath\""; echo
  exit
fi

if [ -f "$HOME/.bashrc" ]; then
  grep S4M_HOME ~/.bashrc > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo "export S4M_HOME=\"$S4M_HOME\"" >> ~/.bashrc
  fi
  grep S4M_DATA ~/.bashrc > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo "export S4M_DATA=\"$instpath\"" >> ~/.bashrc
  fi
  . ~/.bashrc
fi

echo "Installation complete."; echo
