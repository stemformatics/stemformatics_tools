<dataset_id>	Accession	<accession_id>
<dataset_id>	AE Accession	NULL
<dataset_id>	GEO Accession	NULL
<dataset_id>	Title	<dataset_title>
<dataset_id>	Publication Title	<publication_title>
<dataset_id>	Publication Citation	NULL
<dataset_id>	Contact Name	NULL
<dataset_id>	Contact Email	NULL
<dataset_id>	PubMed ID	NULL
<dataset_id>	Description	<study_description>
<dataset_id>	Authors	NULL
<dataset_id>	Contributor Name	<contributor_name>
<dataset_id>	Contributor Email	NULL
<dataset_id>	Affiliation	<contributor_affiliation>
<dataset_id>	Release Date	<dataset_release_date>
<dataset_id>	Organism	<Homo sapiens|Mus musculus>
<dataset_id>	Experimental Design	transcription profiling assay
<dataset_id>	Assay Manufacturer	<array_manufacturer>
<dataset_id>	Assay Platform	<array_platform>
<dataset_id>	Assay Version	NULL
<dataset_id>	Platform	<full array platform description Man + Plat + Version>
<dataset_id>	s4mCurator	<dataset curator name/s>
<dataset_id>	s4mCurationDate	<dataset curation date (usually start date of metadata curation effort>
<dataset_id>	lineGraphSampleOrder	<Ordered JSON dict of biosamples meta 'Day' values for line graph {Day 0:1,Day 02:2,Day 05:3}>
<dataset_id>	sampleTypeDisplayOrder	<Order of sample types for frontend graphing e.g. Sample Type 1[,Sample Type 2]>
<dataset_id>	sampleTypeDisplayGroups	<JSON dict of sample type groupings indexed by sample type name e.g. {"Sample Type 1":0,"Sample Type 2":0,"Sample Type 3":1}>
<dataset_id>	limitSortBy	Sample Type
<dataset_id>	probeCount	<probe_count>
<dataset_id>	probesDetected	<detected_count>
<dataset_id>	minReplicates	1
<dataset_id>	maxReplicates	1
<dataset_id>	topDifferentiallyExpressedGenes	NULL
<dataset_id>	detectionThreshold	NULL
<dataset_id>	medianDatasetExpression	NULL
<dataset_id>	technologyUsed	microarray
<dataset_id>	cellsSamplesAssayed	NULL
<dataset_id>	cellSurfaceAntigenExpression	NULL
<dataset_id>	cellularProteinExpression	NULL
<dataset_id>	nucleicAcidExtract	total RNA extract
<dataset_id>	genePatternAnalysisAccess	Allow
