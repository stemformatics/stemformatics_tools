<s4m_chip_type>	<chip_id>	Sample Type	<curated_sample_type_name>
<chip_type>	<chip_id>	Sample Description	<curated_sample_description>
<chip_type>	<chip_id>	Cell Type	<cell_type_ontology_term>
<chip_type>	<chip_id>	Cell Line	<cell_line_or_remove>
<chip_type>	<chip_id>	Organism	<Homo sapiens|Mus musculus>
<chip_type>	<chip_id>	Organism Part	<sample_anatomy_part>
<chip_type>	<chip_id>	Tissue	<tissue_type_ontology_term_or_remove>
<chip_type>	<chip_id>	Developmental Stage	<adult_or_embryo_etc>
<chip_type>	<chip_id>	Pregnancy Status	<optional_if_appropriate_or_remove>
<chip_type>	<chip_id>	Disease State	<disease_name_or_normal>
<chip_type>	<chip_id>	Gender	<gender_if_available_or_remove>
<chip_type>	<chip_id>	Genetic Modification	<genetic_modification_details_or_remove>
<chip_type>	<chip_id>	GEO Sample Accession	<optional_if_appropriate_or_remove>
<chip_type>	<chip_id>	Source Name	<original_sample_name>
<chip_type>	<chip_id>	Labelling	<antibody labelling protein e.g. "Cy3 dye">
<chip_type>	<chip_id>	Replicate	<replicate_number_usually_1>
<chip_type>	<chip_id>	Replicate Group ID	<curated_sample_id>
.. (rinse and repeat for each sample)
