<dataset_id>	Accession	<accession_id>
<dataset_id>	AE Accession	
<dataset_id>	GEO Accession	
<dataset_id>	Title	<dataset_title>
<dataset_id>	Publication Title	<publication_title>
<dataset_id>	Publication Citation	
<dataset_id>	Contact Name	
<dataset_id>	Contact Email	
<dataset_id>	PubMed ID	
<dataset_id>	Description	<study_description>
<dataset_id>	Authors	
<dataset_id>	Contributor Name	<contributor_name>
<dataset_id>	Contributor Email	
<dataset_id>	Affiliation	<contributor_affiliation>
<dataset_id>	Release Date	<dataset_release_date>
<dataset_id>	Organism	<Homo sapiens|Mus musculus>
<dataset_id>	Experimental Design	transcription profiling assay
<dataset_id>	Assay Manufacturer	<array_manufacturer>
<dataset_id>	Assay Platform	<array_platform>
<dataset_id>	Assay Version	
<dataset_id>	Platform	<full array platform description Man + Plat + Version>
<dataset_id>	s4mCurator	<dataset curator name/s>
<dataset_id>	s4mCurationDate	<dataset curation date (usually start date of metadata curation effort>
<dataset_id>	sampleTypeDisplayOrder	Order of sample types for frontend graphing e.g. Sample Type 1[,Sample Type 2]
<dataset_id>	sampleTypeDisplayGroups	JSON dict of sample type groupings indexed by sample type name e.g. {"Sample Type 1":0,"Sample Type 2":0,"Sample Type 3":1}
<dataset_id>	limitSortBy	Sample Type
<dataset_id>	probeCount	<probe_count>
<dataset_id>	probesDetected	<detected_count>
<dataset_id>	minReplicates	1
<dataset_id>	maxReplicates	1
<dataset_id>	topDifferentiallyExpressedGenes	
<dataset_id>	detectionThreshold	<detection_threshold_value>
<dataset_id>	medianDatasetExpression	<median_expression_value>
<dataset_id>	technologyUsed	microarray
<dataset_id>	cellsSamplesAssayed	Cell / sample type list for front-end experiment browser display
<dataset_id>	cellSurfaceAntigenExpression	
<dataset_id>	cellularProteinExpression	
<dataset_id>	nucleicAcidExtract	total RNA extract