#!/bin/sh
target_file="$1"

if [ ! -f "$target_file" ]; then
  echo "Error: File '$target_file' not found!"
  exit 1
fi

cp "$target_file" "${target_file}.sed.bak"

## Fix zero values in last column
sed -i -e 's/\t0$/\t0\.0/g' $target_file
## Fix zero values elsewhere (NOT first column - there should not be any '0' values there)
sed -i -e 's/\t0\t/\t0\.0\t/g' $target_file

