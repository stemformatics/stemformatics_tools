#!/bin/sh
TARGETDIR="$1"

function check_file_exists_matching_pattern {
  file_pattern="$1"
  ## DEBUG ##
  #echo "ls $TARGETDIR/$file_pattern > /dev/null 2>&1"
  ## END DEBUG ##
  ls $TARGETDIR/$file_pattern > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo "Error: Failed to find file with name like '$file_pattern'!"
  fi
}

## Data and metadata
check_file_exists_matching_pattern "assay_platform.txt"
check_file_exists_matching_pattern "biosamples.txt"
check_file_exists_matching_pattern "biosamples_metadata.txt"
check_file_exists_matching_pattern "dataset.txt"
check_file_exists_matching_pattern "dataset_metadata.txt"
check_file_exists_matching_pattern "probe_expression.txt"
check_file_exists_matching_pattern "probe_expression_avg_replicates.txt"

## pgloader configuration
check_file_exists_matching_pattern "pgloader.conf"
check_file_exists_matching_pattern "assay_platform.conf"
check_file_exists_matching_pattern "dataset.conf"
check_file_exists_matching_pattern "biosamples.conf"
check_file_exists_matching_pattern "probe_expression.conf"

## Meta
check_file_exists_matching_pattern "ACCESSION"
check_file_exists_matching_pattern "VERSION"
#check_file_exists_matching_pattern "DIGEST"
