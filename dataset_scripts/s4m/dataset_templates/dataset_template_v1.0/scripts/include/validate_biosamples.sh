#!/bin/sh
TARGETDIR="$1"
TMPDIR="/tmp"
accession=`cat $TARGETDIR/ACCESSION`

source "$TARGETDIR/scripts/include/funcs.sh"


biosamples="$TARGETDIR/biosamples.txt"
biosamples_metadata="$TARGETDIR/biosamples_metadata.txt"
probe_expression="$TARGETDIR/probe_expression.txt"
probe_expression_replicates="$TARGETDIR/probe_expression_avg_replicates.txt"

assert_file_exists "$biosamples"
assert_file_exists "$biosamples_metadata"
assert_file_exists "$probe_expression"
assert_file_exists "$probe_expression_replicates"

## Check column counts are as expected
count_tabbed_file_cols $biosamples
biosamples_cols="$?"
count_tabbed_file_cols $biosamples_metadata
biosamples_meta_cols="$?"
count_tabbed_file_cols $probe_expression
probe_expression_cols="$?"
count_tabbed_file_cols $probe_expression_replicates
probe_expression_rep_cols="$?"
if [ $biosamples_cols -ne 6 ]; then
  echo "Error: File '$biosamples' has $biosamples_cols columns (6 expected)" 
fi
if [ $biosamples_meta_cols -ne 4 ]; then
  echo "Error: File '$biosamples_metadata' has $biosamples_meta_cols columns (4 expected)" 
fi
if [ $probe_expression_cols -ne 7 ]; then
  echo "Error: File '$probe_expression' has $probe_expression_cols columns (7 expected)" 
fi
if [ $probe_expression_rep_cols -ne 8 ]; then
  echo "Error: File '$probe_expression_replicates' has $probe_expression_rep_cols columns (8 expected)" 
fi


## Generate sample id (chip id) lists and counts
awk -F'\t' '{print $1}' "$biosamples" | sort | uniq > "$TMPDIR/biosamples_chip_ids_$accession.txt"
awk -F'\t' '{print $2}' "$biosamples_metadata" | sort | uniq > "$TMPDIR/biosamples_meta_chip_ids_$accession.txt"
awk -F'\t' '{print $2}' "$probe_expression" | sort | uniq > "$TMPDIR/expression_chip_ids_$accession.txt"
awk -F'\t' '{print $3}' "$probe_expression_replicates" | sort | uniq > "$TMPDIR/expression_rep_chip_ids_$accession.txt"
biosamples_chip_count=`cat "$TMPDIR/biosamples_chip_ids_$accession.txt" | wc -l`
biosamples_meta_chip_count=`cat "$TMPDIR/biosamples_chip_ids_$accession.txt" | wc -l`
expression_chip_count=`cat "$TMPDIR/expression_chip_ids_$accession.txt" | wc -l`
rep_expression_chip_count=`cat "$TMPDIR/expression_rep_chip_ids_$accession.txt" | wc -l`

## DEBUG ##
#echo "biosamples: $biosamples_chip_count"
#echo "biosamples_meta: $biosamples_meta_chip_count"
#echo "expression_samples: $expression_chip_count"
#echo "rep_expression_samples: $rep_expression_chip_count"
## END DEBUG ##


### Assertions ###

## All biosamples have metadata
if [ $biosamples_chip_count -ne $biosamples_meta_chip_count ]; then
  echo "Error: Biosamples chip count does not match biosamples metadata chip count!"
fi
diff --brief "$TMPDIR/biosamples_chip_ids_$accession.txt" "$TMPDIR/biosamples_meta_chip_ids_$accession.txt" > /dev/null 2>&1
if [ $? -ne 0 ]; then
  echo "Error: There is a mismatch between chip ids in biosamples file and biosamples_metadata file!"
fi
## All expressed probes have matching biosamples
if [ $expression_chip_count -ne $biosamples_chip_count ]; then
  echo "Error: Expression chip count does not match biosamples chip count!"
fi
diff --brief "$TMPDIR/expression_chip_ids_$accession.txt" "$TMPDIR/biosamples_chip_ids_$accession.txt" > /dev/null 2>&1
if [ $? -ne 0 ]; then
  echo "Error: There is a mismatch between chip ids in biosamples file and probe expression file!"
fi
## Replicate samples are a subset of biosamples
if [ $biosamples_chip_count -lt $rep_expression_chip_count ]; then
  echo "Error: There are more replicate samples than existing in the biosamples file!"
fi
while read rep_sample_id
do
  grep "$rep_sample_id" "$TMPDIR/biosamples_chip_ids_$accession.txt" > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo "Error: Replicate sample '$rep_sample_id' does not exist in biosamples file!"
  fi
done < "$TMPDIR/expression_rep_chip_ids_$accession.txt"

