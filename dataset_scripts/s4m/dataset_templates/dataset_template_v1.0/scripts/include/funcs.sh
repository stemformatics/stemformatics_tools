#!/bin/sh

if [ -z $TMPDIR ]; then
   TMPDIR="/tmp"
fi

function count_tabbed_file_cols {
  file="$1"
  count=`head -1 $file | awk -F'\t' '{print NF; exit}'`
  return $count
}

function assert_file_exists {
  file="$1"
  if [ ! -f "$file" ]; then
    echo "Error: File '$file' does not exist!"
    exit 1
  fi
}

function assert_file_has_attributes {
  file="$1"
  attribs="$2"
  ret=0
  for a in $attribs; do
    attr="$a"
    echo "$a" | grep "_" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
       attr=`echo "$a" | sed -e 's/_/ /g'`
    fi
    grep "$attr" $file > /dev/null 2>&1
    if [ $? -ne 0 ]; then
      echo "Error: Missing attribute '$attr' in file '$file'!"
      ret=1
    fi
  done
  return $ret
}

function load_database_settings {
  pgloader_conf="$1"
  if [ -f "$pgloader_conf" ]; then
    while read pg_line
    do
      echo $pg_line | grep -P "^host|^port|^base|^user|^pass" > /dev/null 2>&1
      if [ $? -eq 0 ]; then
         var=`echo $pg_line | sed -r -e 's/\s*\=.*$//'`
         val=`echo $pg_line | sed -r -e 's/^\w+\s*\=\s*//'`
         eval "pg_$var=\"$val\""
      fi
    done < "$pgloader_conf"

    ## Create temporary "PGPASSFILE" for 'psql' automatic password usage.
    ## See: http://www.postgresql.org/docs/current/static/libpq-pgpass.html
    timestamp=`date +"%Y%m%d-%H%M%S"`
    export PGPASSFILE="$TMPDIR/.pgpass_$timestamp"
    echo "$pg_host:$pg_port:$pg_base:$pg_user:$pg_pass" > "$PGPASSFILE"
    ## If we don't set mode 600, 'psql' will ignore our password file.
    chmod 600 $PGPASSFILE

  else
    echo "Error: pgloader configuration '$pgloader_conf' not found!"
  fi
}

function database_exec {
  sql_input="$1"
  if [ -f "$sql_input" ]; then
    result=`cat "$sql_input" | psql -U $pg_user -h $pg_host $pg_base 2>&1`
  else
    result=`echo "$sql_input" | psql -U $pg_user -h $pg_host $pg_base 2>&1`
  fi
  ## psql doesn't return non-zero error codes, so we try to intercept errors here.
  echo "$result" | grep -P -i "error|fatal" > /dev/null 2>&1
  if [ "$?" -eq 0 ]; then
    echo "$result" 1>&2
    return 1
  else
    echo "$result"
  fi
  return 0
}

function get_dataset_id {
  data_dir="$1"
  if [ ! -f "$data_dir/dataset.txt" ]; then
    return 1
  fi
  ds_id=`awk -F'\t' '{print $1}' $data_dir/dataset.txt`
  echo $ds_id
}
