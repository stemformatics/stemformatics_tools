#!/bin/sh
TARGETDIR="$1"
TMPDIR="/tmp"
accession=`cat $TARGETDIR/ACCESSION`

source "$TARGETDIR/scripts/include/funcs.sh"

biosamples_metadata="$TARGETDIR/biosamples_metadata.txt"
assert_file_exists "$biosamples_metadata"

## Generate  tmp lists
awk -F'\t' '{print $3}' "$biosamples_metadata" | sort | uniq > "$TMPDIR/biosamples_metadata_attributes_$accession.txt"

### Assertions ###

## Check for existence of mandatory attributes
## NOTE: Underscores are replaced with spaces when performing attribute match..
attribs="Cell_Type Disease_State Organism Organism_Part Replicate Replicate_Group_ID Source_Name"
assert_file_has_attributes "$TMPDIR/biosamples_metadata_attributes_$accession.txt" "$attribs"
if [ $? -ne 0 ]; then
  exit 1
fi

