#!/bin/sh
DATADIR="$1"
TMPDIR="/tmp"
PGLOADER_CONF="$DATADIR/pgloader.conf"

source "$DATADIR/scripts/include/funcs.sh"

## DEBUG ##
#echo "host=[$pg_host]"
#echo "port=[$pg_port]"
#echo "user=[$pg_user]"
#echo "pass=[$pg_pass]"
#echo "pass=[$pg_base]"
## END DEBUG ##

load_database_settings "$PGLOADER_CONF"

`which psql > /dev/null 2>&1`
if [ $? -ne 0 ]; then
  echo "Error: Failed to find 'psql' client in path!"
  exit 1
fi
## Test database connection
database_exec "\dt" > /dev/null
if [ $? -ne 0 ]; then
  echo "Error: Failed to connect to postgresql database! Check configuration: $PGLOADER_CONF"
  exit 1
fi
