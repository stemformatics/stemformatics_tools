#!/bin/sh
DATADIR=".."
PGLOADER_CONF="$DATADIR/pgloader.conf"
TMPDIR="/tmp"
accession=`cat $DATADIR/ACCESSION`

source "$DATADIR/scripts/include/funcs.sh"

DS_ID=`get_dataset_id "$DATADIR"`
if [ $? -ne 0 ]; then
  echo "Error: Failed to determine dataset ID! (Check file: '$DATADIR/dataset.txt')"
  exit 1
fi

load_database_settings "$PGLOADER_CONF"
## Test db connection
database_exec "\dt" > /dev/null 2>&1
if [ $? -ne 0 ]; then
  echo "Error: Database connection could not be established! Check config: $PGLOADER_CONF"
  exit 1
fi

unload_dataset="
delete from probe_expressions where ds_id='$DS_ID';
delete from stemformatics.probe_expressions_avg_replicates where ds_id='$DS_ID';
delete from biosamples_metadata where chip_id in (select chip_id from biosamples where ds_id='$DS_ID');
delete from biosamples where ds_id='$DS_ID';
delete from dataset_metadata where ds_id='$DS_ID';
delete from datasets where id='$DS_ID';
"

#database_exec "drop_constraints.sql"
database_exec "$unload_dataset"
#database_exec "reload_constraints.sql"
if [ $? -ne 0 ]; then
  echo "Error: Dataset unload failed! See log: $LOG"
fi
