<ds_id>	<chip_type>	<chip_id>	Cell Line	<cell_line_or_NULL>
<ds_id>	<chip_type>	<chip_id>	Cell Type	<cell_type_ontology_term>
<ds_id>	<chip_type>	<chip_id>	Organism	<Homo sapiens|Mus musculus>
<ds_id>	<chip_type>	<chip_id>	Organism Part	<sample_anatomy_part_or_NULL>
<ds_id>	<chip_type>	<chip_id>	Disease State	<disease_name_or_normal>
<ds_id>	<chip_type>	<chip_id>	Labelling	<antibody labelling protein e.g. "Cy3 dye" or NULL>
<ds_id>	<chip_type>	<chip_id>	Age	<sample patient or specimen age_or_NULL>
<ds_id>	<chip_type>	<chip_id>	Gender	<gender_if_available_or_NULL>
<ds_id>	<chip_type>	<chip_id>	Developmental Stage	<adult_or_embryo_etc_or_NULL>
<ds_id>	<chip_type>	<chip_id>	Sample Description	<curated_sample_description>
<ds_id>	<chip_type>	<chip_id>	Source Name	<original_sample_name>
<ds_id>	<chip_type>	<chip_id>	Sample Type	<curated_sample_type_name>
<ds_id>	<chip_type>	<chip_id>	Replicate Group ID	<curated_sample_id>
<ds_id>	<chip_type>	<chip_id>	Replicate	<replicate_number_usually_1>
<ds_id>	<chip_type>	<chip_id>	Reprogramming Method	<cell_reprogramming_method_or_NULL>
<ds_id>	<chip_type>	<chip_id>	Derived From	<cell_derived_from_or_NULL>
<ds_id>	<chip_type>	<chip_id>	Differentiated To	<cell_differentiated_to_or_NULL>
<ds_id>	<chip_type>	<chip_id>	Cells Sorted For	<cells_sorted_for_or_NULL>
<ds_id>	<chip_type>	<chip_id>	Time	<time_point_or_remove>
<ds_id>	<chip_type>	<chip_id>	Day	<time_point_day_or_remove>
<ds_id>	<chip_type>	<chip_id>	Tissue	<tissue_type_ontology_term_or_NULL>
<ds_id>	<chip_type>	<chip_id>	Genetic Modification	<genetic_modification_details_or_remove>
<ds_id>	<chip_type>	<chip_id>	Pregnancy Status	<optional_if_appropriate_for_mouse_studies_or_remove>
<ds_id>	<chip_type>	<chip_id>	GEO Sample Accession	<optional_if_available_or_remove>
