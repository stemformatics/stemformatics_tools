#!/bin/sh
LOGDIR="./log"
DATADIR=".."
SCRIPTDIR="./include"
TIMESTAMP=`date +"%Y%m%d_%H%M%S"`

if [ ! -d $LOGDIR/$TIMESTAMP ]; then
  mkdir -p $LOGDIR/$TIMESTAMP
fi
echo "Log directory: $LOGDIR/$TIMESTAMP"

## Check for presence of all required data, metadata and configuration files
echo -n "Checking existence of source files.. "
$SCRIPTDIR/check_source_files.sh $DATADIR >> $LOGDIR/$TIMESTAMP/source_file_check.log
if [ `cat "$LOGDIR/$TIMESTAMP/source_file_check.log" | wc -l` -eq 0 ]; then
  echo "[ OK ]"
else
  echo "[ FAIL ] Error: Source file check failed. See log: $LOGDIR/$TIMESTAMP/source_file_check.log"
  exit 1
fi

## List non-ASCII input files
echo -n "Checking source file encoding.. "
$SCRIPTDIR/list_non_ascii_files.sh $DATADIR >> $LOGDIR/$TIMESTAMP/non_ascii_files.log
if [ `cat "$LOGDIR/$TIMESTAMP/non_ascii_files.log" | wc -l` -eq 0 ]; then
  echo "[ OK ]"
else
  echo "[ FAIL ] Error: Source file encoding check failed (expecting ASCII only). See log: $LOGDIR/$TIMESTAMP/non_ascii_files.log"
  exit 1
fi

## List and remove trailing tabs in files
echo -n "Checking source file integrity.. "
$SCRIPTDIR/list_and_remove_trailing_tabs_in_files.sh $DATADIR >> $LOGDIR/$TIMESTAMP/trailing_tabs.log
if [ `cat "$LOGDIR/$TIMESTAMP/trailing_tabs.log" | wc -l` -eq 0 ]; then
  echo "[ OK ]"
else
  echo "[ NOTICE ] There were trailing tabs in source files, they were removed. See log: $LOGDIR/$TIMESTAMP/trailing_tabs.log"
fi

## Check dataset metadata
echo -n "Checking dataset metadata.. "
$SCRIPTDIR/validate_dataset_metadata.sh $DATADIR >> $LOGDIR/$TIMESTAMP/dataset_meta_validation.log
if [ `cat "$LOGDIR/$TIMESTAMP/dataset_meta_validation.log" | wc -l` -eq 0 ]; then
  echo "[ OK ]"
else
  echo "[ FAIL ] Dataset metadata integrity check failed. See log: $LOGDIR/$TIMESTAMP/dataset_meta_validation.log"
  exit 1
fi

## Check sample metadata
echo -n "Checking biosample metadata.. "
$SCRIPTDIR/validate_biosample_metadata.sh $DATADIR >> $LOGDIR/$TIMESTAMP/biosample_meta_validation.log
if [ `cat "$LOGDIR/$TIMESTAMP/biosample_meta_validation.log" | wc -l` -eq 0 ]; then
  echo "[ OK ]"
else
  echo "[ FAIL ] Biosample metadata integrity check failed. See log: $LOGDIR/$TIMESTAMP/biosample_meta_validation.log"
  exit 1
fi

## Check "zero" values in expression data right format for pgloader ("0.0")
echo -n "Checking format of zero '0' values in expression data.. "
$SCRIPTDIR/validate_expression_data.sh $DATADIR >> $LOGDIR/$TIMESTAMP/expression_data_validation.log
if [ `cat "$LOGDIR/$TIMESTAMP/expression_data_validation.log" | wc -l` -eq 0 ]; then
  echo "[ OK ]"
else
  echo "[ FAIL ] Expression data 'zero' value integrity check failed. See log: $LOGDIR/$TIMESTAMP/expression_data_validation.log"
  exit 1
fi

## Check sample mappings
echo -n "Checking biosample expression integrity (this could take some time).. "
$SCRIPTDIR/validate_biosamples.sh $DATADIR >> $LOGDIR/$TIMESTAMP/biosamples_validation.log
if [ `cat "$LOGDIR/$TIMESTAMP/biosamples_validation.log" | wc -l` -eq 0 ]; then
  echo "[ OK ]"
else
  echo "[ FAIL ] Biosample expression integrity check failed. See log: $LOGDIR/$TIMESTAMP/biosamples_validation.log"
  exit 1
fi

## Check platform chip type consistency
echo -n "Checking chip type consistency.. "
$SCRIPTDIR/validate_chip_types.sh $DATADIR >> $LOGDIR/$TIMESTAMP/chip_types_validation.log
if [ `cat "$LOGDIR/$TIMESTAMP/chip_types_validation.log" | wc -l` -eq 0 ]; then
  echo "[ OK ]"
else
  echo "[ FAIL ] Chip type consistency check failed. See log: $LOGDIR/$TIMESTAMP/chip_types_validation.log"
  exit 1
fi

## Check replicate group ids
echo -n "Checking collapsed (replicate) group ID match with biosample metadata (this could take some time).. "
$SCRIPTDIR/validate_replicate_groups.sh $DATADIR >> $LOGDIR/$TIMESTAMP/replicate_group_validation.log
if [ `cat "$LOGDIR/$TIMESTAMP/replicate_group_validation.log" | wc -l` -eq 0 ]; then
  echo "[ OK ]"
else
  echo "[ FAIL ] Replicate groups integrity check failed. See log: $LOGDIR/$TIMESTAMP/replicate_group_validation.log"
  exit 1
fi


echo; echo "Dataset validation passed. You may now load the dataset: nohup load_dataset.sh &"; echo

