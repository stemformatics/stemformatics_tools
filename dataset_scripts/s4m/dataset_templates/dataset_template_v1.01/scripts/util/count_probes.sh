#!/bin/sh
DATADIR="$1"
echo "Counting probes.. "
count=`awk -F'\t' '{print $3}' "$DATADIR/probe_expression.txt" | sort | uniq | wc -l`
echo "Probe count: $count"; echo
