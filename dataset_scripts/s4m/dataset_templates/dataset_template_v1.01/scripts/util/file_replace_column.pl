#!/usr/bin/perl

use Data::Dumper;

$script_name = $0;
sub usage {
  print "Usage: $script_name <map_file> <target_file> <target_column>\n\n";
  exit 1
}

usage() unless scalar(@_) == 3;

$map_file=shift;
$target=shift;
$column=int(shift);


open MAPFILE, "<", $map_file or die $!;
open INFILE, "<", $target or die $!;
open OUTFILE, ">", $target.'.NEW' or die $!;

%mapHash = {};
while (<MAPFILE>) {
   chomp $_;
   if ($_ =~ /\w+\t\w+/) {
      @tokens = split(/\t/, $_);
      $mapHash{$tokens[0]} = $tokens[1];
   }
}

### DEBUG ###
#print Dumper(\%mapHash);
#exit;
### END DEBUG ###

while (<INFILE>) {
   @tokens = split(/\t/, $_);
   $replaceToken = $tokens[$column-1];
   for ($i = 0; $i < $column-1; $i++) {
      print OUTFILE $tokens[$i]."\t";
   }
   if (exists $mapHash{$replaceToken}) {
      $tokens[$column-1] = $mapHash{$replaceToken};
   }
   print OUTFILE $tokens[$column-1];
   for ($i = $column; $i < scalar @tokens; $i++) {
      print OUTFILE "\t".$tokens[$i];
   }
}

