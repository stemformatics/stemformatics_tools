alter table stemformatics.probe_expressions_avg_replicates add constraint probe_expressions_avg_replicates_chip_id_fkey FOREIGN KEY (chip_id)
  REFERENCES biosamples (chip_id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION;

alter table stemformatics.probe_expressions_avg_replicates add constraint probe_expressions_avg_replicates_ds_id_fkey FOREIGN KEY (ds_id)
  REFERENCES datasets (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION;

alter table probe_expressions add constraint probe_expressions_chip_id_fkey FOREIGN KEY (chip_id)
  REFERENCES biosamples (chip_id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION;
  
alter table probe_expressions add constraint probe_expressions_ds_id_fkey FOREIGN KEY (ds_id)
  REFERENCES datasets (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION;



