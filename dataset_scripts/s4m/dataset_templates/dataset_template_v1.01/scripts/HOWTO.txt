Validate dataset:

  cd <dataset_dir>/scripts
  ./validatate_dataset.sh


Load data:

  cd <dataset_dir>/scripts
  nohup ./load_dataset.sh > load_dataset.log 2>&1 &

Unload data:

  cd <dataset_dir>/scripts
  nohup ./unload_dataset.sh > unload_dataset.log 2>&1 &

Retrieve log:

  cp /tmp/pgloader_<accession>.log .

