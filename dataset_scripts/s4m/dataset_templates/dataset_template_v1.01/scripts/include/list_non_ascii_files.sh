#!/bin/sh
TARGETDIR="$1"

files=`ls $TARGETDIR/*txt`
for f in $files; do
  if [ -f $f ] && [ ! -x $f ]; then
    file $f | grep ASCII > /dev/null 2>&1
    if [ $? -ne 0 ]; then
      echo $f
    fi
  fi
done
