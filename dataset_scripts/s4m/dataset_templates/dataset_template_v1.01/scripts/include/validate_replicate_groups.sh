#!/bin/sh
TARGETDIR="$1"
TMPDIR="/tmp"
accession=`cat $TARGETDIR/ACCESSION`

source "$TARGETDIR/scripts/include/funcs.sh"

biosamples_metadata="$TARGETDIR/biosamples_metadata.txt"
probe_expression_replicates="$TARGETDIR/probe_expression_avg_replicates.txt"

assert_file_exists "$biosamples_metadata"
assert_file_exists "$probe_expression_replicates"

## Generate replicate group ID lists
grep "Replicate Group ID" "$biosamples_metadata" | awk -F'\t' '{print $4}' | sort | uniq > "$TMPDIR/biosamples_meta_group_ids_$accession.txt"
awk -F'\t' '{print $5}' "$probe_expression_replicates" | sort | uniq > "$TMPDIR/expression_rep_group_ids_$accession.txt"

## Assert all replicate groups in expression data correspond to those specified in biosamples metadata
while read rep_group
do
  grep "$rep_group" "$TMPDIR/biosamples_meta_group_ids_$accession.txt" > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo "Error: Replicate group ID '$rep_group' does not exist in biosamples metadata file!"
  fi
done < "$TMPDIR/expression_rep_group_ids_$accession.txt"

