#!/bin/sh
TARGETDIR="$1"
TMPDIR="/tmp"
accession=`cat $TARGETDIR/ACCESSION`

source "$TARGETDIR/scripts/include/funcs.sh"

dataset="$TARGETDIR/dataset.txt"
dataset_metadata="$TARGETDIR/dataset_metadata.txt"

assert_file_exists "$dataset"
assert_file_exists "$dataset_metadata"

## Generate  tmp lists
awk -F'\t' '{print $2}' "$dataset_metadata" | sort | uniq > "$TMPDIR/dataset_metadata_attributes_$accession.txt"

### Assertions ###

## Check for existence of mandatory attributes.
## NOTE: These may not apply to private datasets. Needs re-visiting.
## NOTE: Underscores are replaced with spaces when performing attribute match..
public_attribs="PubMed_ID Publication_Title Publication_Citation Release_Date"
core_attribs="Accession Title Description Authors Contributor_Name Contributor_Email Contact_Name Contact_Email Affiliation Organism Experimental_Design Assay_Manufacturer Assay_Platform Assay_Version Platform"
internal_attribs="limitSortBy probeCount probesDetected minReplicates maxReplicates detectionThreshold medianDatasetExpression"
assert_file_has_attributes "$TMPDIR/dataset_metadata_attributes_$accession.txt" "$core_attribs $public_attribs $internal_attribs" 
if [ $? -ne 0 ]; then
  exit 1
fi

