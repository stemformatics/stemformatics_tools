#!/bin/sh
TARGETDIR="$1"
TMPDIR="/tmp"
accession=`cat $TARGETDIR/ACCESSION`

source "$TARGETDIR/scripts/include/funcs.sh"

assay_platform="$TARGETDIR/assay_platform.txt"
biosamples="$TARGETDIR/biosamples.txt"
biosamples_metadata="$TARGETDIR/biosamples_metadata.txt"
probe_expression_avg_replicates="$TARGETDIR/probe_expression_avg_replicates.txt"

assert_file_exists "$assay_platform"
assert_file_exists "$biosamples"
assert_file_exists "$biosamples_metadata"
assert_file_exists "$probe_expression_avg_replicates"

chip_type=`cat $assay_platform | awk -F'\t' '{print $1}'`
chip_count=`echo $chip_type | wc -l`
biosamples_chip_type=`cat $biosamples | awk -F'\t' '{print $2}' | sort | uniq`
biosamples_chip_count=`echo $biosamples_chip_type | wc -l`
biosamples_metadata_chip_type=`cat $biosamples_metadata | awk -F'\t' '{print $1}' | sort | uniq`
biosamples_metadata_chip_count=`echo $biosamples_metadata_chip_type | wc -l`
probe_expression_chip_type=`cat $probe_expression_avg_replicates | awk -F'\t' '{print $2}' | sort | uniq`
probe_expression_chip_count=`echo $probe_expression_chip_type | wc -l`


### Assertions ###

if [ "$chip_count" != "1" ]; then
  echo "Error: Expecting only one chip type in assay platforms, but found $chip_count";
fi
if [ "$biosamples_chip_count" != "1" ]; then
  echo "Error: Expecting only one chip type in biosamples, but found $biosamples_chip_count";
fi
if [ "$biosamples_metadata_chip_count" != "1" ]; then
  echo "Error: Expecting only one chip type in biosamples metadata, but found $biosamples_metadata_chip_count";
fi
if [ "$probe_expression_chip_count" != "1" ]; then
  echo "Error: Expecting only one chip type in probe expression avg replicates, but found $probe_expression_chip_count";
fi


if [ "$biosamples_chip_type" != "$chip_type" ]; then
   echo "Error: Biosamples chip type does not match assay platforms chip type!";
fi 
if [ "$biosamples_metadata_chip_type" != "$chip_type" ]; then
   echo "Error: Biosamples metadata chip type does not match assay platforms chip type!";
fi 
if [ "$probe_expression_chip_type" != "$chip_type" ]; then
   echo "Error: Probe expression avg replicates chip type does not match assay platforms chip type!";
fi 

