#!/bin/sh
TARGETDIR="$1"
TMPDIR="/tmp"
accession=`cat $TARGETDIR/ACCESSION`

source "$TARGETDIR/scripts/include/funcs.sh"


probe_expression="$TARGETDIR/probe_expression.txt"
probe_expression_replicates="$TARGETDIR/probe_expression_avg_replicates.txt"

assert_file_exists "$probe_expression"
assert_file_exists "$probe_expression_replicates"


### Assertions ###

grep -P "\t0\t|\t0$" $probe_expression > /dev/null 2>&1
if [ $? -eq 0 ]; then
  echo "Error: Expression file '$probe_expression' contains '0' values (must be '0.0' to prevent import error)."
  echo "Fix with: $TARGETDIR/scripts/util/fix_zero_values.sh $probe_expression"; echo
fi

grep -P "\t0\t|\t0$" $probe_expression_replicates > /dev/null 2>&1
if [ $? -eq 0 ]; then
  echo "Error: Expression file '$probe_expression_replicates' contains '0' values (must be '0.0' to prevent import error)."
  echo "Fix with: $TARGETDIR/scripts/util/fix_zero_values.sh $probe_expression_replicates"; echo
fi
