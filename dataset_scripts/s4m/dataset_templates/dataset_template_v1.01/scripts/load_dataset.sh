#!/bin/sh
DATADIR=".."
SCRIPTDIR="./include"
TMPDIR="/tmp"
LOGDIR="$TMPDIR"

accession=`cat $DATADIR/ACCESSION`
PGLOG="$LOGDIR/pgloader_$accession.log"

which pgloader > /dev/null 2>&1
if [ $? -ne 0 ]; then
  echo "Error: 'pgloader' not found in path!" 1>&2
  exit 1
fi

## Build our pgloader conf files by merging global options with individual loader rules
cat $DATADIR/pgloader.conf $DATADIR/assay_platform.conf > "$TMPDIR/assay_platform_$accession.conf"
cat $DATADIR/pgloader.conf $DATADIR/dataset.conf > "$TMPDIR/dataset_$accession.conf"
cat $DATADIR/pgloader.conf $DATADIR/biosamples.conf > "$TMPDIR/biosamples_$accession.conf"
cat $DATADIR/pgloader.conf $DATADIR/probe_expression.conf > "$TMPDIR/probe_expression_$accession.conf"

## Test DB connection before invoking pgloader
$SCRIPTDIR/test_db.sh $DATADIR
if [ $? -ne 0 ]; then
  echo "Error: Cannot load data due to database connection error." 1>&2
  exit 1
fi

## Need to be in data directory, since .conf files load files with relative path
cd $DATADIR

pgloader -v -s -D --logfile=$PGLOG -c "$TMPDIR/assay_platform_$accession.conf"
pgloader -v -s -D --logfile=$PGLOG -c "$TMPDIR/dataset_$accession.conf"
pgloader -v -s -D --logfile=$PGLOG -c "$TMPDIR/biosamples_$accession.conf"
## Note: Run a "vacuum analyze" after final data load.
pgloader -v -s -D -V --logfile=$PGLOG -c "$TMPDIR/probe_expression_$accession.conf"


