<dataset_id>	Accession	<accession_id>
<dataset_id>	AE Accession	<optional_remove_if_not_applicable>
<dataset_id>	GEO Accession	<optional_remove_if_not_applicable>
<dataset_id>	Title	<dataset_title>
<dataset_id>	Publication Title	<publication_title>
<dataset_id>	Publication Citation	<publication_citation>
<dataset_id>	Contact Name	<publication_contact_person/s>
<dataset_id>	Contact Email	<publication_contact_email/s>
<dataset_id>	PubMed ID	<pubmed_id>
<dataset_id>	Description	<study_description>
<dataset_id>	Authors	<authors_list>
<dataset_id>	Contributor Name	<contributor_name>
<dataset_id>	Contributor Email	<contributor_email>
<dataset_id>	Affiliation	<contributor_affiliation>
<dataset_id>	Release Date	<dataset_release_date>
<dataset_id>	Organism	<Homo sapiens|Mus musculus>
<dataset_id>	Experimental Design	<e.g. Transcription profiling by array>
<dataset_id>	Assay Manufacturer	<array_manufacturer>
<dataset_id>	Assay Platform	<array_platform>
<dataset_id>	Assay Version	<array_version>
<dataset_id>	Platform	<full array platform description Man + Plat + Version>
<dataset_id>	s4mCurator	<dataset curator name/s>
<dataset_id>	s4mCurationDate	<dataset curation date (usually start date of metadata curation effort>
<dataset_id>	lineGraphOrdering	<Ordered JSON dict of biosamples meta 'Day' values for line graph {Day 0:1,Day 02:2,Day 05:3}>
<dataset_id>	sampleTypeDisplayOrder	<Order of sample types for frontend graphing e.g. Sample Type 1[,Sample Type 2]>
<dataset_id>	sampleTypeDisplayGroups	<JSON dict of sample type groupings indexed by sample type name e.g. {"Sample Type 1":0,"Sample Type 2":0,"Sample Type 3":1}>
<dataset_id>	limitSortBy	<Sample Type[,OtherAttributes]>
<dataset_id>	probeCount	<probe_count>
<dataset_id>	probesDetected	<detected_count>
<dataset_id>	minReplicates	1
<dataset_id>	maxReplicates	<max_replicate_count>
<dataset_id>	topDifferentiallyExpressedGenes	<comma_separated_ensembl_ids_atleast_5>
<dataset_id>	detectionThreshold	<detection_threshold_value>
<dataset_id>	medianDatasetExpression	<median_expression_value>
<dataset_id>	technologyUsed	<assay_technology> 
<dataset_id>	cellsSamplesAssayed	<Cell / sample type list for front-end experiment browser display>
<dataset_id>	cellSurfaceAntigenExpression	<cell_surface_antigen_expression> 
<dataset_id>	cellularProteinExpression	<cellular_protein_expression> 
<dataset_id>	nucleicAcidExtract	<nucleic_acid_extract>
<dataset_id>	genePatternAnalysisAccess	<Disable/Allow (or leave blank for default enabled)>
<dataset_id>	maxGraphValue	<dataset max expression value (for max Y axis)>
