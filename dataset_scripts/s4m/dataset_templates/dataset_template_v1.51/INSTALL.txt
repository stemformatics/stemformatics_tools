    DATASET INSTALLATION HOWTO

 1. Customise pgloader.conf.template (add DB settings etc) OR
    Use an existing configuration from:  S4M_HOME/etc/pgconf

 2. Run: s4m.sh --validate <dataset_dir>

 3. If validation succeeded, load dataset into target DB:
    s4m.sh --load <dataset_dir>  -C <pgloader_conf_name>

 4. Install GCT, CLS files etc into target Stemformatics file system:
    s4m.sh --install <dataset_dir> [user@]host:/path/to/SHARED

 5. Restart Stemformatics instance OR load (admin only) URI:
    http://your.stemformatics.host/expressions/setup_all_sample_metadata
