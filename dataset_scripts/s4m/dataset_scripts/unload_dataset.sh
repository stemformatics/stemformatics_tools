#!/bin/sh
##============================================================================
## Script: unload_dataset.sh
## Author: O.Korn, R.Mosbergen
##
## Unload dataset from Postgres database, Redis and installed files from
## disk on target host.
##
## Note: Called by master "s4m.sh" script. Not intended for direct execution.
##
##============================================================================

SCRIPTNAME="$0"
DSID="$1"
PROFILE="$2"
TMP_DEL_CLS=/tmp/delete_dataset_cls_files_$$
SCRIPTDIR=`dirname $0`
if [ -z "$S4M_HOME" ]; then
  export S4M_HOME="$SCRIPTDIR/.."
fi

. "$S4M_HOME/dataset_scripts/include/funcs.sh"


Usage () {
  echo "Usage: $SCRIPTNAME <dsid> <profile_name>"; echo
  exit
}


## Update: 2018-09-13: Replaced database_exec (pgloader-based) with psql wrapper (See: T#2914) -OK
## 
do_unload_dataset_sql () {
  echo -n " (c)ontinue or (s)kip deletion of dataset $DSID from ** Postgres ** ?  "
  read run_or_skip

  if [ "$run_or_skip" = "c" ]; then

    unload_dataset="
delete from biosamples_metadata where ds_id='$DSID';
delete from biosamples where ds_id='$DSID';
delete from dataset_metadata where ds_id='$DSID';
delete from stemformatics.override_private_datasets where ds_id='$DSID';
delete from stemformatics.stats_datasetsummary where ds_id='$DSID';
delete from stemformatics.jobs where dataset_id='$DSID';
delete from datasets where id='$DSID';
"

    echo; echo " Applying dataset deletion from Postgres.."; echo
    
    psql_wrapper=`get_psql_wrapper_by_profile "$PROFILE"`
    if [ $? -ne 0 ]; then
      return 1
    fi
    $psql_wrapper "$unload_dataset"
    if [ $? -ne 0 ]; then
      return 1
    fi
    echo "  done"

  else
    echo "   Skipped.."
  fi
}


do_unload_dataset_redis () {
  ssh_host="$1"
  ret=0

  echo -n " (c)ontinue or (s)kip deletion of dataset $DSID from ** Redis ** ?  "
  read run_or_skip

  if [ "$run_or_skip" = "c" ]; then

    echo; echo " Applying dataset deletion from Redis.."; echo 

    delkeys=`ssh "$ssh_host" redis-cli -s /data/redis/redis.sock --scan --pattern "cumulative_values\|$DSID\|*"`
    if [ -z "$delkeys" ]; then
      echo "   No cumulative_values to remove.."
    else
      echo "   Removing 'cumulative_values' data.."
      ssh "$ssh_host" redis-cli -s /data/redis/redis.sock --scan --pattern "cumulative_values\|$DSID\|*" '|' xargs redis-cli -s /data/redis/redis.sock DEL
      if [ $? -ne 0 ]; then
        echo "Error: Redis remove 'cumulative_values' for $DSID returned non-zero status" 1>&2
        ret=1
      fi
    fi

    echo "   Removing 'cumulative_labels' data (if any).."
    ssh "$ssh_host" redis-cli -s /data/redis/redis.sock DEL "cumulative_labels\|$DSID"
    if [ $? -ne 0 ]; then
      echo "Error: Redis remove 'cumulative_labels' for $DSID returned non-zero status" 1>&2
      ret=1
    fi

    delkeys=`ssh "$ssh_host" redis-cli -s /data/redis/redis.sock --scan --pattern "gct_values\|$DSID\|*"`
    if [ -z "$delkeys" ]; then
      echo "   No gct_values to remove.."
    else
      echo "   Removing 'gct_values' data.."
      ssh "$ssh_host" redis-cli -s /data/redis/redis.sock --scan --pattern "gct_values\|$DSID\|*" '|' xargs redis-cli -s /data/redis/redis.sock DEL
      if [ $? -ne 0 ]; then
        echo "Error: Redis remove 'gct_values' for $DSID returned non-zero status" 1>&2
        ret=1
      fi
    fi

    echo "   Removing 'gct_labels' data (if any).."
    ssh "$ssh_host" redis-cli -s /data/redis/redis.sock DEL "gct_labels\|$DSID"
    if [ $? -ne 0 ]; then
      echo "Error: Redis remove 'gct_labels' for $DSID returned non-zero status" 1>&2
      ret=1
    fi

  else
    echo "   Skipped.."
  fi
  return $ret
}


## PORTABILITY: Non-POSIX code in this function "$()" syntax (Bash)
## TODO: Refactor
##
do_delete_dataset_files_from_disk () {
  prof="$1"

  ssh_target=`get_ssh_target "$prof"`
  if [ $? -ne 0 ]; then
    echo "Error: Failed to determine SSH host from profile '$prof'" 1>&2
    return 1
  fi
  ssh_files_path=`get_ssh_install_path "$prof"`
  if [ $? -ne 0 ]; then
    echo "Error: Failed to determine dataset files path from profile '$prof'" 1>&2
    return 1
  fi
  ##########################################################
  # The files that are to be deleted here should be:
  # 1/ cumulative.txt file for that dataset (Yugene)
  # eg ./data/portal_data/pylons-data/prod/./CUMULATIVEFiles/dataset6075.cumulative.txt
  # 2/ CLS files for that dataset
  # /data/portal_data/pylons-data/prod/./CLSFiles/6075Cell_Line.cls
  #/data/portal_data/pylons-data/prod/./CLSFiles/6075Testing210.cls
  #/data/portal_data/pylons-data/prod/./CLSFiles/6075.cls
  #/data/portal_data/pylons-data/prod/./CLSFiles/6075Testing1.cls
  #/data/portal_data/pylons-data/prod/./CLSFiles/6075Testing21.cls
  #/data/portal_data/pylons-data/prod/./CLSFiles/6075Treatment.cls
  # 3/ Probe file for that dataset
  # /data/portal_data/pylons-data/prod/./ProbeFiles/6075.probes
  # 4/ GCT File for that dataset
  # /data/portal_data/pylons-data/prod/./GCTFiles/dataset6075.gct
  # 5/ Standard Deviations file
  # /data/portal_data/pylons-data/prod/./StandardDeviationFiles/probe_expression_avg_replicates_6275.txt 
  ##########################################################

  echo " Standard files to be removed:"; echo
  files_list=`ssh "$ssh_target"  cd "$ssh_files_path" "&&" find . -type f '|' grep -E "\/$DSID[\.]'|'dataset$DSID[\.]" `
  if [ -z "$files_list" ]; then
    echo "   (None, skipping..)"
  else 
    echo "$files_list" | sed -r -e "s|^|  $ssh_files_path/|g"
    echo; echo -n " (c)ontinue or (s)kip deletion of dataset $DSID ** files ** ?  "
    read run_or_skip
    if [ "$run_or_skip" = "c" ]; then
      ssh "$ssh_target"  cd "$ssh_files_path" "&&" find . -type f '|' grep -E "\/$DSID[\.]'|'dataset$DSID[\.]" '|' xargs rm 
    else
      echo "   Skipped.."
    fi
  fi

  ## search for extra group .CLS files 
  `ssh "$ssh_target"  cd "$ssh_files_path" "&&" find . -type f '|' grep -E "\/$DSID[A-Za-z]+.*\.cls$"  > $TMP_DEL_CLS`
  
  ##############################################################
  # The files to be deleted here are only .cls files
  ##############################################################

  if [ ! $(wc -c < "$TMP_DEL_CLS") = "0" ]; then
    sed -i.bak -r -e "s|^|  $ssh_files_path/|g" $TMP_DEL_CLS
    echo; echo " However, the following non-standard .cls files require confirmation for deletion: "
    cat $TMP_DEL_CLS
    echo; echo -n " (c)ontinue or (s)kip interactive deletion of these ** .cls files ** possibly associated with dataset $DSID ?  "
    read run_or_skip
    if [ "$run_or_skip" = "c" ]; then
      for i in $(cat $TMP_DEL_CLS);
        do 
          ssh "$ssh_target" cd "$ssh_files_path" "&&"  "rm -i $i"
	done
    else
      echo "   Skipped.."
    fi
  else
    echo " No files found needing operator confirmation "
    echo 
  fi

  rm $TMP_DEL_CLS
}


## Return 'ssh_install_uri' value from profile
get_ssh_install_uri () {
  prof="$1"

  ssh_install_uri=`grep ssh_install_uri "$prof"`
  if [ -z "$ssh_install_uri" ]; then
    return 1
  fi
  echo "$ssh_install_uri"
}


get_ssh_install_path () {
  prof="$1"

  ssh_uri=`get_ssh_install_uri "$prof"`
  if [ $? -ne 0 ]; then
    echo "Error: Could not get 'ssh_install_uri' config value from profile '$prof'" 1>&2
    return 1
  fi
  files_path=`echo "$ssh_uri" | cut -d'=' -f 2 | tr -d " " | cut -d':' -f 2`
  echo "$files_path"
}


## Get SSH settings from host profile (extract host part from "ssh_install_uri")
get_ssh_target () {
  prof="$1"

  ssh_uri=`get_ssh_install_uri "$prof"`
  if [ $? -ne 0 ]; then
    echo "Error: Could not get 'ssh_install_uri' config value from profile '$prof'" 1>&2
    return 1
  fi
  ssh_host=`echo "$ssh_uri" | cut -d'=' -f 2 | tr -d " " | cut -d':' -f 1`
  ## is valid host?
  host "$ssh_host" > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo "Error: Could not extract valid hostname from 'ssh_install_uri' string in profile '$prof'" 1>&2
    return 1
  fi
  echo "$ssh_host"
}


test_ssh_connection () {
  target="$1"

  ## Current user running s4m.sh should be able to SSH to target otherwise
  ## things like dataset install would fail too!
  ssh "$target" uname -a > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    return 1
  fi
}


main () {
  if [ -z "$DSID" -o -z "$PROFILE" ]; then
    Usage
  fi

  profile=`find_profile_file`
  if [ $? -ne 0 ]; then
    echo "Error: Could not find target host profile '$PROFILE'" 1>&2
    echo "Directory search order: $S4M_HOME/etc/profile/; $CWD/;" 1>&2
    exit 1
  fi

  ## Test db connection
  psql_wrapper=`get_psql_wrapper_by_profile "$PROFILE"`
  if [ $? -ne 0 ]; then
    return 1
  fi
  $psql_wrapper "\dt" > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo "Error: Database connection could not be established! Check PSQL wrapper: $psql_wrapper"
    exit 1
  fi

  ssh_target=`get_ssh_target "$profile"`
  ## Test SSH access
  test_ssh_connection "$ssh_target" 
  if [ $? -ne 0 ]; then
    echo "Error: Failed to SSH to host '$ssh_target'!"
    exit 1
  fi

  ## If we got this far, it's likely that SQL and Redis calls will work

  echo
  ## [1] Remove dataset from Postgres
  do_unload_dataset_sql
  if [ $? -ne 0 ]; then
    echo "Error: Dataset unload from Postgres had non-zero exit status!" 1>&2
    exit 1
  fi

  echo
  ## [2] Remove dataset from Redis
  do_unload_dataset_redis "$ssh_target"
  if [ $? -ne 0 ]; then
    echo "Error: One or more Redis delete operations had non-zero exit status!" 1>&2
    exit 1
  fi

  echo
  ## [3] Remove dataset files from disk
  do_delete_dataset_files_from_disk "$profile"
  if [ $? -ne 0 ]; then
    echo "Error: Dataset file removal (GCT, CLS, cumulative.txt, .probes etc) had non-zero exit status!" 1>&2
    exit 1
  fi

  echo; echo "All done"
}


### START ###
main

