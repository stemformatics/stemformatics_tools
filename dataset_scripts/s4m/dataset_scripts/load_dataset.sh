#!/bin/sh
##============================================================================
## Script: load_dataset.sh
## Author: O.Korn
##
## Load a Stemformatics dataset into a Postgres DB using a pre-configured
## host profile (See: stemformatics_tools/etc/profile)
##
## Note: Called by master "s4m.sh" script. Not intended for direct execution.
##
##============================================================================
DATADIR="$1"
## Postgres profile
PROFILE="$2"
if [ -z "$DATADIR" -o ! -d "$DATADIR" ]; then
  echo "Error: Bad dataset directory (null or not existing) given to DB load script $0"; echo
  exit 1
fi
if [ -z "$PROFILE" ]; then
  echo "Error: Must specify target host configuration file!"; echo
  exit 1
fi

SCRIPTDIR=`dirname $0`
SCRIPTDIR="$SCRIPTDIR/include"

. "$SCRIPTDIR/funcs.sh"

load_timestamp=`date +"%Y%m%d-%H%M%S"`
#TMPDIR="/tmp/s4m/pgloader_${load_timestamp}"
#mkdir -p $TMPDIR
#chgrp portaladmin $TMPDIR
LOGDIR="$DATADIR/load"
mkdir -p "$LOGDIR"

accession=`cat $DATADIR/ACCESSION`
#PGLOG="$LOGDIR/pgloader_${accession}_${load_timestamp}.log"
PGLOG="$LOGDIR/load_${accession}_${load_timestamp}.log"


## Update 2018-09-13: Replaced use of 'pgloader' with psql_wraper throughout (now using
## psql native 'COPY' command).
## Reason: Loading into postgres v9.2.24 in CentOS 7 causes postgres to hang. -OK
##
main () {
  ## Check validity of given host profile
  use_profile=`find_profile_file "$PROFILE"`
  if [ -z "$use_profile" ]; then
    echo "Error: Host profile '$PROFILE' not found (Check: $S4M_HOME/etc/profile/$PROFILE)
" 1>&2
    exit 1
  fi

  psql_wrapper=`get_psql_wrapper_by_profile "$use_profile"`
  if [ $? -ne 0 ]; then
    exit 1
  fi

  ## Test DB connection
  $psql_wrapper "\dt" > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo "Error: Database connection could not be established! Check profile settings: $use_profile
" 1>&2
    exit 1
  fi

  ## Test for presence of chip type in assay platform, otherwise fail
  ##
  ## UPDATE: 2017-08-31: We shouldn't be checking assay_platforms any more.
  ##	Now checking the mapping_id instead of chip_type. -OK
  ##
  mapping_id=`get_metastore_value "$DATADIR" mapping_id`
  if [ ! -z "$mapping_id" ]; then
    sql="COPY (SELECT count(*) FROM stemformatics.feature_mappings WHERE mapping_id=$mapping_id) TO STDOUT"
    result=`$psql_wrapper "$sql" 2>&1 | tr -d [:cntrl:]`
    if [ $result = "0" ]; then
      echo "Error: Probe mapping $mapping_id not loaded in Stemformatics!"
      exit 1
    fi
  else
    echo "WARNING: Failed to determine mapping_id for 'stemformatics.feature_mappings' check, skipping check.."
  fi

  ## Need to be in data directory, since .conf files load files with relative path
  cd "$DATADIR"

  ## Reformat 'datasets' fields for loading
  cat dataset.txt | awk -F'\t' '{print $1 "\tUNKNOWN\t" $2 "\t" $3 "\ttrue\ttrue\t" $4 "\t" $6 "\tfalse\tfalse\t" $5 "\t\t" $10 "\t" $8 "\t" $9}' > dataset.txt.load
  psql_wrapper_load "$psql_wrapper" datasets dataset.txt.load
  if [ $? -ne 0 ]; then
    cat $PGLOG 1>&2
    exit 1
  fi
  ## Column order matches, no reformatting required
  psql_wrapper_load "$psql_wrapper" dataset_metadata dataset_metadata.txt
  if [ $? -ne 0 ]; then
    cat $PGLOG 1>&2
    exit 1
  fi
  ## Column order matches, no reformatting required
  psql_wrapper_load "$psql_wrapper" biosamples biosamples.txt
  if [ $? -ne 0 ]; then
    cat $PGLOG 1>&2
    exit 1
  fi
  ## Reformat 'biosamples_metadata' fields for loading
  cat biosamples_metadata.txt | awk -F'\t' '{print $2 "\t" $3 "\t" $4 "\t" $5 "\t" $1}' > biosamples_metadata.txt.load
  psql_wrapper_load "$psql_wrapper" biosamples_metadata biosamples_metadata.txt.load
  if [ $? -ne 0 ]; then
    cat $PGLOG 1>&2
    exit 1
  fi

  echo; echo "Updating biosamples dataset summary stats.. "
  ## Update biosamples dataset summary stats
  ## NOTE: Must "source" the script so database_exec() call can access this environment
  . "$SCRIPTDIR/refresh_dataset_sample_summaries.sh" "$psql_wrapper"
  echo "done"
  echo; echo "Don't forget to refresh biosamples metadata cache in your Stemformatics instance."

}

### START ###
main
