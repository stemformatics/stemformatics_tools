#!/bin/sh
SCRIPTNAME="$0"
CUMULATIVE_SCRIPT="$1"
shift
DATADIR="$1"
shift

gctfiles="$@"

Usage () {
  echo; echo "Usage: $SCRIPTNAME </path/to/cumulative_script.R> </path/to/gctfiles/dir> <1.gct [2.gct .. N.gct]>"; echo
  echo "    Creates cumulative rank expression files for input GCT files; outputs to GCT directory."
  exit
}

if [ -z "$CUMULATIVE_SCRIPT" -o -z "$DATADIR" -o -z "$gctfiles" ]; then
  Usage
fi

echo "Creating cumulative ranked expression file/s"
#echo "SCRIPT=$CUMULATIVE_SCRIPT"
#echo "DATADIR=$DATADIR"

created=""
for f in $gctfiles
do
  filebase=`echo "$f" | sed -r -e 's/\..*$//g'`
  R -q --slave "--file=$CUMULATIVE_SCRIPT" --args "$DATADIR" "$f"
  ## Add "Probe" label to first column if not present.
  head -1 "$DATADIR/$filebase.cumulative.txt" | grep -P "^Probe" > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    sed -i '1s/^/Probe/g' "$DATADIR/$filebase.cumulative.txt"
  fi
  created="$created
	$filebase.cumulative.txt"
done

echo "Created $created"
