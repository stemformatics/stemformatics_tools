#!/bin/sh
SCRIPTNAME="$0"
CUMULATIVE_SCRIPT="$1"
shift
DATADIR="$1"
shift

inputfiles="$@"

Usage () {
  echo; echo "Usage: $SCRIPTNAME </path/to/cumulative_script.R> </path/to/data/dir> <1.txt [2.txt .. N.txt]>"; echo
  echo "    Creates cumulative rank expression files for input data tables; outputs to data directory."
  exit
}

if [ -z "$CUMULATIVE_SCRIPT" -o -z "$DATADIR" -o -z "$inputfiles" ]; then
  Usage
fi

echo "Creating cumulative ranked expression file/s"
#echo "SCRIPT=$CUMULATIVE_SCRIPT"
#echo "DATADIR=$DATADIR"

created=""
for f in $inputfiles
do
  R -q --slave "--file=$CUMULATIVE_SCRIPT" --args "$DATADIR" "$f"
  filebase=`basename $f`
  extcol=`echo "$filebase" | awk -F'.' '{print NF}'`
  ext=`echo "$filebase" | cut -d'.' -f $extcol`
  filebase=`basename $filebase ".$ext"`

  #echo "DEBUG: filebase=[$filebase], ext=[$ext]"

  ## Add "Probe" label to first column if not present.
  head -1 "$DATADIR/$filebase.cumulative.txt" | grep -P "^Probe" > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    sed -i '1s/^/Probe/g' "$DATADIR/$filebase.cumulative.txt"
  fi
  ## Remove .CEL from Affy sample IDs (if necessary)
  sed -i '1 s/.CEL//gi' "$DATADIR/$filebase.cumulative.txt"
  created="$created
	$filebase.cumulative.txt"
done

echo "Created $created"
