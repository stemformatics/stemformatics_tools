#!/bin/sh
TARGETDIR="$1"
INCLUDE_DIR=`dirname $0`
TMPDIR="$TARGETDIR"
accession=`cat $TARGETDIR/ACCESSION`
timestamp=`date +"%Y%m%d-%H%M%S"`

. "$INCLUDE_DIR/funcs.sh"

biosamples_metadata="$TARGETDIR/biosamples_metadata.txt"
assert_file_exists "$biosamples_metadata"

## Generate  tmp lists
awk -F'\t' '{print $4}' "$biosamples_metadata" | sort -T "$TMPDIR" | uniq > "$TMPDIR/biosamples_metadata_attributes_${accession}_${timestamp}.txt"


### Assertions ###

## Check for existence of mandatory attributes
## NOTE: Underscores are replaced with spaces when performing attribute match..
attribs="Sample_Type Replicate_Group_ID"
assert_file_has_attributes "$TMPDIR/biosamples_metadata_attributes_${accession}_${timestamp}.txt" "$attribs"
if [ $? -ne 0 ]; then
  rm "$TMPDIR/biosamples_metadata_attributes_${accession}_${timestamp}.txt"
  exit 1
fi

## Check that all metadata field values are non-empty
grep -P "\t$" "$biosamples_metadata" > /dev/null 2>&1
if [ $? -eq 0 ]; then
  echo "Error: Biosamples metadata file '$biosamples_metadata' contains one or more empty field values (use string \"NULL\" instead)"
fi
rm "$TMPDIR/biosamples_metadata_attributes_${accession}_${timestamp}.txt"
