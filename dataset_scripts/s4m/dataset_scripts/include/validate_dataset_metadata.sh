#!/bin/sh
TARGETDIR="$1"
INCLUDE_DIR=`dirname $0`
TMPDIR="$TARGETDIR"
accession=`cat $TARGETDIR/ACCESSION`
timestamp=`date +"%Y%m%d-%H%M%S"`


. "$INCLUDE_DIR/funcs.sh"

dataset="$TARGETDIR/dataset.txt"
dataset_metadata="$TARGETDIR/dataset_metadata.txt"
dataset_id=`get_dataset_id "$TARGETDIR"`
biosamples_metadata="$TARGETDIR/biosamples_metadata.txt"


assert_dataset_metadata_attributes_not_null () {
  file="$1"
  attribs="$2"
  ret=0
  for a in $attribs; do
    attr="$a"
    echo "$a" | grep "_" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
       attr=`echo "$a" | sed -e 's/_/ /g'`
    fi
    line=`grep "$attr" $file 2> /dev/null`
    value=`echo "$line" | awk -F'\t' '{print $3}' | tr -d [:cntrl:]`
    if [ -z "$value" ]; then
      echo "Error: Missing value for '$attr' in file '$file' (must not be blank)!"
      ret=1
    #else
      #defaultValue=`echo "$value" | grep -P "^\<" 2> /dev/null`
      #if [ ! -z "$defaultValue" ]; then
      #  echo "Error: Value for '$attr' is not set (has default value) in file '$file'!"
      #  ret=1
      #fi
    fi
  done
  return $ret
}

assert_dataset_attributes_not_default () {
  file="$1"
  attribs="$2"
  ret=0
  for a in $attribs; do
    attr="$a"
    echo "$a" | grep "_" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
       attr=`echo "$a" | sed -e 's/_/ /g'`
    fi
    line=`grep "$attr" $file 2> /dev/null`
    value=`echo "$line" | awk -F'\t' '{print $3}' | tr -d [:cntrl:]`
    defaultValue=`echo "$value" | grep -P "^\<|^IGNORE" 2> /dev/null`
    if [ ! -z "$defaultValue" ]; then
      echo "Error: Value for '$attr' is not set (has default template value) in file '$file'!"
      ret=1
    fi
  done
  return $ret
}


assert_file_exists "$dataset"
assert_file_exists "$dataset_metadata"

## Generate  tmp lists
awk -F'\t' '{print $2}' "$dataset_metadata" | sort | uniq > "$TMPDIR/dataset_metadata_attributes_${accession}_${timestamp}.txt"

### Assertions ###

## Check for existence of mandatory attributes.
## NOTE: These may not apply to private datasets. Needs re-visiting.
## NOTE: Underscores are replaced with spaces when performing attribute match..
core_attribs="Accession AE_Accession GEO_Accession Title Description Authors Contributor_Name Contributor_Email Contact_Name Contact_Email Affiliation Organism Experimental_Design Assay_Manufacturer Assay_Platform Assay_Version Platform"
public_attribs="PubMed_ID Publication_Title Publication_Citation Release_Date"
internal_attribs="sampleTypeDisplayOrder sampleTypeDisplayGroups limitSortBy probeCount probesDetected minReplicates maxReplicates medianDatasetExpression technologyUsed cellsSamplesAssayed s4mCurator s4mCurationDate topDifferentiallyExpressedGenes probeName"
not_null_attribs="Accession Title Description Contributor_Name Affiliation Organism Experimental_Design Assay_Manufacturer Assay_Platform Platform sampleTypeDisplayOrder sampleTypeDisplayGroups limitSortBy probeCount probesDetected minReplicates maxReplicates medianDatasetExpression technologyUsed cellsSamplesAssayed s4mCurator"
## T#2718: yAxisLabel is treated specially, it should exist, and the default value is encouraged for annotation purposes
assert_file_has_attributes "$TMPDIR/dataset_metadata_attributes_${accession}_${timestamp}.txt" "$core_attribs $public_attribs $internal_attribs yAxisLabel" 
if [ $? -ne 0 ]; then
  rm -f "$TMPDIR/dataset_metadata_attributes_${accession}_${timestamp}.txt"
  exit 1
fi
## Check that values are set in dataset_metadata.txt, and are not default template values like "<detection threshold>"
assert_dataset_metadata_attributes_not_null "$dataset_metadata" "$not_null_attribs"
assert_dataset_attributes_not_default "$dataset_metadata" "$core_attribs $public_attribs $internal_attribs $not_null_attribs"

experimentalDesign=`grep -P "^$dataset_id\tExperimental Design" $dataset_metadata | awk -F'\t' '{print $3}' | tr -d [:cntrl:]`
## Technology-specific not-NULL checks
if [ "$experimentalDesign" = "transcription profiling by array" ]; then
  assert_dataset_metadata_attributes_not_null "detectionThreshold"
  assert_dataset_attributes_not_default "detectionThreshold"
fi


## Check well-formedness of particular fields of interest
## sampleTypeDisplayOrder not empty and contains N-1 commas where N is the number of different cell types from biosamples_metadata.txt
if [ ! -f "$biosamples_metadata" ]; then
  echo "Error: Biosamples metadata file '$biosamples_metadata' required for dataset metadata cross-validation!"; echo
  exit 1
fi
sampleTypeDisplayOrder=`grep -P "^$dataset_id\tsampleTypeDisplayOrder" $dataset_metadata | awk -F'\t' '{print $3}'`
sampleTypeCount=`grep -P "\tSample Type\t" $biosamples_metadata | awk -F'\t' '{print $5}' | sort | uniq | wc -l`
commaCount=`echo "$sampleTypeDisplayOrder" | sed -r -e 's/[^\,]//g' | tr -d [:cntrl:] | wc -c`
if [ $commaCount -ne `expr $sampleTypeCount - 1` ]; then
  echo "Error: Number of sample types in 'sampleTypeDisplayOrder' field does not match ($sampleTypeCount) from biosamples_metadata.txt"; echo
  exit 1
fi

## sampleTypeDisplayGroups not empty, starts and ends with '{' and '}' respectively, contains at least as many commas as there are different cell types from biosamples_metadata.txt
sampleTypeDisplayGroups=`grep -P "^$dataset_id\tsampleTypeDisplayGroups" $dataset_metadata | awk -F'\t' '{print $3}'`
echo "$sampleTypeDisplayGroups" | grep -P "^\{.*\}$"  > /dev/null 2>&1
if [ $? -ne 0 ]; then
  echo "Error: Bad dataset metadata 'sampleTypeDisplayGroups' field format. Expecting: JSON dict text representation (starts with '{', ends with '}')"
  echo "Example:  {\"Sample Type 1\":0,\"Sample Type 2\":0,\"Sample Type 3\":1}\""; echo
  exit 1
fi
commaCount=`echo "$sampleTypeDisplayGroups" | sed -r -e 's/[^\,]//g' | tr -d [:cntrl:] | wc -c`
if [ $commaCount -ne `expr $sampleTypeCount - 1` ]; then
  echo "Error: Number of sample types in 'sampleTypeDisplayGroups' field does not match expected ($sampleTypeCount)"; echo
  exit 1
fi

## limitSortBy is not empty, at least contains 'Sample Type', and optionally another field separated by a comma
limitSortBy=`grep -P "^$dataset_id\tlimitSortBy" $dataset_metadata | awk -F'\t' '{print $3}'`
echo "$limitSortBy" | grep "Sample Type" > /dev/null 2>&1
if [ $? -ne 0 ]; then
  echo "Error: Dataset metadata field 'limitSortBy' must contain at least 'Sample Type'"; echo
  exit 1
fi
## OK This no longer holds - we can now (s4m.sh v1.41+) support multiple limitSortBy fields.
#commaCount=`echo "$limitSortBy" | sed -r -e 's/[^\,]//g' | tr -d [:cntrl:] | wc -c`
#if [ $commaCount -gt 1 ]; then
#  gotCount=`expr $commaCount + 1`
#  echo "Error: Maximum of two biosamples metadata fields may be specified in dataset metadata 'limitSortBy' specification! (Got $gotCount)"; echo
#  exit 1
#fi

## if topDifferentiallyExpressedGenes is not empty, at least 5 Ensembl IDs given
topGenes=`grep -P "^$dataset_id\ttopDifferentiallyExpressedGenes" $dataset_metadata | awk -F'\t' '{print $3}'`
if [ ! -z "$topGenes" ]; then
  if [ "$topGenes" != "NULL" ]; then
    commaCount=`echo "$topGenes" | sed -r -e 's/[^\,]//g' | tr -d [:cntrl:] | wc -c`
    if [ $commaCount -lt 4 ]; then
      echo "Error: If specified, must provide at least 5 comma-separated Ensembl genes in dataset metadata field 'topDifferentiallyExpressedGenes'"; echo
      exit 1
    fi
  fi
fi

technologyUsed=`grep -P "^$dataset_id\ttechnologyUsed" $dataset_metadata | awk -F'\t' '{print $3}' | tr -d [:cntrl:]`
if [ "$technologyUsed" != "short read sequencer" -a "$technologyUsed" != "microarray" -a "$technologyUsed" != "high throughput sequencer" -a "$technologyUsed" != "single molecule sequencing assay" -a "$technologyUsed" != "GCMS" -a "$technologyUsed" != "LCMS" ]; then
  echo "Error: Currently, only 'short read sequencer' OR 'microarray' OR 'high throughput sequencer' OR 'single molecule sequencing assay' OR 'GCMS' OR 'LCMS' types supported for technologyUsed field."; echo
  exit 1
fi

## GEO Accession regex test
grep "GEO Accession" "$dataset_metadata" > /dev/null 2>&1
if [ $? -eq 0 ]; then
  geoAccession=`grep -P "^$dataset_id\tGEO Accession" $dataset_metadata | awk -F'\t' '{print $3}'`
  if [ ! -z "$geAccession" ]; then
    echo "$geoAccession" | grep -P "^GSE\d+" > /dev/null 2>&1
    if [ $? -ne 0 ]; then
      echo "Error: GEO Accession '$geoAccession' does not appear to be a valid GEO dataset accession. Expected format: GSExxxxx where 'x' is a digit."; echo
      exit 1
    fi
  fi
fi

if [ "$experimentalDesign" != "short read sequencing assay" -a "$experimentalDesign" != "transcription profiling by array" -a "$experimentalDesign" != "transcription profiling by high throughput sequencing" -a "$experimentalDesign" != "cap analysis gene expression assay" -a "$experimentalDesign" != "chip-seq" -a "$experimentalDesign" != "gas chromatography mass spectrometry" -a "$experimentalDesign" != "liquid chromatography mass spectrometry" -a "$experimentalDesign" != "assay for transposase-accessible chromatin with high throughput sequencing" ]; then
  echo "Error: Currently, Experimental Design can be any of: 
  'short read sequencing assay'
  'transcription profiling by array'
  'transcription profiling by high throughput sequencing'
  'cap analysis gene expression assay'
  'chip-seq'
  'gas chromatography mass spectrometry'
  'liquid chromatography mass spectrometry'
  'assay for transposase-accessible chromatin with high throughput sequencing'
"
  exit 1
fi

