#!/bin/sh
TARGETDIR="$1"

remove_trailing_tabs () {
  file="$1"
  cp $file $file.bak
  sed -r -i -e 's/\t+$//g' $file 2> /dev/null
  if [ $? -eq 0 ]; then
    echo "[ OK ]"
  else
    echo "[ ERROR ]"
  fi
}

for f in `ls $TARGETDIR`; do
  grep -P "\t$" $f > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    echo -n "File '$f' has trailing tabs! Removing them now.. "
    remove_trailing_tabs "$f"
  fi  
done

