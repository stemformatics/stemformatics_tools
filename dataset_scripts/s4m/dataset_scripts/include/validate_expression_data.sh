#!/bin/sh

TARGETDIR="$1"
INCLUDE_DIR=`dirname $0`
TMPDIR="$TARGETDIR"
accession=`cat $TARGETDIR/ACCESSION`


. "$INCLUDE_DIR/funcs.sh"


probe_expression_replicates="$TARGETDIR/probe_expression_avg_replicates.txt"

## If probe_expression_avg_replicates not found, but we have a GCT file, do not proceed
if [ ! -f "$probe_expression_replicates" -a -f "$TARGETDIR/$accession.gct" ]; then
  exit
fi
assert_file_exists "$probe_expression_replicates"


### Assertions ###

colcount=`awk -F'\t' '{print NF}' $probe_expression_replicates | sort | uniq`
if [ $colcount -ne 8 ]; then
  echo "Error: Expression file '$probe_expression_replicates' has bad column count; expecting 8 columns"
fi

#non_numeric_chip_type_count=`awk -F'\t' '{print $2}' probe_expression.txt | grep -P "^[^\d]" | wc -l`
#if [ $non_numeric_chip_type_count -ne 0 ]; then
#  echo "Error: Expression file '$probe_expression' has $non_numeric_chip_type_count bad chip_type (field 2) values!"
#fi
#ds_id_non_numeric_count=`grep -P "^[^\d]" $probe_expression | wc -l`
#if [ $ds_id_non_numeric_count -ne 0 ]; then
#  echo "Error: Expression file '$probe_expression' has $ds_id_non_numeric_count bad ds_id (field 1) values!"
#fi

grep -P "\t0\t|\t0$" $probe_expression_replicates > /dev/null 2>&1
if [ $? -eq 0 ]; then
  echo "Error: Expression file '$probe_expression_replicates' contains '0' values (must be '0.0' to prevent import error)."
  utildir=`cd $INCLUDE_DIR/../util && pwd`
  echo "Fix with: $utildir/fix_zero_values.sh $probe_expression_replicates"; echo
  #echo "Fix with: $TARGETDIR/scripts/util/fix_zero_values.sh $probe_expression_replicates"; echo
fi

grep -P -v "\t(TRUE|FALSE)$" $probe_expression_replicates > /dev/null 2>&1
if [ $? -eq 0 ]; then
  echo "Error: Expression file '$probe_expression_replicates' has illegal data in 'detection' column! (allowed: boolean 'TRUE' or 'FALSE' only)!"
fi
