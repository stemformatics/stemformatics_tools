#!/bin/sh
TARGETDIR="$1"

utf8_white_list="
$TARGETDIR/dataset.txt
$TARGETDIR/dataset_metadata.txt
$TARGETDIR/biosamples.txt
$TARGETDIR/biosamples_metadata.txt
$TARGETDIR/chip_id_replicate_group_map.txt
$TARGETDIR/probe_expression_avg_replicates.txt
$TARGETDIR/sample_pheno.txt
"

files=`ls $TARGETDIR/*txt`
for f in $files; do
  if [ -f $f ] && [ ! -x $f ]; then
    echo $utf8_white_list | grep "$f" > /dev/null 2>&1
    #if [ "$f" = "$TARGETDIR/dataset_metadata.txt" -o "$f" = "$TARGETDIR/biosamples_metadata.txt" -o "$f" = "$TARGETDIR/dataset.txt" -o "$f" = "$TARGETDIR/biosamples.txt" ]; then
    if [ $? -eq 0 ]; then
      file -L -i $f | grep -P "ascii|utf-8" > /dev/null 2>&1
    else
      file -L -i $f | grep "ascii" > /dev/null 2>&1
    fi
    if [ $? -ne 0 ]; then
      echo $f
    fi
  fi
done
