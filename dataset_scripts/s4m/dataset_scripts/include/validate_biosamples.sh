#!/bin/sh
TARGETDIR="$1"
INCLUDE_DIR=`dirname $0`
TMPDIR="$TARGETDIR"
accession=`cat $TARGETDIR/ACCESSION`
timestamp=`date +"%Y%m%d-%H%M%S"`

. "$INCLUDE_DIR/funcs.sh"


biosamples="$TARGETDIR/biosamples.txt"
biosamples_metadata="$TARGETDIR/biosamples_metadata.txt"
probe_expression_replicates="$TARGETDIR/probe_expression_avg_replicates.txt"

assert_file_exists "$biosamples"
assert_file_exists "$biosamples_metadata"

## Only require existence if GCT file doesn't exist
if [ ! -f "$TARGETDIR/$accession.gct" ]; then
  assert_file_exists "$probe_expression_replicates"
fi

## Check column counts are as expected
count_tabbed_file_cols $biosamples
biosamples_cols="$?"
count_tabbed_file_cols $biosamples_metadata
biosamples_meta_cols="$?"
## Do rudimentary column number check on probe_expression_avg_replicates if it exists
if [ -f " $probe_expression_replicates" ]; then
  count_tabbed_file_cols $probe_expression_replicates
  probe_expression_rep_cols="$?"
fi

if [ $biosamples_cols -ne 6 ]; then
  echo "Error: File '$biosamples' has $biosamples_cols columns (6 expected)" 
fi
if [ $biosamples_meta_cols -ne 5 ]; then
  echo "Error: File '$biosamples_metadata' has $biosamples_meta_cols columns (5 expected)" 
fi

## Generate sample id (chip id) lists and counts
cut -f 1 "$biosamples" | sort | uniq > "$TMPDIR/biosamples_chip_ids_${accession}_${timestamp}.txt"
cut -f 3 "$biosamples_metadata" | sort | uniq > "$TMPDIR/biosamples_meta_chip_ids_${accession}_${timestamp}.txt"
biosamples_chip_count=`cat "$TMPDIR/biosamples_chip_ids_${accession}_${timestamp}.txt" | wc -l`
biosamples_meta_chip_count=`cat "$TMPDIR/biosamples_chip_ids_${accession}_${timestamp}.txt" | wc -l`
## Determine rep group IDs in probe_expression_avg_replicates, if it exists
if [ -f "$probe_expression_replicates" ]; then
  cut -f 3 "$probe_expression_replicates" | sort | uniq > "$TMPDIR/expression_rep_chip_ids_${accession}_${timestamp}.txt"
  rep_expression_chip_count=`cat "$TMPDIR/expression_rep_chip_ids_${accession}_${timestamp}.txt" | wc -l`
fi

## DEBUG ##
#echo "biosamples: $biosamples_chip_count"
#echo "biosamples_meta: $biosamples_meta_chip_count"
#echo "expression_samples: $expression_chip_count"
#echo "rep_expression_samples: $rep_expression_chip_count"
## END DEBUG ##


### Assertions ###

## All biosamples have metadata
if [ $biosamples_chip_count -ne $biosamples_meta_chip_count ]; then
  echo "Error: Biosamples chip count does not match biosamples metadata chip count!"
fi
diff --brief "$TMPDIR/biosamples_chip_ids_${accession}_${timestamp}.txt" "$TMPDIR/biosamples_meta_chip_ids_${accession}_${timestamp}.txt" > /dev/null 2>&1
if [ $? -ne 0 ]; then
  echo "Error: There is a mismatch between chip ids in biosamples file and biosamples_metadata file!"
fi
## Check consistency in probe_expression_avg_replicates, if it exists
if [ -f "$probe_expression_replicates" ]; then
  ## Replicate samples are a subset of biosamples
  if [ $biosamples_chip_count -lt $rep_expression_chip_count ]; then
    echo "Error: There are more sample IDs in expression data than in biosamples metadata!"
  fi
  while read rep_sample_id
  do
    grep -P "^$rep_sample_id$" "$TMPDIR/biosamples_chip_ids_${accession}_${timestamp}.txt" > /dev/null 2>&1
    if [ $? -ne 0 ]; then
      echo "Error: Expression sample ID '$rep_sample_id' does not exist in biosamples metadata!"
    fi
  done < "$TMPDIR/expression_rep_chip_ids_${accession}_${timestamp}.txt"
fi

rm -f "$TMPDIR/"*${accession}_${timestamp}*.txt
