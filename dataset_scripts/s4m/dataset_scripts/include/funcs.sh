#!/bin/sh

if [ -z $TMPDIR ]; then
   TMPDIR="/tmp"
fi

count_tabbed_file_cols () {
  file="$1"
  count=`head -1 $file | awk -F'\t' '{print NF; exit}'`
  return $count
}

assert_file_exists () {
  file="$1"
  if [ ! -f "$file" ]; then
    echo "Error: File '$file' does not exist!"
    exit 1
  fi
}

assert_file_has_attributes () {
  file="$1"
  attribs="$2"
  ret=0
  for a in $attribs; do
    attr="$a"
    echo "$a" | grep "_" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
       attr=`echo "$a" | sed -e 's/_/ /g'`
    fi
    grep "$attr" $file > /dev/null 2>&1
    if [ $? -ne 0 ]; then
      echo "Error: Missing attribute '$attr' in file '$file'!"
      ret=1
    fi
  done
  return $ret
}

load_database_settings () {
  lds_pgloader_conf="$1"
  if [ -f "$lds_pgloader_conf" ]; then
    while read pg_line
    do
      echo $pg_line | grep -P "^host|^port|^base|^user|^pass" > /dev/null 2>&1
      if [ $? -eq 0 ]; then
         var=`echo $pg_line | sed -r -e 's/\s*\=.*$//'`
         val=`echo $pg_line | sed -r -e 's/^\w+\s*\=\s*//'`
         eval "pg_$var=\"$val\""
      fi
    done < "$lds_pgloader_conf"

    ## Create temporary "PGPASSFILE" for 'psql' automatic password usage.
    ## See: http://www.postgresql.org/docs/current/static/libpq-pgpass.html
    lds_timestamp=`date +"%Y%m%d-%H%M%S"`
    export PGPASSFILE="$TMPDIR/.pgpass_${lds_timestamp}"
    echo "$pg_host:$pg_port:$pg_base:$pg_user:$pg_pass" > "$PGPASSFILE"
    ## If we don't set mode 600, 'psql' will ignore our password file.
    chmod 600 $PGPASSFILE

  else
    echo "Error: pgloader configuration '$lds_pgloader_conf' not found!"
  fi
}


find_pgconfig_file () {
  ret=""
  if [ -f "$S4M_HOME/etc/pgconf/$1" ]; then
    ret="$S4M_HOME/etc/pgconf/$1"
  elif [ -f "$CWD/$1" ]; then
    ret="$CWD/$1"
  fi
  echo "$ret"
}


find_profile_file () {
  ret=""
  if [ -f "$S4M_HOME/etc/profile/$PROFILE" ]; then
    ret="$S4M_HOME/etc/profile/$PROFILE"
  elif [ -f "$CWD/$PROFILE" ]; then
    ret="$CWD/$PROFILE"
  fi
  echo "$ret"
}


get_psql_wrapper_by_profile () {
  profile_fbase=`basename "$1" .conf`
  psql_wrapper="psql.$profile_fbase"
  ## Must have PSQL wrapper for that host
  which $psql_wrapper > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo "Error: Failed to find required DB wrapper script '$psql_wrapper'!
" 1>&2
    return 1
  fi
  echo "$psql_wrapper"
}


psql_wrapper_load () {
  psql_wrapper="$1"
  table="$2"
  fromfile="$3"

  nrows=`cat "$fromfile" | wc -l`
  echo "Loading $nrows rows into table '$table'.."
  $psql_wrapper load "$table" "$fromfile" > $PGLOG 2>&1
  ret=$?
  if [ $ret -ne 0 ]; then
    echo "Error: Failed to load table '$table'!
" 1>&2
  else
    echo "  done"
  fi
  return $ret
}


check_pgloader () {
  which pgloader > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo "Error: 'pgloader' not found in path!" 1>&2
    return 1
  fi
}


## Test database connection
test_db () {
  pgloader_conf="$1"
  database_exec "\dt" > /dev/null
  if [ $? -ne 0 ]; then
    echo "Error: Failed to connect to postgresql database! Check configuration: $pgloader_conf"
    return 1
  fi
}

## Execute SQL statement using loaded DB settings
database_exec () {
  sql_input="$1"
  ## Don't continue if DB settings not loaded
  if [ -z "$pg_host" -o -z "$pg_user" -o -z "$pg_port" -o -z "$pg_base" ]; then
    echo "Error: database_exec: Cannot execute queries, database settings not loaded!" 1>&2
    return 1
  fi

  if [ -f "$sql_input" ]; then
    result=`cat "$sql_input" | psql -U $pg_user -h $pg_host -p $pg_port $pg_base 2>&1`
  else
    result=`echo "$sql_input" | psql -U $pg_user -h $pg_host -p $pg_port $pg_base 2>&1`
  fi
  ## psql doesn't return non-zero error codes, so we try to intercept errors here.
  echo "$result" | grep -P -i "error|fatal|not known|help|not connect|no route" > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    echo "$result" 1>&2
    return 1
  else
    echo "$result"
  fi
  return 0
}

## TODO: Refactor to use get_metastore_value() func
get_dataset_id () {
  data_dir="$1"
  ## Try to determine from METASTORE
  if [ -f "$data_dir/METASTORE" ]; then
    ds_id=`grep "s4m_dataset_id=" $data_dir/METASTORE | awk -F'=' '{print $2}'`
    if [ ! -z $ds_id ]; then
      echo $ds_id
      return
    fi
  fi
  ## next try dataset_metadata.txt..
  if [ -f "$data_dir/dataset_metadata.txt" ]; then
    ds_id=`awk -F'\t' '{print $1}' "$data_dir/dataset_metadata.txt" | sort -T "$data_dir" | uniq`
    echo "$ds_id" | grep -P "^\d+$" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
      echo $ds_id
      return
    fi
  fi
  ## next try to determine from dataset.txt..
  if [ -f "$data_dir/dataset.txt" ]; then
    ds_id=`awk -F'\t' '{print $1}' $data_dir/dataset.txt`
    echo "$ds_id" | grep -P "^\d+$" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
      echo $ds_id
      return
    fi
  fi
  ## next try to determine from biosamples_metadata.txt
  if [ -f "$data_dir/biosamples_metadata.txt" ]; then
    colcount=`awk -F'\t' '{print NF}' "$data_dir/biosamples_metadata.txt" | sort -T "$data_dir" | uniq | tr -d [:cntrl:]`
    ## Format v1.4 correctness check.. remove ASAP
    if [ $colcount -eq 5 ]; then
      ds_id=`awk -F'\t' '{print $1}' "$data_dir/biosamples_metadata.txt" | head -1 | tr -d [:cntrl:]`
      echo "$ds_id" | grep -P "^\d+$" > /dev/null 2>&1
      if [ $? -eq 0 ]; then
        echo $ds_id
        return
      fi
    fi
  fi
}

## TODO: Refactor to use get_metastore_value() func
get_accession_id () {
  data_dir="$1"
  ## Try to determine from METASTORE
  if [ -f "$data_dir/METASTORE" ]; then
    acc=`grep "accession_id=" $data_dir/METASTORE | awk -F'=' '{print $2}'`
    if [ ! -z $acc ]; then
      echo $acc
      return
    fi
  fi
  if [ -f "$data_dir/ACCESSION" ]; then
    acc=`cat "$data_dir/ACCESSION" | tr -d [:cntrl]`
    if [ ! -z $acc ]; then
      echo $acc
      return
    fi
  fi
}

get_metastore_value () {
  data_dir="$1"
  key="$2"
  if [ -f "$data_dir/METASTORE" ]; then
    val=`grep -P "^$key=" $data_dir/METASTORE | cut -d'=' -f 2`
    if [ ! -z $val ]; then
      echo $val
      return
    fi
  fi
}

## Path to project-specific passwords repo
get_passwords_repo () {
  if [ ! -z "$S4M_PROJECT_LABEL" ]; then
    echo "$S4M_HOME/../../../${S4M_PROJECT_LABEL}_passwords"
  else
    ## default to s4m
    echo "$S4M_HOME/../../../s4m_passwords"
  fi
}


## Get parsed / var replaced version of input file (taking multiple %VAR%=<value> args
## for token replacement).
ReplaceTokens () {
  template="$1"
  shift

  sed_cmd="cat $template | sed -r "
  sed_opts=""

  while [ ! -z "$1" ];
  do
    ## Escape any ampersands in value (twice because it runs through 'eval')
    ## Note: It is not expected that there are any backslashes in values.
    kv=`echo "$1" | sed -e 's|&|\\\\\&|g'`
    key=`echo "$kv" | cut -d'=' -f 1`
    ## Need to escape some special chars in replace string
    val=`echo "$kv" | cut -d'=' -f 2 | tr -d "'"`
    sed_opts="$sed_opts -e 's|$key|$val|g'"
    shift
  done

  ## DEBUG
  #echo "[eval $sed_cmd $sed_opts]"

  eval "$sed_cmd $sed_opts"
}

## Replace tokens in target file from key/val dictionary (.ini file)
function ReplaceTokensInFileFromINI {
  replacetarget="$1"
  keyvalini="$2"

  replace_spec=""
  ## iterate over key/val pairs to build replacement spec
  while read line
  do
    echo "$line" | grep -P "^#" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
      continue
    fi
    ## if "a = b" then trim spaces around the equal sign
    line=`echo "$line" | sed -r -e 's|\s+\=\s+|\=|'`
    ## extract key and value
    srckey=`echo "$line" | cut -d'=' -f 1`
    srcval=`echo "$line" | cut -d'=' -f 2-`
    replace_spec="$replace_spec %$srckey%='$srcval'"
  done < "$keyvalini"

  #echo "DEBUG: ReplaceTokens $replacetarget $replace_spec > $replacetarget.new" 1>&2

  ReplaceTokens "$replacetarget" $replace_spec > "$replacetarget.new"
  mv "$replacetarget.new" "$replacetarget"
}

## Create user copy of target pgconf file.
## Requires 's4m_passwords' repo for password injection.
install_user_pgconf () {
  pgconf_copy_from="$1"
  ## Need passwords repo or we're doomed
  s4m_passwords=`get_passwords_repo`
  if [ ! -d "$s4m_passwords" ]; then
    echo "Error: Failed to determine path to 's4m_passwords' repository needed for database 
pgconf file setup!" 1>&2; echo 1>&2
    return 1
  else
    if [ ! -f "$s4m_passwords/passwords.ini" ]; then
      echo "Error: Could not find master password .ini file in location '$s4m_passwords/passwords.ini'"
      exit 1
    fi
    ## copy pgconf to user's ~/.s4m/pgconf/ directory, inject correct password from 
    ## s4m_passwords repo at the same time
    mkdir -p "$HOME/.s4m/pgconf"
    cp "$pgconf_copy_from" "$HOME/.s4m/pgconf/"

    userpgconf="$HOME/.s4m/pgconf/"`basename "$pgconf_copy_from"`

    ReplaceTokensInFileFromINI "$userpgconf" "$s4m_passwords/passwords.ini"
    return $?
  fi
}

## Check and install user pgconf file
## NOTE: This re-installs the user pgconf file each time
## (Best way to ensure propagation of correct config settings)
check_install_user_pgconf () {
  pgconfname="$1"
  pgconf=""
  if [ -f "$S4M_HOME/etc/pgconf/$pgconfname" ]; then
    pgconf="$S4M_HOME/etc/pgconf/$pgconfname"
  elif [ -f "$DATADIR/$pgconfname" ]; then
    pgconf="$DATADIR/$pgconfname"
  elif [ -f "$CWD/$pgconfname" ]; then
    pgconf="$CWD/$pgconfname"
  else
    echo "Error: Could not find target pgloader config template '$pgconfname'" 1>&2
    echo "Directory search order: $HOME/.s4m/pgconf/$pgconfname; $S4M_HOME/etc/pgconf/; $DATADIR/; $CWD/;" 1>&2
    return 1
  fi
  ## setup pgconf file with correct password
  install_user_pgconf "$pgconf"
  return $?
}

