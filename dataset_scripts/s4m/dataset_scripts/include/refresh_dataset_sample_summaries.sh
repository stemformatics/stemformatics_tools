#!/bin/sh
PSQL_WRAPPER="$1"

. "$S4M_HOME/dataset_scripts/include/funcs.sh"

## Update biosamples dataset summary stats
## SQL from: stemformatics.git/Portal/guide/model/stemformatics/db/stats_update_increment.sql

#database_exec "
#DELETE FROM stemformatics.stats_datasetsummary;
#INSERT INTO stemformatics.stats_datasetsummary
#SELECT b.ds_id, bmd.md_name, bmd.md_value, count(distinct bmd.chip_id)
#FROM biosamples b
#INNER JOIN biosamples_metadata bmd ON b.chip_id = bmd.chip_id
#LEFT JOIN biosamples_metadata rgi ON rgi.chip_id = bmd.chip_id AND rgi.md_name = 'Replicate Group ID'
#WHERE bmd.chip_id in
#       (SELECT min(chip_id)
#       FROM biosamples_metadata
#       WHERE md_name = 'Replicate Group ID'
#       GROUP BY md_value)
#AND bmd.md_name in ('Gender', 'Cell Line', 'Cell Type', 'Developmental Stage', 'Disease State', 'FACS Profile', 'Labelling', 'Organism', 'Organism Part', 'Pregnancy Status', 'Sample Type', 'Tissue')
#GROUP BY b.ds_id, bmd.md_name, bmd.md_value
#ORDER BY b.ds_id, bmd.md_name, bmd.md_value;
#" >> /dev/null 2>&1

## Update: 2018-09-13: No longer using pgloader-based 'database_exec' due to issues experienced in T#2914
SQL_CMD="
DELETE FROM stemformatics.stats_datasetsummary;
INSERT INTO stemformatics.stats_datasetsummary
SELECT bsm.ds_id, bsm.md_name, bsm.md_value, count(distinct bsm.chip_id)
   FROM biosamples_metadata bsm
   WHERE bsm.md_name IN ('Age', 'Cell Line', 'Cell Type', 'Cells Sorted For', 'Derived From', 'Developmental Stage', 'Differentiated To', 'Disease State', 'Gender', 'Organism', 'Organism Part', 'Pregnancy Status', 'Sample Type', 'Tissue')
   -- Only include first samples in technical rep groups otherwise we'll double up on sample metadata
   AND bsm.chip_id IN (SELECT chip_id FROM biosamples_metadata WHERE md_name='Replicate' AND md_value='1')
   GROUP BY bsm.ds_id, bsm.md_name, bsm.md_value
   ORDER BY bsm.ds_id, bsm.md_name, bsm.md_value;
"
$PSQL_WRAPPER "$SQL_CMD"
exit $?
