#!/bin/sh
TARGETDIR="$1"
INCLUDE_DIR=`dirname $0`
TMPDIR="$TARGETDIR"
accession=`cat $TARGETDIR/ACCESSION`

. "$INCLUDE_DIR/funcs.sh"

assay_platform="$TARGETDIR/assay_platform.txt"
biosamples="$TARGETDIR/biosamples.txt"
biosamples_metadata="$TARGETDIR/biosamples_metadata.txt"
probe_expression_avg_replicates="$TARGETDIR/probe_expression_avg_replicates.txt"

assert_file_exists "$biosamples"
assert_file_exists "$biosamples_metadata"
## Only require probe_expression_avg_replicates if GCT file is not present
if [ ! -f "$TARGETDIR/$accession.gct" ]; then
  assert_file_exists "$probe_expression_avg_replicates"
fi

chip_type=`grep s4m_chip_type "$TARGETDIR/METASTORE" | cut -d'=' -f 2 | tr -d "\n"`
chip_count=`echo $chip_type | wc -l`
biosamples_chip_type=`cat $biosamples | awk -F'\t' '{print $2}' | sort | uniq`
biosamples_chip_count=`echo $biosamples_chip_type | wc -l`
biosamples_metadata_chip_type=`cat $biosamples_metadata | awk -F'\t' '{print $2}' | sort | uniq`
biosamples_metadata_chip_count=`echo $biosamples_metadata_chip_type | wc -l`
## Check expression chip types only if "$probe_expression_avg_replicates" exists
if [ -f "$probe_expression_avg_replicates" ]; then
  probe_expression_chip_type=`cat $probe_expression_avg_replicates | awk -F'\t' '{print $2}' | sort | uniq`
  probe_expression_chip_count=`echo $probe_expression_chip_type | wc -l`
fi


### Assertions ###

if [ "$chip_count" != "1" ]; then
  echo "Error: Expecting only one chip type in assay platforms, but found $chip_count";
fi
if [ "$biosamples_chip_count" != "1" ]; then
  echo "Error: Expecting only one chip type in biosamples, but found $biosamples_chip_count";
fi
if [ "$biosamples_metadata_chip_count" != "1" ]; then
  echo "Error: Expecting only one chip type in biosamples metadata, but found $biosamples_metadata_chip_count";
fi
if [ -f "$probe_expression_avg_replicates" ]; then
  if [ "$probe_expression_chip_count" != "1" ]; then
    echo "Error: Expecting only one chip type in probe expression avg replicates, but found $probe_expression_chip_count";
  fi
fi


if [ "$biosamples_chip_type" != "$chip_type" ]; then
   echo "Error: Biosamples chip type does not match assay platforms chip type!";
fi 
if [ "$biosamples_metadata_chip_type" != "$chip_type" ]; then
   echo "Error: Biosamples metadata chip type does not match assay platforms chip type!";
fi
if [ -f "$probe_expression_avg_replicates" ]; then 
  if [ "$probe_expression_chip_type" != "$chip_type" ]; then
     echo "Error: Probe expression avg replicates chip type does not match assay platforms chip type!";
  fi 
fi
