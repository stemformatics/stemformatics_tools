#!/bin/sh
TARGETDIR="$1"
INCLUDE_DIR=`dirname $0`
TMPDIR="$TARGETDIR"
accession=`cat $TARGETDIR/ACCESSION`
timestamp=`date +"%Y%m%d-%H%M%S"`

. "$INCLUDE_DIR/funcs.sh"

biosamples_metadata="$TARGETDIR/biosamples_metadata.txt"
probe_expression_replicates="$TARGETDIR/probe_expression_avg_replicates.txt"
assert_file_exists "$biosamples_metadata"

## Only do checks on probe_expression_avg_replicates if GCT file not present
if [ ! -f "$TARGETDIR/$accession.gct" ]; then
  assert_file_exists "$probe_expression_replicates"
  ## Generate replicate group ID lists
  grep "Replicate Group ID" "$biosamples_metadata" | awk -F'\t' '{print $5}' | sort | uniq > "$TMPDIR/biosamples_meta_group_ids_${accession}_${timestamp}.txt"
  awk -F'\t' '{print $5}' "$probe_expression_replicates" | sort | uniq > "$TMPDIR/expression_rep_group_ids_${accession}_${timestamp}.txt"

  sync

  ## Assert all replicate groups in expression data correspond to those specified in biosamples metadata
  while read rep_group
  do
    ## Need the \Q and \E to escape regex chars e.g. + or - that might appear in sample names
    grep -P "\Q${rep_group}\E\$" "$TMPDIR/biosamples_meta_group_ids_${accession}_${timestamp}.txt" > /dev/null 2>&1
    if [ $? -ne 0 ]; then
      echo "Error: Replicate group ID '$rep_group' does not exist in biosamples metadata file!"
    fi
  done < "$TMPDIR/expression_rep_group_ids_${accession}_${timestamp}.txt"
fi

rm -f "$TMPDIR/biosamples_meta_group_ids_${accession}_${timestamp}.txt" "$TMPDIR/expression_rep_group_ids_${accession}_${timestamp}.txt"
