#!/bin/sh
TARGETDIR="$1"

check_file_exists_matching_pattern () {
  file_pattern="$1"
  ## DEBUG ##
  #echo "ls $TARGETDIR/$file_pattern > /dev/null 2>&1"
  ## END DEBUG ##
  ls $TARGETDIR/$file_pattern > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo "Error: Failed to find file with name like '$file_pattern'!"
  fi
}

## Data and metadata
check_file_exists_matching_pattern "biosamples.txt"
check_file_exists_matching_pattern "biosamples_metadata.txt"
check_file_exists_matching_pattern "dataset.txt"
check_file_exists_matching_pattern "dataset_metadata.txt"

## Check for probe_expression_avg_replicates if we don't have a GCT file
ls $TARGETDIR/*.gct > /dev/null 2>&1
if [ $? -ne 0 ]; then
  check_file_exists_matching_pattern "probe_expression_avg_replicates.txt"
fi

## Meta
check_file_exists_matching_pattern "ACCESSION"
check_file_exists_matching_pattern "VERSION"
