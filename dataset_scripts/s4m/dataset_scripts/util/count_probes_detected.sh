#!/bin/sh
DATADIR="$1"
TMPDIR="$DATADIR"
echo "Counting detected probes.. "
detected=`grep TRUE "$DATADIR/probe_expression_avg_replicates.txt" | awk -F'\t' '{print $4}' | sort | uniq | wc -l`
echo "Probes detected: $detected"; echo
