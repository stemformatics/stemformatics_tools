#!/bin/sh
mapfile="$1"
beadstudio="$2"


Usage () {
  echo "Usage: $0 <mapfile> <beadstudio_file>"; echo
}


if [ ! -f "$mapfile" ]; then
  echo "Error: Map file '$mapfile' not found!"; echo
  Usage
  exit 1
fi
if [ ! -f "$beadstudio" ]; then
  echo "Error: Beadstudio file '$beadstudio' not found!"; echo
  Usage
  exit 1
fi

while read line
do
  from=`echo "$line" | cut -f 1`
  to=`echo "$line" | cut -f 2`
  #echo "from=$from, to=$to"
  sed -i.sed.bak --follow-symlinks "1 s|\b$from\b|$to|g" "$beadstudio"
done < "$mapfile"

