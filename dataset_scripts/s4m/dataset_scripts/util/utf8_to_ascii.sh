#!/bin/sh
infile="$1"
outfile="$2"

Usage () {
  echo "$0 <input_file> <output_file>"; echo
  exit
}

if [ -z "$infile" -o -z "$outfile" ]; then
  Usage
fi

which iconv > /dev/null 2>&1
if [ $? -ne 0 ]; then
  echo "Error: Failed to find 'iconv' tool; please ensure it is installed and in system path."; echo
  exit 1
fi

iconv -f utf8 -t ascii "$infile" > "$outfile"
if [ $? -ne 0 ]; then
  echo "Error: UTF-8 to ASCII text file conversion failed!"; echo
  exit 1
fi
