#!/bin/sh
gctfile="$1"
TMPDIR=`dirname $gctfile`
max=""
if [ -f "$gctfile" ]; then
  max=`tail -n+4 "$gctfile" | cut -f3- | tr '\t' '\n' | grep -v "[a-zA-Z]" | sort -T "$TMPDIR" -n | tail -1`
fi
echo -n "$max"
