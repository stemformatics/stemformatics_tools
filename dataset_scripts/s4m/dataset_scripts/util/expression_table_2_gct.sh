#!/bin/sh

## Input: A tab-separated table of format:
##
##	SAMPLE1	SAMPLE2	SAMPLE3
##	probe_id	1.0	1.5	1.2
##	probe_id	5.7	4.3	3.9
##	probe_id	0.1	0.3	1.0
##	...
##

table="$1"
## optional: output file, or "-" for STDOUT. Default = create in same directory as input file.
outfile="$2"
if [ ! -f "$1" ]; then
  echo "Error: Input table '$1' not found!
" 1>&2
  exit 1
fi

outdir=`dirname "$table"`
fbase=`basename "$table" .txt`

gctrowcount=`cat "$table" | wc -l | tr -d [:space:]`
gctrowcount=`expr "$gctrowcount" - 1`

oldheader=`head -1 "$table"`
samplecount=`echo "$oldheader" | awk -F'\t' '{print NF}'`
newheader=`echo "$oldheader" | awk -F'\t' 'BEGIN{ORS=""} {print "NAME\tDescription"; for (i=NF;i>=1;i--) {print "\t"$i}; print "\n" }'`

echo "#1.2
$gctrowcount	$samplecount
$newheader" > "/tmp/.gctheader.$$"

tail -$gctrowcount "$table" | awk -F'\t' 'BEGIN{ORS=""} {print $1"\tNA"; for (i=NF;i>=2;i--) {print "\t"$i}; print "\n" }'  > "/tmp/.gctbody.$$"

gctout="$outdir/$fbase.gct"
if [ ! -z "$outfile" ]; then
  if [ "$outfile" = "-" ]; then
    cat "/tmp/.gctheader.$$" "/tmp/.gctbody.$$"
    rm -f "/tmp/.gctbody.$$" "/tmp/.gctbody.$$"
    exit
  fi
  gctout="$outfile"
fi

cat "/tmp/.gctheader.$$" "/tmp/.gctbody.$$" > "$gctout"
rm -f "/tmp/.gctbody.$$" "/tmp/.gctbody.$$"
