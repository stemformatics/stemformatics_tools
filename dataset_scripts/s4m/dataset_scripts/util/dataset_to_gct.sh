#!/bin/sh
## ================================================================================================
## Synopsis:	Generates a 'GCT' file as used by GenePattern, from averaged, normalized
##		Stemformatics dataset expression file.
##		GCT format: http://www.broadinstitute.org/cancer/software/genepattern/tutorial/gp_fileformats.html
##
##		For Stemformatics analyses pipelines that require full-dataset GCT.
## Author:	O.Korn, April 2011
##
## Notes:	Quite slow due to line-by-line expression value modifications. Would be faster
##		to implement an in-memory solution using Perl, for example.
## ================================================================================================

Usage () {
  echo "$0 <dataset_dir> [<optional_output_gct_file_path>]";
  echo 
  echo "NOTE: If GCT output file path not given, created as <dataset_dir>/<accession>.gct"; echo
  exit
}

pid="$$"
if [ ! -z "$1" ]; then
  datadir="$1"
  TMPDIR="$datadir"
else
  Usage
fi
outfile="$2"
if [ -z "$outfile" ]; then
  if [ -f "$datadir/ACCESSION" ]; then
    accession=`cat "$datadir/ACCESSION"`
    outfile="$datadir/$accession.gct"
  else
    outfile="$datadir/gctfile.gct"
  fi
fi
## If "false", intermediate files not cleaned up (useful for debugging perhaps)
cleanup="true"

if [ ! -d "$datadir" ]; then
  echo "Error: Dataset directory '$datadir' does not exist."; echo
  exit 1
fi
input="$datadir/probe_expression_avg_replicates.txt"
if [ ! -f "$input" ]; then
  echo "Error: Missing required input file '$input'"
  echo "This file is an output of the dataset normalization process."; echo
  exit 1
fi

which pr > /dev/null 2>&1
if [ "$?" -ne 0 ]; then
  echo "Error: Could not find required binary 'pr', exiting."; echo
  exit 1
fi
which perl > /dev/null 2>&1
if [ "$?" -ne 0 ]; then
  echo "Error: Could not find required binary 'perl', exiting."; echo
  exit 1
fi
## Unix 'sort' tool sorts in different order to Perl sort function.
## Since sort order of sample names in GCT file must match that of
## CLS file for example (which is currently a Perl script), we forego
## use of command line 'sort' tool and instead, substitute a Perl sort
## when sorting data containing sample names.
sort_cmd="perl -e 'print sort { uc(\$a) cmp uc(\$b) } <STDIN>;'"


## =============== ##
## Create GCT file ##
## =============== ##

echo "Creating GCT from input:"
echo "$input"; echo
echo "Creating GCT header.."
### Output GCT header
echo "#1.2" > "$TMPDIR/$pid.header"
## Output number of probes and samples
probecount=`awk -F'\t' '{print $4}' "$input" | sort | uniq | wc -l`
## Print sample chip IDs
## NOTE: Using Perl 'sort' vs. Unix sort here, see $sort_cmd comment above
awk -F'\t' '{print $3"\t"}' "$input" | eval $sort_cmd | uniq > "$TMPDIR/$pid.samples"
samplecount=`awk -F'\t' '{print $5}' "$input" | sort | uniq | wc -l`
/bin/echo -e "$probecount\t$samplecount" >> "$TMPDIR/$pid.header"
## Build tab-separated sample chip IDs
while read sname
do
  snext=`/bin/echo -e -n "$sname\t"`
  samplenames="${samplenames}${snext}"
done < "$TMPDIR/$pid.samples"
samplenames=`echo "$samplenames" | sed -r -e 's/\t$//'`

/bin/echo -e "NAME\tDescription\t$samplenames" >> "$TMPDIR/$pid.header"
echo "  done"

## Output probe expressions for each sample
echo "Processing sample expression columns.."
## Start building expression columns
## Output probes column and "NA" description column
awk -F'\t' '{print $4"\tNA"}' "$input" | sort -k 1,1 | uniq > "$TMPDIR/$pid.probes"
## Now build a sample expression file for each sample which we'll col merge with the probes
scount=0
while read samplename
do
  scount=`expr $scount + 1`
  grep "	$samplename	" "$input" | awk -F'\t' '{print $4"\t"$5"\t"$6}' | sort -k 1,1 | awk -F'\t' '{print $1"\t"$3}' > "$TMPDIR/$pid.$scount.expression"
  echo "  $scount of $samplecount  (chip ID: $samplename)"
  srows=`cat "$TMPDIR/$pid.$scount.expression" | wc -l`
  totalsamplerows=`expr $totalsamplerows + $srows`
done < "$TMPDIR/$pid.samples"
echo "  done"

echo "Checking expression row count integrity.."
## Check we have the correct number of expression rows for each sample which in total, matches input file
inputrowcount=`cat "$input" | wc -l`
if [ $totalsamplerows -ne $inputrowcount ]; then
  echo "Error: Total sample expression rows $totalsamplerows does not match input row count of $inputrowcount"; echo
  exit 1
fi 
echo "  done"

## Merge sample expression columns
echo "Merging probe and sample columns.."
samplecount=0
while read samplename
do
  samplecount=`expr $samplecount + 1`
  if [ $samplecount -eq 1 ]; then
    join -t"`/bin/echo -e '\t'`" -j 1 "$TMPDIR/$pid.probes" "$TMPDIR/$pid.1.expression" > "$TMPDIR/$pid.sample_expression.merged"
    ### DEBUG ###
    if [ $? -ne 0 ]; then
      cleanup="false"
      echo "DEBUG: Failed on join '$TMPDIR/$pid.probes'  TO  '$TMPDIR/$pid.1.expression'"
    fi
    #tmpcount=`cat "$TMPDIR/$pid.sample_expression.merged" | wc -l`
    #if [ $tmpcount -ne $probecount ]; then
    #   echo "DEBUG: Temp file '$TMPDIR/$pid.sample_expression.merged' at sample count=$samplecount has bad row count $tmpcount, expected $probecount"
    #fi
    ### END DEBUG ###
  else
    join -t"`/bin/echo -e '\t'`" -j 1 "$TMPDIR/$pid.sample_expression.merged" "$TMPDIR/$pid.$samplecount.expression" > "$TMPDIR/$pid.merged"
    ### DEBUG ###
    if [ $? -ne 0 ]; then
      cleanup="false"
      echo "DEBUG: Failed on join '$TMPDIR/$pid.sample_expression.merged'  TO  '$TMPDIR/$pid.$samplecount.expression'"
    fi
    #tmpcount=`cat "$TMPDIR/$pid.merged" | wc -l`
    #if [ $tmpcount -ne $probecount ]; then
    #   echo "DEBUG: Temp file '$TMPDIR/$pid.sample_expression.merged' at sample count=$samplecount has bad row count $tmpcount, expected $probecount"
    #fi
    ### END DEBUG ###
    mv "$TMPDIR/$pid.merged" "$TMPDIR/$pid.sample_expression.merged"
  fi
done < "$TMPDIR/$pid.samples"
echo "  done"

echo "Merging header and expression rows.."
## Merge header and expression rows
cat "$TMPDIR/$pid.header" "$TMPDIR/$pid.sample_expression.merged" > "$outfile" 
echo "  done"

echo "Checking GCT row count integrity.."
gctrowcount=`cat "$outfile" | wc -l`
gctexprcount=`expr $gctrowcount - 3`
if [ $gctexprcount -ne $probecount ]; then
  echo "Error: Total GCT expression rows $gctexprcount does not match input probe count of $probecount"; echo
  exit 1
fi

if [ "$cleanup" = "true" ]; then
  (cd "$TMPDIR" && rm -f $pid*)
fi

echo "Created GCT file:  $outfile"
echo "All Done."

