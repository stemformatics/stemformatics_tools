#!/usr/bin/perl -w

use Data::Dumper;
use List::Util qw(first);

$script_name = $ARGV[0] ? $ARGV[0] : $0;

sub usage {
  print "\nUsage: $script_name <map_file> <target_file> <target_column[,..]> [<options>]\n\n";

  print "  options:\n";
  print "  -b|--blank-targets      Lack of mapping results in replacement with empty string\n\n";
  print "  -k|--keep-unmatching    Preserve unmapped input file lines in output\n\n";
  print "  -m|--one-to-many        If > 1 mapping for given input, add all map targets in output\n\n";
  print "  -d <delimiter>          Supply target file delimiter (default = TAB)\n\n";

  print "NOTE: map_file MUST be: from_id<TAB>to_id  and 'to_id' must NOT be empty/blank.\n";
  print "      If map file contains duplicate 'from_id' then the first mapping is used,\n";
  print "      unless overriden with '-m'\n\n";
  exit 1
}

usage() unless scalar(@ARGV) >= 3;

$map_file=shift @ARGV;
$target=shift @ARGV;
$column=shift @ARGV;

@opts = ();
while ($arg = shift @ARGV) {
   #print "DEBUG: Got arg: $arg\n";
   push(@opts, $arg);
}
#print STDERR "Debug: opts=[" . join(' ', @opts) . "]\n";

## Accept one or more comma-separated target columns for replacement
my @columns = split(/,/, $column);


open MAPFILE, "<", $map_file or die $!;
open INFILE, "<", $target or die $!;

%mapHash = ();
while (<MAPFILE>) {
   chomp $_;
   if ($_ =~ /^[^\t]+\t[^\t]+$/) {
      @tokens = split(/\t/, $_);
      if (! exists($mapHash{$tokens[0]})) {
         $mapHash{$tokens[0]} = [];
         push(@{$mapHash{$tokens[0]}}, $tokens[1]);
      
      # multiple mappings for ID and user wants to use all of them
      } elsif (scalar(grep(/^(\-m|\-\-one\-to\-many)/, @opts))) {
         push(@{$mapHash{$tokens[0]}}, $tokens[1]);
      }
   }
}
close(MAPFILE);

### DEBUG ###
#print Dumper(\%mapHash);
#exit;
### END DEBUG ###

## TODO: Not sure how input meta-chars e.g. "\n" might work. Need to test.
##       Simple (as-is) delimiters okay..
my $delim = "\t";
if (scalar(grep(/^\-d/, @opts))) {

   my $validx = first { $opts[$_] eq '-d' } 0..scalar(@opts);
   if (! defined $validx) {
      usage();
   } else {
      #print STDERR "Debug: validx=[$validx]\n";
      $delim = $opts[$validx+1];
      #print STDERR "Delim=[$delim]\n";
   }
}

while (<INFILE>) {
   chomp $_;
   $line = $_;
   ## Need "-1"arg to split() - arbitrarily large limit on how many tokens are returned
   ## as by default, trailing empty fields would not be returned (i.e. would skip
   ## empty fields that we want to keep).
   @tokens = split(/$delim/, $line, -1);
   unless (scalar(@tokens)) {
      if (scalar(grep(/^(\-k|\-\-keep\-unmatching)/, @opts))) {
         print "$line\n";
      }
      next;
   }

   $replaceToken = $tokens[$columns[0]-1];

   if (scalar(@tokens) >= 1) {

      # treat all replacements as array of N >= 1 (easier to simply iterate over
      # output set regardless whether we're dealing with one target or many)
      my @replacements = ();
      if (exists $mapHash{$replaceToken}) {
         # if target is array, make sure we set multiple output targets
         if (ref($mapHash{$replaceToken}) eq 'ARRAY') {
            @replacements = @{$mapHash{$replaceToken}};
         } else {
            push(@replacements, $mapHash{$replaceToken});
         }
      } else {
         if (scalar(grep(/^(\-k|\-\-keep\-unmatching)/, @opts))) {
            push(@replacements, $replaceToken);
         } elsif (scalar(grep(/^(\-b|\-\-blank\-targets)/, @opts))) {
            push(@replacements, "");
         }
      }

      # print 1 or more output lines depending on how many replacement values
      # exist for input ID (based on whether user override was given to output every
      # mapping target)
      foreach my $replace (@replacements) {
         for ($i = 0; $i < scalar(@tokens); $i++) {
            my $colidx = $i+1;
            if (grep(/^$colidx$/, @columns)) {
               $tokens[$i] = $replace;
            }
            print $tokens[$i];
            if ($i < scalar(@tokens) - 1) {
               print $delim;
            }
         }
         print "\n";
      }
   }
}
close(INFILE);
