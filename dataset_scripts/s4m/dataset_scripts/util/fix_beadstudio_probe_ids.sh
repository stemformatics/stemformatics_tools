#!/bin/sh
## Script:	fix_beadstudio_probe_ids.sh
## Author:	O.Korn
## Created: 	2014-04-07
## ==========================================================================
bsfile="$1"
chiptype="$2"
targetcol="$3"
if [ -z "$targetcol" ]; then
  targetcol=1
fi

assay_platforms="$S4M_HOME/etc/assay_platforms.txt"
annodir="/data/annotation"
bgxdir="$annodir/illumina/bgx"

if [ -z "$S4M_TMP" ]; then
  S4M_TMP="/tmp"
fi

Usage () {
  echo "Usage: $0 <beadstudio_file> <chip_type> [targetcol]

   Replaces internal accession probe IDs with ILMN_xxxxx IDs.

   NOTE: Default 'targetcol' = 1 if not supplied.

   NOTE: Prints replaced file to STDOUT.
"  1>&2
}


if [ -z "$bsfile" -o -z "$chiptype" ]; then
  Usage
  exit
fi

file_replace_column="file_replace_column.pl"
which file_replace_column.pl > /dev/null 2>&1
if [ $? -ne 0 ]; then
  file_replace_column="$S4M_HOME/dataset_scripts/util/file_replace_column.pl"
  if [ ! -x "$file_replace_column" ]; then
    echo "Error: Failed to find required utility script: 'file_replace_column.pl'
" 1>&2;
    exit 1
  fi
fi

## We don't really need this but it's an extra validation that the input chiptype
## number matches the expected platform
if [ ! -f "$assay_platforms" ]; then
  echo "Error: Failed to find assay_platforms.txt in location: '$assay_platforms'
" 1>&2
  exit 1
fi
if [ ! -d "$bgxdir" ]; then
  echo "Error: Failed to find Illumina BGX (txt) chip layout files location: '$bgxdir'
" 1>&2
  exit 1
fi

chipname=`grep -P "^$chiptype\t.*\tIllumina\t" "$assay_platforms" | cut -f 6  2>/dev/null`
if [ $? -ne 0 -o -z "$chipname" ]; then
  echo "Error: Failed to find chip type $chiptype in $assay_platforms 
" 1>&2
  exit 1
fi
bgxfile="$bgxdir/$chiptype.bgx.txt"

if [ ! -f "$bgxfile" ]; then
  echo "Error: Failed to find BGX (txt) file for Illumina chip $chipname (chiptype=$chiptype)
   Try finding at: http://support.illumina.com/downloads/ and download to '$bgxdir/'
"  1>&2
  exit 1
fi

bsfilenew="$S4M_TMP/$$.bs.txt"
## Remove headers from input BeadStudio file if necessary
awk -F'\t' 'NF>1' "$bsfile" > "$bsfilenew"

inprobes="$S4M_TMP/$$.probes"
cut -f $targetcol "$bsfilenew" | grep -P -v "(Probe|PROBE|Target|TARGET)" | grep "\w" | sort  > "$inprobes"
sort "$inprobes" > "$inprobes.sorted"
nprobesi=`cat "$inprobes.sorted" | wc -l`
echo "There are $nprobesi probes in input file" 1>&2

## Check for duplicate probe IDs
dups=`cat "$inprobes.sorted" | uniq -d | wc -l`
if [ $dups -gt 0 ]; then
  echo "Error: Input beadstudio file contains $dups duplicate IDs:
" 1>&2
  exit 1
else
  echo "No duplicate IDs found in input, good.." 1>&2
fi

bgx_aa_ids="$S4M_TMP/$$.bgx.aa.probemap"
## Extract Array_Address_ID from BGX txt file
awk -F'\t' '{print $15"\t"$14}' "$bgxfile" | grep -P "^[0-9]+\tILMN_[0-9]+$" > "$bgx_aa_ids"
sort "$bgx_aa_ids" > "$bgx_aa_ids.sorted"
cut -f 1 "$bgx_aa_ids.sorted" > "$bgx_aa_ids.sorted.1"
nprobesaa=`cat "$bgx_aa_ids.sorted.1" | wc -l`
echo "There are $nprobesaa probe IDs in BGX (txt) file" 1>&2

bgx_gi_ids="$S4M_TMP/$$.bgx.gi.probemap"
awk -F'\t' '{print $10"\t"$14}' "$bgxfile" | grep -P "^[0-9]+\tILMN_[0-9]+$" > "$bgx_gi_ids"
sort "$bgx_gi_ids" > "$bgx_gi_ids.sorted"
cut -f 1 "$bgx_gi_ids.sorted" > "$bgx_gi_ids.sorted.1"

probemap=""
exec 3>"$S4M_TMP/$$.err"

## Venn intersect IDs to work out which type of probe accession the input is
nonoverlap=`comm -23 "$inprobes.sorted" "$bgx_aa_ids.sorted.1" 2>&1`
nonoverlapcount=`echo -n "$nonoverlap" | grep -P "\w" | wc -l`
echo "$nonoverlap" | grep "not in sorted order" > /dev/null
if [ $? -eq 0 -o ! -z "$nonoverlap" ]; then
  echo "Input IDs do not perfectly match Array_Address_Ids ($nonoverlapcount of $nprobesi mismatch)" 1>&2
  echo "  Sample non-matching IDs (head):" 1>&2
  echo "$nonoverlap" | head 1>&2
  echo -n "$nonoverlap" > "$S4M_TMP/$$.aa.nonmatching"
  echo "  See all: $S4M_TMP/$$.aa.nonmatching
" 1>&2
  echo "Testing for zero-padded Array_Address_Id input.." 1>&2
  ## Pad input IDs with zeros to see if they match
  ##  determine ID length from first ID from BGX file
  idlen=`head -1 "$bgx_aa_ids.sorted.1" | tr -d [:cntrl:] | wc -c`
  while read probeid
  do
    replaced=`printf %0${idlen}d $probeid`
    echo "$replaced" >> "$inprobes.sorted.padded"
  done < "$inprobes.sorted"
  sort "$inprobes.sorted.padded" > "$inprobes.sorted.padded.tmp"
  sync
  mv "$inprobes.sorted.padded.tmp" "$inprobes.sorted.padded"

  nonoverlap=`comm -23 "$inprobes.sorted.padded" "$bgx_aa_ids.sorted.1" 2>&1`
  nonoverlapcount=`echo -n "$nonoverlap" | grep -P "\w" | wc -l`
  echo "$nonoverlap" | grep "not in sorted order" > /dev/null
  ## They weren't Array_Address_Ids at all, test for GI
  if [ $? -eq 0 -o ! -z "$nonoverlap" ]; then
    echo "  Zero-padded input IDs do not perfectly match on Array_Address_Ids ($nonoverlapcount of $nprobesi mismatch)" 1>&2
    echo "  Sample non-matching IDs (head):" 1>&2
    echo "$nonoverlap" | head 1>&2
    echo -n "$nonoverlap" > "$S4M_TMP/$$.zero.aa.nonmatching"
    echo "  See all: $S4M_TMP/$$.zero.aa.nonmatching
" 1>&2
    nonoverlap=`comm -23 "$inprobes.sorted" "$bgx_gi_ids.sorted.1" 2>&1`
    nonoverlapcount=`echo -n "$nonoverlap" | grep -P "\w" | wc -l`
    echo "$nonoverlap" | grep "not in sorted order" > /dev/null
    if [ $? -eq 0 -o ! -z "$nonoverlap" ]; then
      echo "Input IDs do not have perfect match on 'GI' IDs ($nonoverlapcount of $nprobesi mismatch)" 1>&2
      echo "  Sample non-matching IDs (head):" 1>&2
      echo "$nonoverlap" | head 1>&2
      echo "$nonoverlap" > "$S4M_TMP/$$.gi.nonmatching"
      echo "  See all: $S4M_TMP/$$.gi.nonmatching
" 1>&2
    ## They ARE GI accessions
    else
      probemap="$bgx_gi_ids"
      echo "Input IDs perfectly match GI accessions, performing replacement from $chipname BGX:  GI -> ILMN" 1>&2
    fi
  ## They ARE non-padded Array_Address_Ids
  else
    probemap="$bgx_aa_ids"
    echo "Zero-padded input IDs perfectly match Array_Address_Id accessions, performing replacement from $chipname BGX:  Array_Address_Id -> ILMN" 1>&2
    echo "  (padding input Array_Address_Id strings before BGX replacement..)" 1>&2
    ## Replace intermediate IDs with zero-padded ones
    pr -Jtm "$inprobes.sorted" "$inprobes.sorted.padded" > "$probemap.tmp"
    $file_replace_column "$probemap.tmp" "$bsfilenew" $targetcol -k > "$bsfilenew.tmp" 2>&3
    sync
    mv "$bsfilenew.tmp" "$bsfilenew"
  fi

## They are regular Array_Address_Ids
else
  probemap="$bgx_aa_ids"
  echo "Input IDs peferctly match Array_Address_Id accessions, performing replacement from $chipname BGX:  Array_Address_Id -> ILMN" 1>&2
fi

if [ -z "$probemap" ]; then
  echo "Error: Input IDs cannot be (easily) replaced, exiting" 1>&2
  exit 1
fi

exec 3>"$S4M_TMP/$$.err"
echo "Performing ID replacement.." 1>&2
$file_replace_column "$probemap" "$bsfilenew" $targetcol -k 2>&3

if [ -s "$S4M_TMP/$$.err" ]; then
  echo "There were errors:" 1>&2
  head "$S4M_TMP/$$.err" 1>&2
fi
echo "All done" 1>&2

