#!/bin/sh
## Try both upper and lower case
## If lower case, rename to upper case CEL too
for i in *.CEL; do mv $i $(echo $i|sed 's/\(GSM[0-9]*\).*/\1.CEL/g'); done > /dev/null 2>&1
for i in *.cel; do mv $i $(echo $i|sed 's/\(GSM[0-9]*\).*/\1.CEL/g'); done > /dev/null 2>&1
