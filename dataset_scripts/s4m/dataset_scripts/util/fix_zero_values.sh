#!/bin/sh
target_file="$1"

if [ ! -f "$target_file" ]; then
  echo "Error: File '$target_file' not found!"
  exit 1
fi

cp "$target_file" "${target_file}.sed.bak"

## Fix zero values in last column, and elsewhere (NOT first column - there should not be any '0' values there)
## NOTE: Yes, the same regex is duplicated twice. For some reason, greedy match does not result in ALL
## instances on a given line to be replaced - but by applying the same regex twice, we pick up consecutive
## data columns.
sed -i -r -e 's/\t0$/\t0\.0/g' -e 's/\t0\t/\t0\.0\t/g' -e 's/\t0\t/\t0\.0\t/g' $target_file

