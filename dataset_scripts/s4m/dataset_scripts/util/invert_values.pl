#!/usr/bin/perl

# invert_values.pl gct_file_name

#~ use strict;
#~ use warnings;


sub is_float {
  my $val = shift;
  return $val =~ m/^\d+[.]?\d+$/;
}


$gct_file_name = $ARGV[0];

print $gct_file_name."\n";

$gct_file_name_new = $gct_file_name . '.new';


open my $gct_file, $gct_file_name;
open NEW_GCT_FILE, ">", $gct_file_name_new;

$count = 0;
while (<$gct_file>) {
    $row = $_;
    $split = "\t";
    @row = split($split,$row);
    

    
    $new_row = "";
    while (<@row>){
        
        if (is_float($_) and $count != 1){
            $new_value = 1/$_;
        }else {
            $new_value = $_;
        }
        
        
        if ($new_row eq ""){
            $new_row = $new_value;
        } else {
            $new_row = $new_row . "\t" . $new_value;
        }
    }
    $count = $count + 1;
    print NEW_GCT_FILE "$new_row\n";
    
    
}
close (NEW_GCT_FILE);
close($gct_file);

