#!/usr/bin/perl -w
## Averages replicate sample expression values.
## Expecting input args: "normalized_expression_detection.txt", "chip_id_replicate_group_map.txt" <output_file>

sub usage {
  print "Usage: " . $0 . " <normalized_expression_detection_file> <chip_id_replicate_map> <output_file>\n\n";
}

if (scalar(@ARGV) < 3) {
  usage;
  exit 1;
}

## Load basic stats package
do('./include/Statistics-Lite.pm') or die "Failed to load package 'Statistics-Lite.pm'!";

## Set to non-zero to enable debug output
my $DEBUG=0;

my $infile = $ARGV[0];
my $repfile = $ARGV[1];
my $outfile = $ARGV[2];

die "Error: Could not open file '$infile'!" unless open(INPUTFILE, "<", $infile);
die "Error: Could not open file '$repfile'!" unless open(REPFILE, "<", $repfile);
die "Error: Could not open file '$outfile' for writing!" unless open(OUTFILE, ">", $outfile);

## Load expression data and replicate info into memory
my @expr_rows = <INPUTFILE>;
close(INPUTFILE);
my @replicate_rows = <REPFILE>;
close(REPFILE);

my %rep_expression = ();
my %rep_map = ();
my %probe_detection = ();

foreach (@replicate_rows) {
   chomp;
   my ($chip_id, $rep_name) = split(/\s/, $_);
   $rep_map{$chip_id} = $rep_name;
   if (! exists($rep_expression{$rep_name})) {
      $rep_expression{$rep_name} = ();
   }
}

if ($DEBUG) {
   print "DEBUG: Loading replicate expression scores per-probe..\n";
}

## If row corresponds to a replicate group, combine probe expression scores
foreach (@expr_rows) {
   chomp;
   my @tokens = split(/\s/, $_);
   ## Skip any blank or dodgy lines, not that we expect any!
   next unless $tokens[0] =~ /\w+/;
   my ($chip, $probe, $score, $det) = @tokens;
   $probe_detection{$chip.'_'.$probe} = $det;
   ## Add expression scores for replicate samples
   my $rep = $rep_map{$chip};
   if (exists($rep_expression{$rep})) {
      if (! exists($rep_expression{$rep}{$probe})) {
         $rep_expression{$rep}{$probe} = ();
      }
      if (! exists($rep_expression{$rep}{$probe}{'scores'})) {
         $rep_expression{$rep}{$probe}{'scores'} = ();
      }
      push(@{$rep_expression{$rep}{$probe}{'scores'}}, $score);
   }
}

if ($DEBUG) {
   print "DEBUG: Averaging probe expressions for replicates..\n";
}

## Now create avg scores by dividing each rep's total expression by the number of samples in the replicate group.
## Replace total expression with average value.
foreach my $repname (keys(%rep_expression)) {
   my $rep_count = grep /$repname/, values(%rep_map);

   ## DEBUG: Old code checks for a particular probe in a particular dataset.
   ## Leaving here as an example in case we ever need to debug this again at a probe level.
   ## NOTE: Commented out because of the specific probe ID!!!
   if ($DEBUG) {
      #my $count = Statistics::Lite::count(@{$rep_expression{$repname}{'1007_s_at'}{'scores'}});
      #print "DEBUG: Got $repname rep_count=[$rep_count]\n";
      #print "DEBUG: Statistics::Lite::count($repname) = [$count]\n";
      #print "DEBUG: Size of probeset for replicate '$repname' is " . scalar(keys(%{ $rep_expression{$repname} })) . "\n";
      #print "DEBUG: Sum of probe signals [1007_s_at], Replicate [$repname] = [" . Statistics::Lite::sum(@{$rep_expression{$repname}{'1007_s_at'}{'scores'}}) . "]\n";
   }
   ### END ###
   
   if ($rep_count > 1) {
      foreach my $probe_id (keys(%{ $rep_expression{$repname} })) {
         $rep_expression{$repname}{$probe_id}{'mean'} = Statistics::Lite::mean(@{$rep_expression{$repname}{$probe_id}{'scores'}});
         $rep_expression{$repname}{$probe_id}{'stddev'} = Statistics::Lite::stddev(@{$rep_expression{$repname}{$probe_id}{'scores'}});
      }
   }
   ### DEBUG: Old code for probe level debugging, see above
   if ($DEBUG) {
      #print "DEBUG: MEAN probe signal [1007_s_at], Replicate [$repname] = [" . $rep_expression{$repname}{'1007_s_at'}{'mean'} . "]\n";
      #print "DEBUG: STDDEV probe signal [1007_s_at], Replicate [$repname] = [" . $rep_expression{$repname}{'1007_s_at'}{'stddev'} . "]\n";
   }
   ### END ###
}

if ($DEBUG) {
   print "DEBUG: Writing avg replicates file..\n";
}

@processed_replicates = ();

## Output collapsed expression using the *first* chip id for a given replicate group
## to represent the collapsed replicate sample.
##
## NOTE: chip IDs are *sorted* from replicate group map, assuming natural order of
##	chip IDs are used throughout. If we don't do this, we sometimes get the second
##	chip ID of a replicate due to non-specific return ordering from keys(%rep_map)
##
foreach my $chip_id (sort(keys(%rep_map))) {
   if ($DEBUG) {
      print "DEBUG: Writing data for chip_id=[$chip_id]\n";
   }
   $repname = $rep_map{$chip_id};

   next if (grep (/$repname/, @processed_replicates));

   foreach my $probe_id (keys(%{$rep_expression{$repname}})) {
      $det = $probe_detection{$chip_id.'_'.$probe_id};
      $mean = $rep_expression{$repname}{$probe_id}{'mean'};
      $stddev = $rep_expression{$repname}{$probe_id}{'stddev'};
      print OUTFILE "$chip_id\t$probe_id\t$repname\t$mean\t$stddev\t$det\n";
   }
   push(@processed_replicates, $repname);
}
close(OUTFILE);

