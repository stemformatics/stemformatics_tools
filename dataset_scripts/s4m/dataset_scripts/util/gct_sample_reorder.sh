#!/bin/sh
targetgct="$1"
## A file of newline delimited sample names (which match those appearing in GCT file)
sampleorder="$2"

Usage () {
  echo "Usage: $0  <gct_file> <new_sample_order_file>"; echo
  echo "  New GCT file output to STDOUT"; echo
}

if [ -z "$targetgct" -o ! -f "$targetgct" -o -z "$sampleorder" ]; then
  Usage 1>&2
  exit
fi

### START ###

TMPDIR=`dirname $targetgct`
fbase=`basename "$targetgct" .gct`

head -2 "$targetgct" > "$TMPDIR/$fbase.fileheader"
head -3 "$targetgct" | tail -1 > "$TMPDIR/$fbase.sampleheader"
gctrowcount=`cat "$targetgct" | wc -l`
gcttablerows=`expr "$gctrowcount" - 2`

newsampleorder=`cat "$sampleorder" | grep -P "\w"`
totalcols=`cat "$TMPDIR/$fbase.sampleheader" | awk -F'\t' '{print NF}'`
samplecount=`expr "$totalcols" - 2`
insamples=`cat "$TMPDIR/$fbase.sampleheader" | tr "\t" "\n" | tail -$samplecount | grep -P "\w"`

echo "DEBUG: Got totalcols=[$totalcols]" 1>&2
echo "DEBUG: Got samplecount=[$samplecount]" 1>&2
echo "DEBUG: Got insamples=[$insamples]" 1>&2
echo "DEBUG: Got newsampleorder=[$newsampleorder]" 1>&2

## check input samples a subset of new sample order list
echo "$insamples" | sort > "$TMPDIR/$fbase.insamples"
echo "$newsampleorder" | sort > "$TMPDIR/$fbase.newsampleorder"

for s in `cat "$TMPDIR/$fbase.insamples"`
do
  grep -P "^\Q$s\E$" "$TMPDIR/$fbase.newsampleorder" > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo "Error: Input sample '$s' not in new sample order list!" 1>&2
    rm -f "$TMPDIR/$fbase"*
    exit 1
  fi
done

## if we got this far, we can change the sample order as required

## extract the GCT data rows
tail -$gcttablerows "$targetgct" > "$TMPDIR/$fbase.gctbody"

## iterate over new sample order and determine the old sample position,
## and append to comma-separated list we'll pass into awk for it to use

awkfields=""
outputsamples=""

rm -f "$TMPDIR/$fbase.awkindexes"
echo "$newsampleorder" | while read sid
do
  ## find old sample pos
  oldpos=`echo "$insamples" | grep -n -P "^\Q$sid\E$" | awk -F':' '{print $1}'`
  ## new sample ID unknown, skip
  if [ -z "$oldpos" ]; then
    continue
  fi
  outputsamples="$outputsamples	$sid"
  ## add 2 to the sample index to account for NAME and DESCRIPTION columns at the start
  echo `expr "$oldpos" + 2` >> "$TMPDIR/$fbase.awkindexes"
done

awkfields=`cat "$TMPDIR/$fbase.awkindexes" | tr "\n" "," | sed -r -e 's/\,$//'`
echo "DEBUG: Got new field ordering [$awkfields]" 1>&2

cat "$TMPDIR/$fbase.gctbody" | awk -F'\t' -v indexes="$awkfields" 'BEGIN{ORS=""} {
   print $1"\t"$2;
   split(indexes, indarray, ",");
   #for (i in indarray)
   for (i=1; i<=length(indarray); i++)
      print "\t" $(indarray[int(i)]);
   print "\n";
}'  > "$TMPDIR/$fbase.newgctbody"

## output new file header and sample labels to STDOUT
cat "$TMPDIR/$fbase.fileheader"
## output new gct body to STDOUT
cat "$TMPDIR/$fbase.newgctbody"

rm -f "$TMPDIR/$fbase"*
