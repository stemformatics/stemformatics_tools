#!/bin/sh
gctfile="$1"
TMPDIR=`dirname $gctfile`
min=""
if [ -f "$gctfile" ]; then
  min=`tail -n+4 "$gctfile" | cut -f3- | tr '\t' '\n' | grep -v "[a-zA-Z]" | sort -T "$TMPDIR" -n | head -1`
fi
echo -n "$min"
