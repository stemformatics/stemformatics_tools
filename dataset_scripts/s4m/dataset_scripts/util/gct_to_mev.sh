#!/bin/sh
## ================================================================================================
## Synopsis:    Generates an 'MeV' file from input GCT file (they are essentially the same thing,
##		minus 2 header lines).
## Author:	O.Korn September 2011
## ================================================================================================

Usage () {
  script=`basename $0`
  echo "Usage: $script <gct_file> <MeV_output_file>"; echo
  exit
}

gct="$1"
mev="$2"
if [ -z "$gct" -o -z "$mev" ]; then
  Usage
fi

gctlines=`cat "$gct" | wc -l`
exprlines=`expr $gctlines - 2`

tail -$exprlines "$gct" > "$mev"
