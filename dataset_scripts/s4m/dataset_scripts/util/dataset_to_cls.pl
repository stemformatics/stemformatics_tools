#!/usr/bin/perl

use Data::Dumper;

$script_name = $ARGV[0];
$DEBUG = 0;

sub usage {
   print "Usage: $script_name <dataset_dir> <output_cls_file_path> [DEBUG]\n\n";
   print "NOTE: Input biosamples_metadata.txt must be sorted (for now at least)!\n";
   exit 1;
}

usage() unless scalar(@ARGV) >= 2;

$dsdir=shift @ARGV;
$clsfile=shift @ARGV;
if (scalar(@ARGV)) {
   $DEBUG=shift @ARGV;
}

open BM, "<$dsdir/biosamples_metadata.txt" or die $!;

## Get dataset numerical ID
$dsid=`grep dataset_id "$dsdir/METASTORE" | awk -F'=' '{print \$2}'`;
chomp $dsid;
print "DEBUG: Got dataset id [$dsid]\n" if ($DEBUG);

## Determine what the sample group-by items ought to be, from dataset METASTORE
$limitSortBy=`grep limitSortBy "$dsdir/METASTORE" | awk -F'=' '{print \$2}'`;
chomp $limitSortBy;
print "DEBUG: Got limitSortBy [$limitSortBy]\n" if ($DEBUG);
@groupByItems = ();

## Set custom group-by item from limitSortBy (non-"Sample Type")
if ($limitSortBy =~ /\,/) {
   @groupByItems = split(/,/, $limitSortBy);
}
if (! scalar($groupByItems)) {
   print "DEBUG: NO custom non-default group-by specified, generating default CLS only.\n" if ($DEBUG);
}

## Build in-memory lists and indexes for samples and sample groups that we'll need
## in order to generate our CLS output, by parsing the biosamples_metadata.txt file
## line by line.
@sampleNames = ();
@sampleTypes = ();
@sampleTypesSpacesRemoved = ();
%sampleNamesHash = ();
%sampleTypesHash = ();
@groupByValues = ();
@groupByValuesSpacesRemoved = ();
%groupByTypesHash = ();
@customGroupByPermutations = ();
%customGroupByPermutationsHash = ();
%samplePermGroupValueHash = ();

@BM_LINES = <BM>;
close(BM);

$chipID = '';
$currentSample = '';
$currentSampleType = '';
$currentGroupByValue = '';
$bmSampleCount = scalar (grep (/Replicate Group ID/, @BM_LINES));
$bmSampleIndex = 0;
$lineCount = 0;

## Create data structures needed for standard CLS file
foreach $line (@BM_LINES) {
   $lineCount++;
   chomp $line;
   @tokens = split(/\t/, $line);

   ## Remove dataset_id column
   shift @tokens;

   ## Got new chip ID, which means we've processed all the lines for the previous sample.
   ## If we don't want to lose the last sample, we need to do this if we've reached
   ## the end of the biosamples metadata file.
   if ((($chipID=~/\w+/ and $tokens[1] !~ /$chipID/) or ($chipID=~/\w+/ and $lineCount eq scalar(@BM_LINES))) and $bmSampleIndex > scalar(keys(%samplePermGroupValueHash))) { 
   #if ($chipID =~ /\w+/ and ($tokens[1] !~ /$chipID/)) {
      $newChipID = $tokens[1];
   }
   ## Set current sample chipID
   if (! $chipID or ($chipID !~ /$tokens[1]/)) {
      $chipID = $tokens[1];
   }
   if ($line =~ /\tReplicate Group ID\t/) {
      $bmSampleIndex++;
      $currentSample = pop @tokens;
      push (@sampleNames, $currentSample) unless grep (/^\Q$currentSample\E$/, @sampleNames);

   ## Process mandatory, default group-by item of "Sample Type"
   } elsif ($line =~ /\tSample Type\t/) {
      if ($DEBUG) {
         print "DEBUG: Empty \$currentSample!\n" unless ($currentSample);
      }
      $currentSampleType = pop @tokens;
      unless (grep (/^\Q$currentSampleType\E$/, @sampleTypes)) {
         push (@sampleTypes, $currentSampleType);
         $valueReplaced = $currentSampleType;
         $valueReplaced =~ s/ /_/g;
         push (@sampleTypesSpacesRemoved, $valueReplaced);
         $sampleTypesHash{$currentSampleType} = scalar(@sampleTypes) - 1;
      }
      $sampleNamesHash{$currentSample} = $currentSampleType;
   }
}

print "DEBUG: There are " . scalar(@sampleNames) . " samples\n" if ($DEBUG);
print "DEBUG: There are " . scalar(@sampleTypes) . " sample types\n" if ($DEBUG);

## Prepare sorted sample names, ignore case
@sampleNamesSorted = sort { uc($a) cmp uc($b) } @sampleNames;
## Prepare sorted sample type names (in sample name sort order)
@sampleTypesSorted = ();
foreach (@sampleNamesSorted) {
   $sampleType = $sampleNamesHash{$_};
   unless (grep (/^\Q$sampleType\E$/, @sampleTypesSorted)) {
      push (@sampleTypesSorted, $sampleType);
   }
}
## Build sample types lookup with increasing sample type order index
%sampleTypesIndexed = ();
while (($key, $val) = each %sampleTypesHash) {
   $sampleTypesIndexed{$val} = $key;
}
sort %sampleTypesIndexed;

## DEBUG: Dump our data structures to verify that the data looks okay ###
if ($DEBUG) {
   print "DEBUG: Sample names (biosamples_metadata order):\n";
   print Dumper(\@sampleNames);

   print "DEBUG: Sample names (Perl sorted):\n";
   print Dumper(\@sampleNamesSorted);

   print "DEBUG: Sample names -> type hash\n";
   print Dumper(\%sampleNamesHash);

   print "DEBUG: Sample Type names (sorted):\n";
   print Dumper(\@sampleTypesSorted);

   print "DEBUG: Sample Types hash:\n";
   print Dumper(\%sampleTypesHash);

   print "DEBUG: Sample Types, index as key:\n";
   print Dumper(\%sampleTypesIndexed);
}

### PRINT DEFAULT CLS ###
open(CLS, ">$clsfile") or die $!;

## 1. Print CLS first line:
##	<number_samples> TAB <number_groups> TAB 1
print CLS '' . scalar(@sampleNames) . "\t" . scalar(@sampleTypes) . "\t1\n";

## 2. Print space-separated sample type names. We necessarily print sample names with spaces
## replaced by underscores.
@sampleTypesSpaceReplaced = @sampleTypesSorted;
s/\Q /\_/g for(@sampleTypesSpaceReplaced);
print CLS '# ' . join(' ', @sampleTypesSpaceReplaced) . "\n";

## 3. Print sample type (group) index for each sample on final line of CLS file.
## The goal is to ensure, for "pretty" output, that we start at '0' and count upwards.
## For example, for 6 samples across 2 groups, we'd like to see:  0 0 0 1 1 1
## Instead of: 1 1 1 0 0 0, or even 0 0 1 1 0 1.
## We *could* have output in any order.. but we want it to look nice.
## If we truly didn't care, we could just have iterated over sample names or any
## other natural order (but that doesn't give us what we want).
$sampleGroupsLine = '';
$outputIndex = 0;
foreach (@sampleTypesSorted) {
   $type = $_;
   foreach (@sampleNamesSorted) {
      $sampleGroup = $sampleNamesHash{$_};
      if ($type eq $sampleGroup) {
         $sampleGroupsLine .= $outputIndex . ' ';
      }
      #else {
      #   print "DEBUG: Sample Type [$type] !== [$sampleGroup]\n" if ($DEBUG);
      #}
   }
   $outputIndex++;
}
## 'rtrim' any trailing spaces
$sampleGroupsLine =~ s/\s+$//g;
print CLS "$sampleGroupsLine\n";
close(CLS);


## Now, create custom CLS files

foreach my $limitSortByItem (@groupByItems)  {
   next if ($limitSortByItem =~ /^Sample Type$/);

   print "DEBUG: Building CLS file for limitSortBy item '$limitSortByItem'\n" if ($DEBUG);

   ## Reset some per-limitSortByItem tracking variables
   $chipID = '';
   $currentSample = '';
   $currentSampleType = '';
   $currentGroupByValue = '';
   $bmSampleIndex = 0;
   $lineCount = 0;
   @groupByValues = ();
   @groupByValuesSpacesRemoved = ();
   %groupByTypesHash = ();
   @customGroupByPermutations = ();
   %customGroupByPermutationsHash = ();
   %samplePermGroupValueHash = ();
   
   ## Build data structures we need for custom CLS file generation
   foreach $line (@BM_LINES) {
      $lineCount++;
      chomp $line;
      @tokens = split(/\t/, $line);
   
      ## Remove dataset_id column
      shift @tokens;
   
      ## Got new chip ID, which means we've processed all the lines for the previous sample.
      ## Update the Sample Type <=> Custom Group By permutations now, if needed.
      ## Also, if we don't want to lose the last sample, we need to do this if we've reached
      ## the end of the biosamples metadata file.
      if ((($chipID=~/\w+/ and $tokens[1] !~ /$chipID/) or ($chipID=~/\w+/ and $lineCount eq scalar(@BM_LINES))) and $bmSampleIndex > scalar(keys(%samplePermGroupValueHash))) { 
      #if ($chipID =~ /\w+/ and ($tokens[1] !~ /$chipID/)) {
         $newChipID = $tokens[1];
         print "DEBUG [$limitSortByItem]: Got new chipID $newChipID, old chipID is $chipID!\n" if ($DEBUG);
         if ($limitSortByItem =~ /\w+/) {
            $perm = $currentGroupByValue.'_'.$currentSampleType;
            print "DEBUG [$limitSortByItem]: Got custom Sample Type <=> Group By item: $perm\n" if ($DEBUG);
            unless (grep (/\Q$perm\E/, @customGroupByPermutations)) {
               push(@customGroupByPermutations, $perm);
               $customGroupByPermutationsHash{$perm} = scalar(@customGroupByPermutations) - 1;
            }
            ## Map sample name to its Group By <=> Sample Type permutation group
            $samplePermGroupValueHash{$currentSample} = $perm;
         }
      }
      ## Set current sample chipID
      if (! $chipID or ($chipID !~ /$tokens[1]/)) {
         $chipID = $tokens[1];
      }
      if ($line =~ /\tReplicate Group ID\t/) {
         $bmSampleIndex++;
         $currentSample = pop @tokens;
         push (@sampleNames, $currentSample) unless grep (/^\Q$currentSample\E$/, @sampleNames);
   
      ## Set current Sample Type
      } elsif ($line =~ /\tSample Type\t/) {
         if ($DEBUG) {
            print "DEBUG: Empty \$currentSample!\n" unless ($currentSample);
         }
         $currentSampleType = pop @tokens;
   
      ## Process non-Sample Type group-by item
      } elsif ($limitSortByItem =~ /\w+/ and $line =~ /\t$limitSortByItem\t/) {
         $currentGroupByValue = pop @tokens;
         print "DEBUG: Processing custom limitSortBy item: $currentGroupByValue\n" if ($DEBUG);
         unless (grep (/^\Q$currentGroupByValue\E$/, @groupByValues)) {
            push (@groupByValues, $currentGroupByValue);
            $valueSpaceReplaced = $currentGroupByValue;
            $valueSpaceReplaced =~ s/ /_/g;
            push (@groupByValuesSpacesRemoved, $valueSpaceReplaced);
            $groupByTypesHash{$currentGroupByValue} = scalar(@groupByValues) - 1;
         }
      }
   }
   
   ## Prepare sorted Sample Type <=> Group By permutation items (in sample name sort order)
      @customGroupByPermutationsSorted = ();
      foreach (@sampleNamesSorted) {
         $permGroup = $samplePermGroupValueHash{$_};
         unless (grep (/^\Q$permGroup\E$/, @customGroupByPermutationsSorted)) {
            push (@customGroupByPermutationsSorted, $permGroup);
         }
      }
      if (scalar(@customGroupByPermutations)) { 
         %customGroupByPermutationsIndexed = ();
         ## Build Group By <=> Sample Types lookup with increasing sample type order index
         while (($key, $val) = each %customGroupByPermutationsHash) {
            $customGroupByPermutationsIndexed{$val} = $key;
         }
         sort %customGroupByPermutationsIndexed;
      }
   
   
   ## DEBUG: Dump our data structures to verify that the data looks okay ###
   if ($DEBUG) {
      if (scalar (keys (%customGroupByPermutationsHash))) {
         print "DEBUG: Custom Group-by values:\n";
         print Dumper(\@groupByValues);
         print "DEBUG: Custom Group-by <=> Sample Type permutations:\n";
         print Dumper(\%customGroupByPermutationsHash);
         print "DEBUG: Sample to permutation group map:\n";
         print Dumper(\%samplePermGroupValueHash);
         print "DEBUG: INDEXED Custom Group-by <=> Sample Type permutations:\n";
         print Dumper(\%customGroupByPermutationsIndexed);
         print "DEBUG: Sorted Custom Group-by permutations:\n";
         print Dumper(\@customGroupByPermutationsSorted);
      }
   }
   ### END DEBUG ###
   
   ### PRINT CUSTOM CLS ###
   
   $customcls = $dsid . $limitSortByItem . '.cls';
   $customcls =~ s/ /\_/g;
   ## Determine custom CLS save path from input standard CLS file path
   @clspathtokens = ($clsfile =~ /\//) ? (split(/\//,$clsfile)) : ();
   pop @clspathtokens;
   push(@clspathtokens, $customcls);
   $customfile = join('/', @clspathtokens);
   if (scalar(@clspathtokens) eq 2) {
      $customfile =~ s/^\//\.\//g;
   }
   
   print "DEBUG: Writing custom CLS file in path: $customfile\n" if ($DEBUG);
   
   open(CUSTOMCLS, ">$customfile") or die $!;
   
   ## 1. Print CLS first line:
   ##	<number_samples> TAB <number_groups> TAB 1
   print CUSTOMCLS '' . scalar(@sampleNames) . "\t" . scalar(keys %customGroupByPermutationsHash) . "\t1\n";

   ## 2. Print space-separated group by <=> sample type permutations. We necessarily print sample names with spaces
   ## replaced by underscores.
   @customGroupByPermutationsSpaceReplaced = @customGroupByPermutationsSorted;
   s/\Q /\_/g for(@customGroupByPermutationsSpaceReplaced);
   print CUSTOMCLS '# ' . join(' ', @customGroupByPermutationsSpaceReplaced) . "\n";
   
   ## 3. Print sample indices that correspond to each Group By <=> Sample Type permutation
   $sampleGroupsLine = '';
   $outputIndex = 0;
   foreach (@customGroupByPermutationsSorted) {
      $permGroup = $_;
      foreach (@sampleNamesSorted) {
         $samplePermGroup = $samplePermGroupValueHash{$_};
         if ($permGroup eq $samplePermGroup) {
            $sampleGroupsLine .= $outputIndex . ' ';
         }
      }
      $outputIndex++;
   }
   ## 'rtrim' any trailing spaces
   $sampleGroupsLine =~ s/\s+$//g;
   print CUSTOMCLS "$sampleGroupsLine\n";
   close(CUSTOMCLS);
   
} ## foreach limitSortByItem
