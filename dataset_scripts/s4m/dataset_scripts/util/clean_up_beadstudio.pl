#use strict;
#use warnings;

$filename_full_path = $ARGV[0];

print $filename_full_path."\n";



open FILE, $filename_full_path or die $!;

while (<FILE>) {

    $header_line = $_;
    @headers = split("\t",$header_line);
    $column_number = 1;
    $keep_column = "";
    foreach (@headers) {
        $header = $_;

        if (!($header =~ m/MAX/)&&!($header=~ m/MIN/)&&!($header=~ m/NBEADS/)) { 
            print $header."\n";
            $keep_column = $keep_column .",". "$column_number";
        } 

        $column_number = $column_number +1;
    }
    last;
}
print "$keep_column\n";

