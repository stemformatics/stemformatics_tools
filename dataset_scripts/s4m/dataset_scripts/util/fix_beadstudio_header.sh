#!/bin/sh
beadstudio="$1"

Usage () {
  echo "Usage: $0 <beadstudio_file>"; echo
}

if [ ! -f "$beadstudio" ]; then
  echo "Error: Beadstudio file '$beadstudio' not found!"; echo
  Usage
  exit 1
fi

## Fix case where we have header like:
## ID_REF  Sample 1  Detection  Sample 2  Detection  Sample 3  Detection ...
##
## needs to be:
##
## ID_REF  Sample 1.AVG_Signal  Sample 1.Detection Pval  Sample 2.AVG_Signal  Sample 2.Detection Pval ...

head -1 "$beadstudio" | grep -P "\tDetection[ Pval]*\t.*\tDetection[ Pval]*\t" > /dev/null 2>&1
staggered_detection="$?"
head -1 "$beadstudio" | grep -P "AVG_Signal.*Detection Pval" > /dev/null 2>&1
correct_format="$?"

if [ $staggered_detection -eq 0 -a $correct_format -ne 0 ]; then
  ## Extract sample names
  sampleindexes=`head -1 "$beadstudio" | tr "\t" "\n" | grep -n -v -P "Detection|ID_REF|ProbeID" | cut -d':' -f 1`
  samplecount=`echo "$sampleindexes" | wc -l`

  #echo "DEBUG: Got sample indices: [$sampleindexes]"

  cutcols=`echo "$sampleindexes" | tr "\n" "," | sed 's/\,$//'`
  echo "DEBUG: cutcols=[$cutcols]"

  sampleids=`head -1 "$beadstudio" | cut -f $cutcols | tr "\t" "\n"`
  echo "DEBUG: sampleids=[$sampleids]"

  idref=`head -1 "$beadstudio" | cut -f 1`

  ## Now write out new header
  #echo "DEBUG: ==="
  #header="$idref"
  rm -rf /tmp/.newheader
  echo -e -n "$idref" >> /tmp/.newheader
  echo "$sampleids" | while read s
  do
    echo -e -n "\t$s.AVG_Signal\t$s.Detection Pval" >> /tmp/.newheader
    #header="$header\t$s.AVG_Signal\t$s.Detection Pval"
  done 
  #echo; echo "===DEBUG"

  header=`cat /tmp/.newheader`
  echo "DEBUG: New header=[$header]"
  sed -i.sed.bak --follow-symlinks "1 s|^.*|$header|" "$beadstudio"

fi
