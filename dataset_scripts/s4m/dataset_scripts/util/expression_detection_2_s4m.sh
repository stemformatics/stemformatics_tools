#!/bin/sh
# Synopsis: Takes input path to dataset directory, outputs Stemformatics
#           formatted expression file (probe_expression_avg_replicates.txt)
#
SCRIPTNAME="$0"
SCRIPTS_HOME=`dirname $0`
SCRIPTS_HOME="${SCRIPTS_HOME}/.."

Usage () {
    echo "Usage: $SCRIPTNAME <dataset_path>"; echo
    exit 1
}

if [ "$#" -lt 1 ]; then
    Usage
fi

DSPATH="$1"

if [ ! -d "$DSPATH" ]; then
    echo "Dataset path '$DSPATH' not found!"; echo
    exit 1
fi

if [ -f "$DSPATH/METASTORE" ]; then
    dataset_id=`grep -P "^s4m_dataset_id\=" "$DSPATH/METASTORE" | tr -d [:cntrl:] | awk -F'=' '{print $2}'`
    chip_type=`grep -P "^s4m_chip_type\=" "$DSPATH/METASTORE" | tr -d [:cntrl:] | awk -F'=' '{print $2}'`
    min_rep=`grep -P "^min_replicates\=" "$DSPATH/METASTORE" | tr -d [:cntrl:] | awk -F'=' '{print $2}'`
    max_rep=`grep -P "^max_replicates\=" "$DSPATH/METASTORE" | tr -d [:cntrl:] | awk -F'=' '{print $2}'`
    if [ -z "$dataset_id" -o -z "$chip_type" ]; then
        echo "Error: Incomplete dataset metadata! Ensure at least these attributes are set: "
        echo "  s4m_dataset_id"
        echo "  s4m_chip_type"
        echo "  min_replicates"
        echo "  max_replicates"
        echo
        exit 1
    fi
else
    echo "Error: Metadata store file '$DSPATH/METASTORE' not found!"; echo
    exit 1
fi

## Disable check for 'raw_expression_unpacked.txt' - not used for anything (TODO: Delete ASAP)
##
#if [ ! -f "$DSPATH/source/normalized/raw_expression_unpacked.txt" ]; then
#    echo "Error: Un-normalized probeset expression file '$DSPATH/source/normalized/raw_expression_unpacked.txt' not found!"; echo
#    exit 1
#fi
if [ ! -f "$DSPATH/source/normalized/normalized_expression_detection.txt" ]; then
    echo "Error: Normalized probeset expression and detection file '$DSPATH/source/normalized/normalized_expression_detection.txt' not found!"; echo
    exit 1
fi
if [ ! -f "$DSPATH/chip_id_replicate_group_map.txt" ]; then
    echo "Error: Sample replicate map file '$DSPATH/chip_id_replicate_group_map.txt' not found!"
    echo "Create one based on template: '$DSPATH/chip_id_replicate_group_map.txt.template'"
    echo "File '$DSPATH/probe_expression_avg_replicates.txt'  not created."; echo
    exit 1
fi

## Create "probe_expression_avg_replicates.txt" file
if [ "$max_rep" -gt 1 ]; then
    echo "INFO: Sample expressions will be collapsed (there are technical replicates)"
    ## Create file with mean and standard deviations of probesets across samples
    ./include/collapse_replicate_expression.pl "$DSPATH/source/normalized/normalized_expression_detection.txt" "$DSPATH/chip_id_replicate_group_map.txt" "$DSPATH/probe_expression_avg_replicate.txt.tmp"
    ## dsid,chip_type,chip_id,probe_id,replicate_group_id,avg_expression,standard_deviation,detected
    awk -F'\t' -v dsid="$dataset_id" -v chiptype="$chip_type" '{print dsid"\t"chiptype"\t"$1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6}' "$DSPATH/probe_expression_avg_replicate.txt.tmp" | sort -T "$DSPATH" > "$DSPATH/probe_expression_avg_replicates.txt"
    rm -f "$DSPATH/probe_expression_avg_replicate.txt.tmp"
else
    ## Don't calculate mean and standard deviation - no replicates to collapse.
    echo "INFO: Sample expression not collapsed (no technical replicates)"
    ## dsid,chip_type,chip_id,probe_id,replicate_group_id,avg_expression,standard_deviation,detected
    awk -F'\t' -v dsid="$dataset_id" -v chiptype="$chip_type" '{print dsid"\t"chiptype"\t"$1"\t"$2"\t"$1"\t"$3"\t0.0\t"$4}' "$DSPATH/source/normalized/normalized_expression_detection.txt" > "$DSPATH/probe_expression_avg_replicates.txt"
    sync
    if [ -s "$DSPATH/probe_expression_avg_replicates.txt" ]; then
        ## Task #269" Strip trailing ".CEL|.cel" from Affy chip IDs as necessary.
        sed -i -r -e 's/\.CEL//gi' "$DSPATH/probe_expression_avg_replicates.txt"
        ## Replace chip_ids in replicate group column with names from replicate group map file
        ## NOTE: -k flag keeps chip IDs that aren't found in chip_id_replicage_group_map.txt
        "$SCRIPTS_HOME/util/file_replace_column.pl" "$DSPATH/chip_id_replicate_group_map.txt" "$DSPATH/probe_expression_avg_replicates.txt" 5 -k  > "$DSPATH/probe_expression_avg_replicate.txt.tmp"
        sync
        if [ ! -s "$DSPATH/probe_expression_avg_replicate.txt.tmp" ]; then
            echo "Error: Bad chip IDs in '$DSPATH/chip_id_replicate_group_map.txt': If Affy, ensure no '.CEL' in chip names in any biosample .txt files"
            rm -f "$DSPATH/probe_expression_avg_replicate.txt.tmp"
            exit 1
        fi
        mv "$DSPATH/probe_expression_avg_replicate.txt.tmp" "$DSPATH/probe_expression_avg_replicates.txt"
    fi
fi
if [ -s "$DSPATH/probe_expression_avg_replicates.txt" ]; then
    ## Pre-emptively replace any "0" expression values with "0.0" otherwise dataset validation will fail
    "$SCRIPTS_HOME/util/fix_zero_values.sh" "$DSPATH/probe_expression_avg_replicates.txt"
    echo "Created $DSPATH/probe_expression_avg_replicates.txt"
fi

echo "Don't forget to run \"s4m.sh --update $DSPATH\""
