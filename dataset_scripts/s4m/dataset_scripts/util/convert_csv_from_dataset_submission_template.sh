#!/bin/bash
# Make sure there are no trailing tabs in the data file.
# You can keep all the headers the way they are. Just need to save as TSV without any text quotations and no extra tab columns
# NOTE: we might need to create another assay_platform table for this new dataset
# eg.insert into assay_platforms values(105,'Mouse','Stemformatics','Protein','unknown','unknown',-10,'Ratio log2', '',105);

# NOTE: we might also not need mappings and we can map something les and use that mapping id in the assay_platform
body() {
    IFS= read -r header
    printf '%s\n' "$header"
    "$@"
}


file=$1
echo $1

db_id=$2
mapping_id=$3

TMPDIR=`dirname $file`

mapping_file=mapping_$1
data_file=data_$1
temp_data_file=$TMPDIR/temp_${data_file}
temp_file=$TMPDIR/temp_$1
cp $1 $temp_file
perl -pi -e '$_ = "" if ($. == 1);' $temp_file 

# This is for the mapping files
cat $temp_file | awk '{ print '"$db_id"' "\t" '"$mapping_id"' "\t" "Gene" "\t" $1 "\t" "Probe" "\t" $2}'  > $mapping_file


# This is for the data files
# From https://docs.google.com/document/d/1mAMJA9aIMogTVsOpyH8oytc4RduqQ3fXz8ZroWjJIwk/edit
cat $1 | cut -f2-  > $temp_data_file
sed  -i '1s/[^	]*	\(.*\)/\1/' $temp_data_file
cat $temp_data_file | body sort | uniq > $data_file
/data/repo/git-working/stemformatics_tools/dataset_scripts/s4m/dataset_scripts/normalization/util/unpack_expression_table.pl  $data_file > raw_expression_unpacked.txt
cat raw_expression_unpacked.txt | while read line; do echo -e "$line\tTRUE"; done > normalized_expression_detection.txt

rm -f $temp_file $temp_data_file
