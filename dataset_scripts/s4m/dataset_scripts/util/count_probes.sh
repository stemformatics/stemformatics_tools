#!/bin/sh
DATADIR="$1"
echo "Counting probes.. "
count=`awk -F'\t' '{print $4}' "$DATADIR/probe_expression_avg_replicates.txt" | sort | uniq | wc -l`
echo "Probe count: $count"; echo
