#!/bin/sh
## Install GCT (and CLS) file/s into target host:/path
SCRIPTNAME="$0"
DATADIR="$1"
HOSTPATH="$2"
## Optional http(s)://<hostname> spec for www API calls.
## Required for SSL hosts.
WEBAPIPATH="$3"

if [ -z "$DATADIR" -o ! -d "$DATADIR" ]; then
  echo "Error: Bad dataset directory (null or not existing) given to install script $0"; echo
  exit 1
fi
if [ -z "$HOSTPATH" ]; then
  echo "Error: Must specify target host and shared file path string for GCT/CLS file destination!";
  echo "Format: [user@host:]/path/to/shared/data"; echo
  exit 1
fi
SCRIPTDIR=`dirname $0`
TMPDIR="/tmp/s4m"
mkdir -p "$TMPDIR"
LOGDIR="$DATADIR/load"
mkdir -p "$LOGDIR"

accession=`cat $DATADIR/ACCESSION`
INSTLOG="$LOGDIR/install_$accession.log"

SHAREDPATH=`echo "$HOSTPATH" | sed -r -e 's/^.*\://g'`
GCTDIR="$SHAREDPATH/GCTFiles"
CLSDIR="$SHAREDPATH/CLSFiles"
CUMULATIVEDIR="$SHAREDPATH/CUMULATIVEFiles"
ROHARTBASE="$SHAREDPATH/RohartFiles"
PROBESDIR="$SHAREDPATH/ProbeFiles"
U=""
HOST=""
echo "$HOSTPATH" | grep "\@" > /dev/null 2>&1
if [ $? -eq 0 ]; then
  U=`echo "$HOSTPATH" | sed -r -e 's/\@.*$//g'`
fi
echo "$HOSTPATH" | grep "\:" > /dev/null 2>&1
if [ $? -eq 0 ]; then
  HOST=`echo "$HOSTPATH" | sed -r -e 's/^.*\@//g' -e 's/\:.*$//g'`
fi


### Functions ###

Usage () {
  echo "Usage:	$SCRIPTNAME <dataset_dir> <[user@host:]/path/to/shared/files_dir>  [http(s)://<host>]"; echo
  echo "Note: Last argument MUST be provided for SSL enabled Stemformatics hosts."; echo
  exit
}

s4m_api_login () {
  host="$1"
  proto="$2"
  u="$3"
  p="$4"
  loginuri="$proto://$host/auth/submit"

  ## NOTE: Assumes default HTTP port 80 - this won't work for Stemformatics instances on non default port
  wget --no-proxy -q --post-data "username=$u&password=$p" --save-cookies $TMPDIR/.s4mcookies.txt --keep-session-cookies $loginuri -O $TMPDIR/.wgetout
  ## Check for 401 auth fail
  grep -P -i "\s+401\s+" $TMPDIR/.wgetout > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    echo "Error: Stemformatics returns 401 Authorization fail:"
    echo "==="
    cat $TMPDIR/.wgetout
    echo "==="
    echo "Cannot perform API calls."
    return 1
  fi
  ## Check for duty user login success or fail
  grep -i "please sign in" $TMPDIR/.wgetout > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    rm -f $TMPDIR/.wgetout
    echo "Error: Failed Stemformatics duty user login (bad user/password). Cannot perform API calls."
    return 1
  fi
  return 0
}

s4m_api_call () {
  uri="$1"
  desc="$2"

  echo "Updating $desc.." 
  ret=`wget --no-proxy -q --load-cookies $TMPDIR/.s4mcookies.txt --keep-session-cookies $uri -O -`
  update_status="false"
  echo "$ret" | grep -i "done" > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    update_status="true"
  fi

  ## clean up tmp files from s4m_api_login() and s4m_api_call()
  rm -f $TMPDIR/.s4mcookies.txt $TMPDIR/.wgetout

  if [ "$update_status" = "true" ]; then
    echo "  done"
  else
    echo "Error: $desc update by API did not complete successfully! Stemformatics returned:"
    echo -e "===\n$ret\n==="
    sleep 2
    return 1
  fi
  return 0
}


### MAIN ###

if [ -z "$DATADIR" ]; then
  Usage
fi

data_type_id=`grep data_type_id "$DATADIR/METASTORE" | awk -F'=' '{print $2}' | tr -d [:cntrl:]`

## find GCT file
gctfile=`ls "$DATADIR" | grep -P "\.gct$"`
if [ -z "$gctfile" ]; then
  echo "Error: Failed to find GCT file in dataset directory '$DATADIR'!"; echo
  exit 1
fi
## find ACCESSION file
accession=`cat "$DATADIR/ACCESSION" | tr -d [:cntrl:]`
if [ -z "$accession" ]; then
  echo "Error: Failed to find ACCESSION file in dataset directory '$DATADIR'!"; echo
  exit 1
fi
## find core CLS file - required for non sc-RNAseq datasets
if [ ! -f "$DATADIR/$accession.cls" -a "$data_type_id" != "3" ]; then
  echo "Error: Failed to find core CLS file '$DATADIR/$accession.cls'!"; echo
  exit 1
fi
## find other CLS file/s
clsfiles=`ls "$DATADIR" | grep -v $accession | grep -P "\.cls$"`
if [ -z "$clsfiles" ]; then
  echo "Info: No extra CLS file/s found in dataset directory '$DATADIR'!"; echo
  sleep 2
fi
## find Rohart file/s
rohartfiles=`ls "$DATADIR" | grep -P "${accession}_rohart.*txt$"`

cumulativefile="${accession}_bgonly.cumulative.txt"

dsid=`grep dataset_id "$DATADIR/METASTORE" | awk -F'=' '{print $2}' | tr -d [:cntrl:]`


echo "Copy cumulative (YuGene) file to SHARED path.."
if [ ! -f "$DATADIR/$cumulativefile" ]; then
  echo "  not found, skipping"
else
  ## Copy file to local install target
  if [ -z "$HOST" -a -z "$U" ]; then
    cp "$DATADIR/$cumulativefile" "$CUMULATIVEDIR/dataset$dsid.cumulative.txt"
  ## Copy file to remote install target
  else
    scp "$DATADIR/$cumulativefile" "$U@$HOST:$CUMULATIVEDIR/dataset$dsid.cumulative.txt"
  fi
  echo "done"
fi

echo "Copy Rohart file(s) to SHARED Rohart project file path(s).."
if [ -z "$rohartfiles" ]; then
  echo "  none found, skipping"
else
  ## Copy file to local install target
  if [ -z "$HOST" -a -z "$U" ]; then
    for r in $rohartfiles
    do
      project=`echo "$r" | sed -r -e 's|^.*_rohart.(.*).txt$|\1|'`
      cp "$DATADIR/$r" "${ROHARTBASE}$project/dataset$dsid.rohart.$project.txt"
    done
  ## Copy file to remote install target
  else
    for r in $rohartfiles
    do
      project=`echo "$r" | sed -r -e 's|^.*_rohart.(.*).txt$|\1|'`
      scp "$DATADIR/$r" "$U@$HOST:${ROHARTBASE}$project/dataset$dsid.rohart.$project.txt"
    done
  fi
  echo "done"
fi

echo "Copy GCT file to SHARED GCT path.."
## Copy GCT to global GCT directory and rename at the same time
## Copy local
if [ -z "$HOST" -a -z "$U" ]; then
  cp "$DATADIR/$gctfile" "$GCTDIR/dataset$dsid.gct"
## Copy remote
else
  scp "$DATADIR/$gctfile" "$U@$HOST:$GCTDIR/dataset$dsid.gct"
fi

echo "done"

## Copy CLS files to global CLS directory
echo "Copy CLS file/s to SHARED CLS path.." 
if [ ! -f "$DATADIR/$accession.cls" ]; then
  echo "  Skipping, no CLS found"
## Copy local
elif [ -z "$HOST" -a -z "$U" ]; then
  cp "$DATADIR/$accession.cls" "$CLSDIR/$dsid.cls"

  if [ ! -z "$clsfiles" ]; then
    echo "Also copying extra CLS file/s.."
    for c in $clsfiles; do
      cp "$DATADIR/$c" "$CLSDIR/"
    done
  fi
## Copy remote
else
  scp "$DATADIR/$accession.cls" "$U@$HOST:$CLSDIR/$dsid.cls"
  if [ ! -z "$clsfiles" ]; then
    echo "Also copying extra CLS file/s.."
    extracls=""
    for c in $clsfiles; do
      extracls="$extracls $DATADIR/$c"
    done
    scp $extracls "$U@$HOST:$CLSDIR/"
  fi
fi
echo "  done";

echo "Generating and transferring .probes file.."
# ProbeFiles .probes
probefile="$dsid.probes"
sed '1,3d' "$DATADIR/$gctfile" | cut -f 1 > "$DATADIR/$probefile"
scp "$DATADIR/$probefile" "$U@$HOST:$PROBESDIR/$probefile"

echo "Calling 'setup new dataset' API to complete setup (BACKGROUND PROCESS).."
echo; echo "*** THIS CAN TAKE 30 MINS OR MORE - THIS IS OKAY ***"; echo
echo "(NOTE: Check in Stemformatics, or run again via Stemformatics Admin interface if needed)"; echo

(nohup ssh $HOST wget -T 0 -O - "http://127.0.0.1:5000/api/setup_new_dataset/$dsid" "| sed -r -e 's|<br[\\/>]*|\\n|g' > /var/www/pylons-data/prod/log/load_gct_${dsid}.log 2>&1")&
echo; echo "For log file, see: $HOST:/var/www/pylons-data/prod/log/load_gct_${dsid}.log"; echo
echo "DONE"; echo

