#!/bin/sh

get_s4m_option () {
  var="$1"
  if [ ! -z "$S4M_SET" ]; then
    s4moptlines=`echo "$S4M_SET" | sed -r -e 's/\,/\n/g'`
    for opt in $s4moptlines; do
      x=`echo $opt | sed -r -e 's/\=.+$//g'`
      y=`echo $opt | sed -r -e 's/^.+\=//g'`
      if [ "$x" = "$var" ]; then
        echo "$y"
        return
      fi
    done
  fi
  echo ""
}


## Fetch list of valid sample pheno files.
##
## Will not return sample_pheno.txt files with extensions:
##	template
##	bak
##	hidd(en)
##	hide
##	old
##	org
##
## NOTE: This is pretty ugly. Need to rationalise and rework this convention.
##
get_sample_pheno_files () {
  searchdir="$1"
  ls $searchdir | grep -P "^sample_pheno\.txt" | grep -i -v -P "(template|bak|hidd|hide|old|org)"
}


## Expression density
plot_dataset_expression_density () {

  echo "Plotting expression density.."
  cd "$SCRIPTS_HOME/normalization"
  if [ -f "$NORMDIR/raw_expression.txt" ]; then
    $R_BIN -f ./R/density.R --args -input="$NORMDIR/raw_expression.txt" -output="$NORMDIR/prenorm_density.png" -title="Pre-norm expression density" -islog2=TRUE  >> "$QC_LOG" 2>&1
  fi
  if [ -f "$NORMDIR/normalized_expression.txt" ]; then
    $R_BIN -f ./R/density.R --args -input="$NORMDIR/normalized_expression.txt" -output="$NORMDIR/postnorm_density.png" -title="Post-norm expression density" -islog2=TRUE  >> "$QC_LOG" 2>&1
  fi
  cd $OLDPWD
  if [ ! -s "$NORMDIR/prenorm_density.png" -o ! -f "$NORMDIR/postnorm_density.png" ]; then
    echo "Error: Expression density plot output failure for raw and/or normalized data
" 1>&2
    return 1
  fi
  echo "done"
}


## PCA
plot_dataset_pca () {
  pheno_files=`get_sample_pheno_files "$DATADIR"`

  echo "Creating PCA plots.."

  ## function exit status
  ret=0

  cd "$SCRIPTS_HOME/normalization"
  npheno=`echo "$pheno_files" | wc -l`

  ## We found at least one sample_pheno.txt[.blah] file
  if [ $npheno -ne 0 ]; then

    ## Create PCA plots for ALL sample_pheno.txt[.blah] files found
    for p in $pheno_files
    do
      echo "  <-- $p"
      ext=`echo $p | sed 's/^sample_pheno\.txt\(.*\)$/\1/'`
      label=""
      filelabel=""
      if [ ! -z "$ext" ]; then
	label="["`echo "$ext" | sed 's|^\.||'`"]"
	filelabel="_${label}"
      else
        label="[default]"
      fi
      ## flag to only use group labels in PCA for any sample_pheno.txt*
      onlygroups="-onlygrouplabels=TRUE"
      ## flag to only use sample labels in PCA for any sample_pheno.txt*
      onlysamples="-onlysamplelabels=TRUE"

      ## Pre-norm PCA - just group names
      $R_BIN -f ./R/PCA.R --args -input="$NORMDIR/raw_expression.txt" -output="$NORMDIR/prenorm_pca${filelabel}_groupnames.png" -phenofile="$DATADIR/$p" -title="Pre-norm PCA $label" $onlygroups >> "$QC_LOG" 2>&1
      retR=$?
      if [ $retR -ne 0 ]; then
	echo "Error: PCA output failure for raw expression data with '-onlygrouplabels=TRUE' (pheno: '$p')"
	ret=$retR
      fi
      ## Add leading TAB to screetable TSV
      sed -i -r -e '1 s|^|\t|' "$NORMDIR/prenorm_pca${filelabel}_groupnames_screetable_prcomp_variance.tsv"
      ## Set first column header to "SampleID" in *sample_data.tsv
      sed -i -r -e '1 s|^\t|SampleID\t|' "$NORMDIR/prenorm_pca${filelabel}_groupnames_sample_data.tsv"

      ## Pre-norm PCA - just sample names
      $R_BIN -f ./R/PCA.R --args -input="$NORMDIR/raw_expression.txt" -output="$NORMDIR/prenorm_pca${filelabel}_sampleids.png" -phenofile="$DATADIR/$p" -title="Pre-norm PCA $label" $onlysamples >> "$QC_LOG" 2>&1
      retR=$?
      if [ $retR -ne 0 ]; then
	echo "Error: PCA output failure for raw expression data with '-onlysamplelabels=TRUE' (pheno: '$p')"
	ret=$retR
      fi
      ## Add leading TAB to screetable TSV
      sed -i -r -e '1 s|^|\t|' "$NORMDIR/prenorm_pca${filelabel}_sampleids_screetable_prcomp_variance.tsv"
      ## Set first column header to "SampleID" in *sample_data.tsv
      sed -i -r -e '1 s|^\t|SampleID\t|' "$NORMDIR/prenorm_pca${filelabel}_sampleids_sample_data.tsv"

      ## Pre-norm PCA no labels
      $R_BIN -f ./R/PCA.R --args -input="$NORMDIR/raw_expression.txt" -output="$NORMDIR/prenorm_pca${filelabel}_nolabels.png" -phenofile="$DATADIR/$p" -title="Pre-norm PCA $label" -nolabels=TRUE >> "$QC_LOG" 2>&1
      retR=$?
      if [ $retR -ne 0 ]; then
	echo "Error: PCA output failure for raw expression data with '-nolabels=TRUE' (pheno: '$p')"
	ret=$retR
      fi
      ## Add leading TAB to screetable TSV
      sed -i -r -e '1 s|^|\t|' "$NORMDIR/prenorm_pca${filelabel}_nolabels_screetable_prcomp_variance.tsv"
      ## Set first column header to "SampleID" in *sample_data.tsv
      sed -i -r -e '1 s|^\t|SampleID\t|' "$NORMDIR/prenorm_pca${filelabel}_nolabels_sample_data.tsv"

      ## Post-norm PCA - just group names
      $R_BIN -f ./R/PCA.R --args -input="$NORMDIR/normalized_expression.txt" -output="$NORMDIR/postnorm_pca${filelabel}_groupnames.png" -phenofile="$DATADIR/$p" -title="Post-norm PCA $label" $onlygroups >> "$QC_LOG" 2>&1
      retR=$?
      if [ $retR -ne 0 ]; then
	echo "Error: PCA output failure for normalized expression data with '-onlygrouplabels=TRUE' (pheno: '$p')"
	ret=$retR
      fi
      ## Add leading TAB to screetable TSV
      sed -i -r -e '1 s|^|\t|' "$NORMDIR/postnorm_pca${filelabel}_groupnames_screetable_prcomp_variance.tsv"
      ## Set first column header to "SampleID" in *sample_data.tsv
      sed -i -r -e '1 s|^\t|SampleID\t|' "$NORMDIR/postnorm_pca${filelabel}_groupnames_sample_data.tsv"

      ## Post-norm PCA - just sample names
      $R_BIN -f ./R/PCA.R --args -input="$NORMDIR/normalized_expression.txt" -output="$NORMDIR/postnorm_pca${filelabel}_sampleids.png" -phenofile="$DATADIR/$p" -title="Post-norm PCA $label" $onlysamples >> "$QC_LOG" 2>&1
      retR=$?
      if [ $retR -ne 0 ]; then
	echo "Error: PCA output failure for normalized expression data with '-onlysamplelabels=TRUE' (pheno: '$p')"
	ret=$retR
      fi
      ## Add leading TAB to screetable TSV
      sed -i -r -e '1 s|^|\t|' "$NORMDIR/postnorm_pca${filelabel}_sampleids_screetable_prcomp_variance.tsv"
      ## Set first column header to "SampleID" in *sample_data.tsv
      sed -i -r -e '1 s|^\t|SampleID\t|' "$NORMDIR/postnorm_pca${filelabel}_sampleids_sample_data.tsv"

      ## Post-norm PCA no labels
      $R_BIN -f ./R/PCA.R --args -input="$NORMDIR/normalized_expression.txt" -output="$NORMDIR/postnorm_pca${filelabel}_nolabels.png" -phenofile="$DATADIR/$p" -title="Post-norm PCA $label" -nolabels=TRUE >> "$QC_LOG" 2>&1
      retR=$?
      if [ $retR -ne 0 ]; then
	echo "Error: PCA output failure for normalized expression data with '-nolabels=TRUE' (pheno: '$p')"
	ret=$retR
      fi
      ## Add leading TAB to screetable TSV
      sed -i -r -e '1 s|^|\t|' "$NORMDIR/postnorm_pca${filelabel}_nolabels_screetable_prcomp_variance.tsv"
      ## Set first column header to "SampleID" in *sample_data.tsv
      sed -i -r -e '1 s|^\t|SampleID\t|' "$NORMDIR/postnorm_pca${filelabel}_nolabels_sample_data.tsv"
    done

  else
    echo "Warning: No PCA plots generated because 'sample_pheno.txt' not found!"
  fi

  cd $OLDPWD
  if [ $ret -eq 0 ]; then
    echo "done"
  fi
  return $ret
}


## Boxplots
plot_dataset_boxplots () {

  phenofile="$DATADIR/sample_pheno.txt"

  ret=0

  echo "Creating boxplots.."

  phenoflag=""
  if [ -f "$phenofile" ]; then
    phenoflag="-phenofile=$phenofile"
  fi
  cd "$SCRIPTS_HOME/normalization"

  $R_BIN -f ./R/boxplot.R --args -input="$NORMDIR/raw_expression.txt" -output="$NORMDIR/prenorm_sample_boxplot.png" -title="Pre-norm sample boxplot" $phenoflag >> "$QC_LOG" 2>&1
  retR=$?
  if [ $retR -ne 0 ]; then
    echo "Error: Boxplot output failure for raw expression data"
    ret=$retR
  fi
  $R_BIN -f ./R/boxplot.R --args -input="$NORMDIR/normalized_expression.txt" -output="$NORMDIR/postnorm_sample_boxplot.png" -title="Post-norm sample boxplot" $phenoflag  >> "$QC_LOG" 2>&1
  retR=$?
  if [ $retR -ne 0 ]; then
    echo "Error: Boxplot output failure for normalized expression data"
    ret=$retR
  fi

  cd $OLDPWD
  if [ $ret -eq 0 ]; then
    echo "done"
  fi
  return $ret
}


## Hierarchical cluster
plot_dataset_hc () {
  phenofile="$DATADIR/sample_pheno.txt"

  ret=0

  echo "Creating hierarchical clusters.."

  phenoflag=""
  if [ -f "$phenofile" ]; then
    phenoflag="-phenofile=$phenofile"
  fi
  cd "$SCRIPTS_HOME/normalization"

  $R_BIN -f ./R/hierarchical_cluster.R --args -input="$NORMDIR/raw_expression.txt" -output="$NORMDIR/prenorm_sample_cluster.png" -title="Pre-norm sample relations" -islog=TRUE $phenoflag >> "$QC_LOG" 2>&1
  retR=$?
  if [ $retR -ne 0 ]; then
    echo "Error: Hierarchical cluster output failure for raw expression data"
    ret=$retR
  fi
  $R_BIN -f ./R/hierarchical_cluster.R --args -input="$NORMDIR/normalized_expression.txt" -output="$NORMDIR/postnorm_sample_cluster.png" -title="Post-norm sample relations" -islog=TRUE $phenoflag >> "$QC_LOG" 2>&1
  retR=$?
  if [ $retR -ne 0 ]; then
    echo "Error: Hierarchical cluster output failure for normalized expression data"
    ret=$retR
  fi

  cd $OLDPWD
  if [ $ret -eq 0 ]; then
    echo "done"
  fi
  return $ret
}


## Bundle QC plots into a PDF file
## NOTE: Any CUSTOM QC plots from within chip-specific normalisation should save outputs as "prenorm_qc*.png" or "postnorm_qc*.png" to be automatically picked up in PDF compilation
compile_QC_PDF () {
  which convert > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    echo "Compiling default and custom QC plots into PDF..
"
    (cd "$DATADIR/source/normalized" && convert prenorm_density.png prenorm_sample_boxplot.png prenorm_sample_cluster.png prenorm_pca*pc12.png prenorm_pca*pc13.png  prenorm_qc*.png \
postnorm_density.png postnorm_sample_boxplot.png postnorm_sample_cluster.png postnorm_pca*pc12.png postnorm_pca*pc13.png postnorm_pca_sampleids_screeplot.png postnorm_qc*.png qc_plots.PDF)
    if [ $? -ne 0 ]; then
      echo "Note: Can usually ignore missing 'prenorm_qc*.png' or 'postnorm_qc*.png' (used for CUSTOM QC only)." 
    fi
    echo "done"
  else
    echo "Error: Skipped QC PDF compilation as 'convert' tool was not found!
" 1>&2
    exit 1
  fi
}


do_dataset_qc_plots () {
  ploterror=0
  ## reset QC plots log
  truncate -s 0 "$QC_LOG"

  if [ ! -f "$DATADIR"/source/normalized/raw_expression.txt ]; then
    echo "Error: Missing '$DATADIR/source/normalized/raw_expression.txt', needed for QC plotting!
" 1>&2
    exit 1
  fi
  if [ ! -f "$DATADIR"/source/normalized/normalized_expression.txt ]; then
    echo "Error: Missing '$DATADIR/source/normalized/normalized_expression.txt', needed for QC plotting!
" 1>&2
    exit 1
  fi

  ## Remove old QC plots and QC PDF
  rm -f "$DATADIR"/source/normalized/*.png "$DATADIR"/source/normalized/qc_plots.PDF > /dev/null 2>&1

  ## Iterate over set of generic plots for brevity
  plots="expression_density
pca
boxplots
hc"

  for plot in $plots
  do
    if [ -z "$plot" ]; then
      continue
    fi
    ## call plot function dynamically by name
    eval "plot_dataset_$plot"
    ret="$?"
    if [ $ret -ne 0 ]; then
      ploterror=$ret
    fi
  done

  ## Do custom QC if available
  if [ -f "$SCRIPTS_HOME/normalization/R/custom_qc_$PLATFORM.R" ]; then
    echo "Found CUSTOM QC script for this platform ($PLATFORM), executing.."
    $R_BIN -f "$SCRIPTS_HOME/normalization/R/custom_qc_$PLATFORM.R" --args -input="$DATADIR/source/raw" -output="$DATADIR/source/normalized" >> "$QC_LOG" 2>&1
    if [ $? -ne 0 ]; then
      ploterror=$?
    else
      echo "done"
    fi
  fi

  if [ $ploterror -ne 0 ]; then
    echo "For QC error log, see: $QC_LOG"; echo
  fi

  compile_QC_PDF

  return $ploterror
}


check_do_dataset_qc_only () {
  qconly=`get_s4m_option "qconly"`
  ## We're only doing QC
  if [ ! -z "$qconly" ]; then
    echo "Performing QC-only
"
    do_dataset_qc_plots
    exit $?
  fi
}


## Check for 'sample_pheno.txt'
## If not found and user wishes to abort, this script exits
check_sample_pheno_exists () {

  pheno_files=`get_sample_pheno_files "$DATADIR"`

  #echo "DEBUG: Got pheno_files=[$pheno_files] in check_sample_pheno_exists()"

  phenocount=`echo "$pheno_files" | wc -l`

  if [ $phenocount -eq 0 ]; then
    echo
    echo "Note: Sample phenotype file(s) with format 'sample_pheno.txt[.factor]' not found in dataset directory."
    echo "At least the default 'sample_pheno.txt' is required to create sample PCA plots during QC (highly recommended)."
    echo -n "Continue? [y/N] "
    read ans
    if [ -z "$ans" ]; then
      ans="N"
    fi
    case $ans in
      n|N) echo "Aborting normalization."; exit
           ;;
      *)
           ;;
    esac
  fi
}


Usage () {
   echo
   echo "Usage: $0 <dataset_dir> <platform_chip_type> [<probe_affinity_or_annotation_or_aroma_working_directory>] [-R <R binary override>|-APT <APT binary override>|-SET a=b[,x=y..]]"
   echo
   echo "where: 'platform_chip_type' is numerical chip_type identifier as specified"
   echo "       in \$S4M_HOME/etc/assay_platforms.txt"
   echo
   echo "       '<probe_affinity_or_annotation_or_aroma_working_directory>' is either:"
   echo "           1) for GeneChip arrays, a read-write location of precomputed R binary probe affinity data files  OR"
   echo "           2) for ST arrays, the Aroma Affymetrix working directory path  OR"
   echo "           3) for GeneChip or ST arrays using APT pipeline, path to platform annotation directory (where directory is CDF name)"
   echo
   echo "           NOTE: This argument is ignored for Illumina arrays."
   echo
   exit
}


### START ###

if [ -z "$S4M_HOME" ]; then
  echo "Error: '$0' requires S4M_HOME path to be set!
" 1>&2
  exit 1
fi

if [ $# -lt 3 ]; then
   Usage
fi

SCRIPTS_HOME="$S4M_HOME/dataset_scripts"
DATADIR="$1"
NORMDIR="$DATADIR/source/normalized"
mkdir -p "$NORMDIR"
QC_LOG="$NORMDIR/qc.log"
CHIP_TYPE="$2"
PROBE_AFFINITY_OR_AROMA_DIR="$3"
FORCE_QC=""

until [ -z "$1" ]; 
do
  if [ "$1" = "-R" ]; then
     shift
     export S4M_R="$1"
     break
  fi
  if [ "$1" = "-APT" ]; then
     shift
     export S4M_APT="$1"
     break
  fi
  shift
done

## Determine R binary if S4M exported a user override
R_BIN="R"
if [ ! -z "$S4M_R" ]; then
    R_BIN="$S4M_R"
    echo "Using 'R': $S4M_R"
fi

## Set platform information so we can redirect to the correct normalization pipeline for a given platform / chip type.
PLATFORM=""
PROBE_AFFINITY_PLATFORM=""
PLATFORM_CDF=""
PLATFORM_CDF_CUSTOM=""

### ILLUMINA ###

grep -P "^$CHIP_TYPE\t\w+\tIllumina" "$S4M_HOME/etc/assay_platforms.txt" > /dev/null 2>&1
## If Illumina chip type, we don't need to set any platform affinity CDF names.
if [ $? -eq 0 ]; then
  PLATFORM="Illumina"

### AGILENT ###
else
  grep -P "^$CHIP_TYPE\t\w+\tAgilent" "$S4M_HOME/etc/assay_platforms.txt" > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    PLATFORM="Agilent"
  fi
fi

### AFFYMETRIX ###

grep -P "^$CHIP_TYPE\t\w+\tAffymetrix" "$S4M_HOME/etc/assay_platforms.txt" > /dev/null 2>&1
if [ $? -eq 0 ]; then
  ## Determine if we're dealing with a GeneChip or ST array or plate-like array (PrimeView, HG-U219).
  ##
  ## Probe affinity names from: http://www.bioconductor.org/packages/2.7/BiocViews.html#___AffymetrixChip
  ## NOTE: Affinity names must match "clean" CDF names as per simpleAffy spec (refer 'simpleAffy.pdf' vignette)
  ## Easy way to check: Look for appropriately named "<platform>cdf.qcdef" files in R lib/simpleaffy/extdata

  PROBE_AFFINITY_PLATFORM=`grep -P "^$CHIP_TYPE\t\w+\tAffymetrix" "$S4M_HOME/etc/assay_platforms.txt" | cut -f 7`
  ARRAY_MODEL=`grep -P "^$CHIP_TYPE\t\w+\tAffymetrix" "$S4M_HOME/etc/assay_platforms.txt" | cut -f 4`
  echo "$ARRAY_MODEL" | grep -i -P "\-ST$" > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    PLATFORM="Affymetrix_ST"
    PLATFORM_CDF=`grep -P "^$CHIP_TYPE\t\w+\tAffymetrix" "$S4M_HOME/etc/assay_platforms.txt" | cut -f 6`
  else
    # PrimeView is functionally a plate but philosophically a genechip
    echo "$ARRAY_MODEL" | grep -i -P "PrimeView|HG-U219" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
      PLATFORM="Affymetrix_Plate"
      PLATFORM_CDF=`grep -P "^$CHIP_TYPE\t\w+\tAffymetrix\tPrimeView|^$CHIP_TYPE\t\w+\tAffymetrix\tHG-U219" "$S4M_HOME/etc/assay_platforms.txt" | cut -f 6`
    else
      ## Check for Clariom S - this is just a regular PM-only plate array but
      ## it requires a custom R/oligo script so we currently treat it like it's
      ## a new platform
      ## NOTE: Not tested with Clariom S "HT" variants
      echo "$ARRAY_MODEL" | grep -i -P "Clariom S" > /dev/null 2>&1
      if [ $? -eq 0 ]; then
        PLATFORM="Affymetrix_Clariom"
      else
        ## NOTE: U133AAofAV2 is a GeneChip and should fall here. It is however treated specially
        ##   when required APT flags are given to route through a modified APT pipeline
        ##   (CDF only vs. PGF,CLF,MPS input chip files combo).
        PLATFORM="Affymetrix_GeneChip"
      fi
    fi
  fi
fi

if [ -z "$PLATFORM" ]; then
   ## Unknown platform, check for QC "force" flag
   forceqc=`get_s4m_option force`
   if [ ! -z $forceqc ]; then
     echo "DEBUG: Applying S4M option [force=TRUE]" 1>&2
     sleep 2
     FORCE_QC="TRUE"
   else
     echo "Error: Unknown chip type '$CHIP_TYPE' (is it in etc/assay_platforms.txt?)!
" 1>&2
     exit 1
   fi
fi

if [ -z "$PROBE_AFFINITY_PLATFORM" -a "$PLATFORM" = "Affymetrix_GeneChip" -a "$PLATFORM_CDF" != "U133AAofAV2" ]; then
   echo "Error: Affymetrix GeneChip chip type '$CHIP_TYPE' is not currently supported!
" 1>&2
   exit 1
fi


## Check for existence of sample pheno file for better QC plot labelling / group info
## Will exit here if user selects that option.
check_sample_pheno_exists


## Call targeted normalization script based on platform

### AFFYMETRIX - GENECHIP ###

if [ "$PLATFORM" = "Affymetrix_GeneChip" ]; then

   ## QC and exit if qconly flag was set
   check_do_dataset_qc_only

   ## If Affymetrix Power Tools is specified, use it in preference to simpleaffy/R 
   ##
   ## NOTE: Only for U133AAofAV2!
   ##
   ## We don't want to accidentally run a GeneChip dataset through the APT pipeline
   ## again and publish bad data.
   ##
   ## So we're ENFORCING this policy strictly.
   ##
   if [ ! -z "$S4M_APT" ]; then
      echo "$ARRAY_MODEL" | grep U133AAofAV2 > /dev/null 2>&1
      if [ $? -ne 0 ]; then
        echo "Error: Only the 'U133AAofAV2' chip is allowed through the APT pipeline." 1>&2
        echo "  All other Affy GeneChip datasets must use the standard GeneChip pipeline." 1>&2
        echo 1>&2
        exit 1
      fi
      cd "$SCRIPTS_HOME/normalization" && ./normalize_affy_genechip_APT.sh "$DATADIR/source/raw" "$DATADIR/source/normalized" "$PROBE_AFFINITY_OR_AROMA_DIR"
   else
      cd "$SCRIPTS_HOME/normalization" && ./normalize_affy_genechip.sh "$DATADIR/source/raw" "$DATADIR/source/normalized" "$PROBE_AFFINITY_OR_AROMA_DIR" "$PROBE_AFFINITY_PLATFORM"
   fi
   if [ $? -eq 0 ]; then
     echo
     ## Generic microarray QC plots
     do_dataset_qc_plots
     exit $?
   else
     exit 1
   fi


### AFFYMETRIX - CLARIOM ('S') ###

elif [ "$PLATFORM" = "Affymetrix_Clariom" ]; then

   ## QC and exit if qconly flag was set
   check_do_dataset_qc_only

   ## NOTE: Clariom R/oligo script not yet integrated! Just QC only.

   ## See Task #2762



### AFFYMETRIX - PLATE ###

# for array plate (eg HG-U219) and plate-like arrays (eg PrimeView)
elif [ "$PLATFORM" = "Affymetrix_Plate" ]; then
   
   ## QC and exit if qconly flag was set
   check_do_dataset_qc_only

   ## Need to use rma because gcrma breaks (no mismatch probes!)
   ## Support for Affymetrix Power Tools not added at this stage 
   cd "$SCRIPTS_HOME/normalization" && ./normalize_affy_rma.sh "$DATADIR/source/raw" "$DATADIR/source/normalized"

   ## We know the background control probe identities for PrimeView
   ## Adding a layer of control flow to simplify addition of future cases
   ## If we encounter more cases of platforms needing automated detection thresholding:
   ##   1) add "|" separated $PLATFORM_CDF to $automated_threshold_compatible
   ##   2) and pass -P flag to grep on the line following that
   ##   3) background control probes are hardcoded, changes may be necessary for new chip
   automated_threshold_compatible="PrimeView"
   echo "$PLATFORM_CDF" | grep -q "$automated_threshold_compatible"
   if [ $? -eq 0 ]; then
     echo "Starting automated detection thresholding..."
     cd "$SCRIPTS_HOME/normalization" && \
     ./threshold_plate_custom_ncprobes.sh "$DATADIR/source/normalized"
   else
     echo "Skipping automated detection thresholding..."
   fi

   if [ $? -eq 0 ]; then
     echo
     ## Generic microarray QC plots
     do_dataset_qc_plots
     exit $?
   else
     exit 1
   fi

   exit $?


### AFFYMETRIX - ST ###

elif [ "$PLATFORM" = "Affymetrix_ST" ]; then
   extraflags=""
   if [ ! -z "$S4M_SET" ]; then
     ## Process Affy ST options
     nonorm=`get_s4m_option "nonorm"`
     if [ ! -z "$nonorm" ]; then
       echo "DEBUG: Applying S4M option [nonorm=TRUE]" 1>&2
       sleep 2
       extraflags="-nonorm"
     fi
   fi

   ## QC and exit if qconly flag was set
   check_do_dataset_qc_only

   ## If we didn't exit early, do normalization and QC
   ## Require use of Affymetrix Power Tools for Affy ST array processing
   cd "$SCRIPTS_HOME/normalization" && ./normalize_affy_st_APT.sh "$DATADIR/source/raw" "$DATADIR/source/normalized" "$PROBE_AFFINITY_OR_AROMA_DIR" "$extraflags"
   if [ $? -eq 0 ]; then
     echo
     ## Generic microarray QC plots
     do_dataset_qc_plots
     exit $?
   else
     exit 1
   fi
   ## Legacy code for R/Bioconductor 'Aroma' normalization for ST arrays, in case we ever want to re-enable this.
   ## NOTE: NOT CURRENTLY USED!
   #
   #      if [ -z "$PLATFORM_CDF_CUSTOM" ]; then
   #         cd "$SCRIPTS_HOME/normalization" && ./normalize_affy_st.sh "$DATADIR/source/raw" "$DATADIR/source/normalized" "$PROBE_AFFINITY_OR_AROMA_DIR" "$PLATFORM_CDF"
   #      else
   #         cd "$SCRIPTS_HOME/normalization" && ./normalize_affy_st.sh "$DATADIR/source/raw" "$DATADIR/source/normalized" "$PROBE_AFFINITY_OR_AROMA_DIR" "$PLATFORM_CDF" "$PLATFORM_CDF_CUSTOM"
   #      fi


### ILLUMINA ###

elif [ "$PLATFORM" = "Illumina" ]; then
   if [ ! -f "$DATADIR/source/raw/beadstudio.txt" ]; then
     echo "Error: Expecting 'raw' expression input file: $DATADIR/source/raw/beadstudio.txt
Please copy or link the desired raw Illumina Beadstudio output file accordingly and re-run this script.
" 1>&2
     exit 1
   fi

   ## Detection flip = FALSE by default
   extraflags="FALSE"

   ## Process lumi options
   if [ ! -z "$S4M_SET" ]; then
     ## OPTION: Detection flipping
     detflip=`get_s4m_option "detectionflip"`
     if [ ! -z "$detflip" ]; then
       echo "DEBUG: Applying S4M option [detectionflip=TRUE]" 1>&2
       extraflags="TRUE"
     fi
     ## OPTION: No background correction
     nobg=`get_s4m_option "nobg"`
     if [ ! -z "$nobg" ]; then
       echo "DEBUG: Applying S4M option [bgcorrect=FALSE]" 1>&2
       extraflags="$extraflags FALSE"
     else
       extraflags="$extraflags TRUE"
     fi
     ## OPTION: Skip Beadstudio file format check
     force=`get_s4m_option "force"`
     if [ ! -z "$force" ]; then
       echo "DEBUG: Applying S4M option [force=TRUE]" 1>&2
       extraflags="$extraflags TRUE"
     else
       extraflags="$extraflags FALSE"
     fi
     sleep 2
   fi

   ## QC and exit if qconly flag was set
   check_do_dataset_qc_only


   ### DEBUG ###
   #echo "cd $SCRIPTS_HOME/normalization && ./normalize_illumina.sh $DATADIR $DATADIR/source/raw/beadstudio.txt $DATADIR/source/normalized $extraflags"
   #sleep 5
   ### END ###

   ## If we didn't exit early, do normalization and QC
   cd "$SCRIPTS_HOME/normalization" && ./normalize_illumina.sh "$DATADIR" "$DATADIR/source/raw/beadstudio.txt" "$DATADIR/source/normalized" $extraflags
   if [ $? -eq 0 ]; then
     echo
     ## Generic microarray QC plots
     do_dataset_qc_plots
     exit $?
   else
     exit 1
   fi


### AGILENT ###

elif [ "$PLATFORM" = "Agilent" ]; then
   extraflags=""
   if [ ! -z "$S4M_SET" ]; then
     ## Process Agilent options
     nobg=`get_s4m_option "nobg"`
     if [ ! -z "$nobg" ]; then
       echo "DEBUG: Applying S4M option [bgcorrect=FALSE]" 1>&2
       sleep 2
       extraflags="-nobg=TRUE"
     fi
   fi

   ## QC and exit if qconly flag was set
   check_do_dataset_qc_only

   ## If we didn't exit early, do normalization and QC
   cd "$SCRIPTS_HOME/normalization" && ./normalize_agilent.sh "$DATADIR/source/raw" "$DATADIR/source/normalized" "$extraflags"
   if [ $? -eq 0 ]; then
     echo
     ## Generic microarray QC plots
     do_dataset_qc_plots
     exit $?
   else
     exit 1
   fi

elif [ "$FORCE_QC" = "TRUE" ]; then
   ## QC and exit if qconly flag was set
   check_do_dataset_qc_only

else
   echo "Sorry, the given platform '$PLATFORM' is currently unsupported!
" 1>&2
   exit 1
fi

