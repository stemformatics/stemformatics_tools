#!/bin/sh
## Script: 	cumulative_to_ensembl.sh
##
## Description:	Convert a probe ID row-encoded YuGene (Cumulative) file to
##		one coded by Ensembl Gene ID.
##
##		For genes that have multiple probes, we select the probe with
##		the highest mean expression across all samples and use its
##		expression values.
##
## Author: 	O.Korn
## ==========================================================================

## Path to R script which does the conversion of probe ID -> Ensembl ID
rscript="$1"
## The input probe ID-based YuGene file
input_cumulative="$2"
## The output Ensembl ID encoded YuGene file
output_cumulative="$3"
## The probe ID -> Ensembl ID map (TSV) file
probe_map="$4"
## Optional path to R if not using system-wide version
R_path="$5"

if [ ! -z "$R_path" ]; then
  S4M_R="$R_path"
else
  S4M_R="R"
fi

$S4M_R --slave -f "$rscript" --args yugenefile="$input_cumulative" outputfile="$output_cumulative" mapfile="$probe_map" > "$output_cumulative.log" 2>&1
exit $?
