#!/bin/sh
## Script:      ensembl_yugene_to_rohart.sh
##
## Description: Create a "Rohart" score file from an input Ensembl ID based 
##		YuGene (Cumulative) file.
##
##		Loads model parameters (gene signature etc) from a given model
##		.Rdata file.
##
## Author:      O.Korn
## ==========================================================================

## Path to R script which calls the Rohart bootstrapping code
rscript="$1"
## The input Ensembl ID-based YuGene file
yugenefile="$2"
## The output Rohart score file
outputfile="$3"
## The input model Rdata file
modelrdat="$4"
## The project label for the gene signature project e.g. "MSC"
projectlabel="$5"
## The upper Rohart score
upper="$6"
## The lower Rohart score
lower="$7"
## Number of times to subsample
subsamples="$8"
## Optional path to R installation which has the "bootsPLS" / Rohart library
R_path="$9"

if [ ! -z "$R_path" ]; then
  S4M_R="$R_path"
else
  S4M_R="R"
fi

$S4M_R --slave -f "$rscript" --args modelrdatfile="$modelrdat" yugenefile="$yugenefile" outputfile="$outputfile" project="$projectlabel" upper="$upper" lower="$lower" subsamples="$subsamples"  > "$outputfile.log" 2>&1
exit $?
