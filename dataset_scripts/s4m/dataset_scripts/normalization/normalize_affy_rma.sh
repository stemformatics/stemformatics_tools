#!/bin/sh

Usage () {
  echo "$0 <raw_data_path> <output_path> [--detection-only]"; echo
  exit
}

DATAPATH="$1"
OUTPATH="$2"

R_BIN="R"
if [ ! -z "$S4M_R" ]; then
    R_BIN="$S4M_R"
fi
echo "Using 'R': $R_BIN"
sleep 2

if [ -z "$DATAPATH" -o -z "$OUTPATH" ]; then
  Usage
fi

if [ ! -f "$DATAPATH/ACCESSION" ]; then
  if [ ! -f "$DATAPATH/../../ACCESSION" ]; then
    echo "Error: Required accession file '$DATAPATH/ACCESSION' or '$DATAPATH/../../ACCESSION' not found!"
    echo "Please create it manually or use the 's4m' tool to create it for the given expression dataset."
    exit 1
  else
    ACCESSION=`cat "$DATAPATH/../../ACCESSION" | tr -d [:cntrl:]`
  fi
else
  ACCESSION=`cat "$DATAPATH/ACCESSION" | tr -d [:cntrl:]`
fi

if [ ! -d "$OUTPATH" ]; then
  mkdir -p "$OUTPATH"
fi

sleep 1

echo "Starting normalization.."; echo
  $R_BIN -f ./R/normalize_affy_rma.R --args -input="$DATAPATH" -output="$OUTPATH"
  if [ $? -ne 0 ]; then
    echo "Fatal Error: Normalization failed."; echo
    exit 1
  fi

## Unpack raw and RMA 
if [ -f "$OUTPATH/raw_expression.txt" -a -f "$OUTPATH/normalized_expression.txt" ]; then
  echo; echo "Normalization complete."
  echo "Unpacking RMA expression values.."
  # Unpack the expression data table to a format that can be used to build Stemformatics expression files
  # Required to plot
  (cd util && ./unpack_expression_table.sh "$OUTPATH/raw_expression.txt" "$OUTPATH/raw_expression_unpacked.txt")
  (cd util && ./unpack_expression_table.sh "$OUTPATH/normalized_expression.txt" "$OUTPATH/normalized_expression_unpacked.txt")
  if [ $? -ne 0 ]; then
    echo "Fatal Error: Failed to unpack expression values."
    exit 1
  else
    echo "done"
  fi
fi

## Simulate detection scores (all TRUE) as MAS5 was not performed
## these are not actually used at any point and will have no effect on the result
sed -r -e "s|(^.*\t.*\t.*$)|\1\tTRUE|" "$OUTPATH/normalized_expression_unpacked.txt" > "$OUTPATH/normalized_expression_detection.txt"
