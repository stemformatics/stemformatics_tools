#!/bin/sh

Usage () {
  echo "$0 <expression_table_file> <output_shifted_expression_table_file>"; echo
  exit
}

INFILE="$1"
OUTFILE="$2"

R_BIN="R"
if [ ! -z "$S4M_R" ]; then
    R_BIN="$S4M_R"
fi
echo "Using 'R': $R_BIN"
sleep 2

if [ -z "$INFILE" -o -z "$OUTFILE" ]; then
  Usage
fi

echo "Generating shifted probe expressions table.."; echo
sleep 2
cat ./R/expression_shift.R | $R_BIN --no-save -input="$INFILE" -output="$OUTFILE"

if [ -s "$OUTFILE" ]; then
  echo; echo "Expression shift complete."
else
  echo; echo "Error: Failed to write shifted expression table!"
fi
