#!/bin/sh

Usage () {
  echo "$0 <raw_data_path> <output_path> <aroma_working_dir> <platform_cdf_name> [<optional_custom_cdf_tag>]"; echo
  exit
}

INPATH="$1"
OUTPATH="$2"
WORKDIR="$3"
CDFNAME="$4"
CDFTAG="$5"

R_BIN="R"
if [ ! -z "$S4M_R" ]; then
    R_BIN="$S4M_R"
    echo "Using 'R': $S4M_R"
fi

if [ -z "$INPATH" -o -z "$OUTPATH" -o -z "$WORKDIR" -o -z "$CDFNAME" ]; then
  Usage
fi

if [ ! -f "$INPATH/ACCESSION" ]; then
  if [ ! -f "$INPATH/../../ACCESSION" ]; then
    echo "Error: Required accession file '$INPATH/ACCESSION' or '$INPATH/../../ACCESSION' not found!"
    echo "Please create it manually or use the 's4m' tool to create it for the given expression dataset."
    exit 1
  else
    ACCESSION=`cat "$INPATH/../../ACCESSION" | tr -d [:cntrl:]`
  fi
else
  ACCESSION=`cat "$INPATH/ACCESSION" | tr -d [:cntrl:]`
fi

if [ ! -d "$OUTPATH" ]; then
  mkdir -p "$OUTPATH"
fi

## NOTE: "RMA" normalization is enforced for now in this pipeline until we can determine a way of choosing
##       GCRMA for those ST arrays that have negative control ("MM") probesets.
##       It may come down to "hard coding" for those ones we know about e.g. MoGene-1_0-st-v1
##       (We could decide this at an earlier stage and pass in as an argument to this script)
echo "Starting normalization and QC output.."; echo
sleep 1
if [ ! -z "$CDFTAG" ]; then
  ## DEBUG ##
  #echo "DEBUG: CMD=[ cat ./R/normalize_affy_st_rma.R | $R_BIN --no-save -input=\"$INPATH\" -output=\"$OUTPATH\" -workdir=\"$WORKDIR\" -cdfname=\"$CDFNAME\" -cdftag=\"$CDFTAG\" ]"
  #echo; echo "ENTER to continue.."
  #read
  ## END DEBUG ##
  cat ./R/normalize_affy_st_rma.R | $R_BIN --no-save -input="$INPATH" -output="$OUTPATH" -workdir="$WORKDIR" -cdfname="$CDFNAME" -cdftag="$CDFTAG"
else
  cat ./R/normalize_affy_st_rma.R | $R_BIN --no-save -input="$INPATH" -output="$OUTPATH" -workdir="$WORKDIR" -cdfname="$CDFNAME"
fi


## Unpack raw and normalized
if [ -f "$OUTPATH/raw_expression.txt" -a -f "$OUTPATH/normalized_expression.txt" ]; then
  echo; echo "Normalization complete."
  echo "Unpacking normalized expression values.."
  # Unpack the expression data table to a format that can be used to build Stemformatics expression files
  # Required to plot
  (cd util && ./unpack_expression_table.sh "$OUTPATH/raw_expression.txt" "$OUTPATH/raw_expression_unpacked.txt")
  (cd util && ./unpack_expression_table.sh "$OUTPATH/normalized_expression.txt" "$OUTPATH/normalized_expression_unpacked.txt")
  if [ $? -ne 0 ]; then
    echo "Fatal Error: Failed to unpack expression values."
    exit 1
  else
    echo "done"
  fi
fi

## QC plots
if [ -f "$OUTPATH/raw_expression_unpacked.txt" ]; then
  echo "Plotting raw expression density.."
  mkdir -p "$OUTPATH/qc_raw"
  ./plot_expression_density.sh "$OUTPATH/raw_expression_unpacked.txt" "$OUTPATH/qc_raw" "$ACCESSION"
  if [ $? -ne 0 ]; then
    echo "Error: Failed to produce raw probe expression density plot."
  else
    echo "done"
  fi
fi
if [ -f "$OUTPATH/normalized_expression_unpacked.txt" ]; then
  echo "Plotting normalized expression density.."
  mkdir -p "$OUTPATH/qc_normalized"
  ./plot_expression_density.sh "$OUTPATH/normalized_expression_unpacked.txt" "$OUTPATH/qc_normalized" "$ACCESSION"
  if [ $? -ne 0 ]; then
    echo "Fatal Error: Failed to produce normalized probe expression density plot."
    exit 1
  fi
  echo "done"
fi


echo; echo "All Done."
