#!/bin/sh

u133aaofav2_probe_filter_regex () {
  echo "
^(AFFX-At-Cab_at|AFFX-At-Ltp4_at|AFFX-At-Ltp6_at|AFFX-At-NAC1_at|AFFX-At-PRKase_at|AFFX-At-Ra_at|AFFX-At-rbcL_at|AFFX-At-RCP1_at|AFFX-At-TIM_at|AFFX-At-XCP2_at|AFFX-BioBcopy10-3_at|AFFX-BioBcopy10-5_at|AFFX-BioBcopy10-M_at|AFFX-BioBcopy11-3_at|AFFX-BioBcopy11-5_at|AFFX-BioBcopy11-M_at|AFFX-BioBcopy12-3_at|AFFX-BioBcopy12-5_at|AFFX-BioBcopy12-M_at|AFFX-BioBcopy13-3_at|AFFX-BioBcopy13-5_at|AFFX-BioBcopy1-3_at|AFFX-BioBcopy13-M_at|AFFX-BioBcopy14-3_at|AFFX-BioBcopy14-5_at|AFFX-BioBcopy14-M_at|AFFX-BioBcopy15-3_at|AFFX-BioBcopy15-5_at|AFFX-BioBcopy1-5_at|AFFX-BioBcopy15-M_at|AFFX-BioBcopy16-3_at|AFFX-BioBcopy16-5_at|AFFX-BioBcopy16-M_at|AFFX-BioBcopy17-3_at|AFFX-BioBcopy17-5_at|AFFX-BioBcopy17-M_at|AFFX-BioBcopy18-3_at|AFFX-BioBcopy18-5_at|AFFX-BioBcopy18-M_at|AFFX-BioBcopy19-3_at|AFFX-BioBcopy19-5_at|AFFX-BioBcopy19-M_at|AFFX-BioBcopy1-M_at|AFFX-BioBcopy20-3_at|AFFX-BioBcopy20-5_at|AFFX-BioBcopy20-M_at|AFFX-BioBcopy21-3_at|AFFX-BioBcopy21-5_at|AFFX-BioBcopy21-M_at|AFFX-BioBcopy22-3_at|AFFX-BioBcopy22-5_at|AFFX-BioBcopy22-M_at|AFFX-BioBcopy23-3_at|AFFX-BioBcopy23-5_at|AFFX-BioBcopy2-3_at|AFFX-BioBcopy23-M_at|AFFX-BioBcopy24-3_at|AFFX-BioBcopy24-5_at|AFFX-BioBcopy24-M_at|AFFX-BioBcopy25-3_at|AFFX-BioBcopy25-5_at|AFFX-BioBcopy2-5_at|AFFX-BioBcopy25-M_at|AFFX-BioBcopy26-3_at|AFFX-BioBcopy26-5_at|AFFX-BioBcopy26-M_at|AFFX-BioBcopy27-3_at|AFFX-BioBcopy27-5_at|AFFX-BioBcopy27-M_at|AFFX-BioBcopy28-3_at|AFFX-BioBcopy28-5_at|AFFX-BioBcopy28-M_at|AFFX-BioBcopy29-3_at|AFFX-BioBcopy29-5_at|AFFX-BioBcopy29-M_at|AFFX-BioBcopy2-M_at|AFFX-BioBcopy30-3_at|AFFX-BioBcopy30-5_at|AFFX-BioBcopy30-M_at|AFFX-BioBcopy31-3_at|AFFX-BioBcopy31-5_at|AFFX-BioBcopy31-M_at|AFFX-BioBcopy32-3_at|AFFX-BioBcopy32-5_at|AFFX-BioBcopy32-M_at|AFFX-BioBcopy33-3_at|AFFX-BioBcopy33-5_at|AFFX-BioBcopy3-3_at|AFFX-BioBcopy33-M_at|AFFX-BioBcopy34-3_at|AFFX-BioBcopy34-5_at|AFFX-BioBcopy34-M_at|AFFX-BioBcopy35-3_at|AFFX-BioBcopy35-5_at|AFFX-BioBcopy3-5_at|AFFX-BioBcopy35-M_at|AFFX-BioBcopy36-3_at|AFFX-BioBcopy36-5_at|AFFX-BioBcopy36-M_at|AFFX-BioBcopy37-3_at|AFFX-BioBcopy37-5_at|AFFX-BioBcopy37-M_at|AFFX-BioBcopy38-3_at|AFFX-BioBcopy38-5_at|AFFX-BioBcopy38-M_at|AFFX-BioBcopy39-3_at|AFFX-BioBcopy39-5_at|AFFX-BioBcopy39-M_at|AFFX-BioBcopy3-M_at|AFFX-BioBcopy40-3_at|AFFX-BioBcopy40-5_at|AFFX-BioBcopy40-M_at|AFFX-BioBcopy41-3_at|AFFX-BioBcopy41-5_at|AFFX-BioBcopy41-M_at|AFFX-BioBcopy42-3_at|AFFX-BioBcopy42-5_at|AFFX-BioBcopy42-M_at|AFFX-BioBcopy43-3_at|AFFX-BioBcopy43-5_at|AFFX-BioBcopy4-3_at|AFFX-BioBcopy43-M_at|AFFX-BioBcopy44-3_at|AFFX-BioBcopy44-5_at|AFFX-BioBcopy44-M_at|AFFX-BioBcopy45-3_at|AFFX-BioBcopy45-5_at|AFFX-BioBcopy4-5_at|AFFX-BioBcopy45-M_at|AFFX-BioBcopy46-3_at|AFFX-BioBcopy46-5_at|AFFX-BioBcopy46-M_at|AFFX-BioBcopy47-3_at|AFFX-BioBcopy47-5_at|AFFX-BioBcopy47-M_at|AFFX-BioBcopy48-3_at|AFFX-BioBcopy48-5_at|AFFX-BioBcopy48-M_at|AFFX-BioBcopy49-3_at|AFFX-BioBcopy49-5_at|AFFX-BioBcopy49-M_at|AFFX-BioBcopy4-M_at|AFFX-BioBcopy5-3_at|AFFX-BioBcopy5-5_at|AFFX-BioBcopy5-M_at|AFFX-BioBcopy6-3_at|AFFX-BioBcopy6-5_at|AFFX-BioBcopy6-M_at|AFFX-BioBcopy7-3_at|AFFX-BioBcopy7-5_at|AFFX-BioBcopy7-M_at|AFFX-BioBcopy8-3_at|AFFX-BioBcopy8-5_at|AFFX-BioBcopy8-M_at|AFFX-BioBcopy9-3_at|AFFX-BioBcopy9-5_at|AFFX-BioBcopy9-M_at|AFFX-BioCcopy10-3_at|AFFX-BioCcopy10-5_at|AFFX-BioCcopy11-3_at|AFFX-BioCcopy11-5_at|AFFX-BioCcopy12-3_at|AFFX-BioCcopy12-5_at|AFFX-BioCcopy13-3_at|AFFX-BioCcopy13-5_at|AFFX-BioCcopy1-3_at|AFFX-BioCcopy14-3_at|AFFX-BioCcopy14-5_at|AFFX-BioCcopy15-3_at|AFFX-BioCcopy15-5_at|AFFX-BioCcopy1-5_at|AFFX-BioCcopy16-3_at|AFFX-BioCcopy16-5_at|AFFX-BioCcopy17-3_at|AFFX-BioCcopy17-5_at|AFFX-BioCcopy18-3_at|AFFX-BioCcopy18-5_at|AFFX-BioCcopy19-3_at|AFFX-BioCcopy19-5_at|AFFX-BioCcopy20-3_at|AFFX-BioCcopy20-5_at|AFFX-BioCcopy21-3_at|AFFX-BioCcopy21-5_at|AFFX-BioCcopy22-3_at|AFFX-BioCcopy22-5_at|AFFX-BioCcopy23-3_at|AFFX-BioCcopy23-5_at|AFFX-BioCcopy2-3_at|AFFX-BioCcopy24-3_at|AFFX-BioCcopy24-5_at|AFFX-BioCcopy25-3_at|AFFX-BioCcopy25-5_at|AFFX-BioCcopy2-5_at|AFFX-BioCcopy26-3_at|AFFX-BioCcopy26-5_at|AFFX-BioCcopy27-3_at|AFFX-BioCcopy27-5_at|AFFX-BioCcopy28-3_at|AFFX-BioCcopy28-5_at|AFFX-BioCcopy29-3_at|AFFX-BioCcopy29-5_at|AFFX-BioCcopy30-3_at|AFFX-BioCcopy30-5_at|AFFX-BioCcopy31-3_at|AFFX-BioCcopy31-5_at|AFFX-BioCcopy32-3_at|AFFX-BioCcopy32-5_at|AFFX-BioCcopy33-3_at|AFFX-BioCcopy33-5_at|AFFX-BioCcopy3-3_at|AFFX-BioCcopy34-3_at|AFFX-BioCcopy34-5_at|AFFX-BioCcopy35-3_at|AFFX-BioCcopy35-5_at|AFFX-BioCcopy3-5_at|AFFX-BioCcopy36-3_at|AFFX-BioCcopy36-5_at|AFFX-BioCcopy37-3_at|AFFX-BioCcopy37-5_at|AFFX-BioCcopy38-3_at|AFFX-BioCcopy38-5_at|AFFX-BioCcopy39-3_at|AFFX-BioCcopy39-5_at|AFFX-BioCcopy40-3_at|AFFX-BioCcopy40-5_at|AFFX-BioCcopy41-3_at|AFFX-BioCcopy41-5_at|AFFX-BioCcopy42-3_at|AFFX-BioCcopy42-5_at|AFFX-BioCcopy43-3_at|AFFX-BioCcopy43-5_at|AFFX-BioCcopy4-3_at|AFFX-BioCcopy44-3_at|AFFX-BioCcopy44-5_at|AFFX-BioCcopy45-3_at|AFFX-BioCcopy45-5_at|AFFX-BioCcopy4-5_at|AFFX-BioCcopy46-3_at|AFFX-BioCcopy46-5_at|AFFX-BioCcopy47-3_at|AFFX-BioCcopy47-5_at|AFFX-BioCcopy48-3_at|AFFX-BioCcopy48-5_at|AFFX-BioCcopy49-3_at|AFFX-BioCcopy49-5_at|AFFX-BioCcopy5-3_at|AFFX-BioCcopy5-5_at|AFFX-BioCcopy6-3_at|AFFX-BioCcopy6-5_at|AFFX-BioCcopy7-3_at|AFFX-BioCcopy7-5_at|AFFX-BioCcopy8-3_at|AFFX-BioCcopy8-5_at|AFFX-BioCcopy9-3_at|AFFX-BioCcopy9-5_at|AFFX-BioDncopy10-3_at|AFFX-BioDncopy10-5_at|AFFX-BioDncopy11-3_at|AFFX-BioDncopy11-5_at|AFFX-BioDncopy12-3_at|AFFX-BioDncopy12-5_at|AFFX-BioDncopy13-3_at|AFFX-BioDncopy13-5_at|AFFX-BioDncopy1-3_at|AFFX-BioDncopy14-3_at|AFFX-BioDncopy14-5_at|AFFX-BioDncopy15-3_at|AFFX-BioDncopy15-5_at|AFFX-BioDncopy1-5_at|AFFX-BioDncopy16-3_at|AFFX-BioDncopy16-5_at|AFFX-BioDncopy17-3_at|AFFX-BioDncopy17-5_at|AFFX-BioDncopy18-3_at|AFFX-BioDncopy18-5_at|AFFX-BioDncopy19-3_at|AFFX-BioDncopy19-5_at|AFFX-BioDncopy20-3_at|AFFX-BioDncopy20-5_at|AFFX-BioDncopy21-3_at|AFFX-BioDncopy21-5_at|AFFX-BioDncopy22-3_at|AFFX-BioDncopy22-5_at|AFFX-BioDncopy23-3_at|AFFX-BioDncopy23-5_at|AFFX-BioDncopy2-3_at|AFFX-BioDncopy24-3_at|AFFX-BioDncopy24-5_at|AFFX-BioDncopy25-3_at|AFFX-BioDncopy25-5_at|AFFX-BioDncopy2-5_at|AFFX-BioDncopy26-3_at|AFFX-BioDncopy26-5_at|AFFX-BioDncopy27-3_at|AFFX-BioDncopy27-5_at|AFFX-BioDncopy28-3_at|AFFX-BioDncopy28-5_at|AFFX-BioDncopy29-3_at|AFFX-BioDncopy29-5_at|AFFX-BioDncopy30-3_at|AFFX-BioDncopy30-5_at|AFFX-BioDncopy31-3_at|AFFX-BioDncopy31-5_at|AFFX-BioDncopy32-3_at|AFFX-BioDncopy32-5_at|AFFX-BioDncopy33-3_at|AFFX-BioDncopy33-5_at|AFFX-BioDncopy3-3_at|AFFX-BioDncopy34-3_at|AFFX-BioDncopy34-5_at|AFFX-BioDncopy35-3_at|AFFX-BioDncopy35-5_at|AFFX-BioDncopy3-5_at|AFFX-BioDncopy36-3_at|AFFX-BioDncopy36-5_at|AFFX-BioDncopy37-3_at|AFFX-BioDncopy37-5_at|AFFX-BioDncopy38-3_at|AFFX-BioDncopy38-5_at|AFFX-BioDncopy39-3_at|AFFX-BioDncopy39-5_at|AFFX-BioDncopy40-3_at|AFFX-BioDncopy40-5_at|AFFX-BioDncopy41-3_at|AFFX-BioDncopy41-5_at|AFFX-BioDncopy42-3_at|AFFX-BioDncopy42-5_at|AFFX-BioDncopy43-3_at|AFFX-BioDncopy43-5_at|AFFX-BioDncopy4-3_at|AFFX-BioDncopy44-3_at|AFFX-BioDncopy44-5_at|AFFX-BioDncopy45-3_at|AFFX-BioDncopy45-5_at|AFFX-BioDncopy4-5_at|AFFX-BioDncopy46-3_at|AFFX-BioDncopy46-5_at|AFFX-BioDncopy47-3_at|AFFX-BioDncopy47-5_at|AFFX-BioDncopy48-3_at|AFFX-BioDncopy48-5_at|AFFX-BioDncopy49-3_at|AFFX-BioDncopy49-5_at|AFFX-BioDncopy5-3_at|AFFX-BioDncopy5-5_at|AFFX-BioDncopy6-3_at|AFFX-BioDncopy6-5_at|AFFX-BioDncopy7-3_at|AFFX-BioDncopy7-5_at|AFFX-BioDncopy8-3_at|AFFX-BioDncopy8-5_at|AFFX-BioDncopy9-3_at|AFFX-BioDncopy9-5_at|AFFX-CreXcopy10-3_at|AFFX-CreXcopy10-5_at|AFFX-CreXcopy11-3_at|AFFX-CreXcopy11-5_at|AFFX-CreXcopy12-3_at|AFFX-CreXcopy12-5_at|AFFX-CreXcopy13-3_at|AFFX-CreXcopy13-5_at|AFFX-CreXcopy1-3_at|AFFX-CreXcopy14-3_at|AFFX-CreXcopy14-5_at|AFFX-CreXcopy15-3_at|AFFX-CreXcopy15-5_at|AFFX-CreXcopy1-5_at|AFFX-CreXcopy16-3_at|AFFX-CreXcopy16-5_at|AFFX-CreXcopy17-3_at|AFFX-CreXcopy17-5_at|AFFX-CreXcopy18-3_at|AFFX-CreXcopy18-5_at|AFFX-CreXcopy19-3_at|AFFX-CreXcopy19-5_at|AFFX-CreXcopy20-3_at|AFFX-CreXcopy20-5_at|AFFX-CreXcopy21-3_at|AFFX-CreXcopy21-5_at|AFFX-CreXcopy22-3_at|AFFX-CreXcopy22-5_at|AFFX-CreXcopy23-3_at|AFFX-CreXcopy23-5_at|AFFX-CreXcopy2-3_at|AFFX-CreXcopy24-3_at|AFFX-CreXcopy24-5_at|AFFX-CreXcopy25-3_at|AFFX-CreXcopy25-5_at|AFFX-CreXcopy2-5_at|AFFX-CreXcopy26-3_at|AFFX-CreXcopy26-5_at|AFFX-CreXcopy27-3_at|AFFX-CreXcopy27-5_at|AFFX-CreXcopy28-3_at|AFFX-CreXcopy28-5_at|AFFX-CreXcopy29-3_at|AFFX-CreXcopy29-5_at|AFFX-CreXcopy30-3_at|AFFX-CreXcopy30-5_at|AFFX-CreXcopy31-3_at|AFFX-CreXcopy31-5_at|AFFX-CreXcopy32-3_at|AFFX-CreXcopy32-5_at|AFFX-CreXcopy33-3_at|AFFX-CreXcopy33-5_at|AFFX-CreXcopy3-3_at|AFFX-CreXcopy34-3_at|AFFX-CreXcopy34-5_at|AFFX-CreXcopy35-3_at|AFFX-CreXcopy35-5_at|AFFX-CreXcopy3-5_at|AFFX-CreXcopy36-3_at|AFFX-CreXcopy36-5_at|AFFX-CreXcopy37-3_at|AFFX-CreXcopy37-5_at|AFFX-CreXcopy38-3_at|AFFX-CreXcopy38-5_at|AFFX-CreXcopy39-3_at|AFFX-CreXcopy39-5_at|AFFX-CreXcopy40-3_at|AFFX-CreXcopy40-5_at|AFFX-CreXcopy41-3_at|AFFX-CreXcopy41-5_at|AFFX-CreXcopy42-3_at|AFFX-CreXcopy42-5_at|AFFX-CreXcopy43-3_at|AFFX-CreXcopy43-5_at|AFFX-CreXcopy4-3_at|AFFX-CreXcopy44-3_at|AFFX-CreXcopy44-5_at|AFFX-CreXcopy45-3_at|AFFX-CreXcopy45-5_at|AFFX-CreXcopy4-5_at|AFFX-CreXcopy46-3_at|AFFX-CreXcopy46-5_at|AFFX-CreXcopy47-3_at|AFFX-CreXcopy47-5_at|AFFX-CreXcopy48-3_at|AFFX-CreXcopy48-5_at|AFFX-CreXcopy49-3_at|AFFX-CreXcopy49-5_at|AFFX-CreXcopy5-3_at|AFFX-CreXcopy5-5_at|AFFX-CreXcopy6-3_at|AFFX-CreXcopy6-5_at|AFFX-CreXcopy7-3_at|AFFX-CreXcopy7-5_at|AFFX-CreXcopy8-3_at|AFFX-CreXcopy8-5_at|AFFX-CreXcopy9-3_at|AFFX-CreXcopy9-5_at|AFFX-r2-TagA_at|AFFX-r2-TagA_x_at|AFFX-r2-TagB_at|AFFX-r2-TagB_x_at|AFFX-r2-TagC_at|AFFX-r2-TagC_x_at|AFFX-r2-TagD_at|AFFX-r2-TagD_x_at|AFFX-r2-TagE_at|AFFX-r2-TagE_x_at|AFFX-r2-TagF_at|AFFX-r2-TagF_x_at|AFFX-r2-TagG_at|AFFX-r2-TagG_x_at|AFFX-r2-TagH_at|AFFX-r2-TagH_x_at|AFFX-r2-TagIN-3_at|AFFX-r2-TagIN-3_x_at|AFFX-r2-TagIN-5_at|AFFX-r2-TagIN-5_x_at|AFFX-r2-TagIN-M_at|AFFX-r2-TagIN-M_x_at|AFFX-r2-TagJ-3_at|AFFX-r2-TagJ-3_x_at|AFFX-r2-TagJ-5_at|AFFX-r2-TagJ-5_x_at|AFFX-r2-TagO-3_at|AFFX-r2-TagO-3_x_at|AFFX-r2-TagO-5_at|AFFX-r2-TagO-5_x_at|AFFX-r2-TagQ-3_at|AFFX-r2-TagQ-3_x_at|AFFX-r2-TagQ-5_at|AFFX-r2-TagQ-5_x_at|HC-AB000449_at|HC-AB006533_at|HC-AB011100_at|HC-AB012853_at|HC-AB019036_at|HC-AB028639_at|HC-AF007551_at|HC-AF016582_at|HC-AF022385_at|HC-AF025794_at|HC-AF035315_at|HC-AF035812_at|HC-AF039029_at|HC-AF052432_at|HC-AF054177_at|HC-AF064093_at|HC-AF064094_at|HC-AF070552_at|HC-AF071202_at|HC-AF072836_at|HC-AF091090_at|HC-AF102544_at|HC-AJ001699_at|HC-AJ001810_at|HC-AJ006267_at|HC-AJ006288_at|HC-AL049321_at|HC-AL049450_at|HC-AL049987_at|HC-AL050261_at|HC-AL050373_at|HC-AL080063_at|HC-AL080116_at|HC-AL096739_at|HC-D13633_at|HC-D13645_at|HC-D63390_at|HC-D78335_at|HC-D84145_at|HC-D87120_at|HC-D87127_at|HC-L02547_at|HC-L07541_at|HC-L19161_at|HC-L34600_at|HC-L35546_at|HC-L36529_at|HC-L41887_at|HC-M37033_at|HC-M55150_at|HC-M73077_at|HC-M74091_x_at|HC-M86917_at|HC-U18543_at|HC-U18914_at|HC-U21551_at|HC-U28424_at|HC-U33017_at|HC-U52521_at|HC-U63809_at|HC-U92980_at|HC-U93868_at|HC-U95735_at|HC-U97198_at|HC-X06272_at|HC-X17576_at|HC-X51688_at|HC-X52142_at|HC-X57303_at|HC-X66360_at|HC-X66435_at|HC-X76388_at|HC-X76732_at|HC-X85133_at|HC-X86098_at|HC-Y09321_at|HC-Y11411_at|MC_AB006758_s_at|MC_AB008516_s_at|MC_AB019541_s_at|MC_AF004874_s_at|MC_AF100778_s_at|MC_AJ001418_s_at|MC_AJ002390_s_at|MC_AJ006278_s_at|MC_AJ010108_s_at|MC_D10837_s_at|MC_D37801_s_at|MC_D49730_s_at|MC_D86563_s_at|MC_D89677_s_at|MC_J02652_s_at|MC_L12030_s_at|MC_M21952_s_at|MC_M27960_s_at|MC_M31131_s_at|MC_M32490_s_at|MC_M34141_s_at|MC_M57647_s_at|MC_M73748_s_at|MC_M84487_s_at|MC_M94623_s_at|MC_M95604_s_at|MC_NM_008509_s_at|MC_NM_008885_s_at|MC_NM_009242_s_at|MC_NM_010133_at|MC_NM_010284_s_at|MC_NM_010430_s_at|MC_NM_010607_s_at|MC_NM_010736_s_at|MC_NM_010743_s_at|MC_NM_011074_s_at|MC_NM_011824_s_at|MC_NM_012043_s_at|MC_NM_019764_s_at|MC_NM_020006_s_at|MC_NM_022315_s_at|MC_NM_023063_s_at|MC_NM_023277_s_at|MC_NM_024217_s_at|MC_NM_026172_s_at|MC_NM_030565_s_at|MC_U13370_s_at|MC_U36384_s_at|MC_U77630_s_at|MC_U79523_s_at|MC_U88328_s_at|MC_X04367_s_at|MC_X57413_s_at|MC_X62940_s_at|MC_X65128_s_at|MC_X83577_s_at|MC_Y13090_s_at|RAE230_MC_AB006758_at|RAE230_MC_AB008516_at|RAE230_MC_AB019541_at|RAE230_MC_AF004874_at|RAE230_MC_AF100778_at|RAE230_MC_AJ001418_at|RAE230_MC_AJ002390_at|RAE230_MC_AJ006278_at|RAE230_MC_AJ010108_at|RAE230_MC_D10837_at|RAE230_MC_D37801_at|RAE230_MC_D49730_at|RAE230_MC_D86563_at|RAE230_MC_D89677_at|RAE230_MC_J02652_at|RAE230_MC_L12030_at|RAE230_MC_M21952_s_at|RAE230_MC_M27960_at|RAE230_MC_M31131_s_at|RAE230_MC_M32490_at|RAE230_MC_M34141_at|RAE230_MC_M57647_at|RAE230_MC_M73748_at|RAE230_MC_M84487_at|RAE230_MC_M94623_at|RAE230_MC_M95604_at|RAE230_MC_NM_008509_at|RAE230_MC_NM_008885_at|RAE230_MC_NM_009242_at|RAE230_MC_NM_010133_at|RAE230_MC_NM_010284_at|RAE230_MC_NM_010430_at|RAE230_MC_NM_010607_at|RAE230_MC_NM_010736_at|RAE230_MC_NM_010743_at|RAE230_MC_NM_011074_at|RAE230_MC_NM_011824_at|RAE230_MC_NM_012043_at|RAE230_MC_NM_019764_at|RAE230_MC_NM_020006_at|RAE230_MC_NM_022315_at|RAE230_MC_NM_023063_at|RAE230_MC_NM_023277_at|RAE230_MC_NM_024217_at|RAE230_MC_NM_026172_at|RAE230_MC_NM_030565_at|RAE230_MC_U13370_at|RAE230_MC_U36384_at|RAE230_MC_U77630_at|RAE230_MC_U79523_at|RAE230_MC_U88328_at|RAE230_MC_X04367_at|RAE230_MC_X57413_at|RAE230_MC_X62940_at|RAE230_MC_X65128_at|RAE230_MC_X83577_at|RAE230_MC_Y13090_at)\t"
}

Usage () {
  echo "$0 <raw_data_path> <output_path> <platform_annotation_data_path>"; echo
  exit
}

DATAPATH="$1"
OUTPATH="$2"
ANNOTATIONPATH="$3"

APT_NORM_BIN="apt-probeset-summarize"
APT_MAS5_BIN="apt-mas5"
APT_CHP_BIN="apt-chp-to-txt"
if [ ! -z "$S4M_APT" ]; then
    APT_NORM_BIN="$S4M_APT/$APT_NORM_BIN"
    APT_MAS5_BIN="$S4M_APT/$APT_MAS5_BIN"
    APT_CHP_BIN="$S4M_APT/$APT_CHP_BIN"
fi
echo "Using Affymetrix Power Tools binaries:"
echo "  $APT_NORM_BIN"
echo "  $APT_MAS5_BIN  (if required)"
echo "  $APT_CHP_BIN  (if required)"
sleep 2

if [ -z "$DATAPATH" -o -z "$OUTPATH" -o -z "$ANNOTATIONPATH" ]; then
  Usage
fi

if [ ! -f "$DATAPATH/ACCESSION" ]; then
  if [ ! -f "$DATAPATH/../../ACCESSION" ]; then
    echo "Error: Required accession file '$DATAPATH/ACCESSION' or '$DATAPATH/../../ACCESSION' not found!"
    echo "Please create it manually or use the 's4m' tool to create it for the given expression dataset."
    exit 1
  else
    ACCESSION=`cat "$DATAPATH/../../ACCESSION" | tr -d [:cntrl:]`
  fi
else
  ACCESSION=`cat "$DATAPATH/ACCESSION" | tr -d [:cntrl:]`
fi

if [ ! -d "$OUTPATH" ]; then
  mkdir -p "$OUTPATH"
fi

## Check which annotation data files are present
has_cdf="false"
has_pgf="false"
has_clf="false"
has_bgp="false"

cdfname=`basename $ANNOTATIONPATH`
cdf_file=`find $ANNOTATIONPATH -iname "$cdfname.cdf"`
if [ ! -z "$cdf_file" ]; then
  has_cdf="true"
fi
pgf_file=`find $ANNOTATIONPATH -iname "$cdfname.pgf"`
if [ ! -z "$pgf_file" ]; then
  has_pgf="true"
fi
clf_file=`find $ANNOTATIONPATH -iname "$cdfname.clf"`
if [ ! -z "$clf_file" ]; then
  has_clf="true"
fi
## check for background probes file
bgp_file=`find $ANNOTATIONPATH -iname "$cdfname.bgp"`
if [ ! -z "$bgp_file" ]; then
  has_bgp="true"
fi

## If no CDF and no PGF + CLF, we can't proceed
if [ "$has_cdf" = "false" ]; then
  if [ "$has_pgf" = "false" -o "$has_clf" = "false" ]; then
    echo "Error: No CDF or PGF,CLF files found in chip type annotation path: $ANNOTATIONPATH"
    echo "Cannot continue, exiting."; echo
    exit 1
  fi
fi


## Determine CEL file extensions (expect them to be consistent across a set!)
## Need to do this as we pass a "*.<CEL|cel>" target to APT tool and this is case sensitive.
celfiles=`ls $DATAPATH | grep CEL`
if [ "$?" -eq 0 -a ! -z "$celfiles" ]; then
  cel="CEL"
else
  celfiles=`ls $DATAPATH | grep cel`
  if [ ! -z "$celfiles" ]; then
    cel="cel"
  else
    echo "Error: No affymetrix CEL files in source directory: $DATAPATH!"
    exit 1
  fi
fi

## Flag to decide whether to perform MAS5 detection calling, based on given parameters
do_mas5="true"

## Set APT normalization flags accordingly
if [ "$has_cdf" = "true" ]; then
  #args="-a rma"
  #aptoutname="rma.summary.txt"
  ## RMA with "pm-mm" option for GeneChip platform seems to yield better bi-modal expression curve
  norm_args="-a rma-bg,quant-norm.sketch=0.usepm=true.bioc=true,pm-mm,med-polish"
  norm_aptoutname="rma-bg.quant-norm.pm-mm.med-polish.summary.txt"
  norm_opts="-d $cdf_file"
  mas5_args="-c $cdf_file"
elif [ $has_pgf = "true" -a $has_clf = "true" ]; then
  #args="-a rma"
  #aptoutname="rma.summary.txt"
  ## RMA with "pm-mm" option for GeneChip platform seems to yield better bi-modal expression curve
  norm_args="-a rma-bg,quant-norm.sketch=0.usepm=true.bioc=true,pm-mm,med-polish"
  norm_aptoutname="rma-bg.quant-norm.pm-mm.med-polish.summary.txt"
  norm_opts="-p $pgf_file -c $clf_file" 
  ## MAS5 detection calling can only be done with a CDF file
  do_mas5="false"
fi
## Perform DABG p-val detection calls in addition to RMA (if "background probes" file exists)
if [ $has_bgp = "true" ]; then
  norm_args="-a dabg $args"
  norm_opts="$opts --bgp-file $bgp_file"
  ## Don't perform MAS5 detection calling if BGP file was supplied.
  do_mas5="false"
fi

## Prepare args to pass to MAS5, raw signal extraction and normalization
raw_aptoutname=".pm-only.med-polish.summary.txt"
mas5_args="$mas5_args -o $OUTPATH/mas5 $DATAPATH/*.$cel"
raw_args="-a no-trans,pm-only,med-polish $norm_opts -o $OUTPATH $DATAPATH/*.$cel"
norm_args="$norm_args $norm_opts -o $OUTPATH $DATAPATH/*.$cel"

## Used to filter out probesets if needed, for example the U133AAofAV2 platform
## which is "massaged" into release chip HT_HG-U133A format for analysis purposes.
probe_filter_regex=""

## Perform MAS5 detection calling, if required
## Tested with U133AAofAV2 and HT_HG-U133A platforms so far.
if [ "$do_mas5" = "true" ]; then
  echo "Producing MAS5 detection calls.."
  sleep 2
  mkdir -p $OUTPATH/mas5

  ## Do MAS5 calls
  $APT_MAS5_BIN $mas5_args

  chp_files=`ls $OUTPATH/mas5/*.chp`
  chp_count=`echo $chp_files | wc -w`
  if [ $chp_count -lt 1 ]; then
    echo "Error: Failed producing MAS5 detection calls!"; echo
    exit 1
  else
    echo "Completed writing MAS5 detection calls."
  fi
  echo "Converting detection calls to 'unpacked' format.."
  $APT_CHP_BIN --cdf-file $cdf_file -o $OUTPATH/mas5 $OUTPATH/mas5/*.chp
  chp_txt_files=`cd $OUTPATH/mas5 && ls *.chp.txt`
  chp_txt_count=`echo "$chp_txt_files" | wc -w`
  detection_out_file="$OUTPATH/detection_calls_unpacked.txt"
  echo "Processing $chp_txt_count samples"

  count=0
  for chp in $chp_txt_files
  do
    cel_name=`grep "#%affymetrix-parent-celfile" "$OUTPATH/mas5/$chp" | sed -r -e 's/^.*\=//g'`
    cel_name=`basename $cel_name`
    count=`expr $count + 1`
    ## For special platform U133AAofAV2, make sure we remove certain probesets from output results
    if [ $count -eq 1 ]; then
       grep "#%affymetrix-array-type" "$OUTPATH/mas5/$chp" | grep -i "U133AAofAv2" > /dev/null 2>&1
       if [ $? -eq 0 ]; then
         echo "Notice: Platform 'U133AAofAV2' detected, will be removing 676 superfluous probesets from all outputs."
         probe_filter_regex=`u133aaofav2_probe_filter_regex`
       fi
    fi
    echo -n "."
    if [ ! -z "$probe_filter_regex" ]; then
      grep -P -v "(^#|^Probe|^Center)" $OUTPATH/mas5/$chp | grep "_at" | grep -P -v $probe_filter_regex | awk -v sample="$cel_name" -F'\t' '{print sample"\t"$1"\t"$2}' > "$OUTPATH/chp.tmp"
    else
      grep -P -v "(^#|^Probe|^Center)" $OUTPATH/mas5/$chp | grep "_at" | awk -v sample="$cel_name" -F'\t' '{print sample"\t"$1"\t"$2}' > "$OUTPATH/chp.tmp"
    fi
    if [ $count -gt 1 ]; then
      cat $detection_out_file "$OUTPATH/chp.tmp" >> $detection_out_file.new
      mv $detection_out_file.new $detection_out_file
      rm "$OUTPATH/chp.tmp"
    else
      mv "$OUTPATH/chp.tmp" $detection_out_file
    fi
  done

  echo; echo "Creating boolean detection file from P/M/A values.."
  ## Create present-only version while we're here (keep only "P" probes, remove "M" and "A")
  grep -P "P$" "$detection_out_file" > "$OUTPATH/detection_calls_unpacked_presentonly.txt"
  ## Create TRUE/FALSE boolean version where P=TRUE, M=FALSE, A=FALSE
  cat "$detection_out_file" | sed -r -e 's/P$/TRUE/g' -e 's/(M|A)$/FALSE/g' > "$OUTPATH/detection_calls_unpacked_boolean.txt"
  echo "Done"
fi

## Extract raw expression signals
echo "Extracting raw expression signals.."
sleep 2
$APT_NORM_BIN $raw_args

## Create s4m formatted raw expression output from APT output
if [ -f "$OUTPATH/$raw_aptoutname" ]; then
  grep -P "^#" -v "$OUTPATH/$raw_aptoutname" > "$OUTPATH/raw_expression.txt.tmp"
  ## Filter probesets if required
  if [ ! -z "$probe_filter_regex" ]; then
    grep -P -v $probe_filter_regex "$OUTPATH/raw_expression.txt.tmp" > "$OUTPATH/raw_expression.txt.filtered.tmp"
    mv "$OUTPATH/raw_expression.txt.filtered.tmp" "$OUTPATH/raw_expression.txt.tmp"
  fi
  ## Reformat header
  rowcount=`cat "$OUTPATH/raw_expression.txt.tmp" | wc -l`
  header=`head -1 "$OUTPATH/raw_expression.txt.tmp"`
  new_header=`echo "$header" | sed -r -e 's/^probeset_id\t//g'`
  echo "$new_header" > "$OUTPATH/raw_expression.txt"
  tailcount=`expr $rowcount - 1`
  tail -$tailcount "$OUTPATH/raw_expression.txt.tmp" >> "$OUTPATH/raw_expression.txt"
  rm "$OUTPATH/raw_expression.txt.tmp"
  echo "  Completed extracting raw signals."
else
  echo "Fatal Error: Raw expression extraction failed (output file '$OUTPATH/$raw_aptoutname' not found)!"
  exit 1
fi

## Perform normalization
echo; echo "Normalizing..."; echo
sleep 2
$APT_NORM_BIN $norm_args


## Create s4m formatted normalized expression output from APT output
if [ -f "$OUTPATH/$norm_aptoutname" ]; then
  grep -P "^#" -v "$OUTPATH/$norm_aptoutname" > "$OUTPATH/normalized_expression.txt.tmp"
  ## Filter probesets if required
  if [ ! -z "$probe_filter_regex" ]; then
    grep -P -v $probe_filter_regex "$OUTPATH/normalized_expression.txt.tmp" > "$OUTPATH/normalized_expression.txt.filtered.tmp"
    mv "$OUTPATH/normalized_expression.txt.filtered.tmp" "$OUTPATH/normalized_expression.txt.tmp"
  fi
  ## Reformat header
  rowcount=`cat "$OUTPATH/normalized_expression.txt.tmp" | wc -l`
  header=`head -1 "$OUTPATH/normalized_expression.txt.tmp"`
  new_header=`echo "$header" | sed -r -e 's/^probeset_id\t//g'`
  echo "$new_header" > "$OUTPATH/normalized_expression.txt"
  tailcount=`expr $rowcount - 1`
  tail -$tailcount "$OUTPATH/normalized_expression.txt.tmp" >> "$OUTPATH/normalized_expression.txt"
  rm "$OUTPATH/normalized_expression.txt.tmp"
  echo "  Completed Normalization."
else
  echo "Fatal Error: Normalization failed (output file '$OUTPATH/$norm_aptoutname' not found)!"
  exit 1
fi

cd util

## Unpack raw and RMA expression files 
if [ -f "$OUTPATH/raw_expression.txt" -a -f "$OUTPATH/normalized_expression.txt" ]; then
  echo; echo "Unpacking RMA expression values.."
  # Unpack the expression data table to a format that can be used to build Stemformatics expression files
  # Required to plot
  ./unpack_expression_table.sh "$OUTPATH/raw_expression.txt" "$OUTPATH/raw_expression_unpacked.txt"
  if [ $? -ne 0 ]; then
    echo "Fatal Error: Failed to unpack expression values."
    exit 1
  fi
  ./unpack_expression_table.sh "$OUTPATH/normalized_expression.txt" "$OUTPATH/normalized_expression_unpacked.txt"
  if [ $? -ne 0 ]; then
    echo "Fatal Error: Failed to unpack expression values."
    exit 1
  else
    echo "  done"
  fi
fi

## Combine normalized expression and boolean detection calls
if [ -f "$OUTPATH/detection_calls_unpacked_boolean.txt" ]; then
  echo "Combining normalized expression scores and detection calls.."
  pr -Jtm "$OUTPATH/normalized_expression_unpacked.txt" "$OUTPATH/detection_calls_unpacked_boolean.txt" > "$OUTPATH/.s4mtmp"
  if [ $? -ne 0 ]; then
    echo "Error: Failed to merge expression and detection calls!"
    rm -f "$OUTPATH/.s4mtmp"
  else
    cut -f 1,2,3,6 "$OUTPATH/.s4mtmp" > "$OUTPATH/normalized_expression_detection.txt"
    if [ $? -ne 0 ]; then
      echo "Error: Failed to merge expression and detection calls!"
      rm -f "$OUTPATH/.s4mtmp" "$OUTPATH/normalized_expression_detection.txt"
    fi
    rm -f "$OUTPATH/.s4mtmp"
  fi
  echo "Done"
fi

## Back up to normalization scripts directory
cd ..

## Plot expression density curves
if [ -f "$OUTPATH/raw_expression_unpacked.txt" ]; then
  echo "Plotting raw expression density.."
  mkdir -p "$OUTPATH/qc_raw"
  ./plot_expression_density.sh "$OUTPATH/raw_expression_unpacked.txt" "$OUTPATH/qc_raw" "$ACCESSION"
  if [ $? -ne 0 ]; then
    echo "Error: Failed to produce raw probe expression density plot."
  else
    echo "  done"
  fi
fi
if [ -f "$OUTPATH/normalized_expression_unpacked.txt" ]; then
  echo "Plotting normalized expression density.."
  mkdir -p "$OUTPATH/qc_normalized"
  ./plot_expression_density.sh "$OUTPATH/normalized_expression_unpacked.txt" "$OUTPATH/qc_normalized" "$ACCESSION"
  if [ $? -ne 0 ]; then
    echo "Fatal Error: Failed to produce normalized probe expression density plot."
    exit 1
  fi
  echo "  done"
fi

echo; echo "All Done."


