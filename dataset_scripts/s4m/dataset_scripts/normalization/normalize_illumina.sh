#!/bin/sh

Usage () {
  echo "$0 <dataset_path> <input_file_path> <output_path> [<detection_flip_boolean> <bgcorrect_boolean> <skip_format_check>]"
  echo
  echo "   where: detection_flip_boolean TRUE or FALSE. Default = FALSE"; echo
  echo "          bgcorrect_boolean TRUE or FALSE. Default = TRUE"; echo
  echo "          skip_format_check TRUE or FALSE. Default = FALSE"; echo
  exit
}

beadstudio_format_is_correct () {
  bsfile="$1"
  ndetcol=`head -1 "$bsfile" | tr "\t" "\n" | grep -P "\.Detection Pval" | wc -l`
  nsignal=`head -1 "$bsfile" | tr "\t" "\n" | grep -P "\.AVG_Signal" | wc -l`
  ## Number of cols not ProbeID, Target(ID) or ChipID.Detection Pval or ChipID.AVG_Signal
  othercols=`head -1 "$bsfile" | tr "\t" "\n" | grep -v -P "(\.Detection Pval|\.AVG_Signal|ProbeID|PROBE_ID|Target)" | wc -l`
  if [ $ndetcol -eq 0 ]; then
    echo "Error: BeadStudio detection columns incorrect format (expecting 'Detection Pval')!"; echo
    return 1
  fi
  if [ $ndetcol -ne $nsignal ]; then
    echo "Error: BeadStudio file mismatch between # of avg_signal and detection columns!"; echo
    return 1
  fi
  if [ $othercols -gt 0 ]; then
    echo
    echo "WARNING: BeadStudio file contains non-signal and non-detection columns - this might affect the normalization process."; echo
    echo "Press ENTER to continue, or CTRL-C to quit and resolve before continuing."
    read ans
    echo
  fi
  return 0
}

unpack_illumina_expression_data () {
  cd "$SCRIPT_HOME/util"
  ## Unpack raw and normalized
  if [ -f "$OUTPATH/raw_expression.txt" -a -f "$OUTPATH/normalized_expression.txt" ]; then
    echo "Unpacking expression values.."
    # Unpack the expression data table to a format that can be used to build Stemformatics expression files
    # Required to plot
    ./unpack_expression_table.sh "$OUTPATH/raw_expression.txt" "$OUTPATH/raw_expression_unpacked.txt"
    if [ $? -ne 0 ]; then
      echo "Fatal Error: Failed to unpack expression values."
      exit 1
    fi
    ./unpack_expression_table.sh "$OUTPATH/normalized_expression.txt" "$OUTPATH/normalized_expression_unpacked.txt"
    if [ $? -ne 0 ]; then
      echo "Fatal Error: Failed to unpack expression values."
      exit 1
    fi
    if [ -f "$OUTPATH/normalized_expression_raw.txt" ]; then
      ./unpack_expression_table.sh "$OUTPATH/normalized_expression_raw.txt" "$OUTPATH/normalized_expression_raw_unpacked.txt"
    fi
    echo "done"
fi
}

unpack_illumina_detection_data () {
  cd "$SCRIPT_HOME/util"
  ## Unpack detection scores
  if [ -f "$OUTPATH/detection_scores.txt" ]; then
    echo "Unpacking detection scores.."
    ./unpack_detection_scores.sh "$OUTPATH/detection_scores.txt" "$OUTPATH/detection_scores_unpacked.txt"
    if [ $? -ne 0 ]; then
      echo "Fatal Error: Failed to unpack detection scores."
      exit 1
    else
      echo "done"
    fi
  fi
  ## Unpack detection calls (boolean based on p-val cutoff)
  if [ -f "$OUTPATH/detection_calls_boolean.txt" ]; then
    echo "Unpacking detection calls.."
    ./unpack_detection_calls_lumi.sh "$OUTPATH/detection_calls_boolean.txt" "$OUTPATH/detection_calls_boolean_unpacked.txt"
    if [ $? -ne 0 ]; then
      echo "Fatal Error: Failed to unpack detection scores."
      exit 1
    else
      echo "done"
    fi
    ## Attempt to create combined expression and detection scores file so we can plot them together
    if [ -f "$OUTPATH/detection_calls_boolean_unpacked.txt" ]; then
      echo "Combining expression scores and detection calls.."
      pr -Jtm "$OUTPATH/normalized_expression_unpacked.txt" "$OUTPATH/detection_calls_boolean_unpacked.txt" > "$OUTPATH/.s4mtmp"
      if [ $? -ne 0 ]; then
        echo "Error: Failed to merge expression and detection calls!"
        rm -f "$OUTPATH/.s4mtmp"
      else
        cut -f 1,2,3,6 "$OUTPATH/.s4mtmp" > "$OUTPATH/normalized_expression_detection.txt"
        if [ $? -ne 0 ]; then
          echo "Error: Failed to merge expression and detection calls!"
          rm -f "$OUTPATH/.s4mtmp" "$OUTPATH/normalized_expression_detection.txt"
        fi
        rm -f "$OUTPATH/.s4mtmp"
      fi
    fi
  fi
}


do_normalize_pipeline () {
  detflip="$1"
  bgcorrect="$2"
  echo "Starting normalization and QC output.."; echo
  sleep 1
  detectionflip=""
  cd "$SCRIPT_HOME"
  if [ "$detflip" = "TRUE" ]; then
    detectionflip="-detectionflip=TRUE"
  fi
  nobg=""
  if [ "$bgcorrect" = "FALSE" ]; then
    nobg="-nobg=TRUE"
  fi

  ### DEBUG ###
  #echo "$R_BIN -f ./R/normalize_illumina.R --args -input=$INFILEPATH -output=$OUTPATH $detectionflip $nobg"
  #sleep 5
  ### END ###

  ## normalize
  $R_BIN -f ./R/normalize_illumina.R --args -input="$INFILEPATH" -output="$OUTPATH" $detectionflip $nobg
  if [ $? -ne 0 ]; then
    echo "Fatal Error: Normalization failed."; echo
    exit 1
  fi

  ## create files formatted for s4m "finalize" to use
  unpack_illumina_expression_data
  unpack_illumina_detection_data
}


count_total_probes () {
  count=`cat "$OUTPATH/normalized_expression.txt" | wc -l`
  echo `expr $count - 1`
}


count_detected_probes () {
  count=`grep TRUE "$OUTPATH/detection_calls_boolean.txt" | wc -l`
  echo "$count"
}


### START ###

SCRIPT_HOME="$S4M_HOME/dataset_scripts/normalization"

DATAPATH="$1"
INFILEPATH="$2"
OUTPATH="$3"
DETECTIONFLIP="FALSE"
if [ ! -z "$4" ]; then
  DETECTIONFLIP="$4"
fi
BGCORRECT="TRUE"
if [ ! -z "$5" ]; then
  BGCORRECT="$5"
fi
SKIPCHECK="FALSE"
if [ ! -z "$6" ]; then
  SKIPCHECK="$6"
fi


#echo "$0: DEBUG: DETECTIONFLIP=[$DETECTIONFLIP]"
#sleep 3

R_BIN="R"
if [ ! -z "$S4M_R" ]; then
    R_BIN="$S4M_R"
    echo "Using 'R': $S4M_R"
fi

if [ -z "$DATAPATH" -o -z "$INFILEPATH" -o -z "$OUTPATH" ]; then
  Usage
fi

if [ ! -f "$DATAPATH/ACCESSION" ]; then
  echo "Error: Required accession file '$DATAPATH/ACCESSION' not found!"
  echo "Please create it manually or use the 's4m' tool to create it for the given expression dataset."
  exit 1
else
  ACCESSION=`cat "$DATAPATH/ACCESSION" | tr -d [:cntrl:]`
fi

if [ ! -d "$OUTPATH" ]; then
  mkdir -p "$OUTPATH"
fi

if [ $SKIPCHECK != "TRUE" ]; then
  ## Check BeadStudio column header format / number of columns
  echo "Checking input BeadStudio.txt file format.."
  beadstudio_format_is_correct "$INFILEPATH"
  if [ $? -ne 0 ]; then
    exit 1
  fi
  echo "[ OKAY ]"
  sleep 2
else
  echo "WARNING: SKIPPED BeadStudio.txt file format check.."
  sleep 2
fi

do_normalize_pipeline "$DETECTIONFLIP" "$BGCORRECT"

## Some Illumina Bead/GenomeStudio files can have detection p-vals "reversed". Check for that here.
total_probes=`count_total_probes`
detected_probes=`count_detected_probes`
half_total=`expr $total_probes \/ 2`
echo; echo "There are $detected_probes probes (of $total_probes) called detected (according to Illumina detection p-val < 0.01)"


if [ $detected_probes -lt $half_total -a $SKIPCHECK = "FALSE" ]; then
  echo -n "
WARNING: Got less than half of total probes called detected.

Possible reasons:
  - Incorrect BeadStudio.txt format *** CHECK/FIX IF NEEDED ***
  - Limited sample phenotype in experiment
  - Poor detection quality on instrument and/or due to software settings
  - Detection scores have inverse polarity (1.0 = max detection, vs. more common 0.0)

If the latter, we can run again with reversed detection p-values now.
If detected probe count is less than 10,000 it will most likely be safe to proceed
BUT you should check house-keeping probe(s) to be sure.

NOTE: Check control probe or house-keeping gene expression. Please don't guess!

Run again with reversed detection p-values? [Y/n] "

  read ans
  if [ -z "$ans" ]; then
    ans="y"
  fi
  case $ans in
    Y|y)
      do_normalize_pipeline "TRUE" "$BGCORRECT"
      ## Some Illumina Bead/GenomeStudio files can have detection p-vals "reversed". Check for that here.
      total_probes=`count_total_probes`
      detected_probes=`count_detected_probes`
      half_total=`expr $total_probes \/ 2`
      echo; echo "There are $detected_probes probes (of $total_probes) called detected (according to Illumina detection p-val < 0.01)"
      ;;
    N|n)
      ;;
    *)
      ;;
  esac

elif [ $SKIPCHECK = "TRUE" ]; then
  echo "Skipping detection flip check as 'force' mode is enabled.."
  sleep 2
fi

echo; echo "Normalization complete."
