#!/bin/sh

Usage () {
  echo "$0 <raw_data_path> <output_path> <platform_affinity_data_path> [--detection-only]"; echo
  exit
}

DATAPATH="$1"
OUTPATH="$2"
AFFINITYPATH="$3"

MAS5_ONLY="false"
if [ ! -z "$4" -a "$4" = "--detection-only" ]; then
    MAS5_ONLY="true"
fi

R_BIN="R"
if [ ! -z "$S4M_R" ]; then
    R_BIN="$S4M_R"
fi
echo "Using 'R': $R_BIN"
sleep 2

if [ -z "$DATAPATH" -o -z "$OUTPATH" -o -z "$AFFINITYPATH" ]; then
  Usage
fi

if [ ! -f "$DATAPATH/ACCESSION" ]; then
  if [ ! -f "$DATAPATH/../../ACCESSION" ]; then
    echo "Error: Required accession file '$DATAPATH/ACCESSION' or '$DATAPATH/../../ACCESSION' not found!"
    echo "Please create it manually or use the 's4m' tool to create it for the given expression dataset."
    exit 1
  else
    ACCESSION=`cat "$DATAPATH/../../ACCESSION" | tr -d [:cntrl:]`
  fi
else
  ACCESSION=`cat "$DATAPATH/ACCESSION" | tr -d [:cntrl:]`
fi

if [ ! -d "$OUTPATH" ]; then
  mkdir -p "$OUTPATH"
fi

sleep 1
if [ "$MAS5_ONLY" = "true" ]; then
  echo "Producing MAS5 detection calls.."
  $R_BIN -f ./R/normalize_affy_genechip_MAS5_only.R --args -input="$DATAPATH" -output="$OUTPATH" -affinitypath="$AFFINITYPATH"

else
  echo "Starting normalization.."; echo
  $R_BIN -f ./R/normalize_affy_genechip.R --args -input="$DATAPATH" -output="$OUTPATH" -affinitypath="$AFFINITYPATH"
  if [ $? -ne 0 ]; then
    echo "Fatal Error: Normalization failed."; echo
    exit 1
  fi
fi

## Unpack raw and GCRMA 
if [ "$MAS5_ONLY" = "false" -a -f "$OUTPATH/raw_expression.txt" -a -f "$OUTPATH/normalized_expression.txt" ]; then
  echo; echo "Normalization complete."
  echo "Unpacking GCRMA expression values.."
  # Unpack the expression data table to a format that can be used to build Stemformatics expression files
  # Required to plot
  (cd util && ./unpack_expression_table.sh "$OUTPATH/raw_expression.txt" "$OUTPATH/raw_expression_unpacked.txt")
  (cd util && ./unpack_expression_table.sh "$OUTPATH/normalized_expression.txt" "$OUTPATH/normalized_expression_unpacked.txt")
  if [ $? -ne 0 ]; then
    echo "Fatal Error: Failed to unpack expression values."
    exit 1
  else
    echo "done"
  fi
fi
## Unpack MAS5
if [ -f "$OUTPATH/normalized_expression_mas5.txt" ]; then
  echo "Unpacking MAS5 expression values.."
  (cd util && ./unpack_expression_table.sh "$OUTPATH/normalized_expression_mas5.txt" "$OUTPATH/normalized_expression_mas5_unpacked.txt")
  if [ $? -ne 0 ]; then
    echo "Fatal Error: Failed to unpack expression values."
    exit 1
  else
    echo "done"
  fi
fi

## Unpack MAS5 detection calls
if [ -f "$OUTPATH/detection_calls_mas5.txt" ]; then
  echo "Unpacking MAS5 \"PMA\" detection calls.."
  (cd util && ./unpack_detection_calls_mas5.sh "$OUTPATH/detection_calls_mas5.txt" "$OUTPATH/detection_calls_unpacked_mas5.txt")
  if [ $? -ne 0 ]; then
    echo "Fatal Error: Failed to unpack detection calls."
    exit 1
  else
    echo "done"
  fi
  ## Attempt to create combined expression and detection calls file for both GCRMA and MAS5
  ## so we can plot them side-by-side
  if [ -f "$OUTPATH/detection_calls_unpacked_mas5_boolean.txt" ]; then
    echo "Combining GCRMA expression scores and detection calls.."
    pr -Jtm "$OUTPATH/normalized_expression_unpacked.txt" "$OUTPATH/detection_calls_unpacked_mas5_boolean.txt" > "$OUTPATH/.s4mtmp"
    if [ $? -ne 0 ]; then
      echo "Error: Failed to merge GCRMA expression and detection calls!"
    else
      cut -f 1,2,3,6 "$OUTPATH/.s4mtmp" > "$OUTPATH/normalized_expression_detection.txt"
      if [ $? -ne 0 ]; then
        echo "Error: Failed to merge GCRMA expression and detection calls!"
        rm -f "$OUTPATH/normalized_expression_detection.txt"
      else
        ## only remove tmp file if we succeeded
        rm -f "$OUTPATH/.s4mtmp"
      fi
    fi

    if [ -f "$OUTPATH/normalized_expression_mas5_unpacked.txt" ]; then
      echo "Combining MAS5 expression scores and detection calls.."
      pr -Jtm "$OUTPATH/normalized_expression_mas5_unpacked.txt" "$OUTPATH/detection_calls_unpacked_mas5_boolean.txt" > "$OUTPATH/.s4mtmp2"
      if [ $? -ne 0 ]; then
        echo "Error: Failed to merge MAS5 expression and detection calls!"
      else
        cut -f 1,2,3,6 "$OUTPATH/.s4mtmp2" > "$OUTPATH/normalized_expression_detection_mas5.txt"
        if [ $? -ne 0 ]; then
          echo "Error: Failed to merge MAS5 expression and detection calls!"
          rm -f "$OUTPATH/normalized_expression_detection_mas5.txt"
        else
          ## only remove tmp file if we succeeded
          rm -f "$OUTPATH/.s4mtmp2"
        fi
      fi
    fi

  fi
fi
