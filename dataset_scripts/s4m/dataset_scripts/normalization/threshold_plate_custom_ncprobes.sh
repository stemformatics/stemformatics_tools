#!/bin/sh
# use custom list of negative control probes to get threshold for plate, plate-like arrays
# this includes PrimeView (and possibly HG-U219, still under investigation)

### INCLUDES ###
. ./includes.sh

### OPTIONS ###
export S4M_DEBUG="true"
R_BIN="R"

### FUNCTIONS ###
Usage () {
  echo "$0 <normalized_data_path>"
  exit
}

# we can grep these from file, and these have been published as well
antigenomic_probes="AFFX-BkGr-GC03_at|AFFX-BkGr-GC04_at|AFFX-BkGr-GC05_at|AFFX-BkGr-GC06_at|AFFX-BkGr-GC07_at|AFFX-BkGr-GC08_at|AFFX-BkGr-GC09_at|AFFX-BkGr-GC10_at|AFFX-BkGr-GC11_at|AFFX-BkGr-GC12_at|AFFX-BkGr-GC13_at|AFFX-BkGr-GC14_at|AFFX-BkGr-GC15_at|AFFX-BkGr-GC16_at|AFFX-BkGr-GC17_at|AFFX-BkGr-GC18_at|AFFX-BkGr-GC19_at|AFFX-BkGr-GC20_at|AFFX-BkGr-GC21_at|AFFX-BkGr-GC22_at|AFFX-BkGr-GC23_at|AFFX-BkGr-GC24_at|AFFX-BkGr-GC25_at"

## MAIN ##
NORM_DATA_PATH="$1"

## Remove existing antigenomic probes file, if any
rm -f "$NORM_DATA_PATH/antigenomic_normalized_expression_subset.txt"

# Probe identities and expression vals are attributes of CEL (column name)
data_samples=`head -n1 "$NORM_DATA_PATH/normalized_expression.txt" | tr '\t' '\n'`

# the first col is the probe names, this index refers to the data column
index=2

for sample in ${data_samples}; do 
  grep -P "$antigenomic_probes" "$NORM_DATA_PATH/normalized_expression.txt" |\
  cut -f1,${index} |\
  sed -r -e "s|^|${sample}\t|"
  index=`expr ${index} + 1`
done > "$NORM_DATA_PATH/antigenomic_normalized_expression_subset.txt"

if [ $? -eq 1 ]; then
  s4m_debug "Antigenomic probe $probe not found in normalized Exon-level expression results."
fi

unset index

## Write antigenomic median log2 normalized expression value
if [ -s "$NORM_DATA_PATH/antigenomic_normalized_expression_subset.txt" ]; then
  echo "Calculating median of antigenomic probe expression to use as detection floor..";     echo
  R_BIN="R"

  if [ ! -z "$S4M_R" ]; then
      R_BIN="$S4M_R"
  fi
  
  echo "Using 'R': $R_BIN"
    
  $R_BIN -f ./R/expression_median.R --no-save --args -input="$NORM_DATA_PATH/antigenomic_normalized_expression_subset.txt" -output="$NORM_DATA_PATH/antigenomic_median.txt" > /dev/null
  
  if [ -f "$NORM_DATA_PATH/antigenomic_median.txt" ]; then
    echo; echo "Antigenomic median derivation complete."
  else
    echo "Fatal Error: There was an error during the antigenomic probe expression median derivation process."
    exit 1
  fi
    
  ## Extract median value from R output
  if [ -f "$NORM_DATA_PATH/antigenomic_median.txt" ]; then
    awk '{printf("%2.02f",$2)}' "$NORM_DATA_PATH/antigenomic_median.txt" > "$NORM_DATA_PATH/ANTIGENOMIC_MEDIAN"
  else
    echo "Fatal Error: Failed to determine median of antigenomic (background) probe expression"
    exit 1
  fi

  ## clean up
  mkdir -p "$NORM_DATA_PATH/threshold_derivation"
  mv -f "$NORM_DATA_PATH/antigenomic_median.txt" "$NORM_DATA_PATH/antigenomic_normalized_expression_subset.txt" "$NORM_DATA_PATH/threshold_derivation/"

else
  echo "Error: Failed to extract expression values for antigenomic probes"
  exit 1
fi
