#===============================
# Affy ST normalization gcrma
#===============================
# Author: O.Korn

# Path to directory containing CEL files belonging to target Affy study
input_path <- ""
# Path to output directory
output_path <- ""
# Path to Aroma working directory
working_dir <- ""
# CDF name e.g. HuEx-1_0-st-v2
# This CDF is used at least for background correction, if not the entire pipeline.
# (Depending upon fitness for purpose of the CDF file and its source etc.)
cdf_name <- ""
# Tag of custom CDF file (if required). e.g. "fullR2,A20070914,EP" for HuEx-1_0-st-v2
# If given, used for all steps beyond background correction.
cdf_tag_custom <- ""

# Args processing code from: mugen_tutorial_affy_qa.pdf
for (e in commandArgs()) {
   ta <- strsplit(e,"=",fixed=TRUE);
   if(! is.na(ta[[1]][2])) {
      temp <- ta[[1]][2];
      if(substr(ta[[1]][1],nchar(ta[[1]][1]),nchar(ta[[1]][1])) == "I") {
         temp <- as.integer(temp);
      }
      if(substr(ta[[1]][1],nchar(ta[[1]][1]),nchar(ta[[1]][1])) == "N") {
         temp <- as.numeric(temp);
      }
      assign(ta[[1]][1],temp);
      if ( ta[[1]][1] == "-input" ) {
         input_path <- temp;
      }
      if ( ta[[1]][1] == "-output" ) {
         output_path <- temp;
      }
      if ( ta[[1]][1] == "-workdir" ) {
         working_dir <- temp;
      }
      if ( ta[[1]][1] == "-cdfname" ) {
         cdf_name <- temp;
      }
      if ( ta[[1]][1] == "-cdftag" ) {
         cdf_tag_custom <- temp;
      }
   } else {
      assign(ta[[1]][1],TRUE);
   }
}

if(nchar(input_path) == 0) {
   print("Error: Path to input CEL files must be provided!\n\n")
   q()
}
if(nchar(output_path) == 0) {
   print("Error: Output directory must be provided!\n\n")
   q()
}
if(nchar(working_dir) == 0) {
   print("Error: Working directory must be provided!\n\n")
   q()
}
if(nchar(cdf_name) == 0) {
   print("Error: Array platform CDF name must be provided!\n\n")
   q()
}

# Load required libraries
library("aroma.affymetrix")
library("simpleaffy")
library("oligo")
library("affyPLM")

# Switch to working directory - the directory containing:
# |__ annotationData
# |__ probeData
# |__ plmData
# |__ .. and related
# NOTE: probeData and plmData should probably be removed before every
#       aroma.affymetrix run otherwise previous run data will be
#       loaded if it is already cached on disk.
oldcwd <- setwd(working_dir)

verbose <- Arguments$getVerbose(-10, timestamp=TRUE)


###############################################################################
#
# Initialise CDF environments
#
###############################################################################

cdf <- AffymetrixCdfFile$byChipType(cdf_name);
# Output CDF summary details
sink(paste(output_path,"/cdf.txt",sep=""))
print(cdf)
sink()

cdfCustom <- NULL

if (nchar(cdf_tag_custom) >= 1) {
   cdfCustom <- AffymetrixCdfFile$byChipType(cdf_name, tags=cdf_tag_custom);
   # Output custom CDF summary details
   sink(paste(output_path,"/cdf_custom.txt",sep=""))
   print(cdfCustom)
   sink()
}


###############################################################################
#
# Load CEL files
#
###############################################################################

# Load Affy ST data from directory containing CEL files

csR <- AffymetrixCelSet$fromFiles(input_path, cdf=cdfCustom)
# Output CEL summary
sink(paste(output_path,"/array_summary.txt",sep=""))
print(csR)
sink()


###############################################################################
#
# QC plots and metrics
#
###############################################################################

## TODO


###############################################################################
#
# Output detection calls
#
###############################################################################

## TODO: Remove this code and replace with something else, if possible.
##       Cannot do MAS5 calls because only chips with MM probes in addition
##       to PM probes will work. Some ST arrays don't have MM probes!

#affyBatch <- extractAffyBatch(csR)
#det.mas5 <- detection.p.val(affyBatch)
#write.table(det.mas5, paste(output_path,"/detection_calls_mas5.txt",sep=""), sep="\t")
#expr.mas5 = call.exprs(affyBatch, "mas5")
#write.table(exprs(expr.mas5), paste(output_path,"/normalized_expression_mas5.txt",sep=""), sep="\t")


###############################################################################
#
# GC-RMA Background correction and Normalization
#
###############################################################################

# Make sure we're using the "unsupported" CDF for background correction
setCdf(csR, cdf)

## If the array chip type does not have MM (negative control) probes then this
## GCRMA normalization with probe affinities will FAIL.
## Not all Affy ST arrays have MM probes (e.g. MoGene-st-v1 does not).
## In that case, standard RMA must be used.
bc <- GcRmaBackgroundCorrection(csR, type="affinities")
sink(paste(output_path,"/bg_summary.txt",sep=""))
print(bc)
sink()

csB <- process(bc, verbose=verbose)

if (! is.null(cdfCustom)) {
   ## Now we can revert to our custom CDF before the normalization step
   setCdf(csR, cdfCustom); # undo
   setCdf(csB, cdfCustom); # undo
}

## DEBUG ##
print(csB)

## BREAKPOINT ##
#q()

qn <- QuantileNormalization(csB, typesToUpdate="pm")
sink(paste(output_path,"/qn_summary.txt",sep=""))
print(qn)
sink()

csN <- process(qn, verbose=verbose)

## Verify which CDF was used
getCdf(csN)

## BREAKPOINT ##
#q()


###############################################################################
#
# Fit probe model and write expression data to disk
#
###############################################################################

## Fit to probe model
plmTr <- ExonRmaPlm(csN, mergeGroups=TRUE)
# run the model
fit(plmTr, verbose=verbose) # this takes a while!

## generate a data frame of probe/transcript model and write to disk
cesTr <- getChipEffectSet(plmTr)
# write "raw" (non-log2, with additional data columns) normalized data frame to disk
trFit <- extractDataFrame(cesTr, units=NULL, addNames=TRUE)
write.table(trFit, paste(output_path,"/normalized_expression_raw.txt",sep=""), sep="\t")

## Extract expression values, log2 them and write to disk
exprsMatrix <- extractMatrix(cesTr)
if (! is.null(cdfCustom)) {
   rownames(exprsMatrix) <- getUnitNames(cdfCustom)
} else {
   rownames(exprsMatrix) <- getUnitNames(cdf)
}
exprsMatrix <- log2(exprsMatrix)
# Get sample names and write header to output file
cnames <- colnames(trFit)
sampleNames <- cnames[-(1:5)]
write(sampleNames, paste(output_path,"/normalized_expression.txt",sep=""), sep="\t", ncolumns=length(sampleNames))
# Append final, log2 probe expression data
write.table(exprsMatrix, paste(output_path,"/normalized_expression.txt",sep=""), append=TRUE, sep="\t", row.names=TRUE, col.names=FALSE, quote=FALSE)

