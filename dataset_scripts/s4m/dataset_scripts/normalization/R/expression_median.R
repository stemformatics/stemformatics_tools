#==============================================================================
# Write median value from input expression table
#==============================================================================
# Author: O.Korn

args <- commandArgs()
# Expecting input "unpacked" normalized expression data file.
input_file_name <- ""
output_file_name <- ""
# Args processing code from: mugen_tutorial_affy_qa.pdf
for (e in commandArgs()) {
   ta <- strsplit(e,"=",fixed=TRUE);
   if(! is.na(ta[[1]][2])) {
      temp <- ta[[1]][2];
      if(substr(ta[[1]][1],nchar(ta[[1]][1]),nchar(ta[[1]][1])) == "I") {
         temp <- as.integer(temp);
      }
      if(substr(ta[[1]][1],nchar(ta[[1]][1]),nchar(ta[[1]][1])) == "N") {
         temp <- as.numeric(temp);
      }
      assign(ta[[1]][1],temp);
      if ( ta[[1]][1] == "-input" ) {
         input_file_name <- temp;
      }
      if ( ta[[1]][1] == "-output" ) {
         output_file_name <- temp;
      }
   } else {
      assign(ta[[1]][1],TRUE);
   }
}

# Load expression table
expression_table<-read.table(input_file_name, header=FALSE)

# Output median
sink(output_file_name)
median(expression_table$V3)
sink()
