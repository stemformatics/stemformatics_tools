#===================================
# Generic boxplot
#===================================
# Author: O.Korn

source(paste(Sys.getenv("S4M_HOME"),"/dataset_scripts/util/R/util.R",sep=""))

Usage <- function() {
   print("Usage:  R -f <script_name.R> --args -input=<dataframe.txt> -output=<output.png> [-phenofile=<sample_info.txt>] [-title=Custom title]")
   print("")
   print(" 'phenofile' expects 2 or 3 tab-separated columns: SAMPLE_ID  GROUP  [OPTIONAL_SAMPLE_LABEL]")
}


# Expecting input data frame
inputfile <- ARGV.get("input",T)
outputfile <- ARGV.get("output",T)
title <- ARGV.get("title")
phenofile <- ""

if (is.na(ARGV.get("phenofile"))) {
   print("Warning: Phenotype file (sample name to group) not given, only sample IDs will be used.")
   Usage()
   Sys.sleep(1)
} else {
   phenofile <- as.character(ARGV.get("phenofile"))
}

outFileBase <- sub(".png$", "", as.character(outputfile))

## Load input data frame
## Works for data frames with or without leading TAB in header row
data <- read.table(as.character(inputfile), sep="\t", header=T, row.names=1, na.strings="", quote="", check.names=F, as.is=T)
sampleNames <- colnames(data)

## Strip .CEL or .txt from sample IDs if present (Affy and Agilent respectively)
##
## TODO: This is rather specific - however would be hard to relegate this responsibility
##   to the calling layer because we'd have to modify column headers in input data frame(s)
##   and possibly create temporary versions of data. Easier to leave this here, for now.
if (length(grep(".CEL", sampleNames, fixed=T))) {
   sampleNames <- gsub(".CEL", "", sampleNames, fixed=T)
} else if (length(grep(".txt", sampleNames, fixed=T))) {
   sampleNames <- gsub(".txt", "", sampleNames, fixed=T)
}

if (nchar(phenofile)) {
  if (file.exists(phenofile)) {
    phenoMap <- read.table(phenofile, sep="\t", stringsAsFactors=F, colClasses="character", check.names=F, quote="")
    if (identical(sort(as.vector(phenoMap$V1)), sort(sampleNames)) != TRUE) {
      print(paste("Error: Samples in phenotype file '",phenofile,"' do not match samples in expression data (check quantity and IDs)!",sep=""))
      print("Samples from phenoMap: ")
      print(sort(as.vector(phenoMap$V1)))
      print("Samples from data: ")
      print(sort(as.vector(sampleNames)))
      q(status=1)
    }

    ## sample ID -> group name, ordered as per data
    sampleNames <- unlist(lapply(sampleNames, function(x) { paste(x, phenoMap[phenoMap$V1 == x,]$V2, sep=" - ") }))
  }
}

if (! nchar(title)) {
  title <- "Sample boxplot"
}
## truncate all sample names to <= 35 chars if we need to - otherwise we get ugly auto-truncation
## of the beginning of names falling off the bottom of the plot
sampleNames <- substr(sampleNames, 1, 35)

## Plot boxplot
bitmap(outputfile, type="png16m", height=1200, width = 1200, units = "px")
## Extend bottom margin to 15 lines - left, top and right using defaults
par(mar = c(15, 4, 4, 2) + 0.1)
## Decrease font size if many samples to prevent label overlap
if (length(sampleNames) >= 50) {
  par(cex.axis=0.45)
}
## las=2 makes vertical sample labels
boxplot(as.matrix(data), outline=FALSE, main=title, ylab="intensity", las=2, names=sampleNames)
dev.off()

