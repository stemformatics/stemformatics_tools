#===============================
# Affy normalization using rma
#===============================
# Author: T.Chen, based on GC-RMA script by O.Korn

source(paste(Sys.getenv("S4M_HOME"),"/dataset_scripts/util/R/util.R",sep=""))

### METHODS ###

Usage <- function() {
   print("Usage:  R -f <script_name.R> --args -input=<CEL_input_path> -output=<output_path>")
   print("")
}


### START ###

# Path to directory containing CEL files belonging to target Affy study
input_path <- ARGV.get("input",T)
# Path to output directory
output_path <- ARGV.get("output",T)

# Load required libraries
library('affy')
library('affycoretools')
library('affydata')
library('simpleaffy')

oldcwd <- setwd(input_path)


###############################################################################
#
# Load CEL files
#
###############################################################################

# Load affy data from directory containing CEL files
data <- ReadAffy()
setwd(oldcwd)
sink(paste(output_path,"/array_summary.txt",sep=""))
data
sink()


###############################################################################
#
# RMA Background correction and Normalization
#
###############################################################################

# Perform rma normalization, with non-specific binding adjustments for platform factored in
# Included: background correction, quantile normalization and log2 expression value output.
   # Write raw expression scores which we need for Stemformatics datasets.
   # Perform only background correction which gives us the "raw" expression score.
   # Without this minimal pre-processing, the "raw" scores are pretty meaningless.
   #
   # NOTE: "Raw" here simply means un-normalized. The expression scores must be
   #       calculated using the same approach as rma() function. The expression
   #       score is output in log2 - we cannot access the non-log2 values.
   data.bgonly <- bg.correct.rma(data)
   # Just give us the expression scores, don't perform additional background
   # correction or normalization.
   data.bgexpr <- rma(data.bgonly, background=FALSE, normalize=FALSE)
   write.table(exprs(data.bgexpr), paste(output_path,"/raw_expression.txt",sep=""), sep="\t", quote=F)

   # Now normalize. Unfortunately, background correction is repeated again.
   # rma() is supposed to honour flags like "background=FALSE" but passing in
   # "data.bgonly" will still kick off a second round of background correction.
   # So, until we figure out why this is happening, we have to do the whole thing again
   # with normalization on top of that.
   norm <- rma(data, background=TRUE, normalize=TRUE)
   # This should have given some summary output, not working now for some reason?
   sink(paste(output_path,"/normalized_summary.txt",sep=""))
   summary(norm)
   sink()
   write.table(exprs(norm), paste(output_path,"/normalized_expression.txt",sep=""), sep="\t", quote=F)

   # The "un-tabilizing" (matrix to vector) operation for probe expression scores
   # are transformed externally to this script.

