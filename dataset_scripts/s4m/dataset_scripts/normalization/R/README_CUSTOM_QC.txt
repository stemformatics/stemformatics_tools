
Scripts named "custom_qc_<platform>.R" are automatically executed during QC / normalization
of a microarray dataset.

For valid platform names, see: ../../normalize_dataset.sh

