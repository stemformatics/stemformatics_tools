#!/bin/sh

Usage () {
  echo "$0 <unpacked_expression_file> <output_dir> <expression_threshold>"; echo
  exit
}

INFILE="$1"
OUTPATH="$2"
THRESHOLD="$3"
## TRUE or FALSE
QUIET="$4"

R_FLAGS=""
if [ "$QUIET" = "TRUE" ]; then
    R_FLAGS="--slave -q"
fi

R_BIN="R"
if [ ! -z "$S4M_R" ]; then
    R_BIN="$S4M_R"
fi
if [ "$QUIET" != "TRUE" ]; then
  echo "Using 'R': $R_BIN"
fi
sleep 2

if [ -z "$INFILE" -o -z "$OUTPATH" -o -z "$THRESHOLD" ]; then
  Usage
fi

if [ ! -d "$OUTPATH" ]; then
  mkdir -p "$OUTPATH"
fi
echo -n "$THRESHOLD" > "$OUTPATH/THRESHOLD"

if [ $QUIET != "TRUE" ]; then
  echo "Generating above-threshold subset of probe expression, and calculating median.."; echo
fi
sleep 2
cat ./R/threshold.R | $R_BIN --no-save $R_FLAGS --args -input="$INFILE" -output="$OUTPATH" -threshold="$THRESHOLD" 2> /dev/null

if [ -f "$OUTPATH/normalized_expression_unpacked_above_threshold.txt" ]; then
  if [ "$QUIET" != "TRUE" ]; then
    echo; echo "Thresholding complete."
    echo "Generating above-threshold density plot/s.."
  fi
  if [ "$QUIET" = "TRUE" ]; then
    cat ./R/threshold_plot.R | $R_BIN --no-save $R_FLAGS --args -input="$OUTPATH/normalized_expression_unpacked_above_threshold.txt" -output="$OUTPATH/" -title="Probes_Above_Threshold_$THRESHOLD" > /dev/null 2>&1
  else
    cat ./R/threshold_plot.R | $R_BIN --no-save $R_FLAGS --args -input="$OUTPATH/normalized_expression_unpacked_above_threshold.txt" -output="$OUTPATH/" -title="Probes_Above_Threshold_$THRESHOLD"
  fi
  if [ -f "$OUTPATH/thresholded_density_plot_Probes_Above_Threshold_$THRESHOLD.png" ]; then
    if [ "$QUIET" != "TRUE" ]; then
      echo "done"
    fi
  else
    echo "Fatal Error: Failed to produce above-threshold density plot."
    exit 1
  fi

  ## Generate the left and right shifted plots too. We shouldn't get any errors, so not really checking for errors here.
  if [ -f "$OUTPATH/normalized_expression_unpacked_above_threshold_left_shift.txt" ]; then
    new_threshold="minus_0.5"
    whereis bc > /dev/null 2>&1
    if [ $? -eq 0 ]; then
      new_threshold=`echo "$THRESHOLD - 0.5" | bc`
    fi
    if [ "$QUIET" = "TRUE" ]; then
      cat ./R/threshold_plot.R | $R_BIN --no-save $R_FLAGS --args -input="$OUTPATH/normalized_expression_unpacked_above_threshold_left_shift.txt" -output="$OUTPATH/" -title="Probes_Above_Threshold_${new_threshold}" > /dev/null 2>&1
    else
      cat ./R/threshold_plot.R | $R_BIN --no-save $R_FLAGS --args -input="$OUTPATH/normalized_expression_unpacked_above_threshold_left_shift.txt" -output="$OUTPATH/" -title="Probes_Above_Threshold_${new_threshold}"
    fi
  fi
  if [ -f "$OUTPATH/normalized_expression_unpacked_above_threshold_right_shift.txt" ]; then
    new_threshold="plus_0.5"
    whereis bc > /dev/null 2>&1
    if [ $? -eq 0 ]; then
      new_threshold=`echo "$THRESHOLD + 0.5" | bc`
    fi
    if [ "$QUIET" = "TRUE" ]; then
      cat ./R/threshold_plot.R | $R_BIN --no-save $R_FLAGS --args -input="$OUTPATH/normalized_expression_unpacked_above_threshold_right_shift.txt" -output="$OUTPATH/" -title="Probes_Above_Threshold_${new_threshold}" > /dev/null 2>&1
    else
      cat ./R/threshold_plot.R | $R_BIN --no-save $R_FLAGS --args -input="$OUTPATH/normalized_expression_unpacked_above_threshold_right_shift.txt" -output="$OUTPATH/" -title="Probes_Above_Threshold_${new_threshold}"
    fi
  fi

else
  echo "Fatal Error: There was an error during the thresholding process."
  exit 1
fi

## Extract median value
if [ -f "$OUTPATH/normalized_expression_median_above_threshold.txt" ]; then
  awk '{printf("%2.02f",$2)}' "$OUTPATH/normalized_expression_median_above_threshold.txt" > "$OUTPATH/MEDIAN_ABOVE_THRESHOLD"
else
  echo "Fatal Error: Failed to determine above-threshold expression median"
  exit 1
fi
