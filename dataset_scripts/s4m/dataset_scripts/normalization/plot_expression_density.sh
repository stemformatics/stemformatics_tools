#!/bin/sh
inputfile="$1"
outpath="$2"
plottitle="$3"

R_BIN="R"
if [ ! -z "$S4M_R" ]; then
    R_BIN="$S4M_R"
    echo "Using 'R': $S4M_R"
fi

if [ ! -f "$inputfile" ]; then
  echo "Error: Failed to find input unpacked expression file '$inputfile'!";
  exit 1
fi
if [ ! -d "$outpath" ]; then
  echo "Error: Failed to locate output path '$outpath'!";
  exit 1
fi
if [ -z "$plottitle" ]; then
  echo "Error: Plot title must be given!";
  exit 1
fi
cat ./R/density_plot.R | $R_BIN --no-save -input="$inputfile" -output="$outpath" -title="$plottitle"
if [ $? -eq 0 ]; then
  echo; echo "Density plot created."
fi
