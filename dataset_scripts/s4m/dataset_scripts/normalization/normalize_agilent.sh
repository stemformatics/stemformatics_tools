#!/bin/sh

Usage () {
  echo "$0 <raw_data_path> <output_path> [<extra flags>]"; echo
  exit
}

DATAPATH="$1"
OUTPATH="$2"
EXTRAFLAGS="$3"


R_BIN="R"
if [ ! -z "$S4M_R" ]; then
    R_BIN="$S4M_R"
fi
echo "Using 'R': $R_BIN"
sleep 2

if [ -z "$DATAPATH" -o -z "$OUTPATH" ]; then
  Usage
fi

if [ ! -f "$DATAPATH/ACCESSION" ]; then
  if [ ! -f "$DATAPATH/../../ACCESSION" ]; then
    echo "Error: Required accession file '$DATAPATH/ACCESSION' or '$DATAPATH/../../ACCESSION' not found!"
    echo "Please create it manually or use the 's4m' tool to create it for the given expression dataset."
    exit 1
  else
    ACCESSION=`cat "$DATAPATH/../../ACCESSION" | tr -d [:cntrl:]`
  fi
else
  ACCESSION=`cat "$DATAPATH/ACCESSION" | tr -d [:cntrl:]`
fi

if [ ! -d "$OUTPATH" ]; then
  mkdir -p "$OUTPATH"
fi

echo "Starting normalization and QC output.."; echo

## Fix any potential issues with header field ontology namespacing e.g. as added by ArrayExpress
head -10 "$DATAPATH"/*.txt | grep -P "Feature Extraction Software\:" > /dev/null 2>&1
if [ $? -eq 0 ]; then
  (cd "$DATAPATH" && sed -i -r -e 's/Feature Extraction Software\://g' *.txt)
fi

## Normalize
$R_BIN -f ./R/normalize_agilent.R --args -input="$DATAPATH" -output="$OUTPATH" $EXTRAFLAGS
if [ $? -ne 0 ]; then
  echo "Fatal Error: Normalization failed."; echo
  exit 1
fi
if [ -f "$OUTPATH/raw_expression_nobg.txt" ]; then
  cp "$OUTPATH/raw_expression_nobg.txt" "$OUTPATH/raw_expression.txt"
fi

## Unpack raw and GCRMA 
if [ -f "$OUTPATH/raw_expression.txt" -a -f "$OUTPATH/normalized_expression.txt" ]; then
  echo; echo "Normalization complete."
  echo "Unpacking RMA expression values.."
  # Unpack the expression data table to a format that can be used to build Stemformatics expression files
  # Required to plot
  (cd util && ./unpack_expression_table.sh "$OUTPATH/raw_expression.txt" "$OUTPATH/raw_expression_unpacked.txt")
  (cd util && ./unpack_expression_table.sh "$OUTPATH/normalized_expression.txt" "$OUTPATH/normalized_expression_unpacked.txt")
  if [ $? -ne 0 ]; then
    echo "Fatal Error: Failed to unpack expression values."
    exit 1
  else
    echo "done"
  fi
fi

## Convert and unpack detection calls
if [ -f "$OUTPATH/detection_calls.txt" ]; then
  echo "Unpacking detection calls.."
  ## Note: Using "lumi" detection calls script because it works for us (same boolean format)
  (cd util && ./unpack_detection_calls_lumi.sh "$OUTPATH/detection_calls.txt" "$OUTPATH/detection_calls_unpacked.txt")
  if [ $? -ne 0 ]; then
    echo "Fatal Error: Failed to unpack detection calls."
    exit 1
  else
    echo "done"
  fi
  ## Attempt to create combined expression and detection calls file
  ## so we can plot them side-by-side
  if [ -f "$OUTPATH/detection_calls_unpacked.txt" ]; then
    echo "Combining RMA expression scores and detection calls.."
    pr -Jtm "$OUTPATH/normalized_expression_unpacked.txt" "$OUTPATH/detection_calls_unpacked.txt" > "$OUTPATH/.s4mtmp"
    if [ $? -ne 0 ]; then
      echo "Error: Failed to merge RMA expression and detection calls!"
    else
      cut -f 1,2,3,6 "$OUTPATH/.s4mtmp" > "$OUTPATH/normalized_expression_detection.txt"
      if [ $? -ne 0 ]; then
        echo "Error: Failed to merge RMA expression and detection calls!"
        rm -f "$OUTPATH/normalized_expression_detection.txt"
      else
        ## only remove tmp file if we succeeded
        rm -f "$OUTPATH/.s4mtmp"
        ## Convert 1 to TRUE and 0 to FALSE
        sed -i -r -e 's/1$/TRUE/g' -e 's/0$/FALSE/g' "$OUTPATH/normalized_expression_detection.txt"
      fi
    fi
  fi
fi

