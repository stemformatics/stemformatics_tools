#!/bin/sh

s4m_debug () {
  msg="$1"
  if [ "$S4M_DEBUG" = "true" ]; then
    echo "DEBUG: $msg"
  fi
}
