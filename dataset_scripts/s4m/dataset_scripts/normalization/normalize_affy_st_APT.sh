#!/bin/sh

### INCLUDES ###
. ./includes.sh


### OPTIONS ###
export S4M_DEBUG="true"


### FUNCTIONS ###

Usage () {
  echo "$0 <raw_data_path> <output_path> <platform_annotation_data_path> [opt_flags]"; echo
  echo "Where [opt_flags]:

   'nonorm'        - Skip normalization
"
  exit
}

## TODO: Should probably extract these from the BGP files themselves, so we're not
##       hard-coding platform CDF names and we can flexibly cover future ST
##       array platforms that will use BGP files.
get_antigenomic_probes_for_platform () {
  platform_cdf_name="$1"
  if [ $platform_cdf_name = "MoGene-1_0-st-v1" ]; then
    ## Extracted unique probeset IDs from antigenomic .bgp file
    echo "10338002 10338005 10338006 10338007 10338008 10338009 10338010 10338011 10338012 10338013 10338014 10338015 10338016 10338018 10338019 10338020 10338021 10338022 10338023 10338024 10338027 10338028 10338030 10338031 10338032 10338033 10338034 10338038 10338039 10338040 10338043 10338045 10338046 10338048 10338049 10338050 10338051 10338052 10338053 10338054 10338055 10338057 10338058 10338061 10338062"

  elif [ $platform_cdf_name = "MoGene-2_0-st" ]; then
    echo "17550635 17550637 17550639 17550641 17550643 17550645 17550647 17550649 17550651 17550653 17550655 17550657 17550659 17550661 17550663 17550665 17550667 17550669 17550671 17550673 17550675 17550677 17550679"

  elif [ $platform_cdf_name = "HuGene-1_0-st-v1" ]; then
    echo "7892601 7892698 7892756 7892815 7892916 7892943 7892971 7893026 7893348 7893368 7893409 7893426 7893625 7893643 7893687 7893988 7894132 7894198 7894480 7894592 7894750 7894862 7894881 7894892 7894998 7895078 7895402 7895613 7895644 7895667 7895703 7895732 7895892 7895923 7895949 7895968 7895973 7896216 7896237 7896388 7896419 7896573 7896637 7896658 7896667"

  elif [ $platform_cdf_name = "HuGene-1_1-st-v1" ]; then
    echo "7892601 7892698 7892756 7892815 7892916 7892943 7892971 7893026 7893348 7893368 7893409 7893426 7893625 7893643 7893687 7893988 7894132 7894198 7894480 7894592 7894750 7894862 7894881 7894892 7894998 7895078 7895402 7895613 7895644 7895667 7895703 7895732 7895892 7895923 7895949 7895968 7895973 7896216 7896237 7896388 7896419 7896573 7896637 7896658 7896667"

  elif [ $platform_cdf_name = "HuGene-2_0-st-v1" ]; then
    echo "17127187 17127189 17127191 17127193 17127195 17127197 17127199 17127201 17127203 17127205 17127207 17127209 17127211 17127213 17127215 17127217 17127219 17127221 17127223 17127225 17127227 17127229 17127231"

  elif [ $platform_cdf_name = "HuEx-1_0-st-v2" ]; then
    echo "4057134 4058539 4066064 4067383 4071119 4071304 4072531 4072638 4074569 4074773 4075794 4080210 4080378 4084260 4086961 4087585 4090155 4091726 4092629 4095766 4096241 4097506 4100260 4100584 4101561 4103303 4108074 4108487 4111658 4116679 4117601 4118090 4118211 4118357 4121398 4121718 4123936 4125694 4127657 4131120 4132641 4132887 4133999 4134077 4136112"

  elif [ $platform_cdf_name = "MoEx-1_0-st-v1" ]; then
    echo "4320392 4343118 4467583 4489569 4551876 4554316 4575641 4577314 4608036 4611462 4628124 4699300 4702057 4764598 4808397 4819071 4859711 4885003 4899260 4952027 4959792 4981134 5025999 5031145 5047415 5076452 5156483 5163348 5215184 5297483 5312520 5320125 5321918 5324259 5374576 5379503 5416419 5444402 5476140 5533688 5558252 5562214 5580322 5581768 5615374"

  else
    echo ""
  fi
}


## MAIN ##

DATAPATH="$1"; shift
OUTPATH="$1"; shift
ANNOTATIONPATH="$1"; shift
OPTFLAGS="$@"

APT_BIN="apt-probeset-summarize"
APT_MAS5_BIN="apt-mas5"
APT_CHP_BIN="apt-chp-to-txt"
if [ ! -z "$S4M_APT" ]; then
  APT_BIN="$S4M_APT/$APT_BIN"
  APT_MAS5_BIN="$S4M_APT/$APT_MAS5_BIN"
  APT_CHP_BIN="$S4M_APT/$APT_CHP_BIN"
## No user APT flag given, check global path
else
  fail="false"
  for b in $APT_BIN $APT_MAS5_BIN $APT_CHP_BIN; do
    which "$b" > /dev/null 2>&1
    if [ $? -ne 0 ]; then
      fail="true"
      echo "Error: Failed to find required Affymetrix APT binary '$b'"
    fi
  done
  if [ $fail = "true" ]; then
    echo "Please add Affymetrix APT bundle 'bin' directory to your \$PATH and try again."; echo
    exit 1
  fi
fi

echo "Using Affymetrix Power Tools binaries:"
echo "  $APT_BIN"
echo "  $APT_MAS5_BIN  (if required)"
echo "  $APT_CHP_BIN  (if required)"
sleep 2

if [ -z "$DATAPATH" -o -z "$OUTPATH" -o -z "$ANNOTATIONPATH" ]; then
   Usage
fi

if [ ! -f "$DATAPATH/ACCESSION" ]; then
  if [ ! -f "$DATAPATH/../../ACCESSION" ]; then
    echo "Error: Required accession file '$DATAPATH/ACCESSION' or '$DATAPATH/../../ACCESSION' not found!"
    echo "Please create it manually or use the 's4m' tool to create it for the given expression dataset."
    exit 1
  else
    ACCESSION=`cat "$DATAPATH/../../ACCESSION" | tr -d [:cntrl:]`
  fi
else
  ACCESSION=`cat "$DATAPATH/ACCESSION" | tr -d [:cntrl:]`
fi

if [ ! -d "$OUTPATH" ]; then
  mkdir -p "$OUTPATH"
fi

## Check which annotation data files are present
has_cdf="false"
has_pgf="false"
has_clf="false"
has_bgp="false"
has_qcc="false"

cdfname=`basename $ANNOTATIONPATH`
s4m_debug "Got cdfname=[$cdfname]"
cdf_file=`find $ANNOTATIONPATH -iname "$cdfname.cdf"`
if [ ! -z "$cdf_file" ]; then
  has_cdf="true"
  s4m_debug "Found CDF file"
fi
pgf_file=`find $ANNOTATIONPATH -iname "$cdfname.pgf"`
if [ ! -z "$pgf_file" ]; then
  has_pgf="true"
  s4m_debug "Found PGF file"
fi
clf_file=`find $ANNOTATIONPATH -iname "$cdfname.clf"`
if [ ! -z "$clf_file" ]; then
  has_clf="true"
  s4m_debug "Found CLF file"
fi
## check for background probes file (named <CDF>.bgp OR <CDF>.antigenomic.bgp)
bgp_file=`find $ANNOTATIONPATH -iname "$cdfname.bgp"`
bgp_file_antigenomic=`find $ANNOTATIONPATH -iname "$cdfname.antigenomic.bgp"`
if [ ! -z "$bgp_file_antigenomic" -a -z "$bgp_file" ]; then
  bgp_file="$bgp_file_antigenomic"
fi
if [ ! -z "$bgp_file" ]; then
  has_bgp="true" 
  s4m_debug "Found BGP file; will attempt DABG analysis"
fi
## Check for control probes file
qcc_file=`find $ANNOTATIONPATH -iname "$cdfname.qcc"`
if [ ! -z "$qcc_file" ]; then
  has_qcc="true" 
  s4m_debug "Found QCC file; QC outptus will be produced"
fi
mps_file=`find $ANNOTATIONPATH -iname "$cdfname.mps"`
## Lack of .mps file only an issue for the CLF + PGF combo - if CDF only
## we might get away with it e.g. for the U133AAovAV2 chip
if [ -z "$mps_file" -a "$has_clf" = "true" -a "$has_pgf" = "true" ]; then
   echo "Error: No .mps file found!"
   echo "   (The .mps file is required for gene-level probeset summarization)."
   echo "Cannot continue, exiting."; echo
  exit 1
fi

## If no CDF and no PGF + CLF, we can't proceed
if [ "$has_cdf" = "false" ]; then
  if [ "$has_pgf" = "false" -o "$has_clf" = "false" ]; then
    echo "Error: No CDF or PGF,CLF files found in chip type annotation path: $ANNOTATIONPATH"
    echo "Using cdfname of $cdfname"
    echo "Cannot continue, exiting."; echo
    exit 1
  fi
fi

## Determine CEL file extensions (expect them to be consistent across a set!)
## Need to do this as we pass a "*.<CEL|cel>" target to APT tool and this is case sensitive.
celfiles=`ls $DATAPATH | grep CEL`
if [ "$?" -eq 0 -a ! -z "$celfiles" ]; then
  cel="CEL"
else
  celfiles=`ls $DATAPATH | grep cel`
  if [ ! -z "$celfiles" ]; then
    cel="cel"
  else
    echo "Error: No affymetrix CEL files in source directory: $DATAPATH!"
    exit 1
  fi
fi

## Set APT normalization flags accordingly
if [ $has_pgf = "true" -a $has_clf = "true" ]; then
  s4m_debug "Using PGF, CLF and MPS combo"
  file_opts="-p $pgf_file -c $clf_file -m $mps_file"
  file_opts_without_mps="-p $pgf_file -c $clf_file"
  if [ $has_qcc = "true" ]; then
     file_opts="$file_opts --qc-probesets $qcc_file"
  fi

elif [ "$has_cdf" = "true" ]; then
  file_opts="-d $cdf_file"
  s4m_debug "Using CDF file only"
fi

## Prepare args to pass to raw signal extraction, normalization and "DABG" analysis
raw_apt_args="-a no-trans,pm-only,med-polish $file_opts -o $OUTPATH $DATAPATH/*.$cel"
## RMA with "pm-only" option for ST platform seems to yield better expression curve.
## NOTE: According to APT docs, pre-canned chipstream "rma" equates to:  rma-bg,quant-norm.sketch=0.bioc=true.lowprecision=false.usepm=true.target=0.doavg=false,pm-only,med-polish.FixFeatureEffect=false.UseInputModel=false.FitFeatureResponse=true.expon=false.attenuate=true.l=0.005.h=-1,expr.genotype=false.strand=false.allele-a=false
norm_apt_args="-a rma $file_opts -o $OUTPATH $DATAPATH/*.$cel"
## For normalization run without summarization; required to output antigenomic probeset expressions for our threshold (detection floor) derivation
norm_apt_args_without_mps="-a rma $file_opts_without_mps -o $OUTPATH $DATAPATH/*.$cel"
dabg_apt_args="-a dabg $file_opts --bgp-file $bgp_file -o $OUTPATH $DATAPATH/*.$cel"

## Used to filter out probesets if needed, for example the U133AAofAV2 platform
## which is "massaged" into release chip HT_HG-U133A format for analysis purposes.
## NOTE: Not currently used for ST arrays.
probe_filter_regex=""


#############################################################################
#                                                                           #
# [1] Extract Raw Expression signals                                        #
#                                                                           #
#############################################################################

## The name of the file that APT produces.
## NOTE: We CANNOT change this for current apt-probeset-summarize version, it is automatically
##       generated according to chipstreams applied.
raw_out=".pm-only.med-polish.summary.txt"
raw_report=".pm-only.med-polish.report.txt"
norm_out="rma.summary.txt"
norm_report="rma.report.txt"
dabg_out="dabg.summary.txt"
dabg_report="dabg.report.txt"

echo "$OPTFLAGS" | grep "nonorm" > /dev/null 2>&1
if [ $? -eq 0 ]; then
  echo "SKIPPING raw expression extraction as requested.."; echo

else

  echo "Extracting raw expression signals.."
  s4m_debug "Executing: [$APT_BIN $raw_apt_args]"
  sleep 2
  $APT_BIN $raw_apt_args

  ## Create s4m formatted raw expression output from APT output
  if [ -f "$OUTPATH/$raw_out" ]; then
    grep -v -P "^#" "$OUTPATH/$raw_out" > "$OUTPATH/raw_expression.txt.tmp"
    ## Filter probesets if required
    ## NOTE: Not currently used for ST arrays.
    if [ ! -z "$probe_filter_regex" ]; then
      grep -v -P $probe_filter_regex "$OUTPATH/raw_expression.txt.tmp" > "$OUTPATH/raw_expression.txt.filtered.tmp"
      mv "$OUTPATH/raw_expression.txt.filtered.tmp" "$OUTPATH/raw_expression.txt.tmp"
    fi
    rowcount=`cat "$OUTPATH/raw_expression.txt.tmp" | wc -l`
    header=`head -1 "$OUTPATH/raw_expression.txt.tmp"`
    new_header=`echo "$header" | sed -r -e 's/^probeset_id\t//g'`
    echo "$new_header" > "$OUTPATH/raw_expression.txt"
    tailcount=`expr $rowcount - 1`
    tail -$tailcount "$OUTPATH/raw_expression.txt.tmp" >> "$OUTPATH/raw_expression.txt"
    rm "$OUTPATH/raw_expression.txt.tmp"
    ## rename log file specifically as output of this section (1)
    if [ -f "$OUTPATH/apt-probeset-summarize.log" ]; then
      mv "$OUTPATH/apt-probeset-summarize.log" "$OUTPATH/apt-probeset-summarize.log.1"
    fi
    echo "  Completed extracting raw signals."
  else
    echo "Fatal Error: Raw expression extraction failed (output file '$OUTPATH/$raw_out' not found)!"
    exit 1
  fi
fi


#############################################################################
#                                                                           #
# [2.1] Extract Background (Antigenomic) expression signals                 #
#                                                                           #
#############################################################################

echo "$OPTFLAGS" | grep -P "nonorm" > /dev/null 2>&1
if [ $? -eq 0 ]; then
  echo "SKIPPING exon-level normalization as requested.."; echo

else

  echo; echo "Normalizing (Exon-level) for antigenomic background level extraction..."; echo
  s4m_debug "Executing: [$APT_BIN $norm_apt_args_without_mps]"
  sleep 2
  $APT_BIN $norm_apt_args_without_mps

  ## Create s4m formatted normalized expression output from APT output
  if [ -f "$OUTPATH/$norm_out" ]; then
    grep -v -P "^#" "$OUTPATH/$norm_out" > "$OUTPATH/normalized_expression_exon.txt.tmp"
    ## Filter probesets if required
    if [ ! -z "$probe_filter_regex" ]; then
      grep -v -P $probe_filter_regex "$OUTPATH/normalized_expression_exon.txt.tmp" > "$OUTPATH/normalized_expression_exon.txt.filtered.tmp"
      mv "$OUTPATH/normalized_expression_exon.txt.filtered.tmp" "$OUTPATH/normalized_expression_exon.txt.tmp"
    fi
    ## Reformat header
    rowcount=`cat "$OUTPATH/normalized_expression_exon.txt.tmp" | wc -l`
    header=`head -1 "$OUTPATH/normalized_expression_exon.txt.tmp"`
    new_header=`echo "$header" | sed -r -e 's/^probeset_id\t//g'`
    echo "$new_header" > "$OUTPATH/normalized_expression_exon.txt"
    tailcount=`expr $rowcount - 1`
    tail -$tailcount "$OUTPATH/normalized_expression_exon.txt.tmp" >> "$OUTPATH/normalized_expression_exon.txt"
    rm "$OUTPATH/normalized_expression_exon.txt.tmp"
    echo "Unpacking Exon-level normalized expression values.."
    cd util && ./unpack_expression_table.sh "$OUTPATH/normalized_expression_exon.txt" "$OUTPATH/normalized_expression_exon_unpacked.txt"
    if [ $? -ne 0 ]; then
      echo "Fatal Error: Failed to unpack Exon-level expression values."
      exit 1
    else
      cd ..
      echo "  done"
    fi
    ## rename log file specifically as output of this section (2.1)
    if [ -f "$OUTPATH/apt-probeset-summarize.log" ]; then
      mv "$OUTPATH/apt-probeset-summarize.log" "$OUTPATH/apt-probeset-summarize.log.2.1"
    fi
    echo "  Completed Exon-level normalization."
  else
    echo "Fatal Error: Exon-level normalization failed (output file '$OUTPATH/$norm_out' not found)!"
    exit 1
  fi
  ## Get subset of normalized probe expressions that correspond to Antigenomic probes,
  ## we'll use these to derive the detection "floor" (as median of expressions across
  ## all antigenomic probes)
  antigenomic_probes=`get_antigenomic_probes_for_platform "$cdfname"`
  s4m_debug "Using antigenomic probes: [$antigenomic_probes]"
  if [ -z "$antigenomic_probes" ]; then
    echo "Fatal Error: Failed to detrmine CDF platform name; needed for detection-floor derivation using antigenomic probes."
  fi
  ## Remove existing antigenomic probes file, if any
  if [ -f "$OUTPATH/antigenomic_normalized_expression_subset.txt" ]; then
    rm "$OUTPATH/antigenomic_normalized_expression_subset.txt"
  fi
  ## Now get expression values for antigenomic probes
  for probe in $antigenomic_probes; do
    grep -P "\t$probe\t" "$OUTPATH/normalized_expression_exon_unpacked.txt" >> "$OUTPATH/antigenomic_normalized_expression_subset.txt"
    if [ $? -eq 1 ]; then
      s4m_debug "Antigenomic probe $probe not found in normalized Exon-level expression results."
    fi
  done
  ## Write antigenomic median log2 normalized expression value
  if [ -s "$OUTPATH/antigenomic_normalized_expression_subset.txt" ]; then
    echo "Calculating median of antigenomic probe expression to use as detection floor.."; echo
    R_BIN="R"
    if [ ! -z "$S4M_R" ]; then
        R_BIN="$S4M_R"
    fi
    echo "Using 'R': $R_BIN"
    sleep 2
    $R_BIN -f ./R/expression_median.R --no-save --args -input="$OUTPATH/antigenomic_normalized_expression_subset.txt" -output="$OUTPATH/antigenomic_median.txt" > /dev/null
    if [ -f "$OUTPATH/antigenomic_median.txt" ]; then
      echo; echo "Antigenomic median derivation complete."
    else
      echo "Fatal Error: There was an error during the antigenomic probe expression median derivation process."
      exit 1
    fi
    ## Extract median value from R output
    if [ -f "$OUTPATH/antigenomic_median.txt" ]; then
      awk '{printf("%2.02f",$2)}' "$OUTPATH/antigenomic_median.txt" > "$OUTPATH/ANTIGENOMIC_MEDIAN"
    else
      echo "Fatal Error: Failed to determine median of antigenomic (background) probe expression"
      exit 1
    fi
    ## clean up
    mkdir -p "$OUTPATH/threshold_derivation"
    mv -f "$OUTPATH/antigenomic_median.txt" "$OUTPATH/antigenomic_normalized_expression_subset.txt" "$OUTPATH/normalized_expression_exon.txt" "$OUTPATH/normalized_expression_exon_unpacked.txt" "$OUTPATH/$norm_out" "$OUTPATH/$norm_report" "$OUTPATH/threshold_derivation/"

  else
    echo "Error: Failed to extract expression values for antigenomic probes"
  fi
fi


#############################################################################
#                                                                           #
# [2.2] Extract Normalized Gene-Level Expression                            #
#                                                                           #
#############################################################################

echo "$OPTFLAGS" | grep -P "nonorm" > /dev/null 2>&1
if [ $? -eq 0 ]; then
  echo "SKIPPING normalization as requested.."; echo

else
  echo; echo "Normalizing with Gene-level summarization..."; echo
  s4m_debug "Executing: [$APT_BIN $norm_apt_args]"
  sleep 2
  $APT_BIN $norm_apt_args
fi

## Create s4m formatted normalized expression output from APT output
if [ -f "$OUTPATH/$norm_out" ]; then
  grep -v -P "^#" "$OUTPATH/$norm_out" > "$OUTPATH/normalized_expression.txt.tmp"
  ## Filter probesets if required
  if [ ! -z "$probe_filter_regex" ]; then
    grep -v -P $probe_filter_regex "$OUTPATH/normalized_expression.txt.tmp" > "$OUTPATH/normalized_expression.txt.filtered.tmp"
    mv "$OUTPATH/normalized_expression.txt.filtered.tmp" "$OUTPATH/normalized_expression.txt.tmp"
  fi
  ## Reformat header
  rowcount=`cat "$OUTPATH/normalized_expression.txt.tmp" | wc -l`
  header=`head -1 "$OUTPATH/normalized_expression.txt.tmp"`
  new_header=`echo "$header" | sed -r -e 's/^probeset_id\t//g'`
  echo "$new_header" > "$OUTPATH/normalized_expression.txt"
  samplecount=`awk -F'\t' '{print NF}' "$OUTPATH/normalized_expression.txt"`
  tailcount=`expr $rowcount - 1`

  ## Task #1570: If we want to REMOVE bad probes, this is one way to do it.
  ##
  ## Not really what we want, though, because it will cause a probe count mismatch when we combine with detection calls etc
  ## and if want to compare a good raw_expression.txt with normalized_expression.txt
  ##
  ## The only thing we can really do instead is REPLACE bad probe rows with blank values.
  ## ===
  ##
  ## Print probe expression rows excluding those that don't contain the number of fields we expect

  #tail -$tailcount "$OUTPATH/normalized_expression.txt.tmp" | awk -F'\t' -v scount=$samplecount '{if (NR==1) { print $0 } else if (NR >= 1) { if (NF == scount+1) {print $0}} }'  >> "$OUTPATH/normalized_expression.txt"

  ## Blank out all values for probe rows that have too few or too many fields (not 1:1 value to sample)
  ## We do this so we can maintain probe count match across different files.
  tail -$tailcount "$OUTPATH/normalized_expression.txt.tmp" | awk -F'\t' -v scount=$samplecount 'BEGIN{ ORS="" } {
    if (NF == scount+1) {
      print $0
    } else {
      print $1;
      for (i=1; i<=scount; i++) {
	print "\t";
      }
    }
    print "\n"
  }'  >> "$OUTPATH/normalized_expression.txt"

  rm "$OUTPATH/normalized_expression.txt.tmp"
  ## rename log file specifically as output of this section (2.2)
  if [ -f "$OUTPATH/apt-probeset-summarize.log" ]; then
    mv "$OUTPATH/apt-probeset-summarize.log" "$OUTPATH/apt-probeset-summarize.log.2.2"
  fi
  echo "  Completed Normalization."

else
  echo "Fatal Error: Normalization failed (output file '$OUTPATH/$norm_out' not found)!"
  exit 1
fi


#############################################################################
#                                                                           #
# [3] Perform "DABG" Detection Calling (if have BGP file)                   #
#                                                                           #
#############################################################################

echo "$OPTFLAGS" | grep -P "nonorm" > /dev/null 2>&1
if [ $? -eq 0 ]; then
  echo "SKIPPING DABG calling as requested.."; echo

else

  if [ $has_bgp = "true" ]; then
    s4m_debug "Executing: [$APT_BIN $dabg_apt_args]"
    $APT_BIN $dabg_apt_args
  fi

  if [ -f "$OUTPATH/$dabg_out" ]; then
    grep -v -P "^#" "$OUTPATH/$dabg_out" > "$OUTPATH/detection_scores.txt"
    sed -i -r -e 's/^probeset_id\t//g' "$OUTPATH/detection_scores.txt"
  fi
fi


#############################################################################
#                                                                           #
# [4] Post-Normalization routines                                           #
#                                                                           #
#############################################################################

## Change into normalization util scripts path
cd util

## Unpack raw and RMA expression files 
if [ -f "$OUTPATH/raw_expression.txt" -a -f "$OUTPATH/normalized_expression.txt" ]; then
  echo; echo "Unpacking RMA expression values.."
  # Unpack the expression data table to a format that can be used to build Stemformatics expression files
  # Required to plot
  ./unpack_expression_table.sh "$OUTPATH/raw_expression.txt" "$OUTPATH/raw_expression_unpacked.txt"
  if [ $? -ne 0 ]; then
    echo "Fatal Error: Failed to unpack expression values."
    exit 1
  fi
  echo "Unpacking normalized expression values.."
  ./unpack_expression_table.sh "$OUTPATH/normalized_expression.txt" "$OUTPATH/normalized_expression_unpacked.txt"
  if [ $? -ne 0 ]; then
    echo "Fatal Error: Failed to unpack expression values."
    exit 1
  else
    echo "  done"
  fi
fi

## Unpack detection scores, if we have them
if [ -f "$OUTPATH/detection_scores.txt" ]; then
  echo "Unpacking detection p-val scores.."
  ./unpack_expression_table.sh  "$OUTPATH/detection_scores.txt" "$OUTPATH/detection_scores_unpacked.txt"
  if [ $? -ne 0 ]; then
    echo "Fatal Error: Failed to unpack detection call values."
    exit 1
  else
    echo "  done"
  fi
  ## Create boolean detection calls from p-val detection scores
  echo "Creating boolean detection calls from p-val detection scores.."
  ./detection_scores_dabg_to_boolean.sh "$OUTPATH/detection_scores.txt" "$OUTPATH/detection_calls_unpacked_boolean.txt"
  if [ $? -ne 0 ]; then
    echo "Fatal Error: Failed to produce boolean detection calls from p-val scoress."
    exit 1
  else
    echo "  done"
  fi
fi

## Combine normalized expression and boolean detection calls
if [ -f "$OUTPATH/detection_calls_unpacked_boolean.txt" ]; then
  echo "Combining normalized expression scores and detection calls.."
  pr -Jtm "$OUTPATH/normalized_expression_unpacked.txt" "$OUTPATH/detection_calls_unpacked_boolean.txt" > "$OUTPATH/.s4mtmp"
  if [ $? -ne 0 ]; then
    echo "Error: Failed to merge expression and detection calls!"
    rm -f "$OUTPATH/.s4mtmp"
  else
    cut -f 1,2,3,6 "$OUTPATH/.s4mtmp" > "$OUTPATH/normalized_expression_detection.txt"
    if [ $? -ne 0 ]; then
    echo "Error: Failed to merge expression and detection calls!"
    rm -f "$OUTPATH/.s4mtmp" "$OUTPATH/normalized_expression_detection.txt"
    fi
    rm -f "$OUTPATH/.s4mtmp"
  fi
  echo "Done"
fi

## Back up to normalization scripts directory
cd $OLDPWD
