#!/usr/bin/perl -w
## Takes a "long", unpacked format expression file and packs it into a table.
##
## Input format:
##
## 	sample_id	probe_idX	value
## 	sample_id	probe_idY	value
## 	sample_id	probe_idZ	value
## 	..
## 	sample_id2	probe_idX	value
## 	sample_id2	probe_idY	value
## 	sample_id2	probe_idZ	value
## 	..
## 	sample_id3	probe_idX	value
## 	sample_id3	probe_idY	value
## 	sample_id3	probe_idZ	value
##
## Output format (data frame):
##
##			sample_id	sample_id2	sampleid_3
##	probe_idX	value		value		value
##	probe_idY	value		value		value
##	probe_idZ	value		value		value
##

sub Usage () {
   print STDERR "Usage: $0 <unpacked_expression_file>\n\n";
}

if (scalar(@ARGV) < 1) {
  print 'Error: Must supply input file argument!';
  Usage();
  exit 1
}

my $input = $ARGV[0];

die "Error: Could not open file '$input'!" unless open(INPUTFILE, "<", $input);
my @rows = <INPUTFILE>;
close(INPUTFILE);

my %probe_rows = ();
my @sample_ids = ();
my @probe_order = ();

foreach (@rows) {
   chomp;
   my @tokens = split(/\t/, $_);
   next unless $tokens[0] =~ /\w+/;

   if (! grep(/^$tokens[0]$/, @sample_ids)) {
      push(@sample_ids, $tokens[0]);
   }
   
   #if (! grep(/^$tokens[1]$/, keys(%probe_rows))) {
   if (! exists($probe_rows{$tokens[1]})) {
      $probe_rows{$tokens[1]} = [$tokens[2]];
      push(@probe_order, $tokens[1]);
   } else {
      push(@{$probe_rows{$tokens[1]}}, $tokens[2]);
   }
}

### DEBUG ###
#print STDERR "Length of sample_ids: " . scalar(@sample_ids) . "\n";
#print STDERR "Length of keys(%probe_rows): " . scalar(keys(%probe_rows)) . "\n";
### END ###

print join("\t", @sample_ids) . "\n";

foreach my $key (@probe_order) {
   print "$key\t";
   print join("\t", @{$probe_rows{$key}}) . "\n";
}
