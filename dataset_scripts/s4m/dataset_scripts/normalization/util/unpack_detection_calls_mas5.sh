#!/bin/sh
## Unpacks MAS5 detection table, as produced by simpleaffy "detection.p.val()" method.

inputfile="$1"
outputfile="$2"
tmpdir=`dirname "$outputfile"`

if [ -z "$inputfile" ]; then
  echo "$0: Error: Must provide name of input file containing probeset detection call table!"; echo
  exit 1
fi
if [ -z "$outputfile" ]; then
  echo "$0: Error: Must provide name of output unpacked detection call file!"; echo
  exit 1
fi

./unpack_detection_calls_mas5.pl "$inputfile" | sort -T "$tmpdir" > "$outputfile"

## Create present-only version (keep only "P" probes, remove "M" and "A")
dirbase=`dirname "$inputfile"`
filebase=`basename "$outputfile" .txt`
presentonly="${dirbase}/${filebase}_presentonly.txt"
grep -P "P$" "$outputfile" > "$presentonly"

## Create TRUE/FALSE boolean version where P=TRUE, M=FALSE, A=FALSE
cat "$outputfile" | sed -r -e 's/P$/TRUE/g' -e 's/(M|A)$/FALSE/g' > "${dirbase}/${filebase}_boolean.txt"
