#!/bin/sh
## Unpacks lumi detection calls (boolean) table, as produced in Illumina script

inputfile="$1"
outputfile="$2"
tmpdir=`dirname "$outputfile"`

if [ -z "$inputfile" ]; then
  echo "$0: Error: Must provide name of input file containing probeset detection call table!"; echo
  exit 1
fi
if [ -z "$outputfile" ]; then
  echo "$0: Error: Must provide name of output unpacked detection call file!"; echo
  exit 1
fi

## Yes, this script name is correct. Does the same job; no need to reinvent the wheel
./unpack_detection_scores.pl "$inputfile" | sort -T "$tmpdir" > "$outputfile"

