#!/usr/bin/perl -w
if (scalar(@ARGV) < 1) {
  print 'Error: Must supply input file argument!';
  exit 1;
}

my $input = $ARGV[0];

my $samples_header=`head -1 "$input" | sed -r -e 's/\ /\_/g' -e 's/\"//g'`;
my @samples = split(/\s/, $samples_header);
die "Error: Could not open file '$input'!" unless open(INPUTFILE, "<", $input);
my @rows = <INPUTFILE>;

# Remove header
shift(@rows);
close(INPUTFILE);

foreach (@rows) {
   chomp;
   my @tokens = split(/\s/, $_);
   next unless $tokens[0] =~ /\w+/;

   my $probe_id = $tokens[0];
   $probe_id =~ s/\"//g;
   for (my $i = 1; $i < scalar(@tokens); $i++) {
      $tokens[$i] =~ s/\"//g;
      $sample_id = $samples[$i-1];
      $sample_id =~ s/\"//g;
      if ($sample_id =~ /\.present/) {
         $sample_id =~ s/^call\.//;
         $sample_id =~ s/\.present$//;
         ## simpleaffy method "detection.p.val()" munges sample IDs by replacing dashes with dots,
         ## so do the reverse here to "fix" it up. Obviously this may cause issues for sample
         ## names originally containing dots..
         $sample_id =~ s/\.([^cel]+)/\-$1/gi;
         print $sample_id . "\t" . $probe_id . "\t" . $tokens[$i] . "\n";
      }
   }
}
