#!/bin/sh
## Unpacks Illumina detection scores table, as produced by lumi "detection()" method.

inputfile="$1"
outputfile="$2"
tmpdir=`dirname "$outputfile"`

if [ -z "$inputfile" ]; then
  echo "$0: Error: Must provide name of input file containing probeset detection scores!"; echo
  exit 1
fi
if [ -z "$outputfile" ]; then
  echo "$0: Error: Must provide name of output unpacked detection scores file!"; echo
  exit 1
fi

./unpack_detection_scores.pl "$inputfile" | sort -T "$tmpdir" > "$outputfile"

## Create TRUE/FALSE boolean version
## <TODO>

## Create present-only version (keep only "TRUE" expressed probes)
## <TODO>

