#!/bin/sh
## Unpacks Affy DABG detection p-val table, as produced by Affymetrix Power Tools (APT)

inputfile="$1"
outputfile="$2"
pval_threshold="0.01"
tmpdir=`dirname "$outputfile"`

if [ -z "$inputfile" ]; then
  echo "$0: Error: Must provide name of input file containing probeset detection p-val table!"; echo
  exit 1
fi
if [ -z "$outputfile" ]; then
  echo "$0: Error: Must provide name of output unpacked detection call file!"; echo
  exit 1
fi

./detection_scores_dabg_to_boolean.pl "$inputfile" "$pval_threshold" | sort -T "$tmpdir" > "$outputfile"

