#!/usr/bin/perl -w
if (scalar(@ARGV) < 1) {
  print 'Error: Must supply input file argument!';
  exit 1;
}

my $input = $ARGV[0];

my $samples_header=`head -1 "$input" | sed -r -e 's/\ /\_/g' -e 's/\"//g'`;
my @samples = split(/\s/, $samples_header);
## Sometimes the first sample header column is null (e.g. from output of
## lumi normalization). In that case, remove it
shift @samples unless length($samples[0]);

die "Error: Could not open file '$input'!" unless open(INPUTFILE, "<", $input);
my @rows = <INPUTFILE>;

# Remove header
shift(@rows);
close(INPUTFILE);

foreach (@rows) {
   chomp;
   my @tokens = split(/\s/, $_);
   next unless $tokens[0] =~ /\w+/;

   my $probe_id = $tokens[0];
   $probe_id =~ s/\"//g;
   for (my $i = 1; $i < scalar(@tokens); $i++) {
      $tokens[$i] =~ s/\"//g;
      $sample_id = $samples[$i-1];
      $sample_id =~ s/\"//g;
      print $sample_id . "\t" . $probe_id . "\t" . $tokens[$i] . "\n";
   }
}
