#!/bin/sh
cd ".."; DATADIR=`pwd`; cd $OLDPWD
SCRIPTDIR="./include"
TMPDIR="/tmp"
LOGDIR="$TMPDIR"

CHIP_TYPE="$1"
PROBE_AFFINITY_OR_AROMA_DIR="$2"

until [ -z "$1" ]; 
do
  if [ "$1" = "-R" ]; then
     shift
     export S4M_R="$1"
     break
  fi
  shift
done


Usage () {
   echo
   echo "Usage: $0 <platform_chip_type> [<probe_affinity_or_aroma_working_directory>] [-R <R binary override]"
   echo
   echo "where: 'platform_chip_type' is numerical chip_type identifier as specified"
   echo "       in \$S4M_HOME/etc/assay_platforms.txt"
   echo
   echo "       '<probe_affinity_or_aroma_working_directory>' is either:"
   echo "           1) for GeneChip arrays, a read-write location of precomputed R binary probe affinity data files  OR"
   echo "           2) for ST arrays, the Aroma Affymetrix working directory path"
   echo
   echo "           NOTE: This argument is ignored for Illumina arrays."
   echo
   exit
}

if [ $# -lt 1 ]; then
   Usage
fi


PLATFORM=""
PROBE_AFFINITY_PLATFORM=""
PLATFORM_CDF=""
PLATFORM_CDF_CUSTOM=""

## TODO: Don't hard code this, the chip_type to probe affinity name needs to be stored
##       in S4M_HOME/etc/probe_affinities.txt or similar location
##
## 1	Mouse	Illumina	MouseWG-6	V2
## 6	Human	Illumina	HumanWG-6	V3
## 7	Human	Illumina	HumanWG-6	V2
## 11	Mouse	Affymetrix	Mouse430_2	mm9
## 16	Human	Affymetrix	HG-U133_2	hg9
## 21	Human	Illumina	HumanRef-8	V2
## 26	Human	Illumina_v1	HumanRef-8	V1
## 31	Mouse	Affymetrix	MoGene-1_0-ST	V1
## 36	Human	Affymetrix	HT-HG-U133A	hg9
##
## Probe affinity names from: http://www.bioconductor.org/packages/2.7/BiocViews.html#___AffymetrixChip
## NOTE: Affinity names must match "clean" CDF names as per simpleAffy spec (refer 'simpleAffy.pdf' vignette)
## Easy way to check: Look for appropriately named "<platform>cdf.qcdef" files in R lib/simpleaffy/extdata

case $CHIP_TYPE in
    11)
       PROBE_AFFINITY_PLATFORM="mouse4302"
       PLATFORM="Affymetrix_GeneChip"
       ;;
    16)
       PROBE_AFFINITY_PLATFORM="hgu133a2"
       PLATFORM="Affymetrix_GeneChip"
       ;;
    21)
       PLATFORM="Illumina"
       ;;
    26)
       PLATFORM="Illumina"
       ;;
    ## TODO: The "ST" arrays should be done using aroma.affymetrix, not simpleaffy.
    ##       The probe affinity platform might not be needed in that case.
    31)
       PROBE_AFFINITY_PLATFORM="mogene10stv1"
       PLATFORM="Affymetrix_ST"
       PLATFORM_CDF="MoGene-1_0-st-v1"
       #PLATFORM_CDF_CUSTOM="mm9"
       ;;
    36)
       PROBE_AFFINITY_PLATFORM="hthgu133a"
       PLATFORM="Affymetrix_GeneChip"
       ;;
    1)
       ## TODO
       PLATFORM="Illumina"
       ;;
    6)
       ## TODO
       PLATFORM="Illumina"
       ;;
    7)
       ## TODO
       PLATFORM="Illumina"
       ;;
    *)
       ;;
esac

if [ -z "$PLATFORM" ]; then
   echo "Error: Unknown chip type '$CHIP_TYPE'!"; echo
   exit 1
fi
if [ -z "$PROBE_AFFINITY_PLATFORM" -a "$PLATFORM" = "Affymetrix_GeneChip" ]; then
   echo "Error: Affymetrix chip type '$CHIP_TYPE' is not currently supported!"; echo
   exit 1
fi

## Call targeted normalization script based on platform
if [ "$PLATFORM" = "Affymetrix_GeneChip" ]; then
   cd ./normalization && ./normalize_affy_genechip.sh "$DATADIR/source/raw" "$DATADIR/source/normalized" "$PROBE_AFFINITY_OR_AROMA_DIR" "$PROBE_AFFINITY_PLATFORM"
   cd $OLDPWD

elif [ "$PLATFORM" = "Affymetrix_ST" ]; then
   if [ -z "$PLATFORM_CDF_CUSTOM" ]; then
      cd ./normalization && ./normalize_affy_st.sh "$DATADIR/source/raw" "$DATADIR/source/normalized" "$PROBE_AFFINITY_OR_AROMA_DIR" "$PLATFORM_CDF"
   else
      cd ./normalization && ./normalize_affy_st.sh "$DATADIR/source/raw" "$DATADIR/source/normalized" "$PROBE_AFFINITY_OR_AROMA_DIR" "$PLATFORM_CDF" "$PLATFORM_CDF_CUSTOM"
   fi

elif [ "$PLATFORM" = "Illumina" ]; then
   ## NOTE: See default Illumina input file; expecting "$DATADIR/source/raw/beadstudio.txt"
   cd ./normalization && ./normalize_illumina.sh "$DATADIR" "$DATADIR/source/raw/beadstudio.txt" "$DATADIR/source/normalized" 

else
   echo "Sorry, the given platform '$PLATFORM' is currently unsupported!"; echo
   exit 1
fi

