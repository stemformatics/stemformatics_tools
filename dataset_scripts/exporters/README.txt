dataset_dump.sh:
	Dumps data and metadata files for an s4m dataset from a target PostgreSQL Stemformatics DB instance.
	(DB connection details passed via "pgpassfile" argument. Run without args for usage.)

	The output files correspond to a 's4m' dataset format version, read script header for version
	compatibility.

	Example "pgpassfile":

	$ cat .pgpassfile
	localhost:5432:portal_db:portaluser:portalpass
