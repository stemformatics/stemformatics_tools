#!/bin/sh
## Dumps a dataset (data and metadata only) from Stemformatics database.
## Dumps in s4m format version 1.3 [August 2011]

write_metastore () {
  attrib="$1"
  value="$2"
  metafile="$3"
  echo "$attrib=$value" >> "$metafile"
}


dataset_metadata_to_METASTORE () {
  dsdir="$1"
  assayplatform="$dsdir/assay_platform.txt"
  dsfile="$dsdir/dataset.txt"
  dsmetafile="$dsdir/dataset_metadata.txt"
  metastore="$dsdir/METASTORE"

  accession_id=`grep -P "\tAccession\t" "$dsmetafile" | awk -F'\t' '{print $3}'`
  write_metastore "accession_id" "$accession_id" "$metastore"

  array=`grep -P "\tPlatform\t" "$dsmetafile" | awk -F'\t' '{print $3}'`
  write_metastore "array" "$array" "$metastore"

  array_manufacturer=`grep -P "\tAssay Manufacturer\t" "$dsmetafile" | awk -F'\t' '{print $3}'`
  write_metastore "array_manufacturer" "$array_manufacturer" "$metastore"

  array_platform=`grep -P "\tAssay Platform\t" "$dsmetafile" | awk -F'\t' '{print $3}'`
  write_metastore "array_platform" "$array_platform" "$metastore"

  array_version=`grep -P "\tAssay Version\t" "$dsmetafile" | awk -F'\t' '{print $3}'`
  write_metastore "array_version" "$array_version" "$metastore"
  
  array_probe_count=`grep -P "\tprobeCount\t" "$dsmetafile" | awk -F'\t' '{print $3}'`
  write_metastore "array_probe_count" "$array_probe_count" "$metastore"

  dataset_accession_ae=`grep -P "\tAE Accession\t" "$dsmetafile" | awk -F'\t' '{print $3}'`
  write_metastore "dataset_accession_ae" "$dataset_accession_ae" "$metastore"
  
  dataset_accession_geo=`grep -P "\tGeo Accession\t" "$dsmetafile" | awk -F'\t' '{print $3}'`
  write_metastore "dataset_accession_geo" "$dataset_accession_geo" "$metastore"

  dataset_contributor=`grep -P "\tContributor Name\t" "$dsmetafile" | awk -F'\t' '{print $3}'`
  write_metastore "dataset_contributor" "$dataset_contributor" "$metastore"
  
  dataset_contributor_affiliation=`grep -P "\tAffiliation\t" "$dsmetafile" | awk -F'\t' '{print $3}'`
  write_metastore "dataset_contributor_affiliation" "$dataset_contributor_affiliation" "$metastore"

  dataset_contributor_email=`grep -P "\tContributor Email\t" "$dsmetafile" | awk -F'\t' '{print $3}'`
  write_metastore "dataset_contributor_email" "$dataset_contributor_email" "$metastore"
  
  dataset_handle=`awk -F'\t' '{print $3}' "$dsfile"`
  write_metastore "dataset_handle" "$dataset_handle" "$metastore"
  
  dataset_release_date=`grep -P "\tRelease Date\t" "$dsmetafile" | awk -F'\t' '{print $3}'`
  write_metastore "dataset_release_date" "$dataset_release_date" "$metastore"
 
  ## This field is not used in S4M format version 1.3, even if it exists in DB for older datasets 
  #dataset_title=`grep -P "\tTitle\t" "$dsmetafile" | awk -F'\t' '{print $3}'`
  #write_metastore "dataset_title" "$dataset_title" "$metastore"

  experiment_type=`grep -P "\tExperimental Design\t" "$dsmetafile" | awk -F'\t' '{print $3}'`
  write_metastore "experiment_type" "$experiment_type" "$metastore"

  max_replicates=`grep -P "\tmaxReplicates\t" "$dsmetafile" | awk -F'\t' '{print $3}'`
  write_metastore "max_replicates" "$max_replicates" "$metastore"
  
  min_replicates=`grep -P "\tminReplicates\t" "$dsmetafile" | awk -F'\t' '{print $3}'`
  write_metastore "min_replicates" "$min_replicates" "$metastore"
  
  postnorm_expression_median=`grep -P "\tmedianDatasetExpression\t" "$dsmetafile" | awk -F'\t' '{print $3}'`
  write_metastore "postnorm_expression_median" "$postnorm_expression_median" "$metastore"
  
  postnorm_expression_threshold=`grep -P "\tdetectionThreshold\t" "$dsmetafile" | awk -F'\t' '{print $3}'`
  write_metastore "postnorm_expression_threshold" "$postnorm_expression_threshold" "$metastore"
  
  postnorm_probes_detected=`grep -P "\tprobesDetected\t" "$dsmetafile" | awk -F'\t' '{print $3}'`
  write_metastore "postnorm_probes_detected" "$postnorm_probes_detected" "$metastore"
  
  publication_authors=`grep -P "\tAuthors\t" "$dsmetafile" | awk -F'\t' '{print $3}'`
  write_metastore "publication_authors" "$publication_authors" "$metastore"
  
  publication_citation=`grep -P "\tPublication Citation\t" "$dsmetafile" | awk -F'\t' '{print $3}'`
  write_metastore "publication_citation" "$publication_citation" "$metastore"
  
  publication_contact=`grep -P "\tContact Name\t" "$dsmetafile" | awk -F'\t' '{print $3}'`
  write_metastore "publication_contact" "$publication_contact" "$metastore"
  
  publication_contact_email=`grep -P "\tContact Email\t" "$dsmetafile" | awk -F'\t' '{print $3}'`
  write_metastore "publication_contact_email" "$publication_contact_email" "$metastore"
  
  publication_description=`grep -P "\tDescription\t" "$dsmetafile" | awk -F'\t' '{print $3}'`
  write_metastore "publication_description" "$publication_description" "$metastore"
  
  publication_title=`grep -P "\tPublication Title\t" "$dsmetafile" | awk -F'\t' '{print $3}'`
  write_metastore "publication_title" "$publication_title" "$metastore"
  
  publication_pubmed_id=`grep -P "\tPubMed ID\t" "$dsmetafile" | awk -F'\t' '{print $3}'`
  write_metastore "publication_pubmed_id" "$publication_pubmed_id" "$metastore"
  
  s4m_chip_type=`awk -F'\t' '{print $1}' "$assayplatform"`
  write_metastore "s4m_chip_type" "$s4m_chip_type" "$metastore"
  
  s4m_curator=`grep -P "\ts4mCurator\t" "$dsmetafile" | awk -F'\t' '{print $3}'`
  write_metastore "s4m_curator" "$s4m_curator" "$metastore"
 
  s4m_curation_date=`grep -P "\ts4mCurationDate\t" "$dsmetafile" | awk -F'\t' '{print $3}'`
  write_metastore "s4m_curation_date" "$s4m_curation_date" "$metastore"
  
  s4m_dataset_id=`awk -F'\t' '{print $1}' "$dsfile"`
  write_metastore "s4m_dataset_id" "$s4m_dataset_id" "$metastore"

  species_long=`grep -P "\tOrganism\t" "$dsmetafile" | awk -F'\t' '{print $3}'`
  write_metastore "species_long" "$species_long" "$metastore"

  species_short=`awk -F'\t' '{print $2}' "$assayplatform"`
  write_metastore "species_short" "$species_short" "$metastore"

  technology_used=`grep -P "\ttechnologyUsed\t" "$dsmetafile" | awk -F'\t' '{print $3}'` 
  write_metastore "technology_used" "$technology_used" "$metastore"
 
  top_genes=`grep -P "\ttopDifferentiallyExpressedGenes\t" "$dsmetafile" | awk -F'\t' '{print $3}'` 
  write_metastore "top_genes" "$top_genes" "$metastore"
 
  sampleTypeDisplayOrder=`grep -P "\tsampleTypeDisplayOrder\t" "$dsmetafile" | awk -F'\t' '{print $3}'` 
  write_metastore "sampleTypeDisplayOrder" "$sampleTypeDisplayOrder" "$metastore"

  sampleTypeDisplayGroups=`grep -P "\tsampleTypeDisplayGroups\t" "$dsmetafile" | awk -F'\t' '{print $3}'` 
  write_metastore "sampleTypeDisplayGroups" "$sampleTypeDisplayGroups" "$metastore"
 
  limitSortBy=`grep -P "\tlimitSortBy\t" "$dsmetafile" | awk -F'\t' '{print $3}'` 
  write_metastore "limitSortBy" "$limitSortBy" "$metastore"
 
  cells_samples_assayed=`grep -P "\tcellsSamplesAssayed\t" "$dsmetafile" | awk -F'\t' '{print $3}'` 
  write_metastore "cells_samples_assayed" "$cells_samples_assayed" "$metastore"
 
  cell_surface_antigen_expression=`grep -P "\tcellSurfaceAntigenExpression\t" "$dsmetafile" | awk -F'\t' '{print $3}'` 
  write_metastore "cell_surface_antigen_expression" "$cell_surface_antigen_expression" "$metastore"
 
  cellular_protein_expression=`grep -P "\tcellularProteinExpression\t" "$dsmetafile" | awk -F'\t' '{print $3}'` 
  write_metastore "cellular_protein_expression" "$cellular_protein_expression" "$metastore"
 
  nucleic_acid_extract=`grep -P "\tnucleicAcidExtract\t" "$dsmetafile" | awk -F'\t' '{print $3}'` 
  write_metastore "nucleic_acid_extract" "$nucleic_acid_extract" "$metastore"
}


## MAIN ##

dsid="$1"
if [ -z "$1" -o -z "$2" ]; then
  echo "Usage: $0 <dsid> <pgpassfile>"; echo
  exit
fi
export PGPASSFILE="$2"

if [ ! -d "$1" ]; then
  mkdir $1
fi

## Determine PostgreSQL connection details from pgpassfile
host=`awk -F':' '{print $1}' $PGPASSFILE`
port=`awk -F':' '{print $2}' $PGPASSFILE`
db=`awk -F':' '{print $3}' $PGPASSFILE`
user=`awk -F':' '{print $4}' $PGPASSFILE`
pass=`awk -F':' '{print $5}' $PGPASSFILE`

## Begin dump
echo "Dumping data:"
echo "  assay_platform.txt"
echo "\copy (select chip_type,species,manufacturer,platform,version from assay_platforms  where chip_type=(select distinct chip_type from biosamples where ds_id=$dsid)) TO '$dsid/assay_platform.txt'" | psql -U $user -h $host -p $port $db
echo "  dataset.txt"
echo "\copy (select id,dtg,handle from datasets where id=$dsid) TO '$dsid/dataset.txt'" | psql -U $user -h $host -p $port $db
echo "  dataset_metadata.txt"
echo "\copy (select ds_id,ds_name,ds_value from dataset_metadata where ds_id=$dsid order by ds_name) TO '$dsid/dataset_metadata.txt'" | psql -U $user -h $host -p $port $db
echo "  biosamples.txt"
echo "\copy (select chip_id,chip_type,ds_id,alias,archive_accession_id,sample_id from biosamples where ds_id=$dsid) TO '$dsid/biosamples.txt'" | psql -U $user -h $host -p $port $db
echo "  biosamples_metadata.txt"
echo "\copy (select ds_id,chip_type,chip_id,md_name,md_value from biosamples_metadata where chip_id in (select chip_id from biosamples where ds_id=$dsid) order by chip_id, md_name) TO '$dsid/biosamples_metadata.txt'" | psql -U $user -h $host -p $port $db
echo "  probe_expression_avg_replicates.txt"
echo "\copy (select ds_id,chip_type,chip_id,probe_id,replicate_group_id,avg_expression,standard_deviation,detected from stemformatics.probe_expressions_avg_replicates where ds_id=$dsid) TO '$dsid/probe_expression_avg_replicates.txt'" | psql -U $user -h $host -p $port $db
#echo "  probe_expression.txt"
#echo "\copy (select ds_id,chip_type,chip_id,probe_id,rawscore,normalizedscore,normalizedlog2score,detection from probe_expressions where ds_id=$dsid) TO '$dsid/probe_expression.txt'" | psql -U $user -h $host -p $port $db
echo "  chip_id_replicate_group_map.txt"
echo "\copy (select distinct chip_id,replicate_group_id from stemformatics.probe_expressions_avg_replicates where ds_id=$dsid order by chip_id) TO '$dsid/chip_id_replicate_group_map.txt'" | psql -U $user -h $host -p $port $db

echo "Post-dump file adjustments.."
## Post-dump file processing
# Convert '0' to '0.0' and 't' to 'TRUE', 'f' to 'FALSE'
sed -i -r -e 's/\t0\t/\t0.0\t/g' -e 's/\tt$/\tTRUE/g' -e 's/\tf$/\tFALSE/g' $dsid/probe_expression_avg_replicates.txt
# Convert '0' to '0.0'
#sed -i -r -e 's/\t0\t/\t0.0\t/g' $dsid/probe_expression.txt
# Convert "\N" to "NULL" in probe expression files
#sed -i -r -e 's/\\N/NULL/g' probe_expression.txt
sed -i -r -e 's/\\N/NULL/g' probe_expression_avg_replicates.txt

# Trim date to just yyyy-mm-dd part in dataset row
sed -i -r -e 's/\ 00:00:00//g' $dsid/dataset.txt
echo "Done."

echo "Creating METASTORE file.."
dataset_metadata_to_METASTORE "$dsid"
echo "Done."

echo "Creating ACCESSION file.."
accession=`grep "accession_id=" $dsid/METASTORE | awk -F'=' '{print $2}' | tr -d [:cntrl:]`
echo "$accession" > $dsid/ACCESSION

