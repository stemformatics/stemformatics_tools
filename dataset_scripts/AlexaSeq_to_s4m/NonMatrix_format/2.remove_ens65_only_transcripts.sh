#!/bin/sh

## NOTE: This script is slow compared to the one implemented in Perl
##       for the "Matrix" format input. It might be better to adapt
##       the Perl script than persevere with this one.


## Input: "normalized" expression file, transcript-level data
input="normalized_expression_detection.txt"
trans_to_remove="../ens_annotation_diff_example/ens65_trans_absent_in_ens63.txt"
removed_log="transcripts_removed_ens65_ens63_downgrade.log"
if [ -f $removed_log ]; then
  rm -f $removed_log
fi
total_count=`cat $trans_to_remove | wc -l`

## Remove Ens v65 transcripts that don't exist in v63.
## Make a log of the removed lines.

echo "Removing Ens65 transcripts not present in Ens63 annotation.."

if [ ! -f "$input.edit" ]; then
  cp $input "$input.edit"
fi

trans_count=0
for t in `cat $trans_to_remove`
do
  grep "$t" "$input.edit" >> $removed_log

  #sed -i -r -e "s/$t/NA/g" "$input.edit"

  grep -v "$t" "$input.edit" > "$input.tmp"
  mv "$input.tmp" "$input.edit"

  trans_count=`expr $trans_count + 1`
  echo "  $trans_count / $total_count"
done

#grep -v "NA" "$input.edit" > "$input.out"

echo "  done"

echo "All Done"
