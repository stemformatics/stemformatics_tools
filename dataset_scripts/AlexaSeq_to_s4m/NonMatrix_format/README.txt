
## For N input sample transcript expression files from AlexaSeq results, output S4M
## formatted raw and normalized expression files

$ ./1.sample_transcript_expression_norm1_to_s4m_normalized.sh <input_dir>

  where input_dir contains files: <sample_name>_TranscriptExpression_v65.txt
  (Update script if different input file name / Ensembl version etc)

  NOTE: Input file should look like the following (first 2 lines of one sample's
        transcript expression results:

Trans_ID	EnsEMBL_Trans_ID	Gene_ID	EnsEMBL_Gene_ID	Gene_Name	Chromosome	Strand	Transcript_Size	Unit1_start_chr	Unit1_end_chr	Base_Count	UnMasked_Base_Count	Coding_Base_Count	Specific_Exon_Region_Count	Specific_Junction_Count	Seq_Name	Feature_List	FID	Cumulative_Coverage	Average_Coverage_RAW	Average_Coverage_NORM1	Bases_Covered_1x	Percent_Coverage_1x	Expressed	Percent_Gene_Expression
1	ENSMUST00000000001	1	ENSMUSG00000000001	Gnai3	3	-1	3262	107910198	107949064	3758	3758	1511	9	8	ENSMUST00000000001	ER1a,E1a_E2a,ER2a,E2a_E3a,ER3a,E3a_E4a,ER4a,E4a_E5a,ER5a,E5a_E6a,ER6a,E6a_E7a,ER7a,E7a_E8a,ER8a,E8a_E9a,ER9a	T1	125567	33.0211063380	304.7811652076	3699	98.43	1	97.03



## [IF NECESSARY]
## Remove transcript IDs from output if Ensembl version newer in source than
## target annotation
$ ./2.remove_ens65_only_transcripts.sh

(Edit file to change annotation versions etc)


## [IF NECESSARY]
## Create "probe" mappings if not already done for this platform

$ ./3.make_probe_mappings.sh

(Edit file to change input annotation versions etc, and output probe mapping file name / format)
