#!/bin/sh
gene_list="../ens_annotation_diff_example/ensmusg_ensmust_v63_July2011"
map_file_out="SOLiD_5500xl_NCBIM37_feature_mapping_table.txt"
## Here, we use Ensembl transcript ID as our "probe ID" which makes generating these
## mappings really easy
awk -F'\t' '{print 46"\t"101"\tGene\t"$1"\tProbe\t"$2}' $gene_list >  $map_file_out

