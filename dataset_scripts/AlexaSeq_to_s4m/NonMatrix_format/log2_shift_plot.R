
args <- commandArgs()
# Expecting input "unpacked" normalized expression data file.
input_file <- ""
output_file <- ""
plot_file <- ""
plot_title <- "Density"
# Args processing code from: mugen_tutorial_affy_qa.pdf
for (e in commandArgs()) {
   ta <- strsplit(e,"=",fixed=TRUE);
   if(! is.na(ta[[1]][2])) {
      temp <- ta[[1]][2];
      if(substr(ta[[1]][1],nchar(ta[[1]][1]),nchar(ta[[1]][1])) == "I") {
         temp <- as.integer(temp);
      }
      if(substr(ta[[1]][1],nchar(ta[[1]][1]),nchar(ta[[1]][1])) == "N") {
         temp <- as.numeric(temp);
      }
      assign(ta[[1]][1],temp);
      if ( ta[[1]][1] == "-input" ) {
         input_file <- temp;
      }
      if ( ta[[1]][1] == "-output" ) {
         output_file <- temp;
      }
      if ( ta[[1]][1] == "-plotout" ) {
         plot_file <- temp;
      }
      if ( ta[[1]][1] == "-plottitle" ) {
         plot_title <- temp;
      }
   } else {
      assign(ta[[1]][1],TRUE);
   }
}

data <- read.table(input_file, header=FALSE, sep="\t", na.strings="")

## Output log2'd "expression" (Average_Coverage_NORM1) column
## (The whole matrix gets log2()'d - only the numeric column is affected
data_log2 <- data
data_log2["V3"] <- log2(data["V3"])


## Determine smallest negative value
## (Excludes -Inf (result of log2(0)) from min value search)
minval <- min(data_log2$V3[is.finite(data_log2$V3)], na.rm=TRUE)
minval
## Shift negative logs to make all values positive
## Add abs(minval) to each value so min now = 0.0
data_log2_shifted <- data_log2
shifted_values <- as.numeric(lapply(data_log2$V3, function(x) { if (is.numeric(x)) { x + abs(minval) } else { x } }))
data_log2_shifted$V3 <- shifted_values

write.table(data_log2_shifted, output_file, row.names=FALSE, col.names=FALSE, sep="\t", quote=FALSE, na="")

sink("stats.txt")
paste("Input min,1Q,med,2Q,max:\n")
fivenum(data$V3)
paste("Shifted min,1Q,med,2Q,max:\n")
fivenum(data_log2_shifted$V3)
print(paste("Median (raw):",median(data$V3, na.rm=TRUE),"\n"))
print(paste("Median (log2):",median(data_log2_shifted$V3, na.rm=TRUE),"\n"))
sink()


### DENSITY PLOT ###

# Set max(x) for plotting x axis
xmax<-ceiling(max(data_log2_shifted$V3, na.rm=TRUE))
xmax

# Calculate density
# Cannot have NA values, so we remove them for density plot only
data_for_dens <- data_log2_shifted$V3[! is.na(data_log2_shifted$V3)]
dens<-density(data_for_dens)

# Y max for plotting purposes
densMaxY<-max(dens$y)

# Output density plot with deciles
bitmap(plot_file, type="png16m", height=1000, width = 1000, units = "px")
plot.density(dens, main=plot_title, ylab="Density of Log2(score)", lwd=1)
xintervals <- seq(from=0,to=xmax,by=0.2)
#The 'y' position calculation of minor tick marks axis was arrived at by trial and error, and seems to give consistent results in all cases for log2 plots
axis(1, at=xintervals, pos=-(densMaxY / 15), cex.axis=0.7)
deciles <- quantile(dens$x, probs=seq(from=0.1,to=0.9,by=0.1))
abline(v=deciles,col="gray80")
dev.off()

