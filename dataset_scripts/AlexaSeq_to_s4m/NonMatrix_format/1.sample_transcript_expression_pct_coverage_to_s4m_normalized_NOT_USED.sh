#!/bin/sh
INPUTDIR="$1"
## We expect to run in the current directory
OUTDIR="."
RAWFILE="$OUTDIR/raw_expression_detection_pct.txt"
NORMFILE="$OUTDIR/normalized_expression_detection_pct.txt"
RBIN="R"

rm -rf $RAWFILE

exprfiles=`ls $INPUTDIR | grep -P "TranscriptExpression_v65.txt$"`

for f in $exprfiles
do
   ## Get the "chipid" from the file name
   chipid=`echo "$f" | sed -r -e 's/_TranscriptExpression_v65\.txt//g'`
   #echo "$chipid"
   cat "$INPUTDIR/$f" | grep -v -P "^Trans_ID" | awk -v c=$chipid -F'\t' '{print c"\t"$2"\t"$23"\t"$24}' >> "$RAWFILE"
done
##  Transform detection column 1/0 to boolean TRUE/FALSE strings
echo "Converting detection '1' to TRUE and '0' to FALSE.."
sed -i -r -e 's/1$/TRUE/g' -e 's/0$/FALSE/g' $RAWFILE
echo "  done"
echo "Converting 'NA' expression to 0.0 and 'NA' detection to FALSE.."
sed -i -r -e 's/\tNA\t/\t0\.0\t/g' -e 's/\tNA$/\tFALSE/g' $RAWFILE
echo "  done"

## Remove zero counts from raw expression - create filtered version
## NOTE: Not currently used
grep -v -P "\t0\.0+\t" $RAWFILE > $RAWFILE.zero_filtered


if [ -f "$RAWFILE" ]; then
  echo "Log2, zero-shift, plotting.."
  ## produces "normalized_expression_detection.txt" from $RAWFILE
  cat log2_shift_plot.R | $RBIN --no-save -input="$RAWFILE" -output="$NORMFILE" -plotout="coverage_avg_pct_density_plot_log2.png" -plottitle="1x_Percent_Coverage_density\nAll_Transcripts"
  echo "  done"

  echo "Post-processing files.."

  ## Replace "-Inf" with 0.0
  sed -i -r -e 's/\-Inf/0.0/g' "$NORMFILE"

  ## Create files needed for s4m.sh finalisation / validation
  ## raw_expression_unpacked.txt
  #awk -F'\t' '{print $1"\t"$2"\t"$3}' "$RAWFILE" > "$OUTDIR/raw_expression_unpacked.txt"

  #echo "  done"
fi

echo "All Done."

