#!/bin/sh
INPUTDIR="$1"
## We expect to run in the current directory
OUTDIR="."
RAWFILE="$OUTDIR/raw_expression_detection.txt"
NORMFILE="$OUTDIR/normalized_expression_detection.txt"
RBIN="R"

rm -rf "$RAWFILE"

## NOTE: Change input file name here
exprfiles=`ls $INPUTDIR | grep -P "TranscriptExpression_v65.txt$"`

for f in $exprfiles
do
   ## Get the "chipid" from the file name.
   ## NOTE: Change the file name here.
   chipid=`echo "$f" | sed -r -e 's/_TranscriptExpression_v65\.txt//g'`
   #echo "$chipid"
   cat "$INPUTDIR/$f" | grep -v -P "^Trans_ID" | awk -v c=$chipid -F'\t' '{print c"\t"$2"\t"$21"\t"$24}' >> "$RAWFILE"
done

##  Transform detection column 1/0 to boolean TRUE/FALSE strings
echo "Converting detection '1' to TRUE and '0' to FALSE.."
sed -i -r -e 's/1$/TRUE/g' -e 's/0$/FALSE/g' "$RAWFILE"
echo "  done"
echo "Converting 'NA' expression to blank and 'NA' detection to FALSE.."
sed -i -r -e 's/\tNA\t/\t\t/g' -e 's/\tNA$/\tFALSE/g' "$RAWFILE"
echo "  done"


if [ -f "$RAWFILE" ]; then
  ## Remove zero counts from raw expression - create filtered version
  ## NOTE: Not currently used but generate it anyway
  grep -v -P "\t0\.0+\t" "$RAWFILE" > "$RAWFILE.zero_filtered"

  echo "Creating '$OUTDIR/raw_expression_unpacked.txt'.."
  ## Create "unpacked" raw expression file we'll need for Stemformatics finalization and validation
  cut -f 1-3 "$RAWFILE" > "$OUTDIR/raw_expression_unpacked.txt"
  echo "  done"

  echo "Log2, zero-shift, plotting.."
  ## produces "normalized_expression_detection.txt" from $OUTFILE
  cat log2_shift_plot.R | $RBIN --no-save -input="$RAWFILE" -output="$NORMFILE" -plotout="coverage_avg_norm1_density_plot_log2_det_only.png" -plottitle="NORM1_Avg_Coverage_density\nDetected_Transcripts"
  echo "  done"

  echo "Post-processing files.."
  ## Replace "-Inf" with blank too (formerly 0.0).
  ## Task #831: Latest updates had us wanting blank vs. zero,
  ##   even though it means we can't distinguish un-detected from minimally
  ##   detected expression values. This might change again but for now,
  ##   we'll run with it.
  sed -i -r -e 's/\-Inf//g' "$NORMFILE"
  echo "  done"
fi

echo "All Done."

