
To convert Matrix transcript expression format, for input called
'Matrix_TranscriptExpression_v65.txt' for example:

## NOTE: If Ensembl version less than target, need to remove transcript IDs
## not present in earlier Ensembl release (as long as within a major GRCh or NCBIM build)
## e.g. from input v65 to target v63:
## [IF NECESSARY] Remove transcript IDs our target annotation version can't handle
$ ./0.remove_ens65_only_transcripts_from_expr_matrix.pl Matrix_TranscriptExpression_v65.txt


## Create "raw" and "normalized" s4m formatted files from the matrix
$ ./1.sample_matrix_transcript_expression_norm1_to_s4m_normalized.sh  Matrix_TranscriptExpression_v65.txt  <sample_start-end>

	e.g. To specify that sample columns start at 5 and finish at 20:
	./1.sample_matrix_transcript_expression_norm1_to_s4m_normalized.sh  Matrix_TranscriptExpression_v65.txt  5-20


## Generate stats (median etc) on final log2 expression values, output file 'stats.txt'
$ ./2.file_column_stats.pl  <tab_delimited_file_with_values>  <target_column_number>  <exclude_zero_values_TRUE_or_FALSE>

	e.g. To get median across entire log2 values, leaving '0' values out of the calculations:
	./2.file_column_stats.pl  normalized_expression_detection.txt  3  TRUE


## At least the 'median' stat can now be used in dataset metadata.

## All we need to do now is link the raw and normalized files so that our S4M dataset
## can be finalized and validated as normal:

cd <dataset_dir>/source/normalized
ln -s <AlexaSeq_processing_dir>/raw_expression_unpacked.txt .
ln -s <AlexaSeq_processing_dir>/normalized_expression_unpacked.txt .

## Now, assuming biosamples have been annotated:

cd <dataset_dir> && s4m.sh --finalize .
