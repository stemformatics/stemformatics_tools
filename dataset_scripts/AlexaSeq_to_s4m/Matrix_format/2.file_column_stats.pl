#!/usr/bin/perl -w
use Math::Complex;
do ('./include/Statistics-Lite.pm') or die("Failed to load package './include/Statistics-Lite.pm'!");

my $SCRIPTNAME = $0;

sub Usage {
        print "Usage: $SCRIPTNAME <tab_delimited_file_with_values> <target_column_number> <exclude_zero_values_TRUE_or_FALSE>\n\n";
        exit;
}

sub is_zero {
        my $num = shift;
        return ($num =~ /^0$/ or $num =~ /^0\.0+$/ or $num =~ /^$/) ? 1 : 0;
}


## MAIN ##

if (scalar(@ARGV) < 3) {
	Usage();
}

my $input_file = $ARGV[0];
my $target_col = $ARGV[1];
my $exclude_zero = $ARGV[2];

if ($target_col !~ /^\d+$/) {
	print "Error: Bad target column spec (must be integer)!\n";
	Usage();
}
if ($exclude_zero !~ /^(TRUE|FALSE)$/) {
	print "Error: Bad zero exclusion boolean - must be TRUE or FALSE\n";
	Usage();
}

open INFILE , "<", $input_file or die "Can't open input file '$input_file'!\n";
my @input_lines = <INFILE>;
close(INFILE);

my @values = ();

if (scalar(@input_lines)) {
	foreach (@input_lines) {
		chomp $_;
		my @tokens = split(/\t/, $_);
		my $val = $tokens[$target_col-1];
		if ($exclude_zero =~ /TRUE/) {
			if (! is_zero($val)) {
				push(@values, $val);
			}
		} else {
			push(@values, $val);

		}
	}

	my $median = Statistics::Lite::median(@values);
	my $mean = Statistics::Lite::mean(@values);
	my $range = Statistics::Lite::range(@values);
	my $min = Statistics::Lite::min(@values);
	my $max = Statistics::Lite::max(@values);

	print "Min =    [$min]\n";
	print "Max =    [$max]\n";
	print "Range =  [$range]\n";
	print "Mean =   [$mean]\n";
	print "Median = [$median]\n\n";

} else {
	print "Error: Got zero input rows!\n\n";
	exit 1
}

