#!/usr/bin/perl -w

my $SCRIPTNAME = $0;

sub Usage {
        print "Usage: $SCRIPTNAME <transcript_expression_matrix>\n\n";
        exit;
}

## MAIN ##

if (scalar(@ARGV) < 1) {
	Usage();
}

my $input_expr_file = $ARGV[0];
my $number_samples = $ARGV[1];

my $output_expr_file="$input_expr_file.ens63_filtered";
my $trans_to_remove="../ens_annotation_diff_example/ens65_trans_absent_in_ens63.txt";
my $rem_log_file="transcripts_removed_above_ens63.log";

open INFILE , "<", $input_expr_file or die "Can't open input file '$input_expr_file'!";
open OUTFILE, ">", $output_expr_file or die "Can't open output '$output_expr_file' for writing!";
open TRANSREMOVE, "<", $trans_to_remove or die "Can't open transcript file '$trans_to_remove'!";
open LOGFILE, ">", $rem_log_file or die "Can't open log file '$rem_log_file' for writing!";

my @expr_lines = <INFILE>;
my $header = shift @expr_lines;
close (INFILE);

my @trans_remove_list = <TRANSREMOVE>;
close (TRANSREMOVE);

print OUTFILE "$header";

foreach (@expr_lines) {
	chomp $_;
	my $line = $_;
	my @tokens = split(/\t/, $line);
	my $transid = $tokens[2];

	if (scalar(grep(/^$transid$/, @trans_remove_list)) > 0) {
		print LOGFILE "$transid\n";
		next;
	}
	print OUTFILE "$line\n";
}

close(OUTFILE);
close(LOGFILE);
