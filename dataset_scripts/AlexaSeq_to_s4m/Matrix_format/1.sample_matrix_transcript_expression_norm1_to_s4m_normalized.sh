#!/bin/sh
INPUTMATRIX="$1"
SAMPLECOLS="$2"
## We expect to run in the current directory
OUTDIR="."
RAWFILE="$OUTDIR/raw_expression_detection.txt"
NORMFILE="$OUTDIR/normalized_expression_detection.txt"
RBIN="R"

Usage () {
   echo "Usage: $0 <input_trans_expr_matrix> <sample_cols_range>"; echo
   exit
}

if [ -z "$INPUTMATRIX" -o -z "$SAMPLECOLS" ]; then
  Usage
fi

### MAIN ###

rm -rf $RAWFILE

#exprfile="$INPUTDIR/Matrix_TranscriptExpression_v65.txt"

firstsample=`echo "$SAMPLECOLS" | sed -r -e 's/\-.*$//g' | tr -d [:cntrl:]`
lastsample=`echo "$SAMPLECOLS" | sed -r -e 's/^[0-9]+\-//g' | tr -d [:cntrl:]`
header=`head -1 "$INPUTMATRIX"`
echo "header=[$header]"
## !! NOTE: We assume the transcript ID column is the third one!
##          If it isn't we need to set it here.
## !!
transcol=3

echo "Building '$RAWFILE'.."

s="$firstsample"
stopsample=`expr "$lastsample" + 1`
while [ "$s" != "$stopsample" ];
do
  chipid=`echo "$header" | awk -F'\t' -v i=$s '{print $i}' | tr -d [:cntrl:]`
  echo "  sample=[$chipid]"
  cat "$INPUTMATRIX" | grep -v -P "^Trans_ID" | awk -F'\t' -v c=$chipid -v t=$transcol -v s=$s '{print c"\t"$t"\t"$s"\tFALSE"}' >> "$RAWFILE"
  s=`expr $s + 1`
done

echo "Converting 'NA' expression to blank and 'NA' detection to FALSE.."
sed -i -r -e 's/\tNA\t/\t\t/g' -e 's/\tNA$/\tFALSE/g' "$RAWFILE"
echo "  done"

if [ -f "$RAWFILE" ]; then
  ## Remove zero counts from raw expression - create filtered version
  ## NOTE: This filtered file not currently used in pipeline but generate it any way
  ##       just in case.
  grep -v -P "\t0\.0+\t" "$RAWFILE" > "$RAWFILE.zero_filtered"

  echo "Creating '$OUTDIR/raw_expression_unpacked.txt'.."
  ## Create "unpacked" raw expression file we'll need for Stemformatics finalization and validation
  cut -f 1-3 "$RAWFILE" > "$OUTDIR/raw_expression_unpacked.txt"
  echo "  done"

  echo "Log2, zero-shift, plotting.."
  ## produces "normalized_expression_detection.txt" from $OUTFILE
  cat log2_shift_plot.R | $RBIN --no-save -input="$RAWFILE" -output="$NORMFILE" -plotout="coverage_avg_norm1_density_plot_log2_det_only.png" -plottitle="NORM1_Avg_Coverage_density\nDetected_Transcripts"
  echo "  done"

  echo "Post-processing files.."
  ## Replace "-Inf" with blank too (formerly 0.0).
  ## Task #831: Latest updates had us wanting blank vs. zero,
  ##   even though it means we can't distinguish un-detected from minimally
  ##   detected expression values. This might change again but for now,
  ##   we'll run with it.
  sed -i -r -e 's/\-Inf//g' "$NORMFILE"
  echo "  done"
fi

echo "All Done."

