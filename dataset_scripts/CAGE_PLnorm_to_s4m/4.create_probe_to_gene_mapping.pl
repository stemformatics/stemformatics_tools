#!/usr/bin/perl -w
use List::MoreUtils qw/ uniq /;

my $input_probe_trans="probe_to_transcript.txt";
my $input_trans_gene="./ens_annotation_diff_example/63/ensg_enst_v63_July2011.txt";
my $output_probe_gene="probe_to_gene.txt";
my $output_feature_map_table="s4m_feature_mapping_probe_gene.txt";

## NOTE: Must change these for each new platform type!
## ===================================================
my $CHIP_TYPE_ID = 102;
my $ANNOT_SPECIES_DBID = 56;
## ===================================================

open INFILE , "<", $input_probe_trans or die "Can't open input file '$input_probe_trans'!";
open MAPFILE , "<", $input_trans_gene or die "Can't open transcript/gene map file '$input_trans_gene'!";
open OUTFILE, ">", $output_probe_gene or die "Can't open output '$output_probe_gene' for writing!";
open S4M_FEATURE_MAP_OUT, ">", $output_feature_map_table or die "Can't open output '$output_feature_map_table' for writing!";

print "Loading Ensembl v63 transcript to gene mappings..\n";
my @probe_trans_lines = <INFILE>;
close (INFILE);
my @trans_gene_lines = <MAPFILE>;
close (MAPFILE);

## Build transcript to gene map
my %trans_gene_map = ();
foreach (@trans_gene_lines) {
	chomp $_;
	my @tokens = split(/\t/, $_);
	$trans_gene_map{$tokens[1]} = $tokens[0];
}

## (ab)use hash to make output mappings unique
%feature_map_out_lines_uniq = ();

print "done\nWriting probe ID to Ensembl gene map and Stemformatics feature map table..\n";
foreach (@probe_trans_lines) {
	chomp $_;
	my @tokens = split(/\t/, $_);
	my $probe = $tokens[0];
	my $trans = $tokens[1];
	if (exists($trans_gene_map{$trans})) {
	        my $gene = $trans_gene_map{$trans};
		print OUTFILE "$probe\t$gene\n";
		## DBID, chip type, from_type, from_id, to_type, to_id
		$feature_map_out_lines_uniq{"$ANNOT_SPECIES_DBID\t$CHIP_TYPE_ID\tGene\t$gene\tProbe\t$probe\n"} = 1;
		#print S4M_FEATURE_MAP_OUT "$ANNOT_SPECIES_DBID\t$CHIP_TYPE_ID\tGene\t$gene\tProbe\t$probe\n";
	} else {
		print "  Warning: transcript '$trans' does not have a gene mapping!\n";
	}
}
## Write out unique probe <-> gene mappings
foreach (keys(%feature_map_out_lines_uniq)) {
	print S4M_FEATURE_MAP_OUT $_;
}

print "done\n";

close(OUTFILE);
close(S4M_FEATURE_MAP_OUT);

my $gene_annot_count = scalar(uniq(values(%trans_gene_map)));
system("echo \"`cut -f 2 \"$output_probe_gene\" | sort | uniq | wc -l` genes are represented in data (of $gene_annot_count)\"");

