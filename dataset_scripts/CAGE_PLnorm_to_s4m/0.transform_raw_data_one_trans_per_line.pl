#!/usr/bin/perl -w

my $SCRIPTNAME = $0;

sub Usage {
        print "Usage: $SCRIPTNAME <input_PL_norm_file> <number_of_sample_columns> <MOUSE|HUMAN>\n\n";
        exit;
}

## MAIN ##

if (scalar(@ARGV) < 3) {
	Usage();
}

my $input_cage_file = $ARGV[0];
my $number_samples = $ARGV[1];
my $species = $ARGV[2];

my $output_cage_file="$input_cage_file.one_trans_per_line";
#my $timestamp=`date +"%Y%m%d-%H%M%S"`;

#open GENELIST, "<", $genes or die "Can't open gene list '$genes'!";
open INFILE , "<", $input_cage_file or die "Can't open input file '$input_cage_file'!";
open OUTFILE, ">", $output_cage_file or die "Can't open output '$output_cage_file' for writing!";

my @cage_lines = <INFILE>;
close (INFILE);

## !! IMPORTANT !!
## Need to update this accordingly for a new input data file.
## The other metdata columns are calculated from this.
## NOTE: First column is 1 (not 0)
my $MAX_SAMPLE_COL = $number_samples + 1;

## NOTE: Change to appropriate prefix for species e.g. 'ENSMUST' for Mouse
my $ENS_TRANS_PREFIX = $species =~ /human/i ? 'ENST' : 'ENSMUST';


my $SHORT_DESC_COL = $MAX_SAMPLE_COL + 1;
my $LONG_DESC_COL = $MAX_SAMPLE_COL + 2;
my $ASSOC_TRANS_COL = $MAX_SAMPLE_COL + 3;
my $ENTREZ_COL = $MAX_SAMPLE_COL + 4;
#my $LAST_COL = $MAX_SAMPLE_COL + 6;

foreach (@cage_lines) {
	chomp $_;
	my $line = $_;
	my @tokens = split(/\t/, $line);
	my $trans = $tokens[$ASSOC_TRANS_COL - 1];
	my @trans_tokens = split(/\,/, $trans);
	@ens_transcripts = ();
	if ($trans =~ /$ENS_TRANS_PREFIX[0-9]+/) {
		#%ensTransOnCurrentLine = ();
		## extract each ENST	
		foreach (@trans_tokens) {
			if ($_ =~ /$ENS_TRANS_PREFIX[0-9]+/) {
				my $ensid = $_;
				$ensid =~ s/^.*($ENS_TRANS_PREFIX[0-9]+).*$/\1/;
				push(@ens_transcripts, $ensid);
			}
		}
	}
	if (scalar(@ens_transcripts)) {
		## Print one line per transcript, appending transcript at the end
		foreach (@ens_transcripts) {
			print OUTFILE "$line\t$_\n";
		}

	## Print line without any modifications or additions (add blank for appended ENST column value)
	} else {
		print OUTFILE "$line\t\n";
	}
}

close(OUTFILE);
