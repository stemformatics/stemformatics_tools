#!/bin/sh
INPUTFILE="$1"
## e.g. "2-31" (cut format) for columns 2 to 31 as sample columns in the input
## power-law normalized table
SAMPLECOLS="$2"

Usage () {
  echo "Usage: $0 <PL_norm_input_file> <sample_cols_range>"; echo
  exit
}
if [ -z "$INPUTFILE" -o -z "$SAMPLECOLS" ]; then
  Usage
fi

## We expect to run in the current directory
OUTDIR="."
OUTFILE="$OUTDIR/raw_expression_detection.txt"
echo "$INPUTFILE" | grep subset > /dev/null 2>&1
if [ $? -eq 0 ]; then
  OUTFILE="$OUTDIR/raw_expression_detection.txt.subset"
fi
PROBE_TRANS="$OUTDIR/probe_to_transcript.txt"

rm -rf "$OUTFILE" "$OUTFILE.with_ens_column" "$PROBE_TRANS"

## STEPS
## 1. Column (sample) names as chip id
## 2. ENST as "probe ID"
## 3. Value as log2'd PL normalized value

sample_index=0
samples=`head -1 "$INPUTFILE" | cut -f $SAMPLECOLS`
max_sample_index=`echo "$SAMPLECOLS" | sed -r -e 's/^[0-9]+\-([0-9]+)$/\1/'`
#echo "DEBUG: max_sample_index=[$max_sample_index]"
input_row_count=`cat "$INPUTFILE" | wc -l`

#echo "DEBUG: got samples [$samples]"

echo "Building expression-detection file.."

for s in $samples
do
  echo "  Sample '$s' (`expr $sample_index + 1` of `expr $max_sample_index - 1`)"

  chipid="$s"
  colindex=`expr 2 + $sample_index`
  ## Long description
  desc_index=`expr $max_sample_index + 2`
  ## The "association_with_transcript" column is 3 columns after last sample column
  transindex=`expr $max_sample_index + 3`

  tail -`expr $input_row_count - 1` "$INPUTFILE" | awk -F'\t' -v c="$chipid" -v i="$colindex" -v d="$desc_index" '{
    peak = $d
    end = ""
    if (peak ~ /_(3|5)end$/) {
      pos = match(peak, /_(3|5)end$/)
      if (RLENGTH >= 0) {
        end = substr(peak, pos+1)
      }
    }
    sub(/CAGE_peak/, "p", peak)
    sub(/_at_/, "@", peak)
    
    pos = match(peak, /^p_*[0-9]+\@/)
    pAt = ""
    if (RLENGTH >= 0) {
      pAt = substr(peak, 0, RLENGTH)
      sub(/_/, "", pAt)
    }
    chr = $1
    if (pAt ~ /p/) {
      probeid = pAt end "_" chr
    } else {
      probeid = chr
    }

    ## Sample ID, probe ID, score, detection boolean and Ensembl transcript ID
    ## to help us create probe mappings in next step.
    if ($i ~ /^0$/) {
      print c "\t" probeid "\t" $i "\tFALSE\t" $NF
    } else {
      print c "\t" probeid "\t" $i "\tTRUE\t" $NF
    }

  }' >> "$OUTFILE.with_ens_column"

  sample_index=`expr $sample_index + 1`
done

echo "done"
echo "Creating probe-to-transcript mapping '$PROBE_TRANS'.."
## Create probe to ENST from output. We'll use this later to build probe to gene mapping.
awk -F'\t' '{print $2"\t"$NF}' "$OUTFILE.with_ens_column" > "$PROBE_TRANS.before_sort_uniq"
echo "  sorting and removing duplicates.."
grep -P "\tENS[A-Z]+[0-9]+$" "$PROBE_TRANS.before_sort_uniq" | sort | uniq > "$PROBE_TRANS"
echo "done"

echo "Finalizing '$OUTFILE'.."
## Now remove the trailing ENST column for the 'raw_expression_detection.txt' file
cut -f 1-4 "$OUTFILE.with_ens_column" > "$OUTFILE"
echo "done"

echo "All done"
