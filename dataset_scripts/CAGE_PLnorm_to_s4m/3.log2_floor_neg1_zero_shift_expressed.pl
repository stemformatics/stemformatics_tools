#!/usr/bin/perl -w
use Math::Complex;
do ('./include/Statistics-Lite.pm') or die("Failed to load package './include/Statistics-Lite.pm'!");

my $SCRIPTNAME = $0;

sub Usage {
	print "Usage: $SCRIPTNAME <input_tab_delimited_file> <comma_separated_columns_indexes> <has_header_row_TRUE_or_FALSE>\n\n";
	exit;
}

sub is_zero {
	my $num = shift;
	return ($num =~ /^0$/ or $num =~ /^0\.0+$/ or $num =~ /^$/) ? 1 : 0;
}


## MAIN ##

if (scalar(@ARGV) < 3) {
	Usage();
}

my $input_file = $ARGV[0];
my $columns_spec = $ARGV[1];
my $has_header = $ARGV[2];

print "DEBUG: Got args input_file=[$input_file], col_spec=[$columns_spec], has_header=[$has_header]\n\n";

@cols = ();
if ($columns_spec =~ /\,/) {
	@cols = split(/\,/, $columns_spec);
} elsif ($columns_spec =~ /^\d+$/) {
	push(@cols, $columns_spec);
}

if (! scalar(@cols)) {
	Usage();
}

## Attempt to load the input file
open INFILE , "<", $input_file or die "Can't open input file '$input_file'!\n";
open OUTFILE, ">", "$input_file.log2" or die "Can't open output file '$input_file.log2' for writing!\n";
my @input_lines = <INFILE>;
close(INFILE);

my @output_lines = ();

if (scalar(@input_lines)) {

	my @firstline = split(/\t/, $input_lines[0]);
	my $colcount = scalar(@firstline);
	foreach (@cols) {
		if ($_ > $colcount) {
			print "Error: Got input column '$_' when file only has $colcount columns!\n";
			exit 1
		}
	}
	## Remove header if we're told we have a file header
	if ($has_header =~ /TRUE/i) {
		shift @input_lines;
	}

	print 'DEBUG: There are ' . scalar(@input_lines) . ' input data rows and ' . scalar(@cols) . " target columns\n\n";
	print 'DEBUG: Total data points = ' . scalar(@input_lines) . ' x ' . scalar(@cols) . ' = ' . scalar(@input_lines) * scalar(@cols) . "\n\n";

	## First pass - replace zero with NA and log2(+ve values)
	$minval = 0;
	foreach (@input_lines) {
		chomp $_;
		my @tokens = split(/\t/, $_);

		foreach (@cols) {
			my $c = $_;
			my $val = $tokens[$c-1];
			if (! is_zero($val)) {
				## prevent default behaviour of very small numbers appearing like "x.yz e-15"
				$tokens[$c-1] = sprintf("%.14f", logn($val, 2));
				if ($tokens[$c-1] < $minval) {
					$minval = $tokens[$c-1];
				}
			## Set zero count tags to 'NA' so we can isolate them for next step
			} else {
				$tokens[$c-1] = 'NA';
			}
		}
		push(@output_lines, join("\t", @tokens));
	}

	print "DEBUG: Max log2(raw) minval = [$minval]\n\n";

	my @all_final_log2_shifted_values = ();

	## Second pass - shift by largest negative (introduced by log2 on very small numbers),
	## ignore 'NA' values (zero count tags) and set them to blank.
	## All other "expressed" values will start at 0. We do this so in Stemformatics we
	## can visualise expressed vs non-expressed more easily.
	foreach (@output_lines) {
		chomp $_;
		my @tokens = split(/\t/, $_);
		foreach (@cols) {
			my $c = $_;
			my $val = $tokens[$c-1];
			if ($val !~ /NA/ and $val =~ /^\-*[\d\.]+$/) {
				$tokens[$c-1] = $val + abs($minval);
				## Force 'zero' to be 0.0 as per S4M requirements for zero spec
				## (to differentiate between NULL or empty in DB tables)
				if ($tokens[$c-1] =~ /^0$/) {
					$tokens[$c-1] = '0.0';
				}
				push(@all_final_log2_shifted_values, $tokens[$c-1]);
			## All the 'NA' values are now set blank
			} elsif ($val =~ /^NA$/) {
				$tokens[$c-1] = '';
			}
		}
		print OUTFILE join("\t", @tokens) . "\n";
	}

	print 'DEBUG: There are ' . scalar(@all_final_log2_shifted_values) . " above-threshold values across input columns with:\n";
	## Calculate and print the median of expression (of zero-or-greater values only)
	my $median = Statistics::Lite::median(@all_final_log2_shifted_values);
	my $mean = Statistics::Lite::mean(@all_final_log2_shifted_values);
	my $range = Statistics::Lite::range(@all_final_log2_shifted_values);
	my $min = Statistics::Lite::min(@all_final_log2_shifted_values);
	my $max = Statistics::Lite::max(@all_final_log2_shifted_values);
	print "Min = 		[$min]\n";
	print "Max = 		[$max]\n";
	print "Range = 	[$range]\n";
	print "Mean = 		[$mean]\n";
	print "Median =	[$median]\n\n";

} else {
	print "Error: Input file '$input_file' has zero rows!\n";
}

close(OUTFILE);
