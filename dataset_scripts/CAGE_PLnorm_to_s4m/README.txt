Steps performed:

0. Transform input PL-norm CAGE data for instances where association with
transcript contains Ensembl trancript IDs. We need to do this so we can
selectively remove individual transcripts that don't map in our Ensembl
version (v63) in case input includes annotation from v64+
	$ ./0.transform_raw_data_one_trans_per_line.pl Annotated_iPS_POWERLAW_NOT_FILTERED_240612.txt 48 HUMAN


(See Appendix for input file format)
 

1. Remove v64+ ensembl transcript rows
	$ ./1.remove_ens65_only_transcripts_from_transformed.pl Annotated_iPS_POWERLAW_NOT_FILTERED_240612.txt.one_trans_per_line 48

	$ sort transcripts_removed_ens64_and_above.log | uniq | wc -l
	3022
	$ tail -218511 Annotated_iPS_POWERLAW_NOT_FILTERED_240612.txt.one_trans_per_line | awk -F'\t' '{print $NF}' | sort | uniq | grep ENS > transcripts_in_raw_cage_data.txt
	$ wc -l transcripts_in_raw_cage_data.txt
	69828

	## There are (3022 / 69828) %4.32 transcripts v64+ transcripts removed, resulting in:

	$ awk -F'\t' '{print $1}' Annotated_iPS_POWERLAW_NOT_FILTERED_240612.txt.one_trans_per_line | sort | uniq | wc -l
	184828
	$ awk -F'\t' '{print $1}' Annotated_iPS_POWERLAW_NOT_FILTERED_240612.txt.one_trans_per_line.ens63_filtered | sort | uniq | wc -l
	182866	
	$ expr 184828 - 182866
	1962

	## .. 1962 cage tags removed (of 184828 total = 1.06%)

	## We may be able to "rescue" some of these if they have been annotated with Entrez IDs or by using some other annotation provided..
	## Something to look at later, after the first cut data is in I guess.


2. Create a "raw_expression_detection.txt" formatted file from v63 filtered PL-norm CAGE data file.

	OPTION 1 - ENSEMBL TRANS AS PROBE ID
	$ ./2.powerlaw_norm_to_s4m.sh Annotated_CD14_POWERLAW_no_filter_220516.txt.one_trans_per_line.ens63_filtered 2-31

	- For those tags that have association with Ensembl transcript, we create probe ID "ENSTxxxxxxx_[-]ybp"
	  where "y" is the base pair offset from transcript either upstream or downstream
	- For tags than don't have Ensembl transcript association, use the tag description to craft a probe ID
	  like  "p@chr10:63213371..63213379,+" which encapsulates chromosomal start and end positions of found
	  tags

	OPTION 2 - ALL PROBE IDS AS CHR LOCATIONS

	Crafted probe ID format:  "p<X>@[(3|5)end]_chr<X>:<start>..<end>,[+|-]"

	$ ./2.powerlaw_norm_to_s4m_all_probes_as_coords.sh Annotated_iPS_POWERLAW_NOT_FILTERED_240612.txt.one_trans_per_line.ens63_filtered 2-49

	## Output: raw_expression_detection.txt.with_ens_column and probe_to_transcript.txt

	$ awk -F'\t' '{print $2}' probe_to_transcript.txt | sort | uniq | wc -l
	66806

	## There are 66,806 transcripts mapped (which is what we expect - 69828 less 3022 = 66806)

	## Create "unpacked" version of "raw" file which we'll need for s4m dataset finalization:

	$ cut -f 1-3 raw_expression_detection.txt.with_ens_column | uniq > raw_expression_unpacked.txt


3. Log2 the non-zero values (set natural zeros to 'NA'), shift all the non-NA values by larges negative score to floor at zero.
   The 'NA' scores are set to blank as "un-expressed" values.

   Also Output some summary stats for the "expressed" data points.

	$ ./3.log2_floor_neg1_zero_shift_expressed.pl raw_expression_detection.txt.with_ens_column 3 FALSE
	DEBUG: Got args input_file=[raw_expression_detection.txt.with_ens_column], col_spec=[3], has_header=[FALSE]

	DEBUG: There are 10306656 input data rows and 1 target columns

	DEBUG: Total data points = 10306656 x 1 = 10306656

	DEBUG: Max log2(raw) minval = [-3.31220782883928]

	DEBUG: There are 4046893 above-threshold values across input columns with:
	Min = 		[0.0]
	Max = 		[19.1191618256293]
	Range = 	[19.1191618256293]
	Mean = 		[4.76094011665088]
	Median =	[4.39376194368561]

	## Output: raw_expression_detection.txt.with_ens_column.log2

	## Create version without trailing ENS column and with duplicates removed, which we can use as our
	## s4m dataset "normalized" expression detection file.
 	## NOTE: We don't "sort" the rows as that would change sample order from raw file.
	## (Have verified that row count is what we expect when performing this operation).
	$ cut -f 1-4 raw_expression_detection.txt.with_ens_column.log2 | uniq > normalized_expression_detection.txt


4. Create probe to ENSG probe mapping file to load in Stemformatics
	$ ./4.create_probe_to_gene_mapping.pl
	Loading Ensembl v63 transcript to gene mappings..
	done
	Writing probe ID to Ensembl gene map and Stemformatics feature map table..
	done
	20716 genes are represented in data (of 53893)

	## There were no warnings about missing mappings from Transcript -> Gene, so we're all good to proceed


===============================================================================
APPENDIX 1. INPUT FILE FORMAT

Example (header and first data row):

Annotation	IPS_C11_d0_CTRL1	IPS_C11_d0_CTRL2	IPS_C11_d0_CTRL3	IPS_C11_d0_DOWNS1	IPS_C11_d0_DOWNS2	IPS_C11_d0_DOWNS3	IPS_C11_d12_CTRL1	IPS_C11_d12_CTRL2	IPS_C11_d12_CTRL3	IPS_C11_d12_DOWNS1	IPS_C11_d12_DOWNS2	IPS_C11_d12_DOWNS3	IPS_C11_d18_CTRL1	IPS_C11_d18_CTRL2	IPS_C11_d18_CTRL3	IPS_C11_d18_DOWNS1	IPS_C11_d18_DOWNS2	IPS_C11_d18_DOWNS3	IPS_C11_d6_CTRL1	IPS_C11_d6_CTRL2	IPS_C11_d6_CTRL3	IPS_C11_d6_DOWNS1	IPS_C11_d6_DOWNS2	IPS_C11_d6_DOWNS3	IPS_C18_d0_DOWNS1	IPS_C18_d0_DOWNS2	IPS_C18_d0_DOWNS3	IPS_C18_d12_DOWNS1	IPS_C18_d12_DOWNS2	IPS_C18_d12_DOWNS3	IPS_C18_d18_DOWNS1	IPS_C18_d18_DOWNS2	IPS_C18_d18_DOWNS3	IPS_C18_d6_DOWNS1	IPS_C18_d6_DOWNS2	IPS_C18_d6_DOWNS3	IPS_C32_d0_CTRL1	IPS_C32_d0_CTRL2	IPS_C32_d0_CTRL3	IPS_C32_d12_CTRL1	IPS_C32_d12_CTRL2	IPS_C32_d12_CTRL3	IPS_C32_d18_CTRL1	IPS_C32_d18_CTRL2	IPS_C32_d18_CTRL3	IPS_C32_d6_CTRL1	IPS_C32_d6_CTRL2	IPS_C32_d6_CTRL3	short_description	description	association_with_transcript	entrezgene_id	hgnc_id	uniprot_id
chr2:191865870..191865884,-	0.997975013220875	0.37426569864631	0.647184226625493	1.16019081557567	0.485540663386202	0.526144090599319	0	0	0	0	0	0	0	0	00	0	0	0	0.211515529272881	0.302452591352483	0.267133751272258	0.823334742833266	0.774361659035738	0	0	0.720968118122356	0	0	0.31085697068048	0	00	0	0.567077477486854	0.231885847447445	0.632470691343449	1.1690065543699	0.55135848067441	0.53844613979524	0.359836404639489	0	0.158322506660151	0	0	0.428863588046745	0	0	p@chr2:191865870..191865884,-	CAGE_peak_at_chr2:191865870..191865884,-	NA	NA	NA	NA	

