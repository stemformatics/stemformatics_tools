
Ensembl Human transcript diff summary
=====================================

From v63 to v67:
- 747 transcript IDs removed
- 23984 transcript IDs added (13.7% extra - this is the max possible "loss" of
  information by removing these extra transripts from v63 data)
