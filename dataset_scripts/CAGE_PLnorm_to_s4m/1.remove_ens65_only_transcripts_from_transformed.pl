#!/usr/bin/perl -w

my $SCRIPTNAME = $0;

sub Usage {
        print "Usage: $SCRIPTNAME <input_PL_norm_with_ens_transcript_column> <number_of_sample_columns>\n\n";
        exit;
}

## MAIN ##

if (scalar(@ARGV) < 2) {
	Usage();
}

my $input_cage_file = $ARGV[0];
my $number_samples = $ARGV[1];

my $output_cage_file="$input_cage_file.ens63_filtered";
my $trans_to_remove="ens_annotation_diff_example/ens_trans_hom_added_after_v63.txt";
my $rem_log_file="transcripts_removed_ens64_and_above.log";

open INFILE , "<", $input_cage_file or die "Can't open input file '$input_cage_file'!";
open OUTFILE, ">", $output_cage_file or die "Can't open output '$output_cage_file' for writing!";
open TRANSREMOVE, "<", $trans_to_remove or die "Can't open transcript file '$trans_to_remove'!";
open LOGFILE, ">", $rem_log_file or die "Can't open log file '$rem_log_file' for writing!";

my @cage_lines = <INFILE>;

## Remove header row from data, but preserve it in output
my $header = shift @cage_lines;
print OUTFILE $header;

close (INFILE);

my @trans_remove_list = <TRANSREMOVE>;
close (TRANSREMOVE);

## !! IMPORTANT !!
## Need to update this accordingly for a new input data file.
## The other metdata columns are calculated from this.
## NOTE: First column is 1 (not 0)
my $MAX_SAMPLE_COL = $number_samples + 1;

## NOTE: Change to appropriate prefix for species e.g. 'ENSMUST' for Mouse
#my $ENS_TRANS_PREFIX = 'ENST';

foreach (@cage_lines) {
	chomp $_;
	my $line = $_;
	my @tokens = split(/\t/, $line);
	my $ensid = $tokens[scalar(@tokens)-1];
	## DEBUG ##
	#print OUTFILE "$ensid\n";
	#next;
	## END ##

	if (length($ensid)) {
		if ($ensid =~ /ENS/) {
			$ensid =~ s/^.*(ENS[A-Z]+\d+).*$/$1/;
			## DEBUG ##
			#print OUTFILE "[$ensid]\n";
			#next;
			## END ##
			## Only keep the line if the last column has a transcript ID that isn't in the remove list
			if (scalar(grep(/^$ensid$/, @trans_remove_list)) > 0) {
				print LOGFILE "$ensid\n";
				next;
			}
		}
	}
	print OUTFILE "$line\n";

}

close(OUTFILE);
close(LOGFILE);
