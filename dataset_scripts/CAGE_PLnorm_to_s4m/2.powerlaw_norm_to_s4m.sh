#!/bin/sh
INPUTFILE="$1"
## e.g. "2-31" (cut format) for columns 2 to 31 as sample columns in the input
## power-law normalized table
SAMPLECOLS="$2"

Usage () {
  echo "Usage: $0 <PL_norm_input_file> <sample_cols_range>"; echo
  exit
}
if [ -z "$INPUTFILE" -o -z "$SAMPLECOLS" ]; then
  Usage
fi

## We expect to run in the current directory
OUTDIR="."
OUTFILE="$OUTDIR/raw_expression_detection.txt"
PROBE_TRANS="$OUTDIR/probe_to_transcript.txt"

rm -rf "$OUTFILE" "$OUTFILE.with_ens_column" "$PROBE_TRANS"

## STEPS
## 1. Column (sample) names as chip id
## 2. ENST as "probe ID"
## 3. Value as log2'd PL normalized value

sample_index=0
samples=`head -1 "$INPUTFILE" | cut -f $SAMPLECOLS`
max_sample_index=`echo "$SAMPLECOLS" | sed -r -e 's/^[0-9]+\-([0-9]+)$/\1/'`
echo "DEBUG: max_sample_index=[$max_sample_index]"
input_row_count=`cat "$INPUTFILE" | wc -l`

#echo "DEBUG: got samples [$samples]"

echo "Building expression-detection file.."

for s in $samples
do
  echo "  Sample '$s' (`expr $sample_index + 1` of `expr $max_sample_index - 1`)"

  chipid="$s"
  colindex=`expr 2 + $sample_index`
  ## The "association_with_transcript" column is 3 columns after last sample column
  transindex=`expr $max_sample_index + 3`

  tail -`expr $input_row_count - 1` "$INPUTFILE" | awk -F'\t' -v c="$chipid" -v t="$transindex" -v i="$colindex" '{
    probeid = $t
    if ($t ~ /^NA$/) {
      ## Use short CAGE peak description in the absence of a transcript association
      ## (2 columns before the transcript association column)
      probeid = $(t-2)

    ## Make the probe ID look like "ENSTxxxxxx_ybp"
    } else if ($t ~ /ENST/) {
      start = match($t, /ENST[0-9]+/, matches)
      if (RLENGTH >= 0) {
        enst = substr($t, start, RLENGTH)
        ## extract "[-]Xbp" part
        start = match($t, /^\-*[0-9]+bp/, matches)
        if (RLENGTH >= 0) {
          bp = substr($t, start, RLENGTH)
          probeid = enst "_" bp
        }
      }

    ## Not an ENST, extract first non-Ensembl identifier.
    ## We just want to grab everything up to the first comma, if there is one or leave as is.
    ## Replaces something like "-63bp_to_AB041267,AB041268_5end" with "AB041267_-63bp"
    } else {
      if ($t ~ /\,/) {
        sub(/\,.*$/, "", probeid)
      }
      start = match(probeid, /^\-*[0-9]+bp/, matches)
      if (RLENGTH >= 0) {
        bp = substr(probeid, start, RLENGTH)
        nonbppart = substr(probeid, length(bp)+5)  #we add 4 for the "_to_" part and +1 for the offset
        probeid = nonbppart "_" bp
      }
    }
    ## Sample ID, probe ID, score, detection boolean and Ensembl transcript ID
    ## to help us create probe mappings in next step.
    if ($i ~ /^0$/) {
      print c "\t" probeid "\t" $i "\tFALSE\t" $NF
    } else {
      print c "\t" probeid "\t" $i "\tTRUE\t" $NF
    }
  }' >> "$OUTFILE.with_ens_column"

  sample_index=`expr $sample_index + 1`
done

echo "done"
echo "Creating probe-to-transcript mapping '$PROBE_TRANS'.."
## Create probe to ENST from output. We'll use this later to build probe to gene mapping.
awk -F'\t' '{print $2"\t"$NF}' "$OUTFILE.with_ens_column" > "$PROBE_TRANS.before_sort_uniq"
echo "  sorting and removing duplicates.."
grep -P "\tENS[A-Z]+[0-9]+$" "$PROBE_TRANS.before_sort_uniq" | sort | uniq > "$PROBE_TRANS"
echo "done"

echo "Finalizing '$OUTFILE'.."
## Now remove the trailing ENST column for the 'raw_expression_detection.txt' file
cut -f 1-4 "$OUTFILE.with_ens_column" > "$OUTFILE"
echo "done"

echo "All done"
