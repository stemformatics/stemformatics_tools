#!/bin/sh
probe_seq_fasta="$1"
ref_seq_fasta="$2"

## A.Miotto config, Dec 2010 probe mappings for Illumina 50bp sequences.
## Also works for Affymetrix 25bp sequences (because it's a percentage).
##
## Same used for July/August 2011 probe mappings Ensembl v63 / grch37.3 mappings,
## for Illumina and Affy GeneChip. Stemformatics v3.0 release.
## NOTE: Affymetrix Gene ST arrays (HuGene, MoGene) were NOT mapped successfully
## with this approach at time of writing.
## -Othmar 2011-08-23
##
./exonerate-2.2.0-i386/bin/exonerate --percent 96 --showvulgar yes --showalignment no --fastasuffix fasta $probe_seq_fasta $ref_seq_fasta

