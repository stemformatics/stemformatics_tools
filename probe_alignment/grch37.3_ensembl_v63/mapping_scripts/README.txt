The resources used to generate probe to transcript and transcript to gene mappings are found here.

Data (FASTA sequences, grch37.3 human and mouse cDNA + ncRNA Ensembl transcript annotated FASTA) is archived in ascc.qfab.org/data/portal_data/archive/ path.

General steps:
1) Download FASTA formatted probe sequences for target microarray platform
2) Genome sequence file/s are available (e.g. cDNA, ncRNA FASTA formatted sequences from Ensembl public FTP, for target genome assembly e.g. grch37.3)
3) Run exonerate (un-gapped, simple alignments) from probe FASTA sequences to genome transcript sequence target
4) Run post-alignment scripts to convert output probe to transcript mappings to probe to gene, using reference Ensembl transcript to gene mappings for target Ensembl build
5) Re-format probe to gene outputs (sort, filter unique mappings only, create TSV format Stemformatics "probe_mappings" tables for pgloader or "COPY FROM .." direct loading into PostgreSQL)

NOTE: As of August 2011, in-house exonerate mappings of "ST" platforms (HuGene ST v1 and MoGene ST) are failing. Using Ensembl v63 mappings from Biomart for those platforms.
      This may change in the near future if/when in-house mappings can correctly align these platform probes to the genome.


===

List of probe sequences files used. These are archived on ascc.qfab.org:/data/portal_data/archive:

HG-U133A_2.probe_fasta
HG-U133A_2.probe_fasta.REFORMATTED
HG-U133A.probe_fasta
HG-U133A.probe_fasta.REFORMATTED
HG-U133_Plus_2.probe_fasta
HG-U133_Plus_2.probe_fasta.REFORMATTED
HT_HG-U133A.probe_tab.fa
HuGene-1_0-st-v1.hg19.probe.fa
HuGene-1_0-st-v1.hg19.probe.fa.REFORMATTED
HumanHT-12_V3_0_R3_11283641_A.fasta
HumanRef-8_V2_0_R4_11223162_A.fasta
HumanRef-8_V3_0_R3_11282963_A.fasta
HumanWG-6_V2_0_R4_11223189_A.fasta
HumanWG-6_V3_0_R3_11282955_A.fasta
MoGene-1_0-st-v1.probe.fa
MoGene-1_0-st-v1.probe.fa.REFORMATTED
Mouse430_2.probe_fasta
Mouse430_2.probe_fasta.REFORMATTED
MouseWG-6_V2_0_R3_11278593_A.fa



Probe sequence (FASTA) input file provenance:

November 2010 A.Miotto downloaded for "exonerate" in-house mappings:

	MouseWG-6_V2_0_R3_11278593_A.fa
		pech.rcs.griffith.edu.au:/scratch/s2686739/BlastIllum2ENSNov2010/MouseWG-6_V2_0_R3_11278593_A.fa
	HumanWG-6_V3_0_R3_11282955_A.fasta
		pech.rcs.griffith.edu.au:/scratch/s2686739/BlastIllum2ENSNov2010/HumanWG-6_V3_0_R3_11282955_A.fasta
	HumanWG-6_V2_0_R4_11223189_A.fasta
		pech.rcs.griffith.edu.au:/scratch/s2686739/BlastIllum2ENSNov2010/HumanWG-6_V2_0_R4_11223189_A.fasta
	HumanRef-8_V2_0_R4_11223162_A.fasta
		pech.rcs.griffith.edu.au:/scratch/s2686739/BlastIllum2ENSNov2010/HumanRef-8_V2_0_R4_11223162_A.fasta
	HumanRef-8_V3_0_R3_11282963_A.fasta
		pech.rcs.griffith.edu.au:/scratch/s2686739/BlastIllum2ENSNov2010/HumanRef-8_V3_0_R3_11282963_A.fasta
	HumanHT-12_V3_0_R3_11283641_A.fasta
		pech.rcs.griffith.edu.au:/scratch/s2686739/BlastIllum2ENSNov2010/HumanHT-12_V3_0_R3_11283641_A.fasta


July/August 2011 O.Korn downloaded for "exonerate" in-house mappings:
	(From: Affymetrix Support site)

	HG-U133A.probe_fasta
	HG-U133A_2.probe_fasta
	HG-U133_Plus_2.probe_fasta
	Mouse430_2.probe_fasta
	MoGene-1_0-st-v1.probe.fa
	HuGene-1_0-st-v1.hg19.probe.fa
	HT_HG-U133A.probe_tab.fa


July/August 2011 O.Korn "reformatted" FASTA files as final input into exonerate alignments:
	(See script: reformat_affy_fasta.pl)

	HG-U133A.probe_fasta.REFORMATTED
	Mouse430_2.probe_fasta.REFORMATTED
	HG-U133_Plus_2.probe_fasta.REFORMATTED
	HuGene-1_0-st-v1.hg19.probe.fa.REFORMATTED
	HG-U133A_2.probe_fasta.REFORMATTED
	MoGene-1_0-st-v1.probe.fa.REFORMATTED

