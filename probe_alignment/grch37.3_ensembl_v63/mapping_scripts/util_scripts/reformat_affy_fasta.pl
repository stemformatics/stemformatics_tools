#!/usr/bin/perl -w
my $input = $ARGV[0] or die "Must give target probe FASTA file argument!";

my $fh = open(INPUT, "<", "$input") or die "Failed opening file!";
@lines = <INPUT>;

foreach (@lines) {
  ## extract probeset id for ST array
  if ($_ =~ /TranscriptClusterID/) {
    $_ =~ s/^.*TranscriptClusterID=(\d+).*$/\>$1/;

  ## extract probeset id for ST array (control,intron,exon,antigenomic & rescue probes)
  } elsif ($_ =~ /ProbeSetID/) {
    $_ =~ s/^.*ProbeSetID=(\d+).*$/\>$1/;

  ## extract probeset id for GeneChip array
  } else {
    $_ =~ s/^.*\:([\/\-\_\w]+_at).*$/\>$1/;
  }
  print $_;
}
