#!/usr/bin/perl -w
my $discards = $ARGV[0];

die "Must supply discarded mappings log file name!" unless ($discards);

die "Failed to read file $discards." unless open (DISCARDS, "<$discards");

## Load basic stats package
do('./Statistics-Lite.pm') or die "Failed to load package 'Statistics-Lite.pm'!";

my @rows = <DISCARDS>;
close(DISCARDS);

my @avg_probes_mapped_in_probeset = ();

my $count = 0;
foreach (@rows) {
  chomp $_;
  $_ =~ m/\((\d+) of (\d+)\)/;
  my ($mapped, $probecount) = ($1, $2);
  my $avg = $mapped / $probecount;
  #if ($count < 50 or $count > 24560 ) {
  #  print "DEBUG: Got [$mapped] of [$probecount] ($avg %)\n";
  #}
  push(@avg_probes_mapped_in_probeset, $avg);
  $count++;
}

#print "DEBUG: Size of averages array: " . scalar(@avg_probes_mapped_in_probeset) . "\n";
#my $sum = 0;
#$sum += $_ for @avg_probes_mapped_in_probeset;
#print "DEBUG: Sum of averages = [$sum]\n";
 
my $mean_of_averages = Statistics::Lite::mean(@avg_probes_mapped_in_probeset);
my $median_of_averages = Statistics::Lite::median(@avg_probes_mapped_in_probeset);
my $stddev = Statistics::Lite::stddev(@avg_probes_mapped_in_probeset);

print "Median probes/probeset %: " . $median_of_averages . "\n";
print "Mean probes/probeset %  : " . $mean_of_averages . "\n";
print "Standard Deviation      : " . $stddev. "\n";

