#!/bin/sh
input="$1"
filebase=`basename $input`
grep -P "^vulgar" $input | awk '{print $2"\t"$6}' > $filebase.map
sort $filebase.map | uniq > $filebase.map.uniq
map_count=`awk -F'\t' '{print $1}' $filebase.map.uniq | sort | uniq | wc -l`
echo "$map_count probes mapped."; echo
exit
