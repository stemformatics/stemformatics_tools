#!/usr/bin/perl -w

use Data::Dumper;

die "No ARGV!" unless scalar(@ARGV);

my $probe_to_trans_file = $ARGV[0];
my $probe_fasta_file = $ARGV[1];
my $trans_to_gene_mapping_file = $ARGV[2];
my $chip_type = $ARGV[3];

open PF, "<$probe_fasta_file" or die $!;
open MAP, "<$trans_to_gene_mapping_file" or die $!;
open PTE, "<$probe_to_trans_file" or die $!;

my %pf = ();
# Load probe fasta file into hash. Indexed by probeset_id, value = probeset count
my @probe_fasta = <PF>;
close(PF);
foreach my $pr (grep /^\>/, @probe_fasta) {
  chomp $pr;
  $pr =~ s/^>//g;
  if (exists($pf{$pr})) {
    $pf{$pr}++;
  } else {
    $pf{$pr} = 1;
  }
}

my %map = ();
# Load transcript to gene mappings
my @gene_trans_map = <MAP>;
close(MAP);

my $count=0;
foreach (@gene_trans_map) {
  chomp $_;
  my ($gene, $trans) = split(/\t/, $_);
  if ($count < 10) {
    print STDERR "DEBUG: Got gene<->transcript pair [$gene,$trans]\n"; 
    $count++;
  }
  $map{$trans} = [] unless exists($map{$trans}); 
  push(@{$map{$trans}}, $gene);
}

## DEBUG
#print STDERR "DEBUG: Printing gene to transcript map\n";
#$Data::Dumper::Indent = 3;
#print STDERR Dumper(\%map);
## END DEBUG


## Build probe to transcript mapping hash indexed by probeset id
my %probes_mapped = ();

my @probe_to_trans = <PTE>;
# Load probe <-> transcript mapping for this chip type
close(PTE);

foreach (@probe_to_trans) {
  chomp $_;
  my ($probeset_id,$trans) = split(/\t/, $_);
  $probes_mapped{$probeset_id} = 0 unless exists($probes_mapped{$probeset_id});
  $probes_mapped{$probeset_id}++;
}


## MAIN ##
foreach (@probe_to_trans) {
  chomp $_;
  my ($probeset_id,$trans) = split(/\t/, $_);
  #my $probes_mapped_count = scalar(grep /^$probeset_id/, @probe_to_trans);
  my $probes_mapped_count = $probes_mapped{$probeset_id};
  my $probes_in_probeset = $pf{$probeset_id};

  if ($probes_mapped_count > 100) {
    print STDERR "DEBUG: Skipping probeset $probeset_id as it maps to $probes_mapped_count transcripts.\n";
    next;
  }

  ## At least 50% of probes in probeset must be mapped
  if ($probes_mapped_count < $probes_in_probeset / 2) {
    print STDERR "DEBUG: Probeset $probeset_id < 50% mapped, skipping ($probes_mapped_count of $probes_in_probeset).\n";
    next;
  }
  die "Error: Could not find transcript $trans in mapping file $trans_to_gene_mapping_file!" unless exists($map{$trans});
  ## Test for multi-mapping transcripts while we're here..
  if (scalar(@{$map{$trans}}) > 1) {
    print STDERR "Error: Transcript $trans maps to multiple genes (in Ensembl v59 annotation)!\n";
    die;
  }
  my $gene = $map{$trans}[0];
  die "DEBUG: Gene [$gene] not in map!\n" unless($gene);

  ## Print final probe_mapping table row (TSV) to stdout
  print "$probeset_id\t$gene\t$chip_type\t56\t-1\n";
}
