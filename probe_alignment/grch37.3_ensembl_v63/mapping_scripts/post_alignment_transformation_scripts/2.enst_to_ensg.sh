#!/bin/sh
probe_to_enst_file="$1"
chip_type="$2"
map_file="../ens_trans_to_gene_reference/ensg_enst_v63_July2011.txt"

Usage () {
  echo "Usage: $0 <probe_to_enst_file> <chip_type>"; echo
  exit 1
}

if [ -z "$1" -o -z "$2" ]; then
  Usage
fi

st=0
## Gene ST results require access to non-unique (full) probe to transcript mappings
echo "$probe_to_enst_file" | grep -P -i "HuGene" > /dev/null 2>&1
if [ $? -eq 0 ]; then
  st=1
  probe_fasta_file="../probe_fasta/HuGene-1_0-st-v1.hg19.probe.fa.REFORMATTED"
  if [ ! -f $probe_fasta_file ]; then
    echo "Error: Full probe sequences FASTA file '$probe_fasta_file' not found!";
    echo "This file is required to finalise Gene ST mappings."; echo
    exit 1
  fi
fi

if [ $st -eq 1 ]; then
  ./filter_mappings_on_probe_hit_percentage.pl "$probe_to_enst_file" "$probe_fasta_file" "$map_file" "$chip_type"
else
  while read line
  do
    probeset_id=`echo "$line" | awk -F'\t' '{print $1}'`
    trans_id=`echo "$line" | awk -F'\t' '{print $2}'`
    grep "$trans_id" $map_file | awk -v pid="$probeset_id" -v chiptype="$chip_type" '{print pid"\t"$1"\t"chiptype"\t56\t-1"}'
  done < $probe_to_enst_file
fi

