#!/bin/sh
cd ../../
./exonerate.sh probe_fasta/MouseWG-6_V2_0_R3_11278593_A.fa ens_transcripts_fasta/Mus_musculus.NCBIM37.63.cdna.all.fa > results_v63/MouseWG-6_V2/MouseWG-6_V2.cdna.out 2>&1
./exonerate.sh probe_fasta/MouseWG-6_V2_0_R3_11278593_A.fa ens_transcripts_fasta/Mus_musculus.NCBIM37.63.ncrna.fa > results_v63/MouseWG-6_V2/MouseWG-6_V2.ncrna.out 2>&1
cd $OLDPWD
