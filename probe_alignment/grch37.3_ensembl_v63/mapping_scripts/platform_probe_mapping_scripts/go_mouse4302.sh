#!/bin/sh
cd ../../
./exonerate.sh probe_fasta/Mouse430_2.probe_fasta.REFORMATTED ens_transcripts_fasta/Mus_musculus.NCBIM37.63.cdna.all.fa > results_v63/Mouse430_2.cdna.out 2>&1
./exonerate.sh probe_fasta/Mouse430_2.probe_fasta.REFORMATTED ens_transcripts_fasta/Mus_musculus.NCBIM37.63.ncrna.fa > results_v63/Mouse430_2.ncrna.out 2>&1
cd $OLDPWD
