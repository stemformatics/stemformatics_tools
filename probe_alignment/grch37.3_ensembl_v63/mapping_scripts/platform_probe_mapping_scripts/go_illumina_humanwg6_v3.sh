#!/bin/sh
cd ../../
./exonerate.sh probe_fasta/HumanWG-6_V3_0_R3_11282955_A.fasta ens_transcripts_fasta/Homo_sapiens.GRCh37.63.cdna.all.fa > results_v63/HumanWG-6_V3/HumanWG-6_V3.cdna.out 2>&1
./exonerate.sh probe_fasta/HumanWG-6_V3_0_R3_11282955_A.fasta ens_transcripts_fasta/Homo_sapiens.GRCh37.63.ncrna.fa > results_v63/HumanWG-6_V3/HumanWG-6_V3.ncrna.out 2>&1
cd $OLDPWD
