#!/bin/sh
cd ../../
./exonerate.sh probe_fasta/HumanRef-8_V2_0_R4_11223162_A.fasta ens_transcripts_fasta/Homo_sapiens.GRCh37.63.cdna.all.fa > results_v63/HumanRef-8_V2/HumanRef-8_V2.cdna.out 2>&1
./exonerate.sh probe_fasta/HumanRef-8_V2_0_R4_11223162_A.fasta ens_transcripts_fasta/Homo_sapiens.GRCh37.63.ncrna.fa > results_v63/HumanRef-8_V2/HumanRef-8_V2.ncrna.out 2>&1
cd $OLDPWD
