#!/bin/sh
cd ../../
./exonerate.sh probe_fasta/HG-U133_Plus_2.probe_fasta.REFORMATTED ens_transcripts_fasta/Homo_sapiens.GRCh37.63.cdna.all.fa > results_v63/HG-U133_Plus_2.cdna.out 2>&1
./exonerate.sh probe_fasta/HG-U133_Plus_2.probe_fasta.REFORMATTED ens_transcripts_fasta/Homo_sapiens.GRCh37.63.ncrna.fa > results_v63/HG-U133_Plus_2.ncrna.out 2>&1
cd $OLDPWD
