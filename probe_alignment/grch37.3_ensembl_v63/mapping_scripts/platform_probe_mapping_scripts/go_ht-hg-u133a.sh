#!/bin/sh
cd ../../
./exonerate.sh probe_fasta/HT_HG-U133A.probe_tab.fa ens_transcripts_fasta/Homo_sapiens.GRCh37.63.cdna.all.fa > results_v63/HT_HG-U133A.cdna.out 2>&1
./exonerate.sh probe_fasta/HT_HG-U133A.probe_tab.fa ens_transcripts_fasta/Homo_sapiens.GRCh37.63.ncrna.fa > results_v63/HT_HG-U133A.ncrna.out 2>&1
cd $OLDPWD
