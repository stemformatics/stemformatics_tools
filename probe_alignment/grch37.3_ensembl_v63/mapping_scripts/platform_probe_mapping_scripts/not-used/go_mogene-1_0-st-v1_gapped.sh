#!/bin/sh
cd ../../
./exonerate-gapped.sh probe_fasta/MoGene-1_0-st-v1.probe.fa.REFORMATTED ens_transcripts_fasta/Mus_musculus.NCBIM37.63.cdna.all.fa > results_v63/MoGene-1_0-st-v1-GAPPED/cdna.gapped.out 2>&1
./exonerate-gapped.sh probe_fasta/MoGene-1_0-st-v1.probe.fa.REFORMATTED ens_transcripts_fasta/Mus_musculus.NCBIM37.63.ncrna.fa > results_v63/MoGene-1_0-st-v1-GAPPED/ncrna.gapped.out 2>&1
cd $OLDPWD
