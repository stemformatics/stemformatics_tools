#!/bin/sh
cd ../../
./exonerate.sh probe_fasta/MoGene-1_0-st-v1.probe.fa.REFORMATTED ens_transcripts_fasta/Mus_musculus.NCBIM37.63.cdna.all.fa > results_v63/MoGene-1_0-st-v1.cdna.out 2>&1
./exonerate.sh probe_fasta/MoGene-1_0-st-v1.probe.fa.REFORMATTED ens_transcripts_fasta/Mus_musculus.NCBIM37.63.ncrna.fa > results_v63/MoGene-1_0-st-v1.ncrna.out 2>&1
cd $OLDPWD
