#!/bin/sh
cd ../../
#./exonerate.sh probe_fasta/HuGene-1_0-st-v1.hg19.probe.fa.REFORMATTED ens_transcripts_fasta/Homo_sapiens.GRCh37.63.cdna.all.fa > results_v63/HuGene-1_0-st-v1.cdna.out 2>&1
./exonerate.sh probe_fasta/HuGene-1_0-st-v1.hg19.probe.fa.REFORMATTED ens_transcripts_fasta/Homo_sapiens.GRCh37.63.ncrna.fa > results_v63/HuGene-1_0-st-v1.ncrna.out 2>&1
cd $OLDPWD
