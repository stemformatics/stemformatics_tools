README file for AltAnalyze related analysis
===========================================

##
## Alistair Chalk
##

## 

1) Download of software
# wget -i http://altanalyze.googlecode.com/files/AltAnalyze_v115release-Py.zip

# run with python ./AltAnalyze.py 
# And update annotations (Ensembl54, Human, Mouse, Rat)
# This takes quite some time (~hours).

1) Create groups and comps file (define required comparisons from metadata and CEL files)
# TODO: - Liase with Amanda on this one, and assume that I have a metadata file for each dataset containing

# Start with CEL files and labels and make the groups and comps file
# LATER: We need a script that runs off the ASCC database to make this

a) Create descriptor of CEL files FILE_DESC.txt

::::::::::::::
FILE_DESC.txt
::::::::::::::
MDMLPS0h1.CEL	0h
MDMLPS0h2.CEL	0h
MDMLPS2h1.CEL	2h
MDMLPS2h2.CEL	2h

b) Create descriptor of comparisons needed to be run
::::::::::::::
FILE_DESC_COMPS.txt
::::::::::::::
0h	2h


c) Create groups and comps files based on the above data

./createGroupsComps.awk FILE_DESC.txt FILE_DESC_COMPS.txt TESTNAME

CEL filenames	    Groups

groups.[datasetName].txt
<filename.CEL>	    <group number>	<groupName>
MDMLPS0h1.CEL	1	0h
MDMLPS0h2.CEL	1	0h
MDMLPS2h1.CEL	2	2h
MDMLPS2h2.CEL	2	2h

comps.[datasetName].txt
<group number>	<group number>

d) Run AltAnlayze in command line mode

# Generic running of AltAnalyze (with defaults)
# TODO: change to have a number of parameter settings
# Add human/mouse

runAltAnalyze_generic.sh


runAltAnalyze_generic.sh <datasetName> <input(and output directory) containing cel files> <altAnalyze directory> <groups file> <comparisons file> <orgamism>
 ./runAltAnalyze_generic.sh /scratch/s2611722/tfv_mercurial/ASCC/scripts/AltAnalyze/1_createFiles_runAltAnalyze/TESTER /scratch/s2611722/tfv_mercurial/ASCC/scripts/AltAnalyze/1_createFiles_runAltAnalyze/AltAnalyze_TESTER /scratch/AltAnalyze_2010_04_12/AltAnalyze_v1release/ /scratch/s2611722/tfv_mercurial/ASCC/scripts/AltAnalyze/1_createFiles_runAltAnalyze/groups.TESTER.txt /scratch/s2611722/tfv_mercurial/ASCC/scripts/AltAnalyze/1_createFiles_runAltAnalyze/comps.TESTER.txt Hs


2) Format/post-process results files

3) Import data into database

4) Methods for data export/visualisation in TFV/ASCC_DB

## 
## Additional
##
## Todo: Run re-annotation analysis based on a set of origional probes 
##
## Add file in: - where - check documentation (this has been done before!).


