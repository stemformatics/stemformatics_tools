#!/usr/bin/gawk -f

##
## Script to create AltAnalyze groups and comps files required for an AltAnalyze analysis.
##
## Input: file with CEL and group names
## MDMLPS0h1.CEL	0h
## MDMLPS0h2.CEL	0h
## MDMLPS2h1.CEL	2h
## MDMLPS2h2.CEL	2h
## MDMLPS6h1.CEL	6h
## MDMLPS6h2.CEL	6h
##
## Input: file with desired comparisons
## 0h 2h
## 0h 6h
##
## Output: 
## Data formatted for input to AltAnalyze
## groups.[datasetName].txt and comps.[datasetName].txt
##
## Alistair Chalk
##

BEGIN {

  if (ARGC < 3){
    print "USAGE: createGroupsComps.awk <infile with cel files and group names> <file with comparisons needed using group names> <datasetName>";
    exit(0);
  }
  incomingCompsFile = ARGV[2];
  datasetName = ARGV[3];
  ARGC = 2;
  
  currentGroup = 0;

  groupsFile = "groups."datasetName".txt";
  compsFile = "comps."datasetName".txt";

  print "IN: "incomingCompsFile;
  print "datasetName: "datasetName;

  oldGroup = "";
}

/^#/{
# Header line
next;
}

//{
  celFile = $1;
  group = $2;
	 
  # If group is new, assign new id
  if (groupMatcher[group]){
  } else {
    currentGroup++;
      }
  
  print "groupMatcher[group] " groupMatcher[group];

  groupMatcher[group] = currentGroup;
  print celFile"\t"currentGroup"\t"group > groupsFile;
  
  next;
}

  END {
# Create comps file
    
    while (getline < incomingCompsFile){
      if ($0~/^#/){
	  # header line - ignore
	  } else {
	  if (NF != 2){
	    print "FATAL - unexpected number of columns in comps file (expected 2)";
	    print $0;
	    exit(0);
	  }
	  
	  # Check the group has been assigned
	  if (groupMatcher[$1]) {
	  } else {
	    print "FATAL - unassigned group - do your group names match in both files?";
	    print $0;
	    exit(0);
	  }
	    if (groupMatcher[$2]) {
	    } else {
	      print "FATAL - unassigned group - do your group names match in both files?";
	      print $0;
	      exit(0);
	    }
	    
	      print "Printing comps line";
	      print groupMatcher[$1]"\t"groupMatcher[$2] > compsFile;
		}
    }
  }

