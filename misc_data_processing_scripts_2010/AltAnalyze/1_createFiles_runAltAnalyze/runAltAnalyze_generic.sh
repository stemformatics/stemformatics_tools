#!/bin/bash 

echo "USAGE: "
if [ -z "$6" ]; then
                  echo "FATAL: not enough args given";
		  echo "USAGE:  runAltAnalyze_generic.sh <datasetName> <input(and output directory) containing cel files> <altAnalyze directory> <groups file> <comparisons file> <orgamism>"
		  echo "USAGE:  ./runAltAnalyze_generic.sh /scratch/s2611722/tfv_mercurial/ASCC/scripts/AltAnalyze/1_createFiles_runAltAnalyze/TESTER /scratch/s2611722/tfv_mercurial/ASCC/scripts/AltAnalyze/1_createFiles_runAltAnalyze/AltAnalyze_TESTER /scratch/AltAnalyze_2010_04_12/AltAnalyze_v1release/ /scratch/s2611722/tfv_mercurial/ASCC/scripts/AltAnalyze/1_createFiles_runAltAnalyze/groups.TESTER.txt /scratch/s2611722/tfv_mercurial/ASCC/scripts/AltAnalyze/1_createFiles_runAltAnalyze/comps.TESTER.txt Hs"
echo ""
echo "Allows command line usage of AltAnalyze using default parameters"
                  exit 0;
fi

abspath=$(cd ${0%/*} && echo $PWD/${0##*/})

# to get the path only - not the script name - add
path_only=`dirname "$abspath"`

#display the paths to prove it works
echo $abspath
echo $path_only

PWD=$abspath

echo "METHOD3 : "$(dirname $(readlink -f ${BASH_SOURCE[0]}))

DATASETNAME=$1
DATADIR=$2;
ALTANALYZE_DIR=$3;
GROUPS_FILE=$4;
COMPS_FILE=$5;
ORGANISM=$6;

echo "DATASETNAME "$DATASETNAME;
echo "DATADIR "$DATADIR;
echo "ALTANALYZE_DIR "$ALTANALYZE_DIR;
echo "GROUPS_FILE "$GROUPS_FILE;
echo "COMPS_FILE "$COMPS_FILE;
echo "ORGANISM "$ORGANISM;

##
## Could read in parameters for AltAnalyze run from a file
##

#
# This part no longer required. 
#
echo "Creating directory structure"
mkdir $path_only"/"$DATADIR"/ExpressionInput"
#echo "copying groups/comps files to " $DATADIR"/expressionInput/."
#cp $GROUPS_FILE $DATADIR"/expressionInput/."
#cp $COMPS_FILE $DATADIR"/expressionInput/."

cd $ALTANALYZE_DIR 
echo "Running AltAnalyze"

echo "METHOD3 : "$(dirname $(readlink -f ${BASH_SOURCE[0]}))
echo $abspath
echo $path_only

##
## Note - new version fails for -noxhyb (removed
##

echo "python ./AltAnalyze.py --species "$ORGANISM" --arraytype exon --celdir $path_only"/"$DATADIR --output $path_only"/"$DATADIR --expname $DATASETNAME --runGOElite no --dabgp 0.01  --rawexp 100 --analyzeAllGroups "all groups"  --GEcutoff 4 --probetype core --altp 0.001 --altmethod FIRMA --altscore 8 --exportnormexp yes --runMiDAS no --ASfilter no --mirmethod "two or more" --calcNIp yes --avgallss yes --groupdir $path_only"/"$GROUPS_FILE --compdir $path_only"/"$COMPS_FILE"

python ./AltAnalyze.py --species $ORGANISM --arraytype exon --celdir $path_only"/"$DATADIR --output $path_only"/"$DATADIR --expname $DATASETNAME --runGOElite no --dabgp 0.01 --rawexp 100 --analyzeAllGroups "all groups"  --GEcutoff 4 --probetype core --altp 0.001 --altmethod FIRMA --altscore 8 --exportnormexp yes --runMiDAS no --ASfilter no --mirmethod "two or more" --calcNIp yes --avgallss yes --groupdir $path_only"/"$GROUPS_FILE --compdir $path_only"/"$COMPS_FILE
# > $DATADIR"/"$DATASETNAME".log"

echo "AltAnalyze completed run.";

exit 1;