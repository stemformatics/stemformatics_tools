Steps for re-annotation of Illumina probes
===========================================

Alistair Chalk
2010-11-01.

- Do this as a start. But the hope is that the pipeline will become available and I won't need to continue this.

On pech the data is here:

Source and output files from scripts and BLAST or bowtie mapping
/scratch/s2611722/tfv_mercurial/ASCC_DATA/Illumina_probeMapping/

Genome databases (i.e. bowtie or BLAST formatted databases for searching)
/scratch/genomes/[hg18|hg19|mm8|mm9]

mrna : UCSC mrna cdnas for a the given genome

Step 1)

Added documentation of Illumina probe stuff in Mercurial.

2) Downloaded Illumina probe data 
wget -i switchtoi.links.txt

(And unzip)

3) Create fasta files from Illumina data:
switchtoi.parser2fasta.awk HumanRef-8_V2_0_R4_11223162_A.txt > HumanRef-8_V2_0_R4_11223162_A.txt.fasta
switchtoi.parser2fasta.awk HumanWG-6_V2_0_R4_11223189_A.txt > HumanWG-6_V2_0_R4_11223189_A.txt.fasta 
switchtoi.parser2fasta.awk HumanWG-6_V3_0_R2_11282955_A.txt > HumanWG-6_V3_0_R2_11282955_A.txt.fasta
switchtoi.parser2fasta.awk HumanRef-8_V3_0_R2_11282963_A.txt > HumanRef-8_V3_0_R2_11282963_A.txt.fasta
switchtoi.parser2fasta.awk  HumanHT-12_V4_0_R1_15002873_B.txt  > HumanHT-12_V4_0_R1_15002873_B.txt.fasta
switchtoi.parser2fasta.awk HumanHT-12_V3_0_R2_11283641_A.txt  > HumanHT-12_V3_0_R2_11283641_A.txt.fasta

# Mouse
switchtoi.parser2fasta.awk MouseRef-8_V1_1_R4_11234312_A.txt > MouseRef-8_V1_1_R4_11234312_A.txt.fasta
switchtoi.parser2fasta.awk MouseRef-8_V2_0_R2_11278551_A.txt > MouseRef-8_V2_0_R2_11278551_A.txt.fasta
switchtoi.parser2fasta.awk MouseWG-6_V2_0_R2_11278593_A.txt > MouseWG-6_V2_0_R2_11278593_A.txt.fasta
switchtoi.parser2fasta.awk MouseWG-6_V1_1_R4_11234304_A.txt >  MouseWG-6_V1_1_R4_11234304_A.txt.fasta

# Rat
switchtoi.parser2fasta.awk RatRef-12_V1_0_R5_11222119_A.txt > RatRef-12_V1_0_R5_11222119_A.txt.fasta

# Create the map files - based on Illumina annotation

switchtoi.parser.awk HumanRef-8_V2_0_R4_11223162_A.txt > HumanRef-8_V2_0_R4_11223162_A.txt.genome.map.txt
switchtoi.parser.awk HumanWG-6_V2_0_R4_11223189_A.txt > HumanWG-6_V2_0_R4_11223189_A.txt.genome.map.txt 
switchtoi.parser.awk HumanWG-6_V3_0_R2_11282955_A.txt > HumanWG-6_V3_0_R2_11282955_A.txt.genome.map.txt
switchtoi.parser.awk HumanRef-8_V3_0_R2_11282963_A.txt > HumanRef-8_V3_0_R2_11282963_A.txt.genome.map.txt
switchtoi.parser.awk  HumanHT-12_V4_0_R1_15002873_B.txt  > HumanHT-12_V4_0_R1_15002873_B.txt.genome.map.txt
switchtoi.parser.awk HumanHT-12_V3_0_R2_11283641_A.txt  > HumanHT-12_V3_0_R2_11283641_A.txt.genome.map.txt

# Mouse
switchtoi.parser.awk MouseRef-8_V1_1_R4_11234312_A.txt > MouseRef-8_V1_1_R4_11234312_A.txt.genome.map.txt
switchtoi.parser.awk MouseRef-8_V2_0_R2_11278551_A.txt > MouseRef-8_V2_0_R2_11278551_A.txt.genome.map.txt
switchtoi.parser.awk MouseWG-6_V2_0_R2_11278593_A.txt > MouseWG-6_V2_0_R2_11278593_A.txt.genome.map.txt
switchtoi.parser.awk MouseWG-6_V1_1_R4_11234304_A.txt >  MouseWG-6_V1_1_R4_11234304_A.txt.genome.map.txt

# Rat
switchtoi.parser.awk RatRef-12_V1_0_R5_11222119_A.txt > RatRef-12_V1_0_R5_11222119_A.txt.genome.map.txt


ls -al *.fasta

# QC check

head *.fasta

4) Download and create genomes

Note - genomes are large files, take care here.

# Downloaded hg18 - done.
# Download hg19 - done.
# Download mm8 - done.
# Download mm9 - done.

# Also need to download rat genome at some point - not done in this case.

# Build bowtie genomes

# Create genome command
ls *.fa | grep -v mrna  | gawk 'BEGIN {count = 1;com = "bowtie-build -f "} //{if (count==1) {com = com" "$1;} else {com = com ","$1}count++;} END {print com;}' > build_genome.sh
# append genome name (i.e. hg18 mm9)

source ./build_genome.sh

# pech
cd /scratch/genomes/hg18/
cd /scratch/genomes/hg19/
cd /scratch/genomes/mm8/
cd /scratch/genomes/mm9/


5) Bowtie of all Illumina probes vs. hg18

Where does the probe map to the genome?

# human
bowtie --shmem -k 5 -p 8 --seedmms 2 --sam --seedlen 20  -f  /scratch/genomes/hg18/hg18 HumanRef-8_V2_0_R4_11223162_A.txt.fasta > HumanRef-8_V2_0_R4_11223162_A.txt.fasta.hg18.k5.seed20.sm2.sam &
bowtie --shmem -k 5 -p 8 --seedmms 2 --sam --seedlen 20  -f  /scratch/genomes/hg18/hg18 HumanWG-6_V2_0_R4_11223189_A.txt.fasta > HumanWG-6_V2_0_R4_11223189_A.txt.fasta.hg18.k5.seed20.sm2.sam &
bowtie --shmem -k 5 -p 8 --seedmms 2 --sam --seedlen 20  -f  /scratch/genomes/hg18/hg18 HumanWG-6_V3_0_R2_11282955_A.txt.fasta > HumanWG-6_V3_0_R2_11282955_A.txt.fasta.hg18.k5.seed20.sm2.sam &
bowtie --shmem -k 5 -p 8 --seedmms 2 --sam --seedlen 20  -f  /scratch/genomes/hg18/hg18 HumanRef-8_V3_0_R2_11282963_A.txt.fasta > HumanRef-8_V3_0_R2_11282963_A.txt.fasta.hg18.k5.seed20.sm2.sam &
bowtie --shmem -k 5 -p 8 --seedmms 2 --sam --seedlen 20  -f  /scratch/genomes/hg18/hg18 HumanHT-12_V4_0_R1_15002873_B.txt.fasta > HumanHT-12_V4_0_R1_15002873_B.txt.fasta.hg18.k5.seed20.sm2.sam &
bowtie --shmem -k 5 -p 8 --seedmms 2 --sam --seedlen 20  -f  /scratch/genomes/hg18/hg18 HumanHT-12_V3_0_R2_11283641_A.txt.fasta > HumanHT-12_V3_0_R2_11283641_A.txt.fasta.hg18.k5.seed20.sm2.sam &

# Bowtie of all Illumina human probes vs. hg19 using bowtie
bowtie --shmem -k 5 -p 8 --seedmms 2 --sam --seedlen 20  -f  /scratch/genomes/hg19/hg19 HumanRef-8_V2_0_R4_11223162_A.txt.fasta > HumanRef-8_V2_0_R4_11223162_A.txt.fasta.hg19.k5.seed20.sm2.sam &
bowtie --shmem -k 5 -p 8 --seedmms 2 --sam --seedlen 20  -f  /scratch/genomes/hg19/hg19 HumanWG-6_V2_0_R4_11223189_A.txt.fasta > HumanWG-6_V2_0_R4_11223189_A.txt.fasta.hg19.k5.seed20.sm2.sam &
bowtie --shmem -k 5 -p 8 --seedmms 2 --sam --seedlen 20  -f  /scratch/genomes/hg19/hg19 HumanWG-6_V3_0_R2_11282955_A.txt.fasta > HumanWG-6_V3_0_R2_11282955_A.txt.fasta.hg19.k5.seed20.sm2.sam &
bowtie --shmem -k 5 -p 8 --seedmms 2 --sam --seedlen 20  -f  /scratch/genomes/hg19/hg19 HumanRef-8_V3_0_R2_11282963_A.txt.fasta > HumanRef-8_V3_0_R2_11282963_A.txt.fasta.hg19.k5.seed20.sm2.sam &
bowtie --shmem -k 5 -p 8 --seedmms 2 --sam --seedlen 20  -f  /scratch/genomes/hg19/hg19 HumanHT-12_V4_0_R1_15002873_B.txt.fasta > HumanHT-12_V4_0_R1_15002873_B.txt.fasta.hg19.k5.seed20.sm2.sam &
bowtie --shmem -k 5 -p 8 --seedmms 2 --sam --seedlen 20  -f  /scratch/genomes/hg19/hg19 HumanHT-12_V3_0_R2_11283641_A.txt.fasta > HumanHT-12_V3_0_R2_11283641_A.txt.fasta.hg19.k5.seed20.sm2.sam &

# Bowtie of all Illumina mouse probes vs. mm8 using bowtie

bowtie --shmem -k 5 -p 8 --seedmms 2 --sam --seedlen 20  -f  /scratch/genomes/mm8/mm8 MouseRef-8_V1_1_R4_11234312_A.txt.fasta > MouseRef-8_V1_1_R4_11234312_A.txt.fasta.mm8.k5.seed20.sm2.sam &
bowtie --shmem -k 5 -p 8 --seedmms 2 --sam --seedlen 20  -f  /scratch/genomes/mm8/mm8  MouseRef-8_V2_0_R2_11278551_A.txt.fasta > MouseRef-8_V2_0_R2_11278551_A.txt.fasta.mm8.k5.seed20.sm2.sam &
bowtie --shmem -k 5 -p 8 --seedmms 2 --sam --seedlen 20  -f  /scratch/genomes/mm8/mm8  MouseWG-6_V1_1_R4_11234304_A.txt.fasta  > MouseWG-6_V1_1_R4_11234304_A.txt.fasta.mm8.k5.seed20.sm2.sam &
bowtie --shmem -k 5 -p 8 --seedmms 2 --sam --seedlen 20  -f  /scratch/genomes/mm8/mm8  MouseWG-6_V2_0_R2_11278593_A.txt.fasta > MouseWG-6_V2_0_R2_11278593_A.txt.fasta.mm8.k5.seed20.sm2.sam &

# Bowtie of all Illumina mouse probes vs. mm9 using bowtie

bowtie --shmem -k 5 -p 8 --seedmms 2 --sam --seedlen 20  -f  /scratch/genomes/mm9/mm9 MouseRef-8_V1_1_R4_11234312_A.txt.fasta > MouseRef-8_V1_1_R4_11234312_A.txt.fasta.mm9.k5.seed20.sm2.sam &
bowtie --shmem -k 5 -p 8 --seedmms 2 --sam --seedlen 20  -f  /scratch/genomes/mm9/mm9 MouseRef-8_V2_0_R2_11278551_A.txt.fasta > MouseRef-8_V2_0_R2_11278551_A.txt.fasta.mm9.k5.seed20.sm2.sam &
bowtie --shmem -k 5 -p 8 --seedmms 2 --sam --seedlen 20  -f  /scratch/genomes/mm9/mm9 MouseWG-6_V1_1_R4_11234304_A.txt.fasta  > MouseWG-6_V1_1_R4_11234304_A.txt.fasta.mm9.k5.seed20.sm2.sam &
bowtie --shmem -k 5 -p 8 --seedmms 2 --sam --seedlen 20  -f  /scratch/genomes/mm9/mm9 MouseWG-6_V2_0_R2_11278593_A.txt.fasta > MouseWG-6_V2_0_R2_11278593_A.txt.fasta.mm9.k5.seed20.sm2.sam &

The results here give the genomic locations of the probes.

Add QC results here for this:

How many probes map in each case?

Also need to add a database with this information.

6) 
##
## Format the blast databases for UCSC (and ENSEMBL, VEGA)
##
# In the given directory /scratch/genomes/[genome]
# s2611722@pech:/scratch/genomes/hg18$ /opt/blast-2.2.17/bin/formatdb  -t hg18_mrna -p F -i mrna.fa 


7) Run BLAST

s2611722@pech:/scratch/s2611722/tfv_mercurial/ASCC/scripts/Illumina/1_DataImport

/opt/blast-2.2.17/bin/blastall -p blastn -d /scratch/genomes/hg18/mrna.fa -i HumanWG-6_V2_0_R4_11223189_A.txt.fasta   | MSPcrunch.LIN -I 90 -d - > HumanWG-6_V2_0_R4_11223189_A.txt.fasta.blastn.mspcrunch.e10.id90.ucsc_hg18.out &

8) Use UCSC tables to map from BLAST results to their location on the genome (Alistair)

- todo

9) Use UCSC/Ensembl tables to map from bowtie genome results to their overlapping transcripts. (Could be done in Galaxy)


################################################################################################################################33
##
## Scratch comments below - please ignore
##

# Question - in how many cases is a probe the same between human and mouse??
# A: Almost none (4)
s2611722@pech:/scratch/s2611722/tfv_mercurial/ASCC/scripts/Illumina/1_DataImport$ gawk '//{seqs[$2]++;}END{for (i in seqs) print i"\t"seqs[i];}' MouseWG-6_V2_0_R2_11278593_A.txt.simple.txt HumanWG-6_V3_0_R2_11282955_A.txt.simple.txt | grep 2
CTAAGGAATTGATGGAATACAAAAAGATTCAAAAGGAGCGTAGGCGCTCA	2
GAGGAAAAGGTGTGGGGTCTGCGTGCCCTGCAAGAGGCTCATCAACTGTG	2
GAGGAGGGCGAGGACGAGGCTTAAAAACTTCTCAGATCAATCGTGCATCC	2
GCCTAGGTACAGAATTAATAGCCCTTAGCAACGACTGCTGCTGGTGTGTA	2

Parameters check needed.

# To search the genomes.
UCSC mRNA sets (need gff too?)

http://genome.qfab.org/goldenPath/hg19/bigZips/mrna.fa.gz
http://genome.qfab.org/goldenPath/hg18/bigZips/mrna.fa.gz
http://genome.qfab.org/goldenPath/mm9/bigZips/mrna.fa.gz
http://genome.qfab.org/goldenPath/mm8/bigZips/mrna.fa.gz

- Need genome coords of these! - In the all_mrna table.

s2611722@pech:/scratch/genomes/hg19$ gunzip all_mrna.txt.gz
s2611722@pech:/scratch/genomes/hg19$ head all_mrna.txt   






