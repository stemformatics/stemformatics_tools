#!/usr/bin/gawk -f

#
# switch to i parser for parsing Illumina annotation files from www.switchtoi.com
# Parses out Illumina generates mappings for the probes to genome coordinates.
#
#
# Alistair Chalk
# 2010-10-27
#
# Usage: switchtoi.parser.awk HumanRef-8_V3_0_R2_11282963_A.txt
#
# Note - many probes have a gene, but no definite coordinates
#

BEGIN {
  FS="\t";

}

/^#/{
# Control probes - ignored
next;
}

/Probe_Id/{
# Header line
  next;
}

//{
  # Keep header lines (after adding "#"
  if (NF<18) {
   # print "#"$0;
    next;
  } else {
    
    print ">"$14"|\n"$18;

  }
}
  
  END {

  }