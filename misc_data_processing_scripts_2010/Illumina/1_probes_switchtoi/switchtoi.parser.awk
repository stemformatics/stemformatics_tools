#!/usr/bin/gawk -f

#
# switch to i parser for parsing Illumina annotation files from www.switchtoi.com
# Parses out Illumina generates mappings for the probes to genome coordinates.
#
#
# Alistair Chalk
# 2010-10-27
#
# Usage: switchtoi.parser.awk HumanRef-8_V3_0_R2_11282963_A.txt
#
# Note - many probes have a gene, but no definite coordinates
#

BEGIN {
  FS="\t";
  for (i=1;i<=5;i++)
    field_distribution[i] = 0;

  for (i=1;i<=50;i++)
    lengthDist[i]=0;

  print "#switchtoi.parser.awk\t"ARGV[1];
}

/^#/{
# Control probes - ignored
next;
}

//{
  # Keep header lines (after adding "#"
  if (NF<18) {
    print "#"$0;
    next;
  } else {
    
      tot_length = 0;
    print $3"\t"$14"\t"$15"\t"$16"\t"$17"\t"$18"\t"$19"\t"$20"\t"$21;
    # Calculate coords length
    flds = split($21,coords,":");
    if (flds > 0){
      print "FLDS\t"flds;
      
      field_distribution[flds]++;
	# multi example
	for (i=1;i<=flds;i++){
	  split(coords[i],coordz,"-");
	  l = coordz[2]-coordz[1]+1;
	  tot_length = tot_length + l;
	  print "#:\t"coordz[1]"\t"coordz[2]"\t"l;
	    }
    }
    lengthDist[tot_length]++;

# Print those < 50 for diagnostics
    if (tot_length < 50)
      print "##LEN_FAIL("tot_length")\t"$3"\t"$14"\t"$15"\t"$16"\t"$17"\t"$18"\t"$19"\t"$20"\t"$21;
    
    print "TOTAL_length\t"tot_length;
      #    } else {
      #      # single example
      #      split($21,coordz,"-");
    #      l = coordz[2]-coordz[1];
    #      print "SINGLE\t"coordz[1]"\t"coordz[2]"\t";
      #    }
      
    linecount++;
    if (length($21)>1)
      has_coords++;
    else
      no_coords++;
    next;
  }
}
  
  END {
    #Print summary information
    print "#Probes_printed\t"linecount;
    print "#has_coords\t"has_coords;
    print "#no_coords\t"no_coords;
    
    # Print length distribution (How many are non-50 (QC check!))
    for (i=1;i<=50;i++)
      print "#Length_distribution\t"i"\t"lengthDist[i];
 
    for (i=1;i<=5;i++)
      # Print number of exons spanned by probes
    print "#Probes_"i"_exon "field_distribution[i];
 
  }