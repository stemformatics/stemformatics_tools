#!/usr/bin/gawk -f

## 
## Clean a file created by java se.ki.cgb.sonnhammer.util.MapperUtility to keep only necessary lines
## And create import for SQL

##
## Alistair Chalk
## 2010-10-28
##
## 

BEGIN {
  count=0;

  if (ARGC <2){
    print "USAGE: cleanCombinedExpFIRMA.awk <filename> [arrayType]"; 
  }

  arrayType = ARGV[2];
  ARGC =2;
}

/^#/{
# Header
  next;
}

//{
  if (count ==0){
    # Calc number of arrays given NF
    arrayCount = (NF-14)/2;
#    print "ArrayCount: "arrayCount;
    count++;
    
    headers[1] = $(4);
    headers[2] = $(5);
    
    # Set array headers
    for (i=1;i<=arrayCount;i++){
      arrayNames[i] = $(7+i);
    } 
    next;
  }

  # Process a line and produce the SQL

    geneID = $(4);
    psrID = $(5);
    
    for (i=1;i<=arrayCount;i++){
      psrExp = $(i + 8);
      psrFIRMA = $(i + 8 + 6 + arrayCount);
      
      print arrayNames[i]"\t"arrayNames[i]"\t"geneID"\t"psrID"\t"psrExp"\t"psrFIRMA"\t"arrayType;
    }
    
#Ctrl11.GSM466907	1	ENSG00000000003	4015402	2.988	1.048	HuEx1.0ST
     
#      print NF;
#      print $0;

}

  END {
    
}