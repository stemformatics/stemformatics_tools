# Generic importing of affymetrix exon array analysis data including
# 1) Gene Expression 
# 2) PSR Expression
# 3) FIRMA scores

# Creates SQL import file

## See below for worked example (now moved to processFIRMAOutput2SQL.sh)

## /scratch/s2611722/tfv_mercurial/ASCC/scripts/FIRMA/3_Database_Import/processFIRMAOutput2SQL.sh GSE18838_GeneExpression_in_Blood_PD GSE18838_GeneExpression_in_Blood_PD_exon_arrays_chipeffects_exon_log2_HuEx-1_0-st-v2,Ensembl54,r3,AMC_2010-05-19.csv GSE18838_GeneExpression_in_Blood_PD_exon_arrays_firma_gene_HuEx-1_0-st-v2,Ensembl54,r3,AMC_2010-05-19.csv HuEx-1_0-st-v2,Ensembl54,r3,AMC > GSE18838_GeneExpression_in_Blood_PD.log

## /scratch/s2611722/tfv_mercurial/ASCC/scripts/FIRMA/3_Database_Import/processFIRMAOutput2SQL.sh MDMLPS MDMLPS_chipeffects_exon_log2_HuEx-1_0-st-v2,Ensembl54,r3,AMC_2009-07-13.csv MDMLPS_firma_gene_HuEx-1_0-st-v2,Ensembl54,r3,AMC_2009-07-13.csv  HuEx-1_0-st-v2,Ensembl54,r3,AMC > MDMLPS.log

## /scratch/s2611722/tfv_mercurial/ASCC/scripts/FIRMA/3_Database_Import/processFIRMAOutput2SQL.sh TESTER TESTER_chipeffects_exon_log2_HuEx-1_0-st-v2,Ensembl54,r3,AMC_2010-10-28.csv  TESTER_firma_HuEx-1_0-st-v2,Ensembl54,r3,AMC_2010-10-28.csv  HuEx-1_0-st-v2,Ensembl54,r3,AMC > TESTER.log

##
## Note that sample/replicate still need to be handled better in a consistent fashion for ALL data import - relies on new tables.
##
##


# The data is formatted for input into the following table - or equivalent.
# Sample name changing should br trivial when we need it.

##
## The important thing to note here is that NOT all probes on the array will be in this dataset as we are restricted to probes ONLY that match the given Ensembl version
##

##
## We could run/import data from AltAnalyze as well - and that would give us 
## DABG, and other useful information
##

##
## Clean the CSV format into tsv format, fix missing first column name.
##

cat GSE18838_GeneExpression_in_Blood_PD_exon_arrays_chipeffects_exon_log2_HuEx-1_0-st-v2,Ensembl54,r3,AMC_2010-05-19.csv | grep -v "#" | sed 's/\"\"/Num/;s/,/\t/g;s/\"//g;' > GSE18838_GeneExpression_in_Blood_PD_exon_arrays_chipeffects_exon_log2_HuEx-1_0-st-v2,Ensembl54,r3,AMC_2010-05-19.csv.tsv
# Fix header (add ID to first line)

cat GSE18838_GeneExpression_in_Blood_PD_exon_arrays_firma_gene_HuEx-1_0-st-v2,Ensembl54,r3,AMC_2010-05-19.csv | grep -v "#" | sed 's/\"\"/Num/;s/,/\t/g;s/\"//g;' > GSE18838_GeneExpression_in_Blood_PD_exon_arrays_firma_gene_HuEx-1_0-st-v2,Ensembl54,r3,AMC_2010-05-19.csv.tsv

## FIX java combiner to give correct number of header lines.

java se.ki.cgb.sonnhammer.util.MapperUtility  GSE18838_GeneExpression_in_Blood_PD_exon_arrays_firma_gene_HuEx-1_0-st-v2,Ensembl54,r3,AMC_2010-05-19.csv.tsv  3 3 GSE18838_GeneExpression_in_Blood_PD_exon_arrays_chipeffects_exon_log2_HuEx-1_0-st-v2,Ensembl54,r3,AMC_2010-05-19.csv.tsv 3 -PRINTALL1 -PRINTALL2 -PRINTUNMAPPEDTOFILE UNMAPPED1.txt  > GSE18838_GeneExpression_in_Blood_PD_exon_arrays_FIRMA_and_Expression.tsv 

## Format this for SQL import
## Automatically detects array Count and deals with headers
/scratch/s2611722/tfv_mercurial/ASCC/scripts/FIRMA/3_Database_Import/cleanCombinedExpFIRMA.awk  GSE18838_GeneExpression_in_Blood_PD_exon_arrays_FIRMA_and_Expression.tsv HuEx-1_0-st-v2,Ensembl54,r3,AMC > GSE18838_GeneExpression_in_Blood_PD_exon_arrays_FIRMA_and_Expression.tsv.sql 

### 
### Now clean the file of ENSG.ENSG (Cases where genes are inseperable for FIRMA analysis)
###

grep -v "ENSG............ENSG" GSE18838_GeneExpression_in_Blood_PD_exon_arrays_FIRMA_and_Expression.tsv.sql > GSE18838_GeneExpression_in_Blood_PD_exon_arrays_FIRMA_and_Expression.tsv.sql.final
grep -e "ENSG............ENSG" GSE18838_GeneExpression_in_Blood_PD_exon_arrays_FIRMA_and_Expression.tsv.sql > GSE18838_GeneExpression_in_Blood_PD_exon_arrays_FIRMA_and_Expression.tsv.sql.removed

# Now import the data into the database

