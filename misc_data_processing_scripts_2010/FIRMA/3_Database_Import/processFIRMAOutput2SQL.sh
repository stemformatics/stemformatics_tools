#!/bin/bash -f

if [ -z "$4" ]; then
                  echo "FATAL: not enough args given";
		  echo "USAGE:  /scratch/s2611722/tfv_mercurial/ASCC/scripts/FIRMA/3_Database_Import/processFIRMAOutput2SQL.sh datasetName [psr level expression output] [firme score output] [chiptype/annot]";
		  echo "USAGE: processFIRMAOutput2SQL.sh GSE18838_GeneExpression_in_Blood_PD GSE18838_GeneExpression_in_Blood_PD_exon_arrays_chipeffects_exon_log2_HuEx-1_0-st-v2,Ensembl54,r3,AMC_2010-05-19.csv GSE18838_GeneExpression_in_Blood_PD_exon_arrays_firma_gene_HuEx-1_0-st-v2,Ensembl54,r3,AMC_2010-05-19.csv HuEx-1_0-st-v2,Ensembl54,r3,AMC";
                  exit 0;
fi
             
datasetName=$1;
psrFile=$2;
firmaFile=$3;
chipAnnot=$4;

echo "datasetName "$datasetName;
echo "psrFile "$psrFile;
echo "firmaFile "$firmaFile;
echo "chipAnnot "$chipAnnot;

echo "";
echo "Running...";

## /scratch/s2611722/tfv_mercurial/ASCC/scripts/FIRMA/3_Database_Import/processFIRMAOutput2SQL.sh
#

# Generic importing of affymetrix exon array analysis data including
# 1) Gene Expression 
# 2) PSR Expression
# 3) FIRMA scores

# The data is formatted for input into the following table - or equivalent.
# Sample name changing should br trivial when we need it.

##
## The important thing to note here is that NOT all probes on the array will be in this dataset as we are restricted to probes ONLY that match the given Ensembl version
##

##
## We could run/import data from AltAnalyze as well - and that would give us 
## DABG, and other useful information
##

##
## Clean the CSV format into tsv format, fix missing first column name.
##

cat $psrFile | grep -v "#" | sed 's/\"\"/Num/;s/,/\t/g;s/\"//g;' > $psrFile".tsv"

cat $firmaFile | grep -v "#" | sed 's/\"\"/Num/;s/,/\t/g;s/\"//g;' > $firmaFile".tsv"

## FIX java combiner to give correct number of header lines.

java se.ki.cgb.sonnhammer.util.MapperUtility  $firmaFile".tsv"  3 3 $psrFile".tsv" 3 -PRINTALL1 -PRINTALL2 -PRINTUNMAPPEDTOFILE  $datasetName"_exon_arrays_FIRMA_and_Expression_UNMAPPED.txt"  > $datasetName"_exon_arrays_FIRMA_and_Expression.tsv"

echo $datasetName"_exon_arrays_FIRMA_and_Expression.tsv created."

## Format this for SQL import
## Automatically detects array Count and deals with headers
/scratch/s2611722/tfv_mercurial/ASCC/scripts/FIRMA/3_Database_Import/cleanCombinedExpFIRMA.awk  $datasetName"_exon_arrays_FIRMA_and_Expression.tsv" $chipAnnot >   $datasetName"_exon_arrays_FIRMA_and_Expression.tsv.sql"

echo $datasetName"_exon_arrays_FIRMA_and_Expression.tsv.sql created."

### 
### Now clean the file of ENSG.ENSG (Cases where genes are inseperable for FIRMA analysis)
###

grep -v "ENSG............ENSG"  $datasetName"_exon_arrays_FIRMA_and_Expression.tsv.sql"  >  $datasetName"_exon_arrays_FIRMA_and_Expression.tsv.sql.final"
grep -e "ENSG............ENSG"  $datasetName"_exon_arrays_FIRMA_and_Expression.tsv.sql" >  $datasetName"_exon_arrays_FIRMA_and_Expression.tsv.sql.removed"

echo $datasetName"_exon_arrays_FIRMA_and_Expression.tsv.sql.final created"
echo $datasetName"_exon_arrays_FIRMA_and_Expression.tsv.sql.removed created"

# Now import the data into the database

exit 1;