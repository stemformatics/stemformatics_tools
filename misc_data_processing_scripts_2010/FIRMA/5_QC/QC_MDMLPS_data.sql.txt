#Quailty control of FIRMA data
#=============================

# USAGE: mysql -h pech.rcs.griffith.edu.au -p transcriptional_framework < QC_MDMLPS_data.sql.txt >  QC_MDMLPS_data.sql.txt.out
#

#General QC of the FIRMA data (has the correct data made it into the system?)

# Select correct server and database
#[pech]
#mysql -p
#use transcriptional_framework;

# 1) Check the number of data points for each chip (Gene level, PSR level)
# Are they the same for each chip/replicate?
# Are they the expected number?

SELECT "Number of data points (gene level)";
SELECT time_point,replicate,COUNT(DISTINCT(ensembl_gene_id)) from firma_psr_MDMLPS group by time_point,replicate;

SELECT "Number of data points (PSR level)";
SELECT time_point,replicate,COUNT(DISTINCT(psr)) from firma_psr_MDMLPS group by time_point,replicate;

# 2) Check the range of expression values for each chip
# Are they within the expected range? [0,16] (Usually min = ~1)

SELECT "Range of PSR level expression";
SELECT time_point,replicate,MIN(expression),AVG(expression),MAX(expression) from firma_psr_MDMLPS group by time_point,replicate;

# 3) Check the range of the FIRMA scores 
# FIRMA data is expected to center around 0, with min/max of [-16,+16], with most figures falling within [-4,+4].

SELECT "Range of PSR level FIRMA scores";
SELECT time_point,replicate,MIN(firma_score),AVG(firma_score),MAX(firma_score) from firma_psr_MDMLPS group by time_point,replicate;

####################################################################################################################################
