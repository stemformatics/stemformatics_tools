#!/bin/sh
HOST="$1"
HOSTALIAS="$2"
PORT="$3"
SQL_OR_CMD="$4"

. "$S4M_HOME"/dataset_scripts/include/funcs.sh

## User's pgpass file for this host
PGPASSFILE="$HOME/.pgpass_${HOSTALIAS}_$USER"
## Must export for 'psql' tool to use
export PGPASSFILE
if [ -z "$S4M_PROJECT_LABEL" ]; then
  export S4M_PROJECT_LABEL="s4m"
fi
## s4m_passwords repo path
s4m_passwords="$S4M_HOME/../../../${S4M_PROJECT_LABEL}_passwords"


## No .pgpass for target host, create it and populate with DBUSER and DBPASSWORD
## from passwords repo
if [ ! -d "$s4m_passwords" ]; then
  echo "Error: Failed to determine path to 's4m_passwords' repository needed for .pgpass setup
(for psql_wrapper.sh host instance script usage)!
" 1>&2;
  exit 1

else
  if [ ! -f "$s4m_passwords/passwords.ini" ]; then
    echo "Error: Could not find master password .ini file in location '$s4m_passwords/passwords.ini'"
    exit 1
  fi

  /bin/echo -n "$HOST:5432:portal_prod:%DBUSER%:%DBPASSWORD%" > "$PGPASSFILE"

  ReplaceTokensInFileFromINI "$PGPASSFILE" "$s4m_passwords/passwords.ini"
  ret=$?
  if [ $ret -ne 0 ]; then
    echo "Error: Failed to set up '$PGPASSFILE' - cannot load DB settings"
    exit $ret
  fi
fi


## .pgpass file must be chmod 600 or psql won't load the pgpass file
chmod 600 "$PGPASSFILE"

DBUSER=`cut -d':' -f 4 "$PGPASSFILE"`


## Load a table from file
if [ "$SQL_OR_CMD" = "load" ]; then
  table="$5"
  sqlfile="$6"
  nullflag="$7"
  nullcmd="NULL AS ''"
  if [ ! -z "$nullflag" ]; then
    if [ "$nullflag" = "-nonull" ]; then
      nullcmd=""
    fi
  fi
  if [ -f "$sqlfile" ]; then
    ## Update: 2018-09-13: Add 'WITH ENCODING' directive for UTF-8
    cat "$sqlfile" | psql -h $HOST -p $PORT -U $DBUSER portal_prod -c "COPY $table FROM STDIN WITH ENCODING 'UTF8' $nullcmd;"
  else
    echo "Error: SQL data file '$sqlfile' not found!"; echo
  fi

## Execute arbitrary SQL as string or from file
else
  if [ -f "$SQL_OR_CMD" ]; then
    cat "$SQL_OR_CMD" | psql -h $HOST -p $PORT -U $DBUSER portal_prod
  else
    echo "$SQL_OR_CMD" | psql -h $HOST -p $PORT -U $DBUSER portal_prod
  fi
fi
